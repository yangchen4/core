/**
 * 
 */
package ca.mcgill.sel.core.perspective;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum of Perspective Actions with their string names
 * 
 * @author hyacinthali
 *
 */
public enum PerspectiveAction {
    
    ADD_CLASS("ClassDiagram.Class.add"),
    EDIT_CLASS("ClassDiagram.Class.edit"),
    DELETE_CLASS("ClassDiagram.Class.delete"),
    
    ADD_NOTE("ClassDiagram.Note.add"),
    EDIT_NOTE("ClassDiagram.Note.edit"),
    DELETE_NOTE("ClassDiagram.Note.delete"),
    
    ADD_ASSOCIATION("ClassDiagram.Association.add"),
    EDIT_ASSOCIATION("ClassDiagram.Association.edit"),
    DELETE_ASSOCIATION("ClassDiagram.Association.delete"),
    
    ADD_NARY_ASSOCIATION("ClassDiagram.NaryAssociation.add"),
    DELETE_NARY_ASSOCIATION("ClassDiagram.NaryAssociation.delete"),
    
    ADD_ATTRIBUTE("ClassDiagram.Classifier.Attribute.add"),
    EDIT_ATTRIBUTE("ClassDiagram.Classifier.Attribute.edit"),
    DELETE_ATTRIBUTE("ClassDiagram.Classifier.Attribute.delete"),
    
    ADD_ENUM("ClassDiagram.CDEnum.add"),
    EDIT_ENUM("ClassDiagram.CDEnum.edit"),
    DELETE_ENUM("ClassDiagram.CDEnum.delete"),
    
    EDIT_STATIC("ClassDiagram.StructuralFeature.static.edit"),
    
    ADD_IMPLEMENTATION_CLASS("ClassDiagram.ImplementationClass.add"),
    EDIT_IMPLEMENTATION_CLASS("ClassDiagram.ImplementationClass.edit"),
    DELETE_IMPLEMENTATION_CLASS("ClassDiagram.ImplementationClass.delete"),
    
    ADD_OPERATION("ClassDiagram.Classifier.Operation.add"),
    EDIT_OPERATION("ClassDiagram.Classifier.Operation.edit"),
    REMOVE_OPERATION("ClassDiagram.Classifier.Operation.delete"),
    
    ADD_OPERATION_PARAMETER("ClassDiagram.Classifier.Operation.Parameter.add"),
    EDIT_OPERATION_PARAMETER("ClassDiagram.Classifier.Operation.Parameter.edit"),
    REMOVE_OPERATION_PARAMETER("ClassDiagram.Classifier.Operation.Parameter.delete"),
    
    EDIT_VISIBILITY("ClassDiagram.VisibilityType.edit"),
    
    CREATE_MAPPING("perspective.create.mapping");
    
    /**
     * name is long form unique identifier for the perspective action built using the CORE metamodel
     */
    private String name;
    
    public String getName() {
        return name;
    }

    PerspectiveAction(String name) {
        this.name = name;
    }
      
    public static List<String> getPerspectiveActions() {
        List<String> actionStrings = new ArrayList<String>();
        for (PerspectiveAction action : PerspectiveAction.values()) {
            actionStrings.add(action.getName());
        }
        return actionStrings;
    }

}
