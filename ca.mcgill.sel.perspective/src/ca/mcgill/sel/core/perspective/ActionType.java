
package ca.mcgill.sel.core.perspective;

/**
 * Enum containing all the create type options, as well as delete type options.
 * 
 * @author hyacinthali
 *
 */
public enum ActionType {

    /**
     * Option for Compulsory Optional-Multiple.
     */
    // C1.1 (C1)
    CAN_CREATE,
    // C1.2 (C9)
    CREATE_OR_USE_NON_MAPPED,
    // C2.1 (C2)
    CREATE,
    // C3.1 (C3)
    CAN_CREATE_MANY,
    // C3.2 (C10)
    CREATE_OR_USE,
    // C4.1 (C4)
    CREATE_AT_LEAST_ONE,
    // C5.1 (C5)
    CAN_CREATE_OR_USE_NON_MAPPED,
    // C6.1 (C6)
    CAN_CREATE_OR_USE,
    // C6.2 (C11)
    CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE,
    // C7.2 (12)
    CAN_CREATE_OR_USE_NON_MAPPED_MANY,
    // C8.1 (C7)
    CREATE_OR_USE_AT_LEAST_ONE,
    // C9.1 (C8)
    CAN_CREATE_OR_USE_MANY,
    JUST_DELETE,
    DELETE_OTHERS,
    DELETE_SINGLEMAPPED
    
}
