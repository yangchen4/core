package ca.mcgill.sel.core.perspective;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CORERedefineAction;
import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.FeatureController;
import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.core.language.registry.CORELanguageRegistry;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * Helper class with convenient methods for working with CORE Perspective.
 * 
 * @author Hyacinth
 */
public final class COREPerspectiveUtil {

	/**
	 * Map of existing models, each with their corresponding role names.
	 */
	private Map<String, EObject> existingModels;

	/**
	 * Singleton instance variable.
	 */
	public static COREPerspectiveUtil INSTANCE = new COREPerspectiveUtil();

	/**
	 * List of CORE perspectives.
	 */
	private Map<String, COREPerspective> perspectives;

	/**
	 * Singleton pattern - initialize only a single instance.
	 */
	private COREPerspectiveUtil() {
		perspectives = new HashMap<String, COREPerspective>();

	}

	/**
	 * Associates the reexpose actions of the perspective with the corresponding
	 * language actions TODO this needs to be changed when dealing with
	 * multi-language perspective
	 * 
	 * @param perspective the perspective
	 */
	private void associateReexposedActions(COREPerspective perspective) {
		for (CORELanguage language : perspective.getLanguages().values()) {
			for (COREAction pAction : perspective.getActions()) {
				if (pAction instanceof COREReexposeAction) {
					for (COREAction lAction : language.getActions()) {
						COREReexposeAction a = (COREReexposeAction) pAction;
						if (a.getName().equals(lAction.getName())) {
							a.setReexposedAction(lAction);
						}
					}
				}
				if (pAction instanceof CORERedefineAction) {
					for (COREAction lAction : language.getActions()) {
						CORERedefineAction a = (CORERedefineAction) pAction;
						if (a.getName().equals(lAction.getName())) {
							a.setRedefinedAction(lAction);
						}
					}
				}
			}
		}
	}

	/**
	 * Return the list of existing perspectives.
	 * 
	 * @return the perspective
	 */
	public Collection<COREPerspective> getPerspectives() {
		return perspectives.values();
	}

	/**
	 * Return the COREPerspective corresponding to the String name or else null.
	 * 
	 * @param name the name of the language
	 * @return the COREPerspective with the name name
	 */
	public COREPerspective getPerspective(String name) {
		return perspectives.get(name);
	}

	/**
	 * Retrieves the perspective {@link COREPerspective} from the
	 * {@link COREArtefactUtil's}
	 * 
	 * @param model root model element of the external artefact
	 * @return the perspective of the model
	 */
	public COREPerspective getModelPerspective(EObject model) {
		String perspectiveName = COREArtefactUtil.getReferencingExternalArtefact(model).getScene().getPerspectiveName();
		return perspectives.get(perspectiveName);
	}

	/**
	 * Loads pre-defined perspectives.
	 */
	public void loadPerspectives() {
		List<String> ps = ResourceUtil.getResourceListing("models/perspectives/", ".core");
		if (ps != null) {
			for (String p : ps) {

				// load existing perspectives
				URI fileURI = URI.createURI(p);
				COREConcern perspectiveConcern = (COREConcern) ResourceManager.loadModel(fileURI);
				for (COREArtefact a : perspectiveConcern.getArtefacts()) {
					if (a instanceof COREPerspective) {
						COREPerspective existingPerspective = (COREPerspective) a;
						perspectives.put(existingPerspective.getName(), existingPerspective);
						associateReexposedActions(existingPerspective);
					}
				}
			}
		}
	}

	/**
	 * Loads pre-defined perspectives. This methos is only used for testing
	 * perspective related functionalities.
	 */
	public void loadPerspectives(String testPersspective) {
		List<String> ps = ResourceUtil.getResourceListing("models/testperspectives/", ".core");
		if (ps != null) {
			for (String p : ps) {

				// load existing perspectives
				URI fileURI = URI.createURI(p);
				COREConcern perspectiveConcern = (COREConcern) ResourceManager.loadModel(fileURI);
				for (COREArtefact a : perspectiveConcern.getArtefacts()) {
					if (a instanceof COREPerspective) {
						COREPerspective existingPerspective = (COREPerspective) a;
						perspectives.put(existingPerspective.getName(), existingPerspective);
						associateReexposedActions(existingPerspective);
					}
				}
			}
		}
	}

	/**
	 * @param c
	 * @param roleName
	 * @param artefact
	 * @throws IOException
	 */
	public void saveModel(COREConcern c, String roleName, COREExternalArtefact artefact) throws IOException {
		URI idealURI = c.eResource().getURI();
		idealURI = idealURI.trimSegments(1);
		idealURI = idealURI.appendSegment(artefact.getLanguageName());

		char addOn = 'a' - 1;
		String fileRoleName = roleName.replace(' ', '_').toLowerCase();

		File suggestedFile;

		do {
			URI attempt;
			if (addOn == 'a' - 1) {
				attempt = idealURI.appendSegment(artefact.getName() + "." + fileRoleName);
			} else {
				attempt = idealURI.appendSegment(artefact.getName() + "-" + addOn + "." + fileRoleName);
			}
			String extension = CORELanguageRegistry.getRegistry().getLanguage(artefact.getLanguageName())
					.getFileExtension();
			attempt = attempt.appendFileExtension(extension);
			suggestedFile = new File(attempt.toFileString());
			addOn++;
		} while (suggestedFile.exists());

		try {
			if (!suggestedFile.getParentFile().exists()) {
				// create parent directory if someone's trying to be tricky.
				suggestedFile.getParentFile().mkdirs();
			}
			suggestedFile.createNewFile();
		} catch (final IOException e) {
			// TODO display something to user
			e.printStackTrace();
		}

		ResourceManager.saveModel(artefact.getRootModelElement(), suggestedFile.getAbsolutePath());
	}

	/**
	 * Operation that results all reuses associated with a feature for a given
	 * perspective. This includes any reuse that has a model reuse attached to an
	 * artefact that is part of a scene that has the provided perspective type. If
	 * the perspective parameter is null, then all reuses associated with any scene
	 * that is linked to the feature are returned.
	 * 
	 * @param feature     which feature we are interested in
	 * @param perspective the perspective we consider (pass null to consider scenes
	 *                    from all perspectives)
	 * @return a HashSet of reuses
	 */
	public Collection<COREReuse> getReuses(COREFeature feature, COREPerspective perspective) {
		Collection<COREReuse> result = new HashSet<COREReuse>();

		Collection<COREScene> scenes = feature.getRealizedBy();
		for (COREScene s : scenes) {
			if (perspective == null
					|| COREPerspectiveUtil.INSTANCE.getPerspective(s.getPerspectiveName()) == perspective) {
				ArrayList<COREArtefact> artefacts = COREArtefactUtil.getAllArtefactsOfScene(s);
				for (COREArtefact a : artefacts) {
					for (COREModelReuse mr : a.getModelReuses()) {
						result.add(mr.getReuse());
					}

				}
			}
		}
		return result;
	}

	public List<COREModelElementMapping> getInstances(CORELanguageElementMapping mappingType, COREScene scene) {
		List<COREModelElementMapping> instances = new ArrayList<COREModelElementMapping>();
		for (COREModelElementMapping elementMapping : scene.getElementMappings()) {
			if (elementMapping.getLEMid() == mappingType.getIdentifier()) {
				instances.add(elementMapping);
			}
		}
		return instances;
	}

	/**
	 * Retrieves the mapping type {@link CORELanguageElementMapping} based on a
	 * given model element mapping.
	 * 
	 * @param perspective
	 * @param mapping
	 * @return the type of the model element mapping.
	 */
	public CORELanguageElementMapping getMappingType(COREPerspective perspective, COREModelElementMapping mapping) {
		CORELanguageElementMapping mappingType = null;
		for (CORELanguageElementMapping type : perspective.getMappings()) {
			if (type.getIdentifier() == mapping.getLEMid()) {
				mappingType = type;
				break;
			}
		}
		return mappingType;
	}

	/**
	 * Retrieves all language element mapping {@link CORELanguageElementMapping}
	 * based on a given language element.
	 * 
	 * @param perspective
	 * @param element     the language metaclass
	 * @return the list of the mapping types.
	 */
	public List<CORELanguageElementMapping> getMappingTypes(COREPerspective perspective, EObject element,
			String roleName) {
		List<CORELanguageElementMapping> mappingTypes = new ArrayList<CORELanguageElementMapping>();
		for (CORELanguageElementMapping mappingtype : perspective.getMappings()) {
			for (MappingEnd mappingEnd : mappingtype.getMappingEnds()) {
				if (mappingEnd.getLanguageElement().getLanguageElement().equals(element)
						&& mappingEnd.getRoleName().equals(roleName)) {
					mappingTypes.add(mappingtype);
				}
			}
		}
		return mappingTypes;
		// 38fcb837
	}

	/**
	 * Retrieve mappings for a given type.
	 * 
	 * @param typeMapping  - an instance of {@link CORELanguageElementMapping}
	 * @param modelElement
	 * @return the mappings of the model element based on the provided language
	 *         element mapping.
	 */
	public List<COREModelElementMapping> getMappings(CORELanguageElementMapping typeMapping, EObject modelElement) {

		List<COREModelElementMapping> modelMappings = new ArrayList<COREModelElementMapping>();
		COREScene scene = COREArtefactUtil.getReferencingExternalArtefact(modelElement).getScene();
		for (COREModelElementMapping mapping : getInstances(typeMapping, scene)) {
			if (mapping.getModelElements().contains(modelElement)) {
				modelMappings.add(mapping);
			}
		}
		return modelMappings;
	}

	/**
	 * Retrieve mappings for a given {@link CORELanguageElementMapping} and
	 * {@link COREScene}.
	 * 
	 * @param lem
	 * @param scene
	 * @param modelElement
	 * @return the mappings
	 */
	public List<COREModelElementMapping> getMappings(CORELanguageElementMapping lem, COREScene scene,
			EObject modelElement) {
		List<COREModelElementMapping> modelMappings = new ArrayList<COREModelElementMapping>();
		for (COREModelElementMapping mapping : getInstances(lem, scene)) {
			if (mapping.getModelElements().contains(modelElement)) {
				modelMappings.add(mapping);
			}
		}
		return modelMappings;
	}

	/**
	 * Get the corresponding mapped element for binary mappings.
	 * 
	 * @param mapping     between the model elements.
	 * @param elementHere the current model element.
	 * @return the corresponding mapped element.
	 */
	public EObject getOtherElement(COREModelElementMapping mapping, EObject elementHere) {
		if (mapping.getModelElements().get(0).equals(elementHere)) {
			return mapping.getModelElements().get(1);
		} else {
			return mapping.getModelElements().get(0);
		}
	}

	/**
	 * Retrieves a list of corresponding mapped elements for a ternary mapping
	 * 
	 * @param mapping      between the concerned model elements.
	 * @param elementHere, the element of interest
	 * @return the corresponding mapped element.
	 */
	public List<EObject> getOtherElements(COREModelElementMapping mapping, EObject element) {
		List<EObject> mappedElements = new ArrayList<EObject>();
		for (EObject modelElement : mapping.getModelElements()) {
			if (!modelElement.equals(element)) {
				mappedElements.add(modelElement);
			}
		}
		return mappedElements;
	}

	/**
	 * Retrieves the language elements of other mapping ends, given the language
	 * current language element of the corresponding mapping-end.
	 * @param mappingType
	 * @param fromLanguageElement - the current language element
	 * @param fromRoleName - the role name of the current language element.
	 * @return the language elements of other mapping ends.
	 */
	public List<EObject> getOtherLanguageElements(CORELanguageElementMapping mappingType, EObject fromLanguageElement, String fromRoleName) {
		List<EObject> otherMetaclasses = new ArrayList<EObject>();
		List<MappingEnd> otherMappingEnds = new ArrayList<MappingEnd>();
		for (MappingEnd mappingEnd : mappingType.getMappingEnds()) {
			boolean sameMetaclass = mappingEnd.getLanguageElement().getLanguageElement().equals(fromLanguageElement);
			boolean sameRoleName = mappingEnd.getRoleName().equals(fromRoleName);

			if (!(sameMetaclass && sameRoleName)) {
				otherMappingEnds.add(mappingEnd);
			}
		}
		for (MappingEnd mappingEnd : otherMappingEnds) {
			otherMetaclasses.add(mappingEnd.getLanguageElement().getLanguageElement());
		}
		return otherMetaclasses;
	}
	
	/**
	 * Retrieves a corresponding model- The use of this method has been suspended.
	 * @param scene - the scene which contains the model element mappings
	 * @param mappingType - the type of the mapping in question.
	 * @param otherRoleName - the role name of the other model to be fetched.
	 * @return the other model.
	 * 
	 */
	public EObject getOtherModel(COREScene scene, CORELanguageElementMapping mappingType, String otherRoleName) {
		EObject otherModel = null;
//		for (COREModelElementMapping mapping : scene.getElementMappings()) {
//			// ensure that the mapping is typed by the provided mappingType
//			if (mapping.getLEMid() == mappingType.getIdentifier() && mapping.isRootElementMapping()) {
//				EObject firstModelElement = mapping.getModelElements().get(0);
//				EObject secondModelElement = mapping.getModelElements().get(1);
//				EObject firstLanguageElement = firstModelElement.eClass();
//				EObject secondLanguageElement = secondModelElement.eClass();
//				for (MappingEnd end : mappingType.getMappingEnds()) {
//					if (end.getRoleName().equals(otherRoleName) && end.getLanguageElement().getLanguage().equals(firstLanguageElement)) {
//						otherModel = firstModelElement;
//					} else if (end.getRoleName().equals(otherRoleName) && end.getLanguageElement().getLanguage().equals(secondLanguageElement)) {
//						otherModel = secondModelElement;
//					}
//				}
//			}
//		}
		return otherModel;
		
	}

	/**
	 * Retrieves other {@link MappingEnd}s, given the a mapping end.
	 * 
	 * @param mappingEnd
	 * 
	 * @return corresponding mapping-end.
	 */
	public List<MappingEnd> getOtherMappingEnds(MappingEnd mappingEnd) {
		List<MappingEnd> otherMappingEnds = new ArrayList<MappingEnd>();
		CORELanguageElementMapping mappingType = mappingEnd.getType();
		for (MappingEnd end : mappingType.getMappingEnds()) {
			if (!end.equals(mappingEnd)) {
				otherMappingEnds.add(end);
			}
		}
		return otherMappingEnds;
	}

	/**
	 * Retrieve all mappings, irrespective of type.
	 * @param modelElement
	 * 
	 * @return
	 */
	public List<COREModelElementMapping> getMappings(EObject modelElement) {
		List<COREModelElementMapping> modelMappings = new ArrayList<COREModelElementMapping>();
		COREScene scene = COREArtefactUtil.getReferencingExternalArtefact(modelElement).getScene();
		List<COREModelElementMapping> allMappings = scene.getElementMappings();
		for (COREModelElementMapping mapping : allMappings) {
			if (mapping.getModelElements().contains(modelElement)) {
				modelMappings.add(mapping);
			}
		}
		return modelMappings;
	}
	
	/**
	 * Retrieve all mappings with a given scene.
	 * @param scene - the scene that contains the mappings
	 * @param modelElement - the element of the interest.
	 * 
	 * @return
	 */
	public List<COREModelElementMapping> getMappings(COREScene scene, EObject modelElement) {
		List<COREModelElementMapping> modelMappings = new ArrayList<COREModelElementMapping>();
		List<COREModelElementMapping> allMappings = scene.getElementMappings();
		for (COREModelElementMapping mapping : allMappings) {
			if (mapping.getModelElements().contains(modelElement)) {
				modelMappings.add(mapping);
			}
		}
		return modelMappings;
	}

	/**
	 * Retrieve all mappings, irrespective of type, with a given scene
	 * 
	 * @param modelElement
	 * @param scene        - the scene of the models
	 * @return
	 */
	public List<COREModelElementMapping> getMappings(EObject modelElement, COREScene scene) {
		List<COREModelElementMapping> modelMappings = new ArrayList<COREModelElementMapping>();
		List<COREModelElementMapping> allMappings = scene.getElementMappings();
		for (COREModelElementMapping mapping : allMappings) {
			if (mapping.getModelElements().contains(modelElement)) {
				modelMappings.add(mapping);
			}
		}
		return modelMappings;
	}

	/**
	 * Creates a model mapping between two instances (i.e., binary mapping),
	 * potentially from different languages.
	 * 
	 * @param perspective
	 * @param mappingType
	 * @param elementHere  model object of interest
	 * @param elementOther the corresponding model object to be mapped with.
	 * @param isRootMapping TODO
	 */
	public COREModelElementMapping createMapping(COREPerspective perspective, CORELanguageElementMapping mappingType, EObject elementHere,
			EObject elementOther, boolean isRootMapping) {

		COREScene scene = COREArtefactUtil.getReferencingExternalArtefact(elementHere).getScene();
		COREModelElementMapping mapping = CoreFactory.eINSTANCE.createCOREModelElementMapping();
		mapping.getModelElements().add(elementHere);
		mapping.getModelElements().add(elementOther);
		mapping.setLEMid(mappingType.getIdentifier());
		if (scene != null) {
			scene.getElementMappings().add(mapping);
		}
		return mapping;
	}

	/**
	 * Creates a model mapping between two instances (i.e., binary mapping),
	 * potentially from different languages.
	 * 
	 * @param perspective
	 * @param scene
	 * @param mappingType
	 * @param elementHere
	 * @param elementOther
	 * @param isRootMapping TODO
	 * @return
	 */
	public COREModelElementMapping createMapping(COREPerspective perspective, COREScene scene,
			CORELanguageElementMapping mappingType, EObject elementHere, EObject elementOther, boolean isRootMapping) {

		COREModelElementMapping mapping = CoreFactory.eINSTANCE.createCOREModelElementMapping();
		mapping.getModelElements().add(elementHere);
		mapping.getModelElements().add(elementOther);
		mapping.setLEMid(mappingType.getIdentifier());
		if (scene != null) {
			scene.getElementMappings().add(mapping);
		}
		return mapping;
	}

	/**
	 * Gets other role name which is not active on UI. This only works with binary
	 * mapping
	 * @param mappingType TODO
	 * @param currentRole
	 * 
	 * @return
	 */
	public String getOtherRoleName(CORELanguageElementMapping mappingType, String currentRole) {
		String otherRole = null;
		for (MappingEnd end : mappingType.getMappingEnds()) {
			if (!end.getRoleName().equals(currentRole)) {
				otherRole = end.getRoleName();
			}
		}
		return otherRole;
	}

	/**
	 * Retrieves the create type language action based on a language element.
	 * 
	 * @param perspective
	 * @param element     language metaclass
	 * @return the create type language action.
	 */
	public CORELanguageAction getCreateLanguageAction(COREPerspective perspective, EObject element) {
		CORELanguageAction createLanguageAction = null;
		for (COREAction action : perspective.getActions()) {
//            if (action instanceof CORELanguageAction) {
//                CORELanguageAction languageAction = (CORELanguageAction) action;
//                if (languageAction.getMainEffect().getLanguageElement().equals(element)
//                        && languageAction.getActionType() == LanguageActionType.CREATE)
//                    ;
//                createLanguageAction = languageAction;
//            }
		}

		return createLanguageAction;
	}

	/**
	 * Retrieves the {@link MappingEnd} based on its role name, mapping type, and
	 * the language element it references.
	 * 
	 * @param mappingType     that has the mapping end.
	 * @param languageELement the mapping end references.
	 * @param roleName        of the mapping end
	 * @return the {@link MappingEnd} that references the language element and the
	 *         role name.
	 */
	public MappingEnd getMappingEnd(CORELanguageElementMapping mappingType, EObject languageELement, String roleName) {
		MappingEnd mappingEnd = null;

		for (MappingEnd end : mappingType.getMappingEnds()) {
			if (end.getLanguageElement().getLanguageElement().equals(languageELement)
					&& end.getRoleName().equals(roleName)) {
				mappingEnd = end;
			}
		}

		return mappingEnd;
	}

	/**
	 * This method returns an instance of {@link CORELanguageElement} for a given
	 * language element.
	 * 
	 * @param perspective of the language element
	 * @param element
	 * @return the core language element of the language element
	 */
	public CORELanguageElement getCoreLanguageElement(COREPerspective perspective, EObject element) {
		CORELanguageElement languageElemet = null;
		outerloop: for (CORELanguage language : perspective.getLanguages().values()) {
			if (language instanceof COREExternalLanguage) {
				COREExternalLanguage extLanguage = (COREExternalLanguage) language;
				for (CORELanguageElement le : extLanguage.getLanguageElements()) {
					if (le.getLanguageElement().equals(element)) {
						languageElemet = le;
						break outerloop;
					}
				}
			}
		}

		return languageElemet;
	}

//	/**
//	 * Retrieves the role names of the corresponding metaclasses in a language
//	 * element mapping.
//	 * 
//	 * @param mappingType     is the language element mapping
//	 *                        {@link LanguageElementMapping}
//	 * @param element         is the metaclass of one mapping end. The role name of
//	 *                        this metaclass is known, i.e., currentRoleName.
//	 *                        However, the methods retrieves the role name of its
//	 *                        corresponding mapped metaclass.
//	 * @param currentRoleName of the current element.
//	 * @return the role name of the corresponding mapped metaclass.
//	 */
//	public List<String> getOtherRoleNames(CORELanguageElementMapping mappingType, EObject element,
//			String currentRoleName) {
//		// // the mapping end of just created element.
//		// MappingEnd currentMappingEnd =
//		// COREPerspectiveUtil.INSTANCE.getMappingEnd(mappingType, element.eClass(),
//		// currentRoleName);
//		// the mapping end of the element to be created.
//		// TODO: check for potential null
//		// MappingEnd mappingEnd =
//		// COREPerspectiveUtil.INSTANCE.getOtherMappingEnds(currentMappingEnd);
//
//		List<String> otherRoles = new ArrayList<String>();
//		for (MappingEnd end : mappingType.getMappingEnds()) {
//			if (!end.getRoleName().equals(currentRoleName)) {
//				otherRoles.add(end.getRoleName());
//			}
//		}
//
//		return otherRoles;
//	}

	/**
	 * This method returns an instance of {@link CORELanguageElement}, given the
	 * owner (core language element) and the structural feature of the element.
	 * 
	 * @param owner
	 * @param element, the structural feature of the model element.
	 * @return the nested {@link CORELanguageElement}
	 */
	public CORELanguageElement getNestedLanguageElement(CORELanguageElement owner, EObject element) {
		CORELanguageElement nestedElement = null;
		for (CORELanguageElement le : owner.getNestedElements()) {
			if (le.getLanguageElement().equals(element)) {
				nestedElement = le;
				break;
			}
		}
		return nestedElement;
	}

	/**
	 * Retrieves the mapping end of a nested {@link CORELanguageElementMapping}.
	 * 
	 * @param ownerMappingType, owner of the nested language element mapping.
	 * @param roleName
	 * @param nestedElement
	 * @return
	 */
	public MappingEnd getNestedMappingEnd(CORELanguageElementMapping ownerMappingType, String roleName,
			CORELanguageElement nestedElement) {
		MappingEnd nestedMappingEnd = null;
		List<CORELanguageElementMapping> nestedMappings = ownerMappingType.getNestedMappings();
		for (CORELanguageElementMapping nestedMapping : nestedMappings) {
			for (MappingEnd mappingEnd : nestedMapping.getMappingEnds()) {
				if (mappingEnd.getRoleName().equals(roleName)
						&& mappingEnd.getLanguageElement().equals(nestedElement)) {
					nestedMappingEnd = mappingEnd;
				}
			}
		}
		return nestedMappingEnd;

	}

	/**
	 * Retrieves othes attribute mapping ends.
	 * 
	 * @param attributeMappingEnd
	 * @return
	 */
	// public List<AttributeMappingEnd>
	// getOtherAttributeMappingEnds(AttributeMappingEnd attributeMappingEnd) {
	// List<AttributeMappingEnd> attributeMappingEnds = new
	// ArrayList<AttributeMappingEnd>();
	// COREAttributeMapping attributeMappingType = attributeMappingEnd.getType();
	// for (AttributeMappingEnd end : attributeMappingType.getMappingEnds()) {
	// if (!end.equals(attributeMappingEnd)) {
	// attributeMappingEnds.add(end);
	// }
	// }
	// return attributeMappingEnds;
	// }

	public boolean canMap(CORELanguageElementMapping mappingType, EObject modelElement, String roleName,
			COREScene scene) {
		boolean canMap = true;
		boolean mapped = false;
		MappingEnd mappingEnd = getMappingEnd(mappingType, modelElement.eClass(), roleName);
		Cardinality cardinality = mappingEnd.getCardinality();
		List<COREModelElementMapping> mappings = getElementMappings(mappingType, scene);
		for (COREModelElementMapping mapping : mappings) {
			Iterator<EObject> elements = mapping.getModelElements().iterator();
			while (elements.hasNext()) {
				EObject element = elements.next();
				if (element.equals(modelElement)) {
					mapped = true;
					break;
				}
			}
		}

		if (mapped) {
			switch (cardinality) {
			case COMPULSORY:
			case OPTIONAL:
				canMap = false;
				break;
			default:
				canMap = false;
			}
		}

		return canMap;

	}

	private List<COREModelElementMapping> getElementMappings(CORELanguageElementMapping mappingType, COREScene scene) {
		List<COREModelElementMapping> mappings = new ArrayList<COREModelElementMapping>();
		for (COREModelElementMapping mapping : scene.getElementMappings()) {
			if (mapping.getLEMid() == mappingType.getIdentifier()) {
				mappings.add(mapping);
			}
		}
		return mappings;
	}

	/**
	 * This method creates a new model based on the given language l, as well as a
	 * new COREExternalArtefact and initializes it to the given name.
	 * 
	 * @param l    the language to be used
	 * @param name the name to be used
	 * @return the new external artefact that points to the new model
	 */
	public COREExternalArtefact createNewRealizationModel(CORELanguage l, String name) {
		EObject externalModel;

		// create the model using the language's model util
		ModelUtil mu = CORELanguageRegistry.getRegistry().getModelUtil(l);
		externalModel = mu.createNewEmptyModel(name);

		// create a COREExternalArtefact that refers to the new model and return it
		COREExternalArtefact externalArtefact = CoreFactory.eINSTANCE.createCOREExternalArtefact();
		externalArtefact.setLanguageName(l.getName());
		externalArtefact.setRootModelElement(externalModel);
		externalArtefact.setName(name);

		return externalArtefact;
	}

	/**
	 * This method creates a new realization scene and associates it with the
	 * "features" features typed by the perspective "perspective". It further
	 * creates a new model using the language that plays the role "role" in the
	 * perspective. Finally, calls the recursive method "createOtherExternalModels"
	 * to create the corresponding model(s), if any, and then establish the mapping
	 * between models.
	 * 
	 * @param concern
	 * @param feature
	 * @param perspective
	 * @param role
	 */
	public COREScene createNewRealizationSceneAndModel(COREConcern concern, COREFeature feature,
			COREPerspective perspective, String role) {

		// initialize existing model maps
		existingModels = new HashMap<String, EObject>();
		String defaultName = "";
		defaultName = defaultName + feature.getName();

		// Create the chosen artefact
		final COREExternalArtefact chosenExternalArtefact;
		CORELanguage l = perspective.getLanguages().get(role);
		chosenExternalArtefact = createNewRealizationModel(l, defaultName);

		EObject chosenExternalModel = chosenExternalArtefact.getRootModelElement();

		// Create the new scene
		COREScene scene = COREModelUtil.createScene();
		scene.setPerspectiveName(perspective.getName());
		EList<COREArtefact> artefacts = new BasicEList<COREArtefact>();
		artefacts.add(chosenExternalArtefact);
		scene.getArtefacts().put(role, artefacts);
		chosenExternalArtefact.setScene(scene);

		try {
			saveModel(concern, role, chosenExternalArtefact);
		} catch (IOException e) {
		}

		// Check if there are any mandatory CORELanguageElementMappings that dictate
		// that other models must be created
		createOtherExternalModels(chosenExternalModel, perspective, role, defaultName, concern, scene);

		// Associate the scene with the feature, and contain all artefacts
		FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
		controller.associateSceneToFeature(scene, feature);

		// clear existing role names
		existingModels.clear();

		return scene;

	}

	/**
	 * This method checks whether there are any mandatory language element mappings
	 * that require the newly created model to be mapped to some other model(s), and
	 * if yes, it recursively creates the corresponding model(s) and then establish
	 * the mapping between models, respectively.
	 * 
	 * @param externalModel
	 * @param perspective
	 * @param currentRoleName
	 * @param defaultName
	 * @param concern
	 * @param scene
	 */
	private void createOtherExternalModels(EObject externalModel, COREPerspective perspective, String currentRoleName,
			String defaultName, COREConcern concern, COREScene scene) {

		existingModels.put(currentRoleName, externalModel);

		List<CORELanguageElementMapping> mappingTypes = COREPerspectiveUtil.INSTANCE.getMappingTypes(perspective,
				externalModel.eClass(), currentRoleName);
		for (CORELanguageElementMapping mappingType : mappingTypes) {
			List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(mappingType, scene,
					externalModel);

			String toCreateRoleName = COREPerspectiveUtil.INSTANCE.getOtherRoleName(mappingType, currentRoleName);

			ActionType actionType = TemplateType.INSTANCE.getCreateType(mappingType, currentRoleName);

			// check if the mapping already exist.
			if (mappings.size() == 0) {
				boolean otherExist = true;
				EObject otherExternalModel = findExistingNewModel(toCreateRoleName);

				// other external artefact
				COREExternalArtefact otherArtefact = null;

				// check if other external model is null, if yes, create the model.
				if (otherExternalModel == null) {
					otherExist = false;

					if (actionType == ActionType.CREATE || actionType == ActionType.CREATE_OR_USE_NON_MAPPED
							|| actionType == ActionType.CREATE_OR_USE || actionType == ActionType.CREATE_AT_LEAST_ONE
							|| actionType == ActionType.CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE
							|| actionType == ActionType.CREATE_OR_USE_AT_LEAST_ONE) {

						CORELanguage otherLanguage = perspective.getLanguages().get(toCreateRoleName);

						// create the additional artefact
						otherArtefact = createNewRealizationModel(otherLanguage, defaultName);
						EList<COREArtefact> otherArtefacts = new BasicEList<COREArtefact>();
						otherArtefacts.add(otherArtefact);
						// add it to the scene
						scene.getArtefacts().put(toCreateRoleName, otherArtefacts);
						otherArtefact.setScene(scene);

						otherExternalModel = otherArtefact.getRootModelElement();

						// TODO EObject obj = EcoreUtil.getRootContainer(perspective);

					}
				}

				// create mapping
				COREPerspectiveUtil.INSTANCE.createMapping(perspective, scene, mappingType, externalModel,
						otherExternalModel, false);

				// Save the model
				if (otherArtefact != null) {
					try {
						saveModel(concern, toCreateRoleName, otherArtefact);
					} catch (IOException e) {

					}
				}

				// stop the recursion if other element exists.
				if (!otherExist) {
					createOtherExternalModels(otherExternalModel, perspective, toCreateRoleName, defaultName, concern,
							scene);
				}

			}
		}
	}

	/**
	 * Checks if a model with the role "role" already exist.
	 * 
	 * @param role - the role of the model
	 * @return the model
	 */
	private EObject findExistingNewModel(String role) {
		EObject existingModel = null;
		for (Map.Entry<String, EObject> entry : existingModels.entrySet()) {
			String key = entry.getKey();
			if (key.equals(role)) {
				existingModel = entry.getValue();
				break;
			}
		}
		return existingModel;
	}

	/**
	 * A helper method which returns a map (key = other role name, value = other feature) which contains
	 * the feature of an element which needs to be updated as a result of updating a synchronized feature
	 * of another element.
	 * @param perspective
	 * @param currentRole - the role name of the updated element.
	 * @param currentMapping - the current mapping of the updated element
	 * @param currentFeature - the current feature of the updated element.
	 * @return
	 */
	public Map<String, EObject> getNestedOtherDetails(COREPerspective perspective, String currentRole,
			COREModelElementMapping currentMapping, EObject currentFeature) {
		
		Map<String, EObject> otherDetails = null;
		CORELanguageElementMapping mappingType = COREPerspectiveUtil.INSTANCE.getMappingType(perspective, currentMapping);
		for (CORELanguageElementMapping innerMapping : mappingType.getNestedMappings()) {
			for (MappingEnd end : innerMapping.getMappingEnds()) {
				if (end.getLanguageElement().getLanguageElement().equals(currentFeature) &&
						end.getRoleName().equals(currentRole)) {
					otherDetails = getOtherDetails(innerMapping, currentRole,currentFeature);
				}
			}
		}
		return otherDetails;
	}

	
	private Map<String, EObject> getOtherDetails(CORELanguageElementMapping innerMapping, String currentRole,
			EObject currentFeature) {
		Map<String, EObject> otherDetails = new HashMap<>();
		MappingEnd firstMappingEnd = innerMapping.getMappingEnds().get(0);
		MappingEnd secondMappingEnd = innerMapping.getMappingEnds().get(1);
		if (firstMappingEnd.getLanguageElement().getLanguageElement().equals(currentFeature) && 
				firstMappingEnd.getRoleName().equals(currentRole)) {
			EObject otherFeature = secondMappingEnd.getLanguageElement().getLanguageElement();
			String otherRoleName = secondMappingEnd.getRoleName();
			otherDetails.put(otherRoleName, otherFeature);
		} else {
			EObject otherFeature = firstMappingEnd.getLanguageElement().getLanguageElement();
			String otherRoleName = firstMappingEnd.getRoleName();
			otherDetails.put(otherRoleName, otherFeature);
		}
		return otherDetails;
	}
}
