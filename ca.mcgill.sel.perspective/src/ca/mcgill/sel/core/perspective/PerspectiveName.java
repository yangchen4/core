package ca.mcgill.sel.core.perspective;

public class PerspectiveName {
    
    /**
     * Domain model perspective name.
     */
    public static final String DOMAIN_MODEL_PERSPECTIVE = "Domain Modelling";
    
    /**
     * Design model perspective name.
     */
    public static final String DESIGN_MODEL_PERSPECTIVE = "Design Modelling";
    
    /**
     * Domain Design model perspective name.
     */
    public static final String DOMAIN_DESIGN_MODEL_PERSPECTIVE = "DomainModel_DesignModel_Gen";
    
    /**
     * Domain Use Case model perspective name.
     */
    public static final String DOMAIN_USECASE_MODEL_PERSPECTIVE = "Domain UseCase";
    
    /**
     * Use Case model perspective name.
     */
    public static final String USECASE_MODEL_PERSPECTIVE = "Use Case Modelling";
    
    /**
     * Domain Design Use Case model perspective name.
     */
    public static final String DOMAIN_DESIGN_USECASE_PERSPECTIVE = "Fondue Requirement";
    
    /**
     * Domain Design Use Case model perspective name.
     */
    public static final String FONDUE_REQUIREMENT_PERSPECTIVE = "Domain_Design_UseCase";
    
    /**
     * The domain model role name.
     */
    public static final String DOMAIN_MODEL_ROLE = "Domain_Model";
    
    /**
     * The design model role name.
     */
    public static final String DESIGN_MODEL_ROLE = "Design_Model";
    
    /**
     * The use case model role name.
     */
    public static final String USECASE_MODEL_ROLE = "Use_Case";
    

}
