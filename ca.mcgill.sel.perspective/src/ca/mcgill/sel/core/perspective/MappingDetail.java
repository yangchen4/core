package ca.mcgill.sel.core.perspective;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREModelElementMapping;

/**
 * This class has details of a potential {@link COREModelElementMapping}, including the flag, if the given two elements
 * can be mapped, and the message to be shown to a user if the mapping is not allowed.
 * @author hyacinthali
 *
 */
public class MappingDetail {

    EObject firstElement;
    EObject secondElement;
    MappingType mappingType;
    boolean map;
    String message;

    /**
     * @return the firstElement
     */
    public EObject getFirstElement() {
        return firstElement;
    }

    /**
     * @param firstElement the firstElement to set
     */
    public void setFirstElement(EObject firstElement) {
        this.firstElement = firstElement;
    }

    /**
     * @return the secondElement
     */
    public EObject getSecondElement() {
        return secondElement;
    }

    /**
     * @param secondElement the secondElement to set
     */
    public void setSecondElement(EObject secondElement) {
        this.secondElement = secondElement;
    }

    /**
     * @return the mappingType
     */
    public MappingType getType() {
        return mappingType;
    }

    /**
     * @param mappingType the mappingType to set
     */
    public void setType(MappingType mappingType) {
        this.mappingType = mappingType;
    }

    /**
     * @return the map
     */
    public boolean isMap() {
        return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(boolean map) {
        this.map = map;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    

}
