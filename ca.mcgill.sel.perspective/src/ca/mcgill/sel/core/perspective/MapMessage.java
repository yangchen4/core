package ca.mcgill.sel.core.perspective;

public class MapMessage {
    
    boolean map;
    String message;
    
    /**
     * @return the map
     */
    public boolean isMap() {
        return map;
    }
    /**
     * @param map the map to set
     */
    public void setMap(boolean map) {
        this.map = map;
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
