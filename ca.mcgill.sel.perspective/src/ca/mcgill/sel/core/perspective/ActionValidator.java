package ca.mcgill.sel.core.perspective;

import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREPerspectiveAction;
import ca.mcgill.sel.core.CORERedefineAction;
import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.CreateModelElementMapping;

/**
 * The controller for {@link COREPerspective}s.
 *
 * @author hyacinthali
 */
public class ActionValidator {
    /**
     * Creates a new instance of {@link ActionValidator}.
     */
    public ActionValidator() {

    }

    public boolean canCreateNewClass(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_CLASS.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateImplementationClass(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_IMPLEMENTATION_CLASS.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
    	
        return false;
    }

    public boolean canRemoveClassifier(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.DELETE_CLASS.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
    	
        return false;
    }

    public boolean canEditClassifier(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.EDIT_CLASS.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateAssociation(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_ASSOCIATION.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        
        return false;
    }

    public boolean canRemoveNote(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.DELETE_NOTE.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateEnum(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_ENUM.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateNewNote(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_NOTE.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateNaryAssociation(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_NARY_ASSOCIATION.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canEditAssociation(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.EDIT_ASSOCIATION.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canDeleteAssociation(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.DELETE_ASSOCIATION.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canEditClass(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.EDIT_CLASS.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canEditVisibillity(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.EDIT_VISIBILITY.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canEditStatic(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.EDIT_STATIC.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canRemoveAttribute(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.DELETE_ATTRIBUTE.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateAttribute(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_ATTRIBUTE.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canEditOperation(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.EDIT_OPERATION.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canRemoveOperation(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.REMOVE_OPERATION.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateOperation(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_OPERATION.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canCreateParameter(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.ADD_OPERATION_PARAMETER.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canEditEnum(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.EDIT_ENUM.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }
        return false;
    }

    public boolean canDeleteEnum(COREPerspective perspective, String languageRole) {

    	for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREPerspectiveAction) {
            	COREPerspectiveAction action = (COREPerspectiveAction) pAction;
                if (action.getName().equals(PerspectiveAction.DELETE_ENUM.getName()) && action.getForRole().equals(languageRole)) {
                	return true;
                }
            } 
        }

        return false;
    }

    public boolean canCreateMapping(COREPerspective perspective, String languageRole) {

        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof CreateModelElementMapping) {
                CreateModelElementMapping action = (CreateModelElementMapping) pAction;
                if (action.getForRole().equals(languageRole)) {
                    String actionName = action.getName();
                    if (actionName.equals(PerspectiveAction.CREATE_MAPPING.getName())) {
                        return true;
                    }
                }

            }
        }
        return false;
    }

}
