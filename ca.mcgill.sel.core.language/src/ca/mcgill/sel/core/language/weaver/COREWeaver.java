package ca.mcgill.sel.core.language.weaver;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.language.weaver.util.COREModelCompositionUpdater;
import ca.mcgill.sel.core.language.weaver.util.COREReferenceUpdater;
import ca.mcgill.sel.core.language.weaver.util.COREWeaverUtil;
import ca.mcgill.sel.core.language.weaver.util.ExtensionDependenciesInfo;
import ca.mcgill.sel.core.language.weaver.util.WeaverListener;
import ca.mcgill.sel.core.language.weaver.util.WeavingInformation;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.ModelCopier;

/**
 * The COREWeaver serves 2 purposes: - it aggregates weavers for all the
 * languages that are being used within CORE. During startup (or at the latest
 * whenever a {@link COREArtefact} is loaded into memory), the corresponding
 * {@link COREModelWeaver} should register with this class - it implements all
 * CORE related composition algorithms
 * 
 * @author Joerg
 */
public final class COREWeaver {

    /**
     * The singleton instance of this class.
     */
    private static COREWeaver instance;

    private Map<String, COREModelWeaver<?>> weavers = new HashMap<String, COREModelWeaver<?>>();

    private WeavingInformation weavingInformation;

    // When weaving a concern, this dependency weaving info object keeps track of
    // the artefacts that need to be woven
    private ExtensionDependenciesInfo extensionDependenciesInfo;

    /**
     * Creates a new instance of the weaver.
     */
    private COREWeaver() {
        weavingInformation = new WeavingInformation();
    }

    /**
     * Returns the singleton instance of {@link COREWeaver}.
     * 
     * @return the singleton instance
     */
    public static COREWeaver getInstance() {
        if (instance == null) {
            instance = new COREWeaver();
        }

        return instance;
    }

    /**
     * Method to register a specific {@link COREModelWeaver} for models (instances
     * of a particular {@link COREExternalLanguage}). used within CORE. Every
     * modelling language must register a weaver during startup. From then on,
     * whenever two {@link COREExternalArtefact}s that belong to the same language
     * are to be woven, the registered {@link COREModelWeaver} will be called.
     * 
     * @param language the language for which the model weaver should be used
     * @param weaver   the instance of {@link COREModelWeaver} to register
     * @param <T>      kind of models that the weaver handles
     */
    public <T extends EObject> void registerCOREModelWeaver(String language, COREModelWeaver<?> weaver) {
        weavers.put(language, weaver);
    }
    
    

    /**
     * Weave a single {@link COREModelComposition} into the given
     * {@link COREArtefact} (could be a reuse or an extension).
     * 
     * @param artefact    - The {@link COREExternalArtefact} to weave into.
     * @param composition - The {@link COREModelComposition} that refers to the
     *                    {@link COREArtefact} that is to be woven into model.
     * @param listener    - The {@link WeaverListener} that handles start / success / failure notifications.
     */
    public void weaveSingle(COREExternalArtefact artefact, COREModelComposition composition,
            WeaverListener<EObject> listener) {
        listener.weavingStarted();

        try {
            // TODO: Needs to be changed once we start using perspectives
            COREModelWeaver<EObject> weaver = getModelWeaver(artefact.getLanguageName());

            // first copy the artefact, so the woven outcome does not tamper the original
            // input.
            COREExternalArtefact artefactCopy = COREArtefactUtil.copyArtefactAndModel(artefact);
            EObject modelCopy = artefactCopy.getRootModelElement();

            // set the name of the artefact to "Woven " + base name + reused/extended name
            artefactCopy.setName("Woven " + artefactCopy.getName() + " " + composition.getSource().getName());

            // add the copied artefact to the list of temporary artefacts associated with
            // the current concern
            COREConcern currentConcern = artefact.getCoreConcern();
            // TODO: change once we decided where to put woven artefact
            // in case we are weaving an already woven artefact, we look for the concern
            // through the language
            if (currentConcern == null) {
                currentConcern = artefact.getTemporaryConcern();
            }
            currentConcern.getTemporaryArtefacts().add(artefactCopy);

            // create a new (temporary) scene to hold the new woven artefact, and set the
            // perspective
            // to the same name as the base
            COREScene wovenScene = CoreFactory.eINSTANCE.createCOREScene();
            artefactCopy.setScene(wovenScene);
            wovenScene.setPerspectiveName(artefact.getScene().getPerspectiveName());

            COREModelComposition compositionCopy = getModelCompositionCopy(artefact, composition, artefactCopy);

            EObject result = null;

            // populate weaving information with the information from the
            // COREModelComposition
            weavingInformation.clear();
            populateWeavingInformation(weavingInformation, compositionCopy);

            result = weaver.weaveModel(artefactCopy, modelCopy, compositionCopy, weavingInformation, false);

            weavingInformation.print();

            // now we need to get rid of the composition specification that we just wove
            if (compositionCopy instanceof COREModelReuse) {
                artefactCopy.getModelReuses().remove(compositionCopy);
            } else {
                artefactCopy.getModelExtensions().remove(compositionCopy);
            }

            weaveAndUpdateCompositions(artefactCopy, composition.getSource(), weavingInformation, false);

            if (result != null) {
                listener.weavingFinished(artefactCopy, result);
            } else {
                listener.weavingFailed(new WeaverException("No result was produced."));
            }
        } catch (WeaverException we) {
            listener.weavingFailed(we);
        }
    }

    /**
     * This operation copies the {$link EObject} (which is supposed to be in a
     * source model of the same type as the target model) into the target model into
     * the same container. The operation copies all the EObjects contained in
     * modelElement recursively.
     * 
     * @param modelElement the model element that must be copied
     * @param target       the COREArtefact into which the element is to be copied
     * @param info         the weaving information
     * @param composition  the COREModelComposition for this weaving
     * @param <M>          the type of model element to be used
     * @return the new copy
     */
    public static <M extends EObject> M copyModelElement(M modelElement, COREExternalArtefact target,
            WeavingInformation info, COREModelComposition composition) {
        HashMap<EObject, EObject> mappings = new HashMap<EObject, EObject>();
        @SuppressWarnings("unchecked")
        M modelElementCopy = (M) ModelCopier.copyModelAndRetrieveMappings(modelElement, mappings);

        // perform automated information hiding if the composition is a reuse
        // TODO: This will have to be done somewhere else now
//        if (composition instanceof COREModelReuse) {
//            if (COREInterfaceUtil.isPublic(target, modelElement)) {
//                COREInterfaceUtil.modelElementCopy.setVisibility(COREVisibilityType.CONCERN);
//            }
//        }

        // to add the copied model element into the target model at the right place
        // first traverse in the original model from the modelElement up to the root,
        // remembering
        // the containing features

//        ArrayList<EStructuralFeature> featureList = new ArrayList<EStructuralFeature>();
//        EStructuralFeature containingFeature = modelElement.eContainingFeature();
//        EObject currentElement = modelElement;
//        while (currentElement != null) {
//            featureList.add(containingFeature);
//            System.out.println("Model element " + currentElement + " is in feature: " + containingFeature);
//            if (currentElement.eContainer().eContainer() != null) {
//                currentElement = currentElement.eContainer();
//                containingFeature = currentElement.eContainingFeature();                
//            } else {
//                currentElement = null;
//            }
//        }
//        
//        
//        // now find the spot to insert the modelElementCopy in the target model
//        int last = featureList.size() - 1;
//        containingFeature = featureList.get(last);
//        currentElement = (EObject) target.getRootModelElement();
//        EObject previousElement = currentElement;
//        while (last > 0) {
//            currentElement = (EObject) currentElement.eGet(containingFeature);
//            System.out.println("Model element: " + currentElement);
//            last--;
//            previousElement = currentElement;
//            containingFeature = featureList.get(last);
//        }
//            
//            // insert the modelElementCopy into the found container
//        ((EList<EObject>) previousElement.eGet(containingFeature)).add(modelElementCopy);

        // add the copy and all it's contained elements to the weaving info
        info.add(modelElement, modelElementCopy);
        for (EObject key : mappings.keySet()) {
            info.add(key, mappings.get(key));
        }
        return modelElementCopy;
    }

//    /**
//     * Weave the set of {@link COREArtefact}s and all their {@link COREModelExtensions} into 
//     * a new model.
//     * 
//     * @param models - The list of {@link COREArtefact}s to weave.
//     * @return a new COREExternalArtefact that refers to the woven model.
//     */
//    public COREExternalArtefact weaveModels(List<COREExternalArtefact> models) {
//        if (models.size() == 0) {
//            // TODO: Figure out what to do if this method is called with an empty list of models
//            return null;
//        }
//
//        //TODO: Needs to be changed once we start using perspectives
//        String languageName = 
//                ((COREExternalArtefact) models.get(0).getScene().getArtefacts().get(0)).getLanguage();
//        try {
//            COREModelWeaver<EObject> weaver = getModelWeaver(languageName);
//
//            COREExternalArtefact newArtefact = null;
//            EObject newModel = null;
//                
//            newArtefact = CoreFactory.eINSTANCE.createCOREExternalArtefact();
//            newArtefact.setLanguage(languageName);
//                
//            // loops though all the models and create a COREModelExtension from the new, empty model 
//            // to the model that is supposed to be woven
//            // Also construct the new name as we go
//            String newName = "Woven";
//                
//            for (COREExternalArtefact model : models) {
//                COREModelExtension modelExtension = CoreFactory.eINSTANCE.createCOREModelExtension();
//                modelExtension.setSource(model);
//                newArtefact.getModelExtensions().add(modelExtension);
//                newName = newName + "_" + model.getName();
//            }
//            // create a new empty model
//            newModel = weaver.createEmptyModel();
//            newArtefact.setRootModelElement(newModel);
//            newArtefact.setName(newName);
//            
//            // add the new artefact to the list of temporary artefacts associated with the current concern
//            COREConcern currentConcern = models.get(0).getCoreConcern();
//            currentConcern.getTemporaryArtefacts().add(newArtefact);
//                    
//            // now loop through all model extensions and weave them successively into the result model
//            int highestRank = dependencyWeavingInfo.getHighestRank(newArtefact);
//
//            while (highestRank >= 1) {
//                COREModelComposition mext = dependencyWeavingInfo.getHighestRankedCompositionSpecification();
//
//                // populate weaving information with the information from the COREModelComposition
//                weavingInformation.clear();
//                populateWeavingInformation(weavingInformation, mext);
//
//                System.out.println("Weaving " + mext.getSource() + " into " + newArtefact);
//                newModel = weaver.weaveModel(newArtefact, newModel, mext, weavingInformation);
//                
//                // remove the woven composition specification from the model
//                newArtefact.getModelExtensions().remove(mext);
//                
//                weaveAndUpdateCompositions(newArtefact, mext.getSource(), weavingInformation);
//                
//                highestRank = dependencyWeavingInfo.getHighestRank(newArtefact);
//            }
//            return newArtefact;
//        } catch (WeaverException e) {
//            System.out.println("No language registered for " + languageName);
//            return null;
//        }
//
//    }

//    /**
//     * Weave the set of {@link COREArtefact}s and all their {@link COREModelExtensions} into 
//     * a new model.
//     * 
//     * @param models - The list of {@link COREArtefact}s to weave.
//     * @param listener - The {@link WeaverListener} to report any exceptions to.
//     */
//    public void weaveMultiple(List<COREExternalArtefact> models, WeaverListener<EObject> listener) {
//        if (models.size() == 0) {
//            // TODO: Figure out what to do if this method is called with an empty list of models
//            return;
//        }
//        listener.weavingStarted();
//
//        COREExternalArtefact result;
//        
//        try {
//            
//            result = weaveModels(models);
//                 
//            listener.weavingFinished(result, result.getRootModelElement());
//            // CHECKSTYLE:IGNORE IllegalCatch: There could be many different exceptions during weaving.
//        } catch (Exception e) {
//            // TODO: catch in lower level weaver operations, and throw custom exception.
//            listener.weavingFailed(new WeaverException(e));
//        }
//    }

    /**
     * Method that returns the registered {@link COREModelWeaver} for a given
     * CORELanguage.
     * 
     * @param language the CORELanguage for which we need to find a weaver
     * @param <T>      the type of the model expressed in the language
     * 
     * @return the previously registered {@link COREModelWeaver}
     * @throws WeaverException is no weaver is registered for the COREArtefact class
     */
    private <T extends EObject> COREModelWeaver<T> getModelWeaver(String language) throws WeaverException {
        if (!weavers.containsKey(language)) {
            throw new WeaverException(String.format("No Weaver registered for language: %s", language));
        }

        @SuppressWarnings("unchecked")
        COREModelWeaver<T> weaver = (COREModelWeaver<T>) weavers.get(language);

        return weaver;
    }

    /**
     * Method that returns the registered {@link COREModelWeaver} for a given
     * COREArtefact.
     * 
     * @param model an instance of COREArtefact that needs to be woven
     * @param <T>   the specific COREArtefact type
     * 
     * @return the previously registered {@link COREModelWeaver}
     * @throws WeaverException is no weaver is registered for the COREArtefact class
     */
//    private <T extends COREArtefact> COREModelWeaver<T> getModelWeaver(T model) throws WeaverException {
//        @SuppressWarnings("unchecked")
//        Class<T> modelClass = (Class<T>) model.getClass();
//        
//        return getModelWeaver(modelClass);
//    }

    /**
     * This operation goes through all CORELinks that link contains, if any, and
     * adds those also to the weaving information.
     * 
     * @param info the weaving information that should be populated
     * @param link the link that might contain child links
     */
    private void populateWeavingInformationOfNestedLinks(WeavingInformation info, CORELink<?> link) {
        for (EObject obj : link.eContents()) {
            @SuppressWarnings("unchecked")
            CORELink<EObject> nestedLink = (CORELink<EObject>) obj;

            weavingInformation.add(nestedLink.getFrom(), nestedLink.getTo());

            // What if the nestedLink has further children?
            populateWeavingInformationOfNestedLinks(info, nestedLink);
        }
    }

    /**
     * This operation goes through all CORELinks that link contains, if any, and
     * adds the reverse of those mappings also to the weaving information.
     * 
     * @param info        the weaving information that should be populated
     * @param link        the link that might contain child links
     * @param reverseLink the reverseLink that is to contain the reverse mapping
     */
    private void reversePopulateWeavingInformationOfNestedLinks(WeavingInformation info, CORELink<?> link,
            CORELink<?> reverseLink) {
        for (EObject obj : link.eContents()) {
            CORELink<?> nestedLink = (CORELink<?>) obj;

            EObject mappedFrom = weavingInformation.getFirstToElement((EObject) nestedLink.getFrom());
            if (mappedFrom == null) {
                mappedFrom = (EObject) nestedLink.getFrom();
            }
            
            info.add((EObject) nestedLink.getTo(), mappedFrom);
            CORELink<EObject> nestedReverseLink = CoreFactory.eINSTANCE.createCOREMapping();
            nestedReverseLink.setFrom((EObject) nestedLink.getTo());
            nestedReverseLink.setTo(mappedFrom);
            @SuppressWarnings("unchecked")
            COREMapping<EObject> mapping = (COREMapping<EObject>) link;
            COREMapping<EObject> reverseMapping = (COREMapping<EObject>) nestedReverseLink;
            mapping.getMappings().add(reverseMapping);

            // What if the nestedLink has further children?
            reversePopulateWeavingInformationOfNestedLinks(info, nestedLink, nestedReverseLink);
        }
    }

    /**
     * This operation takes all the composition mappings from the
     * COREModelComposition and stores the mapping in the weaving information
     * object.
     * 
     * @param info        the weaving information that should be populated
     * @param composition the COREModelComposition that is going to be woven
     */
    private void populateWeavingInformation(WeavingInformation info, COREModelComposition composition) {

        // process the mappings
        for (COREModelElementComposition<?> mec : composition.getCompositions()) {

            // Currently we can only handle CORELinks... If ever we support patterns in the
            // future, we need
            // to figure out how we want to populate the weaving info correctly based on the
            // pattern
            @SuppressWarnings("unchecked")
            CORELink<EObject> link = (CORELink<EObject>) mec;
         
            info.add(link.getFrom(), link.getTo());
            
            // If the link contains other links (such as for example ClassifierMappings, which contain
            // attribute mappings and operation mappings, then we should also take care of those mappings

            populateWeavingInformationOfNestedLinks(info, link);
        }
    }

    /**
     * The weaver requires an input model and a specific mapping of that input model
     * (pointing to a source model of same kind). Before calling this method the
     * weaver has already created a deep copy of the original input model, including
     * a deep copy of the mapping. This method retrieves the clones mapping from the
     * cloned artifact. (If modelCopy is a copy of originalModel, then this operation
     * finds the {@link COREModelComposition} in modelCopy that corresponds to
     * originalComposition.)
     * 
     * @param originalModel      - The model to weave in.
     * @param originalCompositon - The original {@link COREModelComposition}.
     * @param modelCopy          - The copy of the originalModel.
     * @return the found {link COREModelComposition}.
     */
    private COREModelComposition getModelCompositionCopy(COREArtefact originalModel,
            COREModelComposition originalCompositon, COREArtefact modelCopy) {
        EStructuralFeature feature = originalCompositon.eContainingFeature();

        @SuppressWarnings("unchecked")
        EList<? extends COREModelComposition> compositions = (EList<? extends COREModelComposition>) originalModel
                .eGet(feature);
        @SuppressWarnings("unchecked")
        EList<? extends COREModelComposition> compositionsCopy = (EList<? extends COREModelComposition>) modelCopy
                .eGet(feature);

        int index = compositions.indexOf(originalCompositon);
        COREModelComposition compositionCopy = compositionsCopy.get(index);

        return compositionCopy;
    }
//    
//    public <T extends COREArtefact> void weaveAll(T model, WeaverListener<T> listener) {
//        
//    }
//    
//    /**
//     * Weaves the aspects of the selected features of the given concern to one aspect.
//     * 
//     * @param concern - the reused concern
//     * @param configuration - the configuration containing the selection that was made.
//     * @param saveModel - Whether to save the woven result in a file or not.
//     * @param weaveAll - Whether to weave state and AspectMessageViews as well.
//     * @return the woven aspect containing the merge result of all selected features
//     * @throws WeaverException 
//     */
//    public <T extends COREArtefact> void weaveSelectedFeatures(COREConcern concern,
//    COREReuseConfiguration configuration, Class<T> modelClass, boolean saveModel,
//            boolean weaveAll) throws WeaverException {
//        
//    }
//    
//    private <T extends COREArtefact> T createModel(Class<T> modelClass, COREConcern concern,
//    Set<COREArtefact> models) throws WeaverException {
//        COREModelWeaver<T> weaver = getModelWeaver(modelClass);
//        T emptyModel = weaver.createEmptyModel();
//        
//        for (COREArtefact model : models) {
//            COREModelExtension extension = CoreFactory.eINSTANCE.createCOREModelExtension();
//            extension.setSource(model);
//            
//            emptyModel.getModelExtensions().add(extension);
//        }
//        
//        return emptyModel;
//    }
//    
    /**
     * Loads a new instance of a model. This is required, because lower-level model
     * that contain dependencies will be modified, i.e., it's dependencies are woven
     * into it. If this (lower-level) model is loaded somewhere else, e.g., a GUI,
     * the modifications are reflected there, which is unwanted behavior. If the
     * model were cloned, it would require the mappings of the instantiation for
     * this lower-level model to be updated to the new elements, which would be more
     * time-consuming to do. Therefore, to circumvent this, the model is saved in a
     * temporary file and loaded again using a separate resource set, which forces
     * to load other models to be loaded as new instances. Otherwise the existing
     * resource set would get the resources for the lower-level model from its
     * cache.
     * 
     * @param model the model to load a new instance for
     * @return a new instance of the given model
     * @param <T> the specific COREArtefact type
     */
//    private <T extends COREArtefact> T loadNewInstance(T model) {
//        // The given COREArtefact has to be cloned. Otherwise, when adding the model to the new resource
//        // it gets removed from its current resource. This will mean that the given model
//        // is directly modified.
//        T result = EcoreUtil.copy(model);
//
//        String pathName = TEMPORARY_MODEL_FILE.getAbsolutePath();
//
//        // Use our own resource set for saving and loading to workaround the issue.
//        ResourceSet resourceSet = new ResourceSetImpl();
//
//        // Create a resource to temporarily save the aspect.
//        Resource resource = resourceSet.createResource(URI.createFileURI(pathName));
//        resource.getContents().add(result);
//
//        try {
//            resource.save(Collections.EMPTY_MAP);
//
//            // Load the temporary aspect ...
//            resource = resourceSet.getResource(URI.createFileURI(pathName), true);
//            @SuppressWarnings("unchecked")
//            T newCopy = (T) resource.getContents().get(0);
//
//            // Copy the model to loose the reference to the temporary file
//            result = EcoreUtil.copy(newCopy);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // Delete the temporary file ...
//        TEMPORARY_MODEL_FILE.delete();
//
//        return result;
//    }

    /**
     * This method weaves the model compositions by iterating through all
     * compositions in the source model and copies them into the base model, and
     * then updates the model element references to refer to the local elements.
     *
     * @param base               The COREArtefact that has been woven into
     * @param source             The COREArtefact that was woven
     * @param weavingInformation The data structure containing all mapping
     *                           information
     * @param topdown            whether we are weaving topdown or not
     */
    private static void weaveAndUpdateCompositions(COREArtefact base, COREArtefact source,
            WeavingInformation weavingInformation, boolean topdown) {

        // If the source model has compositions it is necessary to copy the compositions
        // to the base model
        for (COREModelExtension externalExtension : source.getModelExtensions()) {
            // don't copy the extension if we are weaving topdown
            if (!topdown) {
                COREModelExtension extensionCopy = EcoreUtil.copy(externalExtension);
                base.getModelExtensions().add(extensionCopy);
            }
        }
        for (COREModelReuse externalModelReuses : source.getModelReuses()) {
            COREModelReuse reuseCopy = EcoreUtil.copy(externalModelReuses);
            base.getModelReuses().add(reuseCopy);
        }

        // Then update any references in the mappings that still refer to the source
        // model to now
        // refer to the correct elements in the base model
        COREReferenceUpdater updater = COREModelCompositionUpdater.getInstance();
        updater.update(base.getModelExtensions(), weavingInformation);
        updater.update(base.getModelReuses(), weavingInformation);

        // Finally merge any model extension compositions that extend the same source
        // model
        int maxindex = base.getModelExtensions().size();
        List<COREModelExtension> extensions = base.getModelExtensions();

        for (int i = 0; i < maxindex - 1; i++) {
            COREModelExtension extension = extensions.get(i);
            for (int j = i + 1; j < maxindex;) {
                COREModelExtension comparedWith = extensions.get(j);
                if ((extension != comparedWith) && (extension.getSource() == comparedWith.getSource())) {
                    // merge the mappings from comparedWith to extension
                    mergeExtensions(extension, comparedWith);
                    // then delete it
                    extensions.remove(j);
                    maxindex--;
                } else {
                    j++;
                }
            }
        }
    }

    private static void mergeExtensions(COREModelExtension into, COREModelExtension from) {

        List<COREModelElementComposition<?>> comps = from.getCompositions();

        for (int i = 0; i < comps.size();) {
            COREModelElementComposition<?> n = comps.get(i);
            if (n instanceof COREMapping) {
                COREMapping<?> otherMapping = (COREMapping<?>) n;

                // check if there is already such a mapping in "into"
                boolean found = false;
                for (COREModelElementComposition<?> m : into.getCompositions()) {
                    if (m instanceof COREMapping) {
                        COREMapping<?> mapping = (COREMapping<?>) m;
                        if ((otherMapping.getFrom() == mapping.getFrom())
                                && (otherMapping.getTo() == mapping.getTo())) {
                            found = true;
                        }
                    }
                }
                if (!found) {
                    // if there is no such mapping then we need to move it into "into"
                    // EMF will automatically remove it from "from"
                    into.getCompositions().add(otherMapping);
                } else {
                    i++;
                }
            }
        }
    }

//    private void createTrace(Aspect base, Aspect wovenAspect, WeavingInformation currentWeavingInformation,
//            boolean extend) {
//        WovenAspect wovenAspectTracing = RamFactory.eINSTANCE.createWovenAspect();
//        // TODO: Might have to be the textual representation of the aspect name...
//        wovenAspectTracing.setName(wovenAspect.getName());
//        wovenAspectTracing.setComesFrom(wovenAspect);
//        // Create tracing hierarchy
//        createTracingHierarchy(wovenAspect, wovenAspectTracing);
//        int index = extend ? 0 : base.getWovenAspects().size();
//
//        base.getWovenAspects().add(index, wovenAspectTracing);
//        // associate the wovenAspects to the Traceable elements
//        createTrace(currentWeavingInformation, wovenAspectTracing);
//    }

    /**
     * This operation weaves all scenes of the reused concern related to the
     * selected features of the selection configuration to produce a new woven
     * scene.
     * 
     * @param reusingArtefact the artefact that is making the reuse
     * @param reusedConcern   the concern that is being reused
     * @param selection       the selected features that are to be woven
     * @return a new COREExternalArtefact that contains the result of weaving the
     *         selected features
     */
    public COREExternalArtefact weaveReuse(COREArtefact reusingArtefact, COREConcern reusedConcern,
            COREConfiguration selection) {
        // Initialize weaving info if necessary
        extensionDependenciesInfo = new ExtensionDependenciesInfo(selection);

        // Determine the name of the file containing the woven artefact
        String filename = COREWeaverUtil.createWovenConcernFilename(reusedConcern, extensionDependenciesInfo);
        File file = new File(filename);

        COREExternalArtefact wovenArtefact = null;

        if (file.exists()) {
            EObject externalModel = ResourceManager.loadModel(file);
            wovenArtefact = COREArtefactUtil.getReferencingExternalArtefact(externalModel);
        } else {
            wovenArtefact = weaveConcernInternal(reusedConcern, selection);
            try {
                // Save the new woven model in the copied folder of the reused concern
                ResourceManager.saveModel(wovenArtefact.getRootModelElement(), filename);
            } catch (IOException e) {
                // Shouldn't happen.
                e.printStackTrace();
            }
            // Now we need to add the woven artefact to the artefacts of the reused concern
            reusedConcern.getArtefacts().add(wovenArtefact);
            Map<Object, Object> saveOptions = new HashMap<Object, Object>();
            try {
                reusedConcern.eResource().save(saveOptions);
            } catch (IOException e) {
                // Shouldn't happen.
                e.printStackTrace();
            }
        }

//        if (wovenArtefact != null) {
//            COREControllerFactory.INSTANCE.getReuseController()
//                .createNewReuseAndModelReuse(reusingArtefact, reusingArtefact.getCoreConcern(),
//                        reusedConcern, wovenArtefact, selection);
//        }      

        return wovenArtefact;
    }

    /**
     * This operation weaves all scenes of the reused concern related to the
     * selected features of the selection configuration to produce a new woven
     * scene.
     * 
     * @param reusedConcern the concern that is being reused
     * @param selection     the selected features that are to be woven
     * @return a new COREExternalArtefact that contains the result of weaving the
     *         selected features
     */
    public COREExternalArtefact weaveConcern(COREConcern reusedConcern, COREConfiguration selection) {
        // Initialize weaving info if necessary
        extensionDependenciesInfo = new ExtensionDependenciesInfo(selection);
        return weaveConcernInternal(reusedConcern, selection);
    }

    /**
     * This operation weaves all scenes of the reused concern related to the
     * selected features of the selection configuration to produce a new woven
     * scene.
     * 
     * @param reusedConcern the concern that is being reused
     * @param selection     the selected features that are to be woven
     * @return a new COREExternalArtefact that contains the result of weaving the
     *         selected features
     */
    public COREExternalArtefact weaveConcernInternal(COREConcern reusedConcern, COREConfiguration selection) {

        List<COREExternalArtefact> artefacts = extensionDependenciesInfo.getWovenArtefacts();

        // TODO: Needs to be changed once we start using perspectives
        String languageName = null;

        if (artefacts.size() == 0) {
            languageName = "ram";
        } else {
            languageName = artefacts.get(0).getLanguageName();
        }
        COREModelWeaver<EObject> weaver;

        try {
            weaver = getModelWeaver(languageName);
        } catch (WeaverException e) {
            System.out.println("No language registered for " + languageName);
            return null;
        }

        COREExternalArtefact newArtefact = null;
        EObject newModel = null;

        newArtefact = CoreFactory.eINSTANCE.createCOREExternalArtefact();
        newArtefact.setLanguageName(languageName);

        // loops though all the models and constructs the new name as we go
        String newName = "Woven";

        if (artefacts.size() == 0) {
            // Since there is nothing to weave, we'll use the name of the concern
            newName = newName + "_" + reusedConcern.getName();
        } else {
            for (COREExternalArtefact artefact : artefacts) {
                newName = newName + "_" + artefact.getName();
            }
        }

        // Create a new model and link it to the artefact
        newModel = weaver.createEmptyModel();
        newArtefact.setRootModelElement(newModel);
        newArtefact.setName(newName);

        // If there is nothing to weave then we are done
        if (artefacts.size() == 0) {
            return newArtefact;
        }

        // Create model extensions from the new artefact to all artefacts
        // at depth 0 (there can be more than 1), then weave them one by
        // one into the new model. In the end we have one single level 0 artefact.

        Set<COREExternalArtefact> levelZeroArtefacts = extensionDependenciesInfo.getArtefactsOfRank(0);

        weavingInformation.clear();

        for (COREExternalArtefact artefact : levelZeroArtefacts) {
            COREModelExtension modelExtension = CoreFactory.eINSTANCE.createCOREModelExtension();
            modelExtension.setSource(artefact);
            newArtefact.getModelExtensions().add(modelExtension);

            // populate weaving information with the information from the
            // COREModelComposition
            populateWeavingInformation(weavingInformation, modelExtension);

            System.out.println("Weaving " + artefact + " into " + newArtefact);
            newModel = weaver.weaveModel(newArtefact, newModel, modelExtension, weavingInformation, false);

            weaveAndUpdateCompositions(newArtefact, artefact, weavingInformation, false);
            newArtefact.getModelExtensions().remove(modelExtension);
        }

        // add the new artefact to the list of temporary artefacts associated with the
        // current concern
        COREConcern currentConcern = artefacts.get(0).getCoreConcern();
        currentConcern.getTemporaryArtefacts().add(newArtefact);

        int currentWeavingRank = 1;

        weavingInformation.print();

        while (currentWeavingRank <= extensionDependenciesInfo.getMaxDepth()) {
            Set<COREExternalArtefact> artefactsToWeaveForThisLevel = extensionDependenciesInfo
                    .getArtefactsOfRank(currentWeavingRank);
            for (COREExternalArtefact artefact : artefactsToWeaveForThisLevel) {

                // populate weaving information with the information with the inverse of
                // all COREModelExtensions
                COREModelExtension combinedExtension = CoreFactory.eINSTANCE.createCOREModelExtension();
                combinedExtension.setSource(artefact);
                WeavingInformation temporaryWeavingInfo = new WeavingInformation();
                for (COREModelExtension mext : artefact.getModelExtensions()) {

                    // process the mappings
                    for (COREModelElementComposition<?> mec : mext.getCompositions()) {

                        // Currently we can only handle CORELinks... If ever we support patterns in the
                        // future, we need
                        // to figure out how we want to populate the weaving info correctly based on the
                        // pattern
                        @SuppressWarnings("unchecked")
                        CORELink<EObject> link = (CORELink<EObject>) mec;

                        CORELink<EObject> reverseLink = CoreFactory.eINSTANCE.createCOREMapping();
                        reverseLink.setFrom(link.getTo());
                        EObject mappedFrom = weavingInformation.getFirstToElement(link.getFrom());
                        if (mappedFrom == null) {
                            mappedFrom = link.getFrom();
                        }
                        reverseLink.setTo(mappedFrom);
                        combinedExtension.getCompositions().add(reverseLink);
                        temporaryWeavingInfo.add(link.getTo(), mappedFrom);

                        // If the link contains other links (such as for example ClassifierMappings,
                        // which contain
                        // attribute mappings and operation mappings, then we should also take care of
                        // those mappings

                        reversePopulateWeavingInformationOfNestedLinks(temporaryWeavingInfo, link, reverseLink);
                    }

                }
                // create a dummy reverse weaving information

                System.out.println("Weaving " + artefact + " into " + newArtefact);
                // now we weave, but "topdown", which results in names being copied from source
                // to target so that
                // the more "specific" names are kept
                newModel = weaver.weaveModel(newArtefact, newModel, combinedExtension, temporaryWeavingInfo, true);

                weaveAndUpdateCompositions(newArtefact, artefact, temporaryWeavingInfo, true);
                weavingInformation.mergeInformation(temporaryWeavingInfo);
            }
            currentWeavingRank++;
        }
        return newArtefact;
    }

}
