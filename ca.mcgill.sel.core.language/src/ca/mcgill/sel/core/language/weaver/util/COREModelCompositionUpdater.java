package ca.mcgill.sel.core.language.weaver.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.Switch;

import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.util.CoreSwitch;

/**
 * Takes care of updating the references of {@link CORELinks}.
 * This class is a singleton and it's instance can be retrieved via {@link #getInstance()}.
 * 
 * @author Jörg
 */
public class COREModelCompositionUpdater extends COREReferenceUpdater {

    /**
     * Switch for each class that needs to be updated according to the weaving information.
     * 
     * @author Jörg
     */
    private class Switcher extends CoreSwitch<Object> {

        @Override
        public <T> Object caseCORELink(CORELink<T> link) {
            EObject toElement = (EObject) link.getTo();
            if (currentWeavingInformation.wasWoven(toElement)) {
                @SuppressWarnings("unchecked")
                T element = (T) currentWeavingInformation.getFirstToElement(toElement);
                link.setTo(element);
            }
            return Boolean.TRUE;
        }

    }

    /**
     * The singleton instance.
     */
    private static COREModelCompositionUpdater instance;

    @Override
    protected Switch<?> createSwitcher() {
        return new Switcher();
    }

    /**
     * Returns the singleton instance of the {@link COREModelCompositionUpdater}.
     * 
     * @return the singleton instance
     */
    public static COREModelCompositionUpdater getInstance() {
        if (instance == null) {
            instance = new COREModelCompositionUpdater();
        }

        return instance;
    }

}
