package ca.mcgill.sel.core.language.weaver.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.Switch;

import ca.mcgill.sel.core.util.CoreSwitch;

/**
 * Takes care of updating the references of {@link CORELinks}.
 * This class is a singleton and it's instance can be retrieved via {@link #getInstance()}.
 * 
 * @author Jörg
 */
public class COREModelElementReferenceUpdater extends COREReferenceUpdater {

    /**
     * Switch for each class that needs to be updated according to the weaving information.
     * 
     * @author Jörg
     */
    private class Switcher extends CoreSwitch<Object> {
        
        @Override
        public Object defaultCase(EObject object) {
            for (EReference reference : object.eClass().getEAllReferences()) {
                if (!reference.isMany() && !reference.isContainer()) {
                    EObject referenceValue = (EObject) object.eGet(reference);

                    if (referenceValue != null && currentWeavingInformation.wasWoven(referenceValue)) {
                        object.eSet(reference, currentWeavingInformation.getFirstToElement(referenceValue));
                    }
                }
            }
            return Boolean.TRUE;
        }

    }

    /**
     * The singleton instance.
     */
    private static COREModelElementReferenceUpdater instance;

    @Override
    protected Switch<?> createSwitcher() {
        return new Switcher();
    }

    /**
     * Returns the singleton instance of the {@link COREModelElementReferenceUpdater}.
     * 
     * @return the singleton instance
     */
    public static COREModelElementReferenceUpdater getInstance() {
        if (instance == null) {
            instance = new COREModelElementReferenceUpdater();
        }

        return instance;
    }

}
