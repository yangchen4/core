package ca.mcgill.sel.core.language.weaver;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.language.weaver.util.WeavingInformation;

/**
 * This class is the abstract superclass that all COREModelWeavers that implement composition operators
 * for languages used in CORE must extend.
 * 
 * @author gemini
 *
 * @param <T> the type COREArtefact that the weaver can compose
 */
public abstract class COREModelWeaver<T extends EObject> {
       
    /**
     * This operation must implement the homogeneous composition operator for the language. Given a target model and a
     * {$link COREModelComposition} defined inside the target model that maps some model elements from some
     * external source model to model elements inside the target model, this operation must produce a new model
     * in which those model elements were composed. If needed, weavingInfo provides additional information about
     * other model elements that have already been woven by other COREModelWeavers that composed the same facet
     * 
     * @TODO: probably this description needs to be updated once we define perspectives and consistency rules.
     * 
     * @param externalBase the artefact into which we are weaving
     * @param target the model into which the source model referred to by modelComposition must be woven
     * @param modelComposition the COREModelComposition that contains the mappings that are to be used for the
     *        composition
     * @param weavingInfo information about mappings in other COREModels of the same scene
     * @param topdown a boolean that indicates whether we are doing top down or bottom up weaving.
     *        when weaving topdown, names of mapped structural elements have to be copied.
     * @return the woven model (which is a new stand-alone model)
     */
    public abstract T weaveModel(COREExternalArtefact externalBase, T target, COREModelComposition modelComposition,
            WeavingInformation weavingInfo, boolean topdown);
        
    /**
     * This operation simply must return an empty model of type T.
     * 
     * @return a new, empty model of type T
     */
    public abstract T createEmptyModel();

}
