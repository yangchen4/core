package ca.mcgill.sel.core.language.registry;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource.Factory;

import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.core.language.weaver.COREModelWeaver;
import ca.mcgill.sel.core.language.weaver.COREWeaver;

/**
 * CORELanguageRegistry is a helper class to register and 
 * keep track of languages supported by CORE.
 * 
 * @author Hyacinth
 * @author ddevine
 * @author joerg
 */
public final class CORELanguageRegistry {
    
    /**
     * Singleton instance variable.
     */
    private static CORELanguageRegistry instance;
    
    /**
     * List of CORE languages.
     */
    private Map<String, COREExternalLanguage> languages; 
    private Map<CORELanguage, ModelUtil> modelUtils; 
    
    /**
     * Singleton pattern - initialize only a single registry.
     */
    private CORELanguageRegistry() {
        languages = new HashMap<String, COREExternalLanguage>();
        modelUtils = new HashMap<CORELanguage, ModelUtil>();
    }
    
    /**
     * Function to get the language registry instance.
     * 
     * @return Returns the CORELanguageRegistry singleton.
     */
    public static CORELanguageRegistry getRegistry() {
        if (instance == null) {
            instance = new CORELanguageRegistry();
        }
        
        return instance;
    }
    
    /**
     * Loads pre-defined languages and registers them in the {@link CORELanguageRegistry}.
     * Currently Ram.core, DomainModel.core, ClassDiagram.core
     */
    public static void loadLanguages() {
        List<String> languages = ResourceUtil.getResourceListing("models/languages/", ".core");

        if (languages != null) {
            for (String l : languages) {

                // load existing languages
                URI fileURI = URI.createURI(l);
                COREConcern languageConcern = (COREConcern) ResourceManager.loadModel(fileURI);
                // register any languages contained in the loaded concern
                for (COREArtefact a : languageConcern.getArtefacts()) {
                    if (a instanceof COREExternalLanguage) {
                        COREExternalLanguage existingLanguage = (COREExternalLanguage) a;
                        getRegistry().registerLanguage(existingLanguage);
                    }
                }
            }
        }
    }
    
    /**
     * Registers languages supported by CORE.
     * 
     * @param language the language to be registered
     */
    public void registerLanguage(COREExternalLanguage language) {
        try {
         
            // Register resource factories of the language.
            Class<?> factoryClass = Class.forName(language.getResourceFactory());
            Factory factory = (Factory) factoryClass.getDeclaredConstructor().newInstance();
            ResourceManager.registerExtensionFactory(language.getFileExtension(), factory);
            
            // Initialize adapter of the language
            @SuppressWarnings("unchecked")
            Class<? extends AdapterFactory> adapterClass =
                    (Class<? extends AdapterFactory>) Class.forName(language.getAdapterFactory());
            AdapterFactoryRegistry.INSTANCE.addAdapterFactory(adapterClass);
            
            // Register the language weaver
            String weaverName = language.getWeaverClassName();
            if (weaverName != null && !weaverName.isEmpty()) {
                Class<?> weaver = Class.forName(weaverName);
                COREModelWeaver<?> modelWeaver = (COREModelWeaver<?>) weaver.getDeclaredConstructor()
                        .newInstance();
                COREWeaver.getInstance().registerCOREModelWeaver(language.getName(), modelWeaver);
            }
            
            // Find and store the Model Util of the language
            String modelUtilName = language.getModelUtilClassName();
            if (modelUtilName != null && !modelUtilName.isEmpty()) {
                Class<?> modelUtil = Class.forName(modelUtilName);
                modelUtils.put(language, (ModelUtil) modelUtil.getDeclaredConstructor().newInstance());
            }
            
            languages.put(language.getName(), language);
            // TODO: figure out whether we want to use file extensions or full name to refer to a language
            // languages.put(language.getFileExtension(), language);
        //CHECKSTYLE:IGNORE IllegalCatch: A lot of different exceptions can be thrown.
        } catch (Exception e) {
            System.err.format("Language registration for '%s' failed: %s\n", language.getName(), e.toString());
        }
    }

    /**
     * Return the list of registered languages.
     * 
     * @return the language
     */
    public Collection<COREExternalLanguage> getLanguages() {
        return languages.values();
    }

    /**
     * Return the CORELanguage corresponding to the String name or else null.
     * 
     * @param name the name of the language 
     * @return the CORELanguage with the name name
     */
    public COREExternalLanguage getLanguage(String name) {
        return languages.get(name);
    }
    
    /**
     * Return the ModelUtil corresponding to the language or else null.
     * 
     * @param l the language 
     * @return the ModelUtil that was registered
     */
    public ModelUtil getModelUtil(CORELanguage l) {
        return modelUtils.get(l);
    }
    
}
