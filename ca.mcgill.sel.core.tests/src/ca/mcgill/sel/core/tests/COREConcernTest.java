/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Concern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getFeatureModel() <em>Feature Model</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getImpactModel() <em>Impact Model</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class COREConcernTest extends CORENamedElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREConcernTest.class);
	}

	/**
	 * Constructs a new CORE Concern test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREConcernTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Concern test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREConcern getFixture() {
		return (COREConcern)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREConcern());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link ca.mcgill.sel.core.COREConcern#getFeatureModel() <em>Feature Model</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.core.COREConcern#getFeatureModel()
	 * @generated
	 */
	public void testGetFeatureModel() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.mcgill.sel.core.COREConcern#setFeatureModel(ca.mcgill.sel.core.COREFeatureModel) <em>Feature Model</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.core.COREConcern#setFeatureModel(ca.mcgill.sel.core.COREFeatureModel)
	 * @generated
	 */
	public void testSetFeatureModel() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.mcgill.sel.core.COREConcern#getImpactModel() <em>Impact Model</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.core.COREConcern#getImpactModel()
	 * @generated
	 */
	public void testGetImpactModel() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.mcgill.sel.core.COREConcern#setImpactModel(ca.mcgill.sel.core.COREImpactModel) <em>Impact Model</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.core.COREConcern#setImpactModel(ca.mcgill.sel.core.COREImpactModel)
	 * @generated
	 */
	public void testSetImpactModel() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //COREConcernTest
