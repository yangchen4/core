/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Weighted Link</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class COREWeightedLinkTest extends CORELinkTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREWeightedLinkTest.class);
	}

	/**
	 * Constructs a new CORE Weighted Link test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREWeightedLinkTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Weighted Link test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREWeightedLink getFixture() {
		return (COREWeightedLink)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREWeightedLink());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //COREWeightedLinkTest
