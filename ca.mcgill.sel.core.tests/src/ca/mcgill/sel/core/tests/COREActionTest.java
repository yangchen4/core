/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREAction;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class COREActionTest extends TestCase {

	/**
	 * The fixture for this CORE Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREAction fixture = null;

	/**
	 * Constructs a new CORE Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREActionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(COREAction fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREAction getFixture() {
		return fixture;
	}

} //COREActionTest
