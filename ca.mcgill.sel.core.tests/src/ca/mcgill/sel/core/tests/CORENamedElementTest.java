/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CORENamedElement;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Named Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CORENamedElementTest extends TestCase {

	/**
	 * The fixture for this CORE Named Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CORENamedElement fixture = null;

	/**
	 * Constructs a new CORE Named Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CORENamedElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Named Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CORENamedElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Named Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CORENamedElement getFixture() {
		return fixture;
	}

} //CORENamedElementTest
