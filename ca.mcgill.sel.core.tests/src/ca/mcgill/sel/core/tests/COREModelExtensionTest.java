/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Model Extension</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class COREModelExtensionTest extends COREModelCompositionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREModelExtensionTest.class);
	}

	/**
	 * Constructs a new CORE Model Extension test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREModelExtensionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Model Extension test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREModelExtension getFixture() {
		return (COREModelExtension)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREModelExtension());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //COREModelExtensionTest
