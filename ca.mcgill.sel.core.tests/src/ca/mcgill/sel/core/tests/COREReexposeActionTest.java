/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Reexpose Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class COREReexposeActionTest extends COREPerspectiveActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREReexposeActionTest.class);
	}

	/**
	 * Constructs a new CORE Reexpose Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREReexposeActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Reexpose Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREReexposeAction getFixture() {
		return (COREReexposeAction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREReexposeAction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //COREReexposeActionTest
