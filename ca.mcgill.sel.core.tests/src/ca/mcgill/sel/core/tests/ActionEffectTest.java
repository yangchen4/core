/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.ActionEffect;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Action Effect</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ActionEffectTest extends TestCase {

	/**
	 * The fixture for this Action Effect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionEffect fixture = null;

	/**
	 * Constructs a new Action Effect test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionEffectTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Action Effect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ActionEffect fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Action Effect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionEffect getFixture() {
		return fixture;
	}

} //ActionEffectTest
