/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREModelComposition;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Model Composition</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class COREModelCompositionTest extends TestCase {

	/**
	 * The fixture for this CORE Model Composition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREModelComposition fixture = null;

	/**
	 * Constructs a new CORE Model Composition test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREModelCompositionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Model Composition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(COREModelComposition fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Model Composition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREModelComposition getFixture() {
		return fixture;
	}

} //COREModelCompositionTest
