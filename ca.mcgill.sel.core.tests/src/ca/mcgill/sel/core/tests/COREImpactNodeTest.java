/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Impact Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class COREImpactNodeTest extends CORENamedElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREImpactNodeTest.class);
	}

	/**
	 * Constructs a new CORE Impact Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREImpactNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Impact Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREImpactNode getFixture() {
		return (COREImpactNode)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREImpactNode());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //COREImpactNodeTest
