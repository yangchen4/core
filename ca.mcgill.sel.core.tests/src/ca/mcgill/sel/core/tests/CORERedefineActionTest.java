/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CORERedefineAction;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Redefine Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CORERedefineActionTest extends COREPerspectiveActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CORERedefineActionTest.class);
	}

	/**
	 * Constructs a new CORE Redefine Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CORERedefineActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Redefine Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CORERedefineAction getFixture() {
		return (CORERedefineAction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCORERedefineAction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CORERedefineActionTest
