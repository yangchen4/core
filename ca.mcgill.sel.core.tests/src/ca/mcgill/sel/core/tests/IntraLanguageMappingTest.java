/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.IntraLanguageMapping;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Intra Language Mapping</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class IntraLanguageMappingTest extends NavigationMappingTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(IntraLanguageMappingTest.class);
	}

	/**
	 * Constructs a new Intra Language Mapping test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntraLanguageMappingTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Intra Language Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected IntraLanguageMapping getFixture() {
		return (IntraLanguageMapping)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createIntraLanguageMapping());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //IntraLanguageMappingTest
