/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.LayoutElement;

import java.util.Map;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Layout Container Map</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LayoutContainerMapTest extends TestCase {

	/**
	 * The fixture for this Layout Container Map test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<EObject, EMap<EObject, LayoutElement>> fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LayoutContainerMapTest.class);
	}

	/**
	 * Constructs a new Layout Container Map test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LayoutContainerMapTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Layout Container Map test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Map.Entry<EObject, EMap<EObject, LayoutElement>> fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Layout Container Map test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<EObject, EMap<EObject, LayoutElement>> getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void setUp() throws Exception {
		setFixture((Map.Entry<EObject, EMap<EObject, LayoutElement>>)CoreFactory.eINSTANCE.create(CorePackage.Literals.LAYOUT_CONTAINER_MAP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LayoutContainerMapTest
