/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREArtefact;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Artefact</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class COREArtefactTest extends CORENamedElementTest {

	/**
	 * Constructs a new CORE Artefact test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREArtefactTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Artefact test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREArtefact getFixture() {
		return (COREArtefact)fixture;
	}

} //COREArtefactTest
