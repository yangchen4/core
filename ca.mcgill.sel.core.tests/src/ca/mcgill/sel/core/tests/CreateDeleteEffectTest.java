/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CreateDeleteEffect;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Create Delete Effect</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CreateDeleteEffectTest extends ActionEffectTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CreateDeleteEffectTest.class);
	}

	/**
	 * Constructs a new Create Delete Effect test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateDeleteEffectTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Create Delete Effect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CreateDeleteEffect getFixture() {
		return (CreateDeleteEffect)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCreateDeleteEffect());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CreateDeleteEffectTest
