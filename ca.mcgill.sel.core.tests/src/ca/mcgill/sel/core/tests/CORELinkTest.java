/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CORELink;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Link</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CORELinkTest extends COREModelElementCompositionTest {

	/**
	 * Constructs a new CORE Link test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CORELinkTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Link test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CORELink<?> getFixture() {
		return (CORELink<?>)fixture;
	}

} //CORELinkTest
