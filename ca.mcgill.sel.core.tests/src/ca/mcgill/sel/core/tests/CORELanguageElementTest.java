/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CoreFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Language Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CORELanguageElementTest extends TestCase {

	/**
	 * The fixture for this CORE Language Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CORELanguageElement fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CORELanguageElementTest.class);
	}

	/**
	 * Constructs a new CORE Language Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CORELanguageElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Language Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CORELanguageElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Language Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CORELanguageElement getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCORELanguageElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CORELanguageElementTest
