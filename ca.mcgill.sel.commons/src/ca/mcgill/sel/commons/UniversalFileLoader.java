package ca.mcgill.sel.commons;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Some stubborn libraries in this project mandate us to load resources form
 * disk as java files (despite the fact that java has an established and
 * reliable way to access resources from classpath). This class offers a
 * workaround, by cloning classpath resources to the file system and returning a
 * java file reference on demand. Purpose is to gain the reliability of standard
 * java resource handling, no matter how the software was built or started,
 * while satisfying poorly designed library interfaces.
 * 
 * HowtoUse: Place resource file wherever you want, but make sure the module
 * lists the containing folder for resource inclusion. Should be a line in the
 * MANIFEST.MF: "source.. = src/,resources/"
 * 
 * @author Maximilian Schiedermeier
 *
 */
public class UniversalFileLoader {

    // Target location is a subfolder in tempdir, named "core-resource-buffer"
    private static final String BUFFER_NAME = "core-resource-buffer";
    private static final String BUFFER_DIR = System.getProperty("java.io.tmpdir") + System.getProperty("file.separator")
            + BUFFER_NAME;

    // private reference for singleton pattern.
    private static UniversalFileLoader singletonReference = null;

    public static UniversalFileLoader getInstance() {
        if (singletonReference == null) {
            singletonReference = new UniversalFileLoader();
        }
        return singletonReference;
    }

    /**
     * Private constructor for singleton access. Ensures buffer exists and is empty.
     */
    private UniversalFileLoader() {
        
        // Wipe all traces of previous caches and ensure buffer exists.
        File bufferDirFile = new File(BUFFER_DIR);
        if(bufferDirFile.exists())
        {
            deleteDir(bufferDirFile);
        }
        bufferDirFile.mkdirs();
    }

    /**
     * Uses standard java mechanisms to clone a resource on the project classpath to
     * the OS temp directory. Then returns a Java file with a reliable reference to
     * the cloned resource.
     * 
     * @param name of a resource indexed by the project classpath.
     * @return File location of the cloned resource in the temp directory.
     */
    public File getFileForResource(String resourceName) {
        File targetFile = buildTargetFile(resourceName);

        // Skip file clone if file already exists, use state of the art java resource
        // handling to clone any resource on classpath to the OS temp dir otherwise
        if (!targetFile.exists()) {
            cloneResource(resourceName, targetFile);
        }

        // At this point the file either already existed or has been cloned, so the file
        // location is valid and can be safely used by *bleep* libraries.
        return targetFile;
    }

    /**
     * Helper method to build the corresponding java file (location only, no
     * copying) for a given resource. Also runs a sanity check on the provided
     * resource string.
     * 
     * @param resourceName
     * @return File location pointing to a location in the temp / buffer dir. The
     *         referenced file may not yet exists on disk.
     */
    private File buildTargetFile(String resourceName) {
        if (resourceName.contains("/")) {
            throw new RuntimeException(
                    "Target resource contains a path separator. Cowardly refusing treatment of the provided resource: "
                            + resourceName);
        }
        return new File(BUFFER_DIR + System.getProperty("file.separator") + resourceName);
    }

    /**
     * Helper method to create an physical file on disk, filled with identical
     * content as a given resource on the classpath. Uses only reliable java
     * resource handling for this task, so it will work no matter how this program
     * was built or started.
     * 
     * @param resourceName as the name of the resource whose content we want to
     *                     clone.
     * @param targetFile   as the java file location of the target file in the temp
     *                     buffer directory
     * @throws IOException
     */
    private void cloneResource(String resourceName, File outputFile) {

        // See: https://mkyong.com/java/java-read-a-file-from-resources-folder/
        // Load content of resource (on classpath) using input stream reader
        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream resourceContentStream = classLoader.getResourceAsStream(resourceName);

        // Verify the specified resource actually exists
        if (resourceContentStream == null) {
            throw new IllegalArgumentException("Resource not found! " + resourceName);
        }

        try {
            // Write the resource content back to an actual file in the buffer directory
            // See: https://www.baeldung.com/convert-input-stream-to-a-file
//            outputFile.createNewFile();
            java.nio.file.Files.copy(resourceContentStream, outputFile.toPath());

            // Tidy up streams\
            resourceContentStream.close();
        } catch (IOException e) {
            throw new RuntimeException("Cloning of resource to file buffer failed: " + e);
        }

    }
    
    /**
     * Helper method to recursively remove a directory with all contents.
     * Source: Stackoverflow: https://stackoverflow.com/a/29175213 
     */
    public static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                if (! Files.isSymbolicLink(f.toPath())) {
                    deleteDir(f);
                }
            }
        }
        file.delete();
    }
}
