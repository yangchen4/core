package ca.mcgill.sel.commons.emf.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.ExternalCrossReferencer;

/**
 * Helper class with convenient static methods for working with EMF objects.
 *
 * @author mschoettle
 */
public final class EMFModelUtil {
    /**
     * Creates a new EMF Model Util instance.
     */
    private EMFModelUtil() {
        // Suppress and hide default constructor.
    }

    /**
     * Returns the next container object in the hierarchy of the given object that {@link EClassifier#isInstance is an
     * instance} of the type.
     *
     * @param eObject the child object to check
     * @param type the type of container to find
     * @param <T> the expected return type
     *
     * @return the first container object of the given type, null if none found
     */
    public static <T extends EObject> T getRootContainerOfType(EObject eObject, EClassifier type) {
        if (eObject != null) {
            EObject currentObject = eObject;

            while (currentObject.eContainer() != null) {
                currentObject = currentObject.eContainer();

                if (type.isInstance(currentObject)) {
                    @SuppressWarnings("unchecked")
                    T typed = (T) currentObject;
                    return typed;
                }
            }
        }

        return null;
    }

    /**
     * Retrieves the entry for the given container from the given {@link EMap}.
     *
     * @param map the map
     * @param container the container the entry is searched for
     * @param <T> the expected return type
     *
     * @return the {@link Entry} of the given container
     */
    public static <T extends EObject> T getEntryFromMap(EMap<?, ?> map, EObject container) {
        for (Entry<?, ?> entry : map.entrySet()) {
            if (entry.getKey() == container) {
                if (entry instanceof EObject) {
                    @SuppressWarnings("unchecked")
                    T mapType = (T) entry;
                    return mapType;
                }
            }
        }

        return null;
    }

    /**
     * Collect all the elements of a given type for the given feature of the given element.
     *
     * @param container - The containing object.
     * @param feature - The feature to get the objects from.
     * @param type - The wished type.
     * @param <T> - Generic type of returned list of elements
     * @return The set of models of the concern.
     */
    public static <T> Collection<T> collectElementsOfType(EObject container, EStructuralFeature feature,
            EClassifier type) {
        if (container == null || feature == null || type == null) {
            return new HashSet<T>();
        }
        Object elements = container.eGet(feature);
        if (elements instanceof Collection<?>) {
            return EcoreUtil.getObjectsByType((Collection<?>) elements, type);
        }
        return new HashSet<T>();
    }

    /**
     * Returns whether the given object of interest is referenced somewhere as a value of the given feature.
     * Checks the complete hierarchy that the object is contained in.
     *
     * @param objectOfInterest the object of interest
     * @param feature the feature the object of interest is referenced in
     * @return true, if at least one reference to object of interest exists, false it is not referenced
     */
    public static boolean referencedInFeature(EObject objectOfInterest, EStructuralFeature feature) {
        EObject root = EcoreUtil.getRootContainer(objectOfInterest);

        Collection<Setting> crossReferences = EcoreUtil.UsageCrossReferencer.find(objectOfInterest, root);

        for (Setting crossReference : crossReferences) {
            if (crossReference.getEStructuralFeature() == feature) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Returns a list of objects that contain at least one structural feature that references
     * the object of interest.
     * The cross references are searched within all objects contained in the root of the object of interest.
     * 
     * @param objectOfInterest the object of interest
     * @return a list of other objects that reference the object of interest, empty if there are none
     */
    public static List<EObject> findObjectsThatCrossReference(EObject objectOfInterest) {
        Collection<Setting> crossReferences = EcoreUtil.UsageCrossReferencer.find(objectOfInterest,
            EcoreUtil.getRootContainer(objectOfInterest));
        List<EObject> result = new ArrayList<EObject>();
    
        for (Setting crossReference : crossReferences) {
            EObject referencingObject = crossReference.getEObject();
            if (!result.contains(referencingObject)) {
                result.add(referencingObject);
            }
        }
        return result;
    }

    /**
     * Returns a list of objects that reference the object of interest using the given feature.
     * The cross references are searched within all objects contained in the given root.
     * 
     * @param root the root and its containment hierarchy to search cross references in
     * @param objectOfInterest the object of interest
     * @param feature the feature the object of interest is referenced in
     * @return a list of other objects that reference the object of interest, empty if there are none
     * @see #findCrossReferencesOfType(EObject, EStructuralFeature, EClassifier)
     */
    public static List<EObject> findCrossReferences(EObject root, EObject objectOfInterest,
            EStructuralFeature feature) {
        Collection<Setting> crossReferences = EcoreUtil.UsageCrossReferencer.find(objectOfInterest, root);
        List<EObject> result = new ArrayList<EObject>();
    
        for (Setting crossReference : crossReferences) {
            if (crossReference.getEStructuralFeature() == feature) {
                result.add(crossReference.getEObject());
            }
        }
        
        return result;
    }

    /**
     * Returns a list of objects that reference the object of interest using the given feature.
     * The cross references are searched within all objects the given object is contained in.
     * 
     * @param objectOfInterest the object of interest
     * @param feature the feature the object of interest is referenced in
     * @return a list of other objects that reference the object of interest, empty if there are none
     * @see #findCrossReferencesOfType(EObject, EStructuralFeature, EClassifier)
     */
    public static List<EObject> findCrossReferences(EObject objectOfInterest, EStructuralFeature feature) {
        EObject root = EcoreUtil.getRootContainer(objectOfInterest);
        
        return findCrossReferences(root, objectOfInterest, feature);
    }
    
    /**
     * Returns a collection of objects that reference the given object of interest as the value of the given feature.
     * The collection is filtered to contain only objects of the given type.
     * Note that the feature must be one that is defined for the type.
     * The cross references are searched within all objects contained in the given root.
     * 
     * @param root the root and its containment hierarchy to search cross references in
     * @param objectOfInterest the object of interest
     * @param feature the feature the object of interest is referenced in
     * @param type the type of the referencing objects
     * @param <T> the type of the objects to be contained in the resulting collection
     * @return a collection of objects of the given type referencing the object of interest in the feature
     * @see #findCrossReferences(EObject, EStructuralFeature)
     */
    public static <T extends EObject> Collection<T> findCrossReferencesOfType(EObject root,
            EObject objectOfInterest, EStructuralFeature feature, EClassifier type) {
        Collection<EObject> referencingObjects = findCrossReferences(root, objectOfInterest, feature);
        return EcoreUtil.getObjectsByType(referencingObjects, type);
    }
    
    /**
     * Returns a collection of objects that reference the given object of interest as the value of the given feature.
     * The collection is filtered to contain only objects of the given type.
     * Note that the feature must be one that is defined for the type.
     * The cross references are searched within all objects the given object is contained in.
     * 
     * @param objectOfInterest the object of interest
     * @param feature the feature the object of interest is referenced in
     * @param type the type of the referencing objects
     * @param <T> the type of the objects to be contained in the resulting collection
     * @return a collection of objects of the given type referencing the object of interest in the feature
     * @see #findCrossReferences(EObject, EStructuralFeature)
     */
    public static <T extends EObject> Collection<T> findCrossReferencesOfType(EObject objectOfInterest, 
            EStructuralFeature feature, EClassifier type) {
        EObject root = EcoreUtil.getRootContainer(objectOfInterest);
        
        return findCrossReferencesOfType(root, objectOfInterest, feature, type);
    }

    /**
     * Finds cross references where the object being referenced is not contained in a resource.
     * Checks all cross references within the given resource.
     * 
     * @param resource the {@link Resource} to check
     * @return a map of cross references where the referenced object is not contained
     */
    public static Map<EObject, Collection<Setting>> findUncontainedCrossReferences(Resource resource) {
        Map<EObject, Collection<Setting>> externalCrossReferences = ExternalCrossReferencer.find(resource);
        Map<EObject, Collection<Setting>> result = new HashMap<>();
        
        for (Entry<EObject, Collection<Setting>> entry : externalCrossReferences.entrySet()) {
            EObject eObject = entry.getKey();
            
            if (eObject.eResource() == null) {
                result.put(eObject, entry.getValue());
            }
        }
        
        return result;
    }
    
    /**
     * Determines the value for the name attribute of the given object, if it has one.
     * The name attribute needs to be named "name".
     * If the given object does not have a name attribute, <code>null</code> is returned.
     * 
     * @param object the {@link EObject} of interest
     * @return the value of the name attribute, <code>null</code> if it doesn't have such an attribute
     */
    public static String getNameAttribute(EObject object) {
        for (EAttribute attribute : object.eClass().getEAllAttributes()) {
            if ("name".equals(attribute.getName())) {
                return (String) object.eGet(attribute);
            }
        }
        
        return null;
    }
    
    /**
     * Gets the EID of an EObject in a model.
     * This is determined as the last fragment of the URI of the object in the model.
     * @param object the {@link EObject} of interest
     * @return The EID, as found in the XML
     */
    public static String getObjectEId(EObject object) {
        return EcoreUtil.getURI(object).fragment();        
    }

}
