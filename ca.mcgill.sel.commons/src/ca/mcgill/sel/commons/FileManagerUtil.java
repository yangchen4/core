package ca.mcgill.sel.commons;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.commons.io.FileUtils;
import org.eclipse.emf.common.util.URI;

/**
 * FileCopyUtil class used to copy or delete files/directories.
 *
 * @author Nishanth
 * @author cbensoussan
 */
public final class FileManagerUtil {

    /**
     * The extension of a jar file.
     */
    private static final String JAR_FILE_EXTENSION = ".jar";

    /**
     * Prevent to instantiate.
     */
    private FileManagerUtil() {
    }

    /**
     * Helper function used to copy the directory.
     * Recursively calls it on all sub files and directories.
     *
     * @param source - The source directory.
     * @param target - THe target directory.
     * @throws IOException - Thrown if not able to copy the directory
     */
    public static void copyDirectory(String source, String target) throws IOException {
        if (source.contains(JAR_FILE_EXTENSION)) {
            copyDirectoryFromJar(source, target);
        } else {
            FileUtils.copyDirectory(new File(source), new File(target));
        }
    }

    /**
     * Copies source directory located inside a jar to a target directory.
     *
     * @param source The path to the source directory inside a jar
     * @param target The target directory
     * @throws IOException - Thrown if not able to copy the directory
     */
    private static void copyDirectoryFromJar(final String source, final String target) throws IOException {
        int index = source.indexOf(File.separator, source.indexOf(JAR_FILE_EXTENSION));
        
        String file = source.substring(0, index - 1);
        final String directory = source.substring(index);
        
        /**
         * Convert encoded URI, since FileSystem creation in visitJAR throws FileSystemNotFoundException otherwise.
         * See issue #458.
         * 
         * Using URI.createURI Creates problems on Windows due to a different separator, 
         * which makes it think the file doesn't exist.
         * URI.createFileURI however cannot handle the file scheme at the beginning.
         * Therefore, the path needs to be "normalized" to use the standard separator /.
         */
        file = file.replace('\\', '/');
        URI fileURI = URI.createURI(file);
        File jarFile = new File(fileURI.toFileString());
        
        visitJar(jarFile.toPath(), directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                String fileRelativePath = file.toString().substring(directory.length());
                String filePath = target + fileRelativePath;
                File destination = new File(filePath);
                destination.getParentFile().mkdirs();
                FileUtils.copyURLToFile(file.toUri().toURL(), destination);
                return FileVisitResult.CONTINUE;
            }
            
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Visits the given jar.
     *
     * @param jarPath The path to the jar file.
     * @param directory The directory inside the jar to look into.
     * @param visitor The visitor
     * @throws IOException - Thrown if anything during the operation fails
     */
    private static void visitJar(Path jarPath, String directory, FileVisitor<Path> visitor) throws IOException {
        try (FileSystem fs = FileSystems.newFileSystem(jarPath, (ClassLoader) null)) {
            Files.walkFileTree(fs.getPath(directory), visitor);
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Helper function used to delete a file. Directories are only deleted if empty.
     *
     * @param f - The file to delete.
     * @throws IOException - If anything during the operation fails.
     */
    public static void deleteFile(File f) throws IOException {
        deleteFile(f, false);
    }

    /**
     * Helper function used to delete a file.
     * If a directory and we want it, recursively delete all sub files and directories.
     *
     * @param f - The file to delete.
     * @param recursive - Whether to delete recursively files in folders. If not, only empty folder will be deleted.
     * @throws IOException - If anything during the operation fails.
     */
    public static void deleteFile(File f, boolean recursive) throws IOException {
        if (recursive && f.isDirectory()) {
            for (File c : f.listFiles()) {
                deleteFile(c, recursive);
            }
        }
        Files.deleteIfExists(f.toPath());
    }
}
