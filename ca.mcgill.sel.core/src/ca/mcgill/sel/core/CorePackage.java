/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.core.CoreFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface CorePackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "core";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://cs.mcgill.ca/sel/core/2.0";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "core";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	CorePackage eINSTANCE = ca.mcgill.sel.core.impl.CorePackageImpl.init();

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORENamedElementImpl <em>CORE Named Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORENamedElementImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORENamedElement()
     * @generated
     */
	int CORE_NAMED_ELEMENT = 6;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_NAMED_ELEMENT__NAME = 0;

	/**
     * The number of structural features of the '<em>CORE Named Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREArtefactImpl <em>CORE Artefact</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREArtefactImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREArtefact()
     * @generated
     */
	int CORE_ARTEFACT = 0;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__NAME = CORE_NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__MODEL_REUSES = CORE_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__CORE_CONCERN = CORE_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__MODEL_EXTENSIONS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__UI_ELEMENTS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__CI_ELEMENTS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__TEMPORARY_CONCERN = CORE_NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT__SCENE = CORE_NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
     * The number of structural features of the '<em>CORE Artefact</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ARTEFACT_FEATURE_COUNT = CORE_NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREImpactModelImpl <em>CORE Impact Model</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREImpactModelImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREImpactModel()
     * @generated
     */
	int CORE_IMPACT_MODEL = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__NAME = CORE_ARTEFACT__NAME;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__MODEL_REUSES = CORE_ARTEFACT__MODEL_REUSES;

	/**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__CORE_CONCERN = CORE_ARTEFACT__CORE_CONCERN;

	/**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__MODEL_EXTENSIONS = CORE_ARTEFACT__MODEL_EXTENSIONS;

	/**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__UI_ELEMENTS = CORE_ARTEFACT__UI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__CI_ELEMENTS = CORE_ARTEFACT__CI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__TEMPORARY_CONCERN = CORE_ARTEFACT__TEMPORARY_CONCERN;

	/**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__SCENE = CORE_ARTEFACT__SCENE;

	/**
     * The feature id for the '<em><b>Impact Model Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__IMPACT_MODEL_ELEMENTS = CORE_ARTEFACT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Layouts</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__LAYOUTS = CORE_ARTEFACT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Contributions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL__CONTRIBUTIONS = CORE_ARTEFACT_FEATURE_COUNT + 2;

	/**
     * The number of structural features of the '<em>CORE Impact Model</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_MODEL_FEATURE_COUNT = CORE_ARTEFACT_FEATURE_COUNT + 3;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREConcernImpl <em>CORE Concern</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREConcernImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREConcern()
     * @generated
     */
	int CORE_CONCERN = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN__NAME = CORE_NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Artefacts</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN__ARTEFACTS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Feature Model</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN__FEATURE_MODEL = CORE_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Impact Model</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN__IMPACT_MODEL = CORE_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Scenes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN__SCENES = CORE_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN__REUSES = CORE_NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Temporary Artefacts</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN__TEMPORARY_ARTEFACTS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
     * The number of structural features of the '<em>CORE Concern</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONCERN_FEATURE_COUNT = CORE_NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREFeatureImpl <em>CORE Feature</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREFeatureImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeature()
     * @generated
     */
	int CORE_FEATURE = 3;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE__NAME = CORE_NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Realized By</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE__REALIZED_BY = CORE_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Children</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE__CHILDREN = CORE_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Parent</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE__PARENT = CORE_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Parent Relationship</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE__PARENT_RELATIONSHIP = CORE_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Requires</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE__REQUIRES = CORE_NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Excludes</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE__EXCLUDES = CORE_NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
     * The number of structural features of the '<em>CORE Feature</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_FEATURE_COUNT = CORE_NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREModelCompositionImpl <em>CORE Model Composition</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREModelCompositionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelComposition()
     * @generated
     */
	int CORE_MODEL_COMPOSITION = 4;

	/**
     * The feature id for the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_COMPOSITION__SOURCE = 0;

	/**
     * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_COMPOSITION__COMPOSITIONS = 1;

	/**
     * The number of structural features of the '<em>CORE Model Composition</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_COMPOSITION_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREModelElementCompositionImpl <em>CORE Model Element Composition</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREModelElementCompositionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelElementComposition()
     * @generated
     */
	int CORE_MODEL_ELEMENT_COMPOSITION = 17;

	/**
     * The number of structural features of the '<em>CORE Model Element Composition</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_ELEMENT_COMPOSITION_FEATURE_COUNT = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORELinkImpl <em>CORE Link</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORELinkImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELink()
     * @generated
     */
	int CORE_LINK = 5;

	/**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LINK__TO = CORE_MODEL_ELEMENT_COMPOSITION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LINK__FROM = CORE_MODEL_ELEMENT_COMPOSITION_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>CORE Link</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LINK_FEATURE_COUNT = CORE_MODEL_ELEMENT_COMPOSITION_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREReuseImpl <em>CORE Reuse</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREReuseImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREReuse()
     * @generated
     */
	int CORE_REUSE = 7;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REUSE__NAME = CORE_NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Reused Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REUSE__REUSED_CONCERN = CORE_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Extends</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REUSE__EXTENDS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REUSE__MODEL_REUSES = CORE_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The number of structural features of the '<em>CORE Reuse</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REUSE_FEATURE_COUNT = CORE_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREImpactNodeImpl <em>CORE Impact Node</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREImpactNodeImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREImpactNode()
     * @generated
     */
	int CORE_IMPACT_NODE = 8;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_NODE__NAME = CORE_NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Scaling Factor</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_NODE__SCALING_FACTOR = CORE_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Offset</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_NODE__OFFSET = CORE_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Outgoing</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_NODE__OUTGOING = CORE_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Incoming</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_NODE__INCOMING = CORE_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>CORE Impact Node</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_IMPACT_NODE_FEATURE_COUNT = CORE_NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREConfigurationImpl <em>CORE Configuration</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREConfigurationImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREConfiguration()
     * @generated
     */
	int CORE_CONFIGURATION = 9;

	/**
     * The feature id for the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION__SOURCE = CORE_MODEL_COMPOSITION__SOURCE;

	/**
     * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION__COMPOSITIONS = CORE_MODEL_COMPOSITION__COMPOSITIONS;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION__NAME = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Selected</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION__SELECTED = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Reexposed</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION__REEXPOSED = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Extending Configurations</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Extended Reuse</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION__EXTENDED_REUSE = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 4;

	/**
     * The number of structural features of the '<em>CORE Configuration</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONFIGURATION_FEATURE_COUNT = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 5;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREFeatureModelImpl <em>CORE Feature Model</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREFeatureModelImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeatureModel()
     * @generated
     */
	int CORE_FEATURE_MODEL = 10;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__NAME = CORE_ARTEFACT__NAME;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__MODEL_REUSES = CORE_ARTEFACT__MODEL_REUSES;

	/**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__CORE_CONCERN = CORE_ARTEFACT__CORE_CONCERN;

	/**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__MODEL_EXTENSIONS = CORE_ARTEFACT__MODEL_EXTENSIONS;

	/**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__UI_ELEMENTS = CORE_ARTEFACT__UI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__CI_ELEMENTS = CORE_ARTEFACT__CI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__TEMPORARY_CONCERN = CORE_ARTEFACT__TEMPORARY_CONCERN;

	/**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__SCENE = CORE_ARTEFACT__SCENE;

	/**
     * The feature id for the '<em><b>Features</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__FEATURES = CORE_ARTEFACT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Root</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL__ROOT = CORE_ARTEFACT_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>CORE Feature Model</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_MODEL_FEATURE_COUNT = CORE_ARTEFACT_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREModelReuseImpl <em>CORE Model Reuse</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREModelReuseImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelReuse()
     * @generated
     */
	int CORE_MODEL_REUSE = 11;

	/**
     * The feature id for the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_REUSE__SOURCE = CORE_MODEL_COMPOSITION__SOURCE;

	/**
     * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_REUSE__COMPOSITIONS = CORE_MODEL_COMPOSITION__COMPOSITIONS;

	/**
     * The feature id for the '<em><b>Reuse</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_REUSE__REUSE = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Configuration</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_REUSE__CONFIGURATION = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>CORE Model Reuse</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_REUSE_FEATURE_COUNT = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREContributionImpl <em>CORE Contribution</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREContributionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREContribution()
     * @generated
     */
	int CORE_CONTRIBUTION = 12;

	/**
     * The feature id for the '<em><b>Relative Weight</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONTRIBUTION__RELATIVE_WEIGHT = 0;

	/**
     * The feature id for the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONTRIBUTION__SOURCE = 1;

	/**
     * The feature id for the '<em><b>Impacts</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONTRIBUTION__IMPACTS = 2;

	/**
     * The number of structural features of the '<em>CORE Contribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CONTRIBUTION_FEATURE_COUNT = 3;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.LayoutMapImpl <em>Layout Map</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.LayoutMapImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLayoutMap()
     * @generated
     */
	int LAYOUT_MAP = 13;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_MAP__KEY = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_MAP__VALUE = 1;

	/**
     * The number of structural features of the '<em>Layout Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_MAP_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.LayoutElementImpl <em>Layout Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.LayoutElementImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLayoutElement()
     * @generated
     */
	int LAYOUT_ELEMENT = 14;

	/**
     * The feature id for the '<em><b>X</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_ELEMENT__X = 0;

	/**
     * The feature id for the '<em><b>Y</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_ELEMENT__Y = 1;

	/**
     * The number of structural features of the '<em>Layout Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_ELEMENT_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.LayoutContainerMapImpl <em>Layout Container Map</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.LayoutContainerMapImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLayoutContainerMap()
     * @generated
     */
	int LAYOUT_CONTAINER_MAP = 15;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_CONTAINER_MAP__KEY = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_CONTAINER_MAP__VALUE = 1;

	/**
     * The number of structural features of the '<em>Layout Container Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_CONTAINER_MAP_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREFeatureImpactNodeImpl <em>CORE Feature Impact Node</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREFeatureImpactNodeImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeatureImpactNode()
     * @generated
     */
	int CORE_FEATURE_IMPACT_NODE = 16;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__NAME = CORE_IMPACT_NODE__NAME;

	/**
     * The feature id for the '<em><b>Scaling Factor</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__SCALING_FACTOR = CORE_IMPACT_NODE__SCALING_FACTOR;

	/**
     * The feature id for the '<em><b>Offset</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__OFFSET = CORE_IMPACT_NODE__OFFSET;

	/**
     * The feature id for the '<em><b>Outgoing</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__OUTGOING = CORE_IMPACT_NODE__OUTGOING;

	/**
     * The feature id for the '<em><b>Incoming</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__INCOMING = CORE_IMPACT_NODE__INCOMING;

	/**
     * The feature id for the '<em><b>Relative Feature Weight</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__RELATIVE_FEATURE_WEIGHT = CORE_IMPACT_NODE_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Represents</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__REPRESENTS = CORE_IMPACT_NODE_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Weighted Links</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE__WEIGHTED_LINKS = CORE_IMPACT_NODE_FEATURE_COUNT + 2;

	/**
     * The number of structural features of the '<em>CORE Feature Impact Node</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_FEATURE_IMPACT_NODE_FEATURE_COUNT = CORE_IMPACT_NODE_FEATURE_COUNT + 3;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREWeightedLinkImpl <em>CORE Weighted Link</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREWeightedLinkImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREWeightedLink()
     * @generated
     */
	int CORE_WEIGHTED_LINK = 18;

	/**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_WEIGHTED_LINK__TO = CORE_LINK__TO;

	/**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_WEIGHTED_LINK__FROM = CORE_LINK__FROM;

	/**
     * The feature id for the '<em><b>Weight</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_WEIGHTED_LINK__WEIGHT = CORE_LINK_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>CORE Weighted Link</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_WEIGHTED_LINK_FEATURE_COUNT = CORE_LINK_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREModelExtensionImpl <em>CORE Model Extension</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREModelExtensionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelExtension()
     * @generated
     */
	int CORE_MODEL_EXTENSION = 19;

	/**
     * The feature id for the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_EXTENSION__SOURCE = CORE_MODEL_COMPOSITION__SOURCE;

	/**
     * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_EXTENSION__COMPOSITIONS = CORE_MODEL_COMPOSITION__COMPOSITIONS;

	/**
     * The number of structural features of the '<em>CORE Model Extension</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_EXTENSION_FEATURE_COUNT = CORE_MODEL_COMPOSITION_FEATURE_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREMappingImpl <em>CORE Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREMapping()
     * @generated
     */
	int CORE_MAPPING = 20;

	/**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING__TO = CORE_LINK__TO;

	/**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING__FROM = CORE_LINK__FROM;

	/**
     * The feature id for the '<em><b>Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING__MAPPINGS = CORE_LINK_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Referenced Mappings</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING__REFERENCED_MAPPINGS = CORE_LINK_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>CORE Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING_FEATURE_COUNT = CORE_LINK_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORESceneImpl <em>CORE Scene</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORESceneImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREScene()
     * @generated
     */
	int CORE_SCENE = 21;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_SCENE__NAME = CORE_NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Realizes</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_SCENE__REALIZES = CORE_NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Perspective Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_SCENE__PERSPECTIVE_NAME = CORE_NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Element Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_SCENE__ELEMENT_MAPPINGS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Artefacts</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_SCENE__ARTEFACTS = CORE_NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>CORE Scene</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_SCENE_FEATURE_COUNT = CORE_NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORELanguageImpl <em>CORE Language</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORELanguageImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguage()
     * @generated
     */
	int CORE_LANGUAGE = 34;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__NAME = CORE_ARTEFACT__NAME;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__MODEL_REUSES = CORE_ARTEFACT__MODEL_REUSES;

	/**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__CORE_CONCERN = CORE_ARTEFACT__CORE_CONCERN;

	/**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__MODEL_EXTENSIONS = CORE_ARTEFACT__MODEL_EXTENSIONS;

	/**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__UI_ELEMENTS = CORE_ARTEFACT__UI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__CI_ELEMENTS = CORE_ARTEFACT__CI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__TEMPORARY_CONCERN = CORE_ARTEFACT__TEMPORARY_CONCERN;

	/**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__SCENE = CORE_ARTEFACT__SCENE;

	/**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE__ACTIONS = CORE_ARTEFACT_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>CORE Language</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_FEATURE_COUNT = CORE_ARTEFACT_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREPerspectiveImpl <em>CORE Perspective</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREPerspectiveImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREPerspective()
     * @generated
     */
	int CORE_PERSPECTIVE = 22;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__NAME = CORE_LANGUAGE__NAME;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__MODEL_REUSES = CORE_LANGUAGE__MODEL_REUSES;

	/**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__CORE_CONCERN = CORE_LANGUAGE__CORE_CONCERN;

	/**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__MODEL_EXTENSIONS = CORE_LANGUAGE__MODEL_EXTENSIONS;

	/**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__UI_ELEMENTS = CORE_LANGUAGE__UI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__CI_ELEMENTS = CORE_LANGUAGE__CI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__TEMPORARY_CONCERN = CORE_LANGUAGE__TEMPORARY_CONCERN;

	/**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__SCENE = CORE_LANGUAGE__SCENE;

	/**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__ACTIONS = CORE_LANGUAGE__ACTIONS;

	/**
     * The feature id for the '<em><b>Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__MAPPINGS = CORE_LANGUAGE_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Languages</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__LANGUAGES = CORE_LANGUAGE_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__DEFAULT = CORE_LANGUAGE_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Navigation Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE__NAVIGATION_MAPPINGS = CORE_LANGUAGE_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>CORE Perspective</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE_FEATURE_COUNT = CORE_LANGUAGE_FEATURE_COUNT + 4;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl <em>CORE External Language</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREExternalLanguageImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREExternalLanguage()
     * @generated
     */
	int CORE_EXTERNAL_LANGUAGE = 23;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__NAME = CORE_LANGUAGE__NAME;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__MODEL_REUSES = CORE_LANGUAGE__MODEL_REUSES;

	/**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__CORE_CONCERN = CORE_LANGUAGE__CORE_CONCERN;

	/**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__MODEL_EXTENSIONS = CORE_LANGUAGE__MODEL_EXTENSIONS;

	/**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__UI_ELEMENTS = CORE_LANGUAGE__UI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__CI_ELEMENTS = CORE_LANGUAGE__CI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__TEMPORARY_CONCERN = CORE_LANGUAGE__TEMPORARY_CONCERN;

	/**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__SCENE = CORE_LANGUAGE__SCENE;

	/**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__ACTIONS = CORE_LANGUAGE__ACTIONS;

	/**
     * The feature id for the '<em><b>Ns URI</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__NS_URI = CORE_LANGUAGE_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Resource Factory</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY = CORE_LANGUAGE_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Adapter Factory</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY = CORE_LANGUAGE_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Weaver Class Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME = CORE_LANGUAGE_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>File Extension</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION = CORE_LANGUAGE_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Language Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS = CORE_LANGUAGE_FEATURE_COUNT + 5;

	/**
     * The feature id for the '<em><b>Model Util Class Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME = CORE_LANGUAGE_FEATURE_COUNT + 6;

	/**
     * The number of structural features of the '<em>CORE External Language</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_LANGUAGE_FEATURE_COUNT = CORE_LANGUAGE_FEATURE_COUNT + 7;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREActionImpl <em>CORE Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREActionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREAction()
     * @generated
     */
	int CORE_ACTION = 35;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ACTION__NAME = 0;

	/**
     * The number of structural features of the '<em>CORE Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_ACTION_FEATURE_COUNT = 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREPerspectiveActionImpl <em>CORE Perspective Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREPerspectiveActionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREPerspectiveAction()
     * @generated
     */
	int CORE_PERSPECTIVE_ACTION = 24;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE_ACTION__NAME = CORE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>For Role</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE_ACTION__FOR_ROLE = CORE_ACTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Action Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER = CORE_ACTION_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>CORE Perspective Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_PERSPECTIVE_ACTION_FEATURE_COUNT = CORE_ACTION_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREExternalArtefactImpl <em>CORE External Artefact</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREExternalArtefactImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREExternalArtefact()
     * @generated
     */
	int CORE_EXTERNAL_ARTEFACT = 25;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__NAME = CORE_ARTEFACT__NAME;

	/**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__MODEL_REUSES = CORE_ARTEFACT__MODEL_REUSES;

	/**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__CORE_CONCERN = CORE_ARTEFACT__CORE_CONCERN;

	/**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__MODEL_EXTENSIONS = CORE_ARTEFACT__MODEL_EXTENSIONS;

	/**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__UI_ELEMENTS = CORE_ARTEFACT__UI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__CI_ELEMENTS = CORE_ARTEFACT__CI_ELEMENTS;

	/**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__TEMPORARY_CONCERN = CORE_ARTEFACT__TEMPORARY_CONCERN;

	/**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__SCENE = CORE_ARTEFACT__SCENE;

	/**
     * The feature id for the '<em><b>Root Model Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT = CORE_ARTEFACT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Language Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME = CORE_ARTEFACT_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>CORE External Artefact</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_EXTERNAL_ARTEFACT_FEATURE_COUNT = CORE_ARTEFACT_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREUIElementImpl <em>COREUI Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREUIElementImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREUIElement()
     * @generated
     */
	int COREUI_ELEMENT = 26;

	/**
     * The feature id for the '<em><b>Model Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int COREUI_ELEMENT__MODEL_ELEMENT = 0;

	/**
     * The number of structural features of the '<em>COREUI Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int COREUI_ELEMENT_FEATURE_COUNT = 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORECIElementImpl <em>CORECI Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORECIElementImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORECIElement()
     * @generated
     */
	int CORECI_ELEMENT = 27;

	/**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORECI_ELEMENT__PARTIALITY = 0;

	/**
     * The feature id for the '<em><b>Model Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORECI_ELEMENT__MODEL_ELEMENT = 1;

	/**
     * The feature id for the '<em><b>Mapping Cardinality</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORECI_ELEMENT__MAPPING_CARDINALITY = 2;

	/**
     * The feature id for the '<em><b>Reference Cardinality</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORECI_ELEMENT__REFERENCE_CARDINALITY = 3;

	/**
     * The number of structural features of the '<em>CORECI Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORECI_ELEMENT_FEATURE_COUNT = 4;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREMappingCardinalityImpl <em>CORE Mapping Cardinality</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREMappingCardinalityImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREMappingCardinality()
     * @generated
     */
	int CORE_MAPPING_CARDINALITY = 28;

	/**
     * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING_CARDINALITY__LOWER_BOUND = 0;

	/**
     * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING_CARDINALITY__UPPER_BOUND = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING_CARDINALITY__NAME = 2;

	/**
     * The number of structural features of the '<em>CORE Mapping Cardinality</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MAPPING_CARDINALITY_FEATURE_COUNT = 3;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl <em>CORE Language Element Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguageElementMapping()
     * @generated
     */
	int CORE_LANGUAGE_ELEMENT_MAPPING = 29;

	/**
     * The feature id for the '<em><b>Actions</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS = 0;

	/**
     * The feature id for the '<em><b>Mapping Ends</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS = 1;

	/**
     * The feature id for the '<em><b>Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER = 2;

	/**
     * The feature id for the '<em><b>Nested Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS = 3;

	/**
     * The feature id for the '<em><b>Match Maker</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER = 4;

	/**
     * The number of structural features of the '<em>CORE Language Element Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT_MAPPING_FEATURE_COUNT = 5;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREReexposeActionImpl <em>CORE Reexpose Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREReexposeActionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREReexposeAction()
     * @generated
     */
	int CORE_REEXPOSE_ACTION = 30;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REEXPOSE_ACTION__NAME = CORE_PERSPECTIVE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>For Role</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REEXPOSE_ACTION__FOR_ROLE = CORE_PERSPECTIVE_ACTION__FOR_ROLE;

	/**
     * The feature id for the '<em><b>Action Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REEXPOSE_ACTION__ACTION_IDENTIFIER = CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER;

	/**
     * The feature id for the '<em><b>Reexposed Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REEXPOSE_ACTION__REEXPOSED_ACTION = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>CORE Reexpose Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REEXPOSE_ACTION_FEATURE_COUNT = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORERedefineActionImpl <em>CORE Redefine Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORERedefineActionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORERedefineAction()
     * @generated
     */
	int CORE_REDEFINE_ACTION = 31;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REDEFINE_ACTION__NAME = CORE_PERSPECTIVE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>For Role</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REDEFINE_ACTION__FOR_ROLE = CORE_PERSPECTIVE_ACTION__FOR_ROLE;

	/**
     * The feature id for the '<em><b>Action Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REDEFINE_ACTION__ACTION_IDENTIFIER = CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER;

	/**
     * The feature id for the '<em><b>Redefined Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REDEFINE_ACTION__REDEFINED_ACTION = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Reused Actions</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REDEFINE_ACTION__REUSED_ACTIONS = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>CORE Redefine Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_REDEFINE_ACTION_FEATURE_COUNT = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORELanguageActionImpl <em>CORE Language Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORELanguageActionImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguageAction()
     * @generated
     */
	int CORE_LANGUAGE_ACTION = 32;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ACTION__NAME = CORE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>Class Qualified Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME = CORE_ACTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Method Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ACTION__METHOD_NAME = CORE_ACTION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ACTION__PARAMETERS = CORE_ACTION_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Secondary Effects</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS = CORE_ACTION_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Action Type</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ACTION__ACTION_TYPE = CORE_ACTION_FEATURE_COUNT + 4;

	/**
     * The number of structural features of the '<em>CORE Language Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ACTION_FEATURE_COUNT = CORE_ACTION_FEATURE_COUNT + 5;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORECreateMappingImpl <em>CORE Create Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORECreateMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORECreateMapping()
     * @generated
     */
	int CORE_CREATE_MAPPING = 33;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CREATE_MAPPING__NAME = CORE_PERSPECTIVE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>For Role</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CREATE_MAPPING__FOR_ROLE = CORE_PERSPECTIVE_ACTION__FOR_ROLE;

	/**
     * The feature id for the '<em><b>Action Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CREATE_MAPPING__ACTION_IDENTIFIER = CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER;

	/**
     * The feature id for the '<em><b>Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CREATE_MAPPING__TYPE = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Extended Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CREATE_MAPPING__EXTENDED_ACTION = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Model Elements</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CREATE_MAPPING__MODEL_ELEMENTS = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 2;

	/**
     * The number of structural features of the '<em>CORE Create Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_CREATE_MAPPING_FEATURE_COUNT = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 3;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CreateModelElementMappingImpl <em>Create Model Element Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CreateModelElementMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCreateModelElementMapping()
     * @generated
     */
	int CREATE_MODEL_ELEMENT_MAPPING = 36;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CREATE_MODEL_ELEMENT_MAPPING__NAME = CORE_PERSPECTIVE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>For Role</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CREATE_MODEL_ELEMENT_MAPPING__FOR_ROLE = CORE_PERSPECTIVE_ACTION__FOR_ROLE;

	/**
     * The feature id for the '<em><b>Action Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CREATE_MODEL_ELEMENT_MAPPING__ACTION_IDENTIFIER = CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER;

	/**
     * The feature id for the '<em><b>Reused Actions</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Create Model Element Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CREATE_MODEL_ELEMENT_MAPPING_FEATURE_COUNT = CORE_PERSPECTIVE_ACTION_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREModelElementMappingImpl <em>CORE Model Element Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREModelElementMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelElementMapping()
     * @generated
     */
	int CORE_MODEL_ELEMENT_MAPPING = 37;

	/**
     * The feature id for the '<em><b>Model Elements</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS = 0;

	/**
     * The feature id for the '<em><b>LE Mid</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_ELEMENT_MAPPING__LE_MID = 1;

	/**
     * The number of structural features of the '<em>CORE Model Element Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_MODEL_ELEMENT_MAPPING_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.MappingEndImpl <em>Mapping End</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.MappingEndImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getMappingEnd()
     * @generated
     */
	int MAPPING_END = 38;

	/**
     * The feature id for the '<em><b>Cardinality</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MAPPING_END__CARDINALITY = 0;

	/**
     * The feature id for the '<em><b>Role Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MAPPING_END__ROLE_NAME = 1;

	/**
     * The feature id for the '<em><b>Type</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MAPPING_END__TYPE = 2;

	/**
     * The feature id for the '<em><b>Language Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MAPPING_END__LANGUAGE_ELEMENT = 3;

	/**
     * The number of structural features of the '<em>Mapping End</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MAPPING_END_FEATURE_COUNT = 4;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.LanguageMapImpl <em>Language Map</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.LanguageMapImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLanguageMap()
     * @generated
     */
	int LANGUAGE_MAP = 39;

	/**
     * The feature id for the '<em><b>Key</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LANGUAGE_MAP__KEY = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LANGUAGE_MAP__VALUE = 1;

	/**
     * The number of structural features of the '<em>Language Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LANGUAGE_MAP_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.ArtefactMapImpl <em>Artefact Map</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.ArtefactMapImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getArtefactMap()
     * @generated
     */
	int ARTEFACT_MAP = 40;

	/**
     * The feature id for the '<em><b>Value</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ARTEFACT_MAP__VALUE = 0;

	/**
     * The feature id for the '<em><b>Key</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ARTEFACT_MAP__KEY = 1;

	/**
     * The number of structural features of the '<em>Artefact Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ARTEFACT_MAP_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.ParameterImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getParameter()
     * @generated
     */
	int PARAMETER = 41;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER__NAME = 0;

	/**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER__TYPE = 1;

	/**
     * The number of structural features of the '<em>Parameter</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER_FEATURE_COUNT = 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl <em>CORE Language Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CORELanguageElementImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguageElement()
     * @generated
     */
	int CORE_LANGUAGE_ELEMENT = 42;

	/**
     * The feature id for the '<em><b>Language Actions</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS = 0;

	/**
     * The feature id for the '<em><b>Language Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT = 1;

	/**
     * The feature id for the '<em><b>Language</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT__LANGUAGE = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT__NAME = 3;

	/**
     * The feature id for the '<em><b>Nested Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS = 4;

	/**
     * The feature id for the '<em><b>Owner</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT__OWNER = 5;

	/**
     * The number of structural features of the '<em>CORE Language Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CORE_LANGUAGE_ELEMENT_FEATURE_COUNT = 6;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.ActionEffectImpl <em>Action Effect</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.ActionEffectImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getActionEffect()
     * @generated
     */
	int ACTION_EFFECT = 51;

	/**
     * The number of structural features of the '<em>Action Effect</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_EFFECT_FEATURE_COUNT = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.ExistingElementEffectImpl <em>Existing Element Effect</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.ExistingElementEffectImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getExistingElementEffect()
     * @generated
     */
	int EXISTING_ELEMENT_EFFECT = 43;

	/**
     * The feature id for the '<em><b>Parameter</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EXISTING_ELEMENT_EFFECT__PARAMETER = ACTION_EFFECT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Parameter Effect</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT = ACTION_EFFECT_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Existing Element Effect</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EXISTING_ELEMENT_EFFECT_FEATURE_COUNT = ACTION_EFFECT_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.DeleteEffectImpl <em>Delete Effect</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.DeleteEffectImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getDeleteEffect()
     * @generated
     */
	int DELETE_EFFECT = 44;

	/**
     * The feature id for the '<em><b>Parameter</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DELETE_EFFECT__PARAMETER = EXISTING_ELEMENT_EFFECT__PARAMETER;

	/**
     * The feature id for the '<em><b>Parameter Effect</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DELETE_EFFECT__PARAMETER_EFFECT = EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT;

	/**
     * The number of structural features of the '<em>Delete Effect</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DELETE_EFFECT_FEATURE_COUNT = EXISTING_ELEMENT_EFFECT_FEATURE_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.UpdateEffectImpl <em>Update Effect</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.UpdateEffectImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getUpdateEffect()
     * @generated
     */
	int UPDATE_EFFECT = 45;

	/**
     * The feature id for the '<em><b>Parameter</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UPDATE_EFFECT__PARAMETER = EXISTING_ELEMENT_EFFECT__PARAMETER;

	/**
     * The feature id for the '<em><b>Parameter Effect</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UPDATE_EFFECT__PARAMETER_EFFECT = EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT;

	/**
     * The feature id for the '<em><b>Affected Feature</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UPDATE_EFFECT__AFFECTED_FEATURE = EXISTING_ELEMENT_EFFECT_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Update Effect</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UPDATE_EFFECT_FEATURE_COUNT = EXISTING_ELEMENT_EFFECT_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.NavigationMappingImpl <em>Navigation Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.NavigationMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getNavigationMapping()
     * @generated
     */
	int NAVIGATION_MAPPING = 46;

	/**
     * The feature id for the '<em><b>Active</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NAVIGATION_MAPPING__ACTIVE = 0;

	/**
     * The number of structural features of the '<em>Navigation Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NAVIGATION_MAPPING_FEATURE_COUNT = 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.InterLanguageMappingImpl <em>Inter Language Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.InterLanguageMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getInterLanguageMapping()
     * @generated
     */
	int INTER_LANGUAGE_MAPPING = 47;

	/**
     * The feature id for the '<em><b>Active</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING__ACTIVE = NAVIGATION_MAPPING__ACTIVE;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING__DEFAULT = NAVIGATION_MAPPING_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Core Language Element Mapping</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING = NAVIGATION_MAPPING_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Inter Language Mapping Ends</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS = NAVIGATION_MAPPING_FEATURE_COUNT + 2;

	/**
     * The number of structural features of the '<em>Inter Language Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING_FEATURE_COUNT = NAVIGATION_MAPPING_FEATURE_COUNT + 3;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl <em>Intra Language Mapping</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.IntraLanguageMappingImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getIntraLanguageMapping()
     * @generated
     */
	int INTRA_LANGUAGE_MAPPING = 48;

	/**
     * The feature id for the '<em><b>Active</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__ACTIVE = NAVIGATION_MAPPING__ACTIVE;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__NAME = NAVIGATION_MAPPING_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Closure</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__CLOSURE = NAVIGATION_MAPPING_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Reuse</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__REUSE = NAVIGATION_MAPPING_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Increase Depth</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH = NAVIGATION_MAPPING_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Change Model</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__CHANGE_MODEL = NAVIGATION_MAPPING_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__FROM = NAVIGATION_MAPPING_FEATURE_COUNT + 5;

	/**
     * The feature id for the '<em><b>Hops</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING__HOPS = NAVIGATION_MAPPING_FEATURE_COUNT + 6;

	/**
     * The number of structural features of the '<em>Intra Language Mapping</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTRA_LANGUAGE_MAPPING_FEATURE_COUNT = NAVIGATION_MAPPING_FEATURE_COUNT + 7;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl <em>Inter Language Mapping End</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getInterLanguageMappingEnd()
     * @generated
     */
	int INTER_LANGUAGE_MAPPING_END = 49;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING_END__NAME = 0;

	/**
     * The feature id for the '<em><b>Origin</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING_END__ORIGIN = 1;

	/**
     * The feature id for the '<em><b>Destination</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING_END__DESTINATION = 2;

	/**
     * The feature id for the '<em><b>Mapping End</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING_END__MAPPING_END = 3;

	/**
     * The number of structural features of the '<em>Inter Language Mapping End</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int INTER_LANGUAGE_MAPPING_END_FEATURE_COUNT = 4;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.CreateEffectImpl <em>Create Effect</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.CreateEffectImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCreateEffect()
     * @generated
     */
	int CREATE_EFFECT = 50;

	/**
     * The feature id for the '<em><b>Corelanguage Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CREATE_EFFECT__CORELANGUAGE_ELEMENT = ACTION_EFFECT_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Create Effect</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CREATE_EFFECT_FEATURE_COUNT = ACTION_EFFECT_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.impl.COREReuseArtefactImpl <em>CORE Reuse Artefact</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.impl.COREReuseArtefactImpl
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREReuseArtefact()
     * @generated
     */
    int CORE_REUSE_ARTEFACT = 52;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__NAME = CORE_ARTEFACT__NAME;

    /**
     * The feature id for the '<em><b>Model Reuses</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__MODEL_REUSES = CORE_ARTEFACT__MODEL_REUSES;

    /**
     * The feature id for the '<em><b>Core Concern</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__CORE_CONCERN = CORE_ARTEFACT__CORE_CONCERN;

    /**
     * The feature id for the '<em><b>Model Extensions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__MODEL_EXTENSIONS = CORE_ARTEFACT__MODEL_EXTENSIONS;

    /**
     * The feature id for the '<em><b>Ui Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__UI_ELEMENTS = CORE_ARTEFACT__UI_ELEMENTS;

    /**
     * The feature id for the '<em><b>Ci Elements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__CI_ELEMENTS = CORE_ARTEFACT__CI_ELEMENTS;

    /**
     * The feature id for the '<em><b>Temporary Concern</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__TEMPORARY_CONCERN = CORE_ARTEFACT__TEMPORARY_CONCERN;

    /**
     * The feature id for the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT__SCENE = CORE_ARTEFACT__SCENE;

    /**
     * The number of structural features of the '<em>CORE Reuse Artefact</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CORE_REUSE_ARTEFACT_FEATURE_COUNT = CORE_ARTEFACT_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.core.COREFeatureRelationshipType <em>CORE Feature Relationship Type</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.COREFeatureRelationshipType
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeatureRelationshipType()
     * @generated
     */
	int CORE_FEATURE_RELATIONSHIP_TYPE = 53;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.COREVisibilityType <em>CORE Visibility Type</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.COREVisibilityType
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREVisibilityType()
     * @generated
     */
	int CORE_VISIBILITY_TYPE = 54;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.COREPartialityType <em>CORE Partiality Type</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.COREPartialityType
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREPartialityType()
     * @generated
     */
	int CORE_PARTIALITY_TYPE = 55;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.CORERelationship <em>CORE Relationship</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.CORERelationship
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORERelationship()
     * @generated
     */
	int CORE_RELATIONSHIP = 56;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.Cardinality <em>Cardinality</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.Cardinality
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCardinality()
     * @generated
     */
	int CARDINALITY = 57;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.LanguageActionType <em>Language Action Type</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.LanguageActionType
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLanguageActionType()
     * @generated
     */
	int LANGUAGE_ACTION_TYPE = 58;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.core.ParameterEffect <em>Parameter Effect</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.core.ParameterEffect
     * @see ca.mcgill.sel.core.impl.CorePackageImpl#getParameterEffect()
     * @generated
     */
	int PARAMETER_EFFECT = 59;


	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREArtefact <em>CORE Artefact</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Artefact</em>'.
     * @see ca.mcgill.sel.core.COREArtefact
     * @generated
     */
	EClass getCOREArtefact();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREArtefact#getModelReuses <em>Model Reuses</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Model Reuses</em>'.
     * @see ca.mcgill.sel.core.COREArtefact#getModelReuses()
     * @see #getCOREArtefact()
     * @generated
     */
	EReference getCOREArtefact_ModelReuses();

	/**
     * Returns the meta object for the container reference '{@link ca.mcgill.sel.core.COREArtefact#getCoreConcern <em>Core Concern</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Core Concern</em>'.
     * @see ca.mcgill.sel.core.COREArtefact#getCoreConcern()
     * @see #getCOREArtefact()
     * @generated
     */
	EReference getCOREArtefact_CoreConcern();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREArtefact#getModelExtensions <em>Model Extensions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Model Extensions</em>'.
     * @see ca.mcgill.sel.core.COREArtefact#getModelExtensions()
     * @see #getCOREArtefact()
     * @generated
     */
	EReference getCOREArtefact_ModelExtensions();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREArtefact#getUiElements <em>Ui Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Ui Elements</em>'.
     * @see ca.mcgill.sel.core.COREArtefact#getUiElements()
     * @see #getCOREArtefact()
     * @generated
     */
	EReference getCOREArtefact_UiElements();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREArtefact#getCiElements <em>Ci Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Ci Elements</em>'.
     * @see ca.mcgill.sel.core.COREArtefact#getCiElements()
     * @see #getCOREArtefact()
     * @generated
     */
	EReference getCOREArtefact_CiElements();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREArtefact#getTemporaryConcern <em>Temporary Concern</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Temporary Concern</em>'.
     * @see ca.mcgill.sel.core.COREArtefact#getTemporaryConcern()
     * @see #getCOREArtefact()
     * @generated
     */
	EReference getCOREArtefact_TemporaryConcern();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREArtefact#getScene <em>Scene</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Scene</em>'.
     * @see ca.mcgill.sel.core.COREArtefact#getScene()
     * @see #getCOREArtefact()
     * @generated
     */
	EReference getCOREArtefact_Scene();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREImpactModel <em>CORE Impact Model</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Impact Model</em>'.
     * @see ca.mcgill.sel.core.COREImpactModel
     * @generated
     */
	EClass getCOREImpactModel();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREImpactModel#getImpactModelElements <em>Impact Model Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Impact Model Elements</em>'.
     * @see ca.mcgill.sel.core.COREImpactModel#getImpactModelElements()
     * @see #getCOREImpactModel()
     * @generated
     */
	EReference getCOREImpactModel_ImpactModelElements();

	/**
     * Returns the meta object for the map '{@link ca.mcgill.sel.core.COREImpactModel#getLayouts <em>Layouts</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Layouts</em>'.
     * @see ca.mcgill.sel.core.COREImpactModel#getLayouts()
     * @see #getCOREImpactModel()
     * @generated
     */
	EReference getCOREImpactModel_Layouts();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREImpactModel#getContributions <em>Contributions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Contributions</em>'.
     * @see ca.mcgill.sel.core.COREImpactModel#getContributions()
     * @see #getCOREImpactModel()
     * @generated
     */
	EReference getCOREImpactModel_Contributions();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREConcern <em>CORE Concern</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Concern</em>'.
     * @see ca.mcgill.sel.core.COREConcern
     * @generated
     */
	EClass getCOREConcern();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREConcern#getArtefacts <em>Artefacts</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Artefacts</em>'.
     * @see ca.mcgill.sel.core.COREConcern#getArtefacts()
     * @see #getCOREConcern()
     * @generated
     */
	EReference getCOREConcern_Artefacts();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREConcern#getFeatureModel <em>Feature Model</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Feature Model</em>'.
     * @see ca.mcgill.sel.core.COREConcern#getFeatureModel()
     * @see #getCOREConcern()
     * @generated
     */
	EReference getCOREConcern_FeatureModel();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREConcern#getImpactModel <em>Impact Model</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Impact Model</em>'.
     * @see ca.mcgill.sel.core.COREConcern#getImpactModel()
     * @see #getCOREConcern()
     * @generated
     */
	EReference getCOREConcern_ImpactModel();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREConcern#getScenes <em>Scenes</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Scenes</em>'.
     * @see ca.mcgill.sel.core.COREConcern#getScenes()
     * @see #getCOREConcern()
     * @generated
     */
	EReference getCOREConcern_Scenes();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREConcern#getReuses <em>Reuses</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Reuses</em>'.
     * @see ca.mcgill.sel.core.COREConcern#getReuses()
     * @see #getCOREConcern()
     * @generated
     */
	EReference getCOREConcern_Reuses();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREConcern#getTemporaryArtefacts <em>Temporary Artefacts</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Temporary Artefacts</em>'.
     * @see ca.mcgill.sel.core.COREConcern#getTemporaryArtefacts()
     * @see #getCOREConcern()
     * @generated
     */
	EReference getCOREConcern_TemporaryArtefacts();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREFeature <em>CORE Feature</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Feature</em>'.
     * @see ca.mcgill.sel.core.COREFeature
     * @generated
     */
	EClass getCOREFeature();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREFeature#getRealizedBy <em>Realized By</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Realized By</em>'.
     * @see ca.mcgill.sel.core.COREFeature#getRealizedBy()
     * @see #getCOREFeature()
     * @generated
     */
	EReference getCOREFeature_RealizedBy();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREFeature#getChildren <em>Children</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Children</em>'.
     * @see ca.mcgill.sel.core.COREFeature#getChildren()
     * @see #getCOREFeature()
     * @generated
     */
	EReference getCOREFeature_Children();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREFeature#getParent <em>Parent</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Parent</em>'.
     * @see ca.mcgill.sel.core.COREFeature#getParent()
     * @see #getCOREFeature()
     * @generated
     */
	EReference getCOREFeature_Parent();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREFeature#getParentRelationship <em>Parent Relationship</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Parent Relationship</em>'.
     * @see ca.mcgill.sel.core.COREFeature#getParentRelationship()
     * @see #getCOREFeature()
     * @generated
     */
	EAttribute getCOREFeature_ParentRelationship();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREFeature#getRequires <em>Requires</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Requires</em>'.
     * @see ca.mcgill.sel.core.COREFeature#getRequires()
     * @see #getCOREFeature()
     * @generated
     */
	EReference getCOREFeature_Requires();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREFeature#getExcludes <em>Excludes</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Excludes</em>'.
     * @see ca.mcgill.sel.core.COREFeature#getExcludes()
     * @see #getCOREFeature()
     * @generated
     */
	EReference getCOREFeature_Excludes();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREModelComposition <em>CORE Model Composition</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Model Composition</em>'.
     * @see ca.mcgill.sel.core.COREModelComposition
     * @generated
     */
	EClass getCOREModelComposition();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREModelComposition#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Source</em>'.
     * @see ca.mcgill.sel.core.COREModelComposition#getSource()
     * @see #getCOREModelComposition()
     * @generated
     */
	EReference getCOREModelComposition_Source();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREModelComposition#getCompositions <em>Compositions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Compositions</em>'.
     * @see ca.mcgill.sel.core.COREModelComposition#getCompositions()
     * @see #getCOREModelComposition()
     * @generated
     */
	EReference getCOREModelComposition_Compositions();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORELink <em>CORE Link</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Link</em>'.
     * @see ca.mcgill.sel.core.CORELink
     * @generated
     */
	EClass getCORELink();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CORELink#getTo <em>To</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>To</em>'.
     * @see ca.mcgill.sel.core.CORELink#getTo()
     * @see #getCORELink()
     * @generated
     */
	EReference getCORELink_To();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CORELink#getFrom <em>From</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>From</em>'.
     * @see ca.mcgill.sel.core.CORELink#getFrom()
     * @see #getCORELink()
     * @generated
     */
	EReference getCORELink_From();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORENamedElement <em>CORE Named Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Named Element</em>'.
     * @see ca.mcgill.sel.core.CORENamedElement
     * @generated
     */
	EClass getCORENamedElement();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORENamedElement#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.core.CORENamedElement#getName()
     * @see #getCORENamedElement()
     * @generated
     */
	EAttribute getCORENamedElement_Name();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREReuse <em>CORE Reuse</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Reuse</em>'.
     * @see ca.mcgill.sel.core.COREReuse
     * @generated
     */
	EClass getCOREReuse();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREReuse#getReusedConcern <em>Reused Concern</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Reused Concern</em>'.
     * @see ca.mcgill.sel.core.COREReuse#getReusedConcern()
     * @see #getCOREReuse()
     * @generated
     */
	EReference getCOREReuse_ReusedConcern();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREReuse#getExtends <em>Extends</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Extends</em>'.
     * @see ca.mcgill.sel.core.COREReuse#getExtends()
     * @see #getCOREReuse()
     * @generated
     */
	EReference getCOREReuse_Extends();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREReuse#getModelReuses <em>Model Reuses</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Model Reuses</em>'.
     * @see ca.mcgill.sel.core.COREReuse#getModelReuses()
     * @see #getCOREReuse()
     * @generated
     */
	EReference getCOREReuse_ModelReuses();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREImpactNode <em>CORE Impact Node</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Impact Node</em>'.
     * @see ca.mcgill.sel.core.COREImpactNode
     * @generated
     */
	EClass getCOREImpactNode();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREImpactNode#getScalingFactor <em>Scaling Factor</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Scaling Factor</em>'.
     * @see ca.mcgill.sel.core.COREImpactNode#getScalingFactor()
     * @see #getCOREImpactNode()
     * @generated
     */
	EAttribute getCOREImpactNode_ScalingFactor();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREImpactNode#getOffset <em>Offset</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Offset</em>'.
     * @see ca.mcgill.sel.core.COREImpactNode#getOffset()
     * @see #getCOREImpactNode()
     * @generated
     */
	EAttribute getCOREImpactNode_Offset();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREImpactNode#getOutgoing <em>Outgoing</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Outgoing</em>'.
     * @see ca.mcgill.sel.core.COREImpactNode#getOutgoing()
     * @see #getCOREImpactNode()
     * @generated
     */
	EReference getCOREImpactNode_Outgoing();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREImpactNode#getIncoming <em>Incoming</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Incoming</em>'.
     * @see ca.mcgill.sel.core.COREImpactNode#getIncoming()
     * @see #getCOREImpactNode()
     * @generated
     */
	EReference getCOREImpactNode_Incoming();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREConfiguration <em>CORE Configuration</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Configuration</em>'.
     * @see ca.mcgill.sel.core.COREConfiguration
     * @generated
     */
	EClass getCOREConfiguration();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREConfiguration#getSelected <em>Selected</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Selected</em>'.
     * @see ca.mcgill.sel.core.COREConfiguration#getSelected()
     * @see #getCOREConfiguration()
     * @generated
     */
	EReference getCOREConfiguration_Selected();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREConfiguration#getReexposed <em>Reexposed</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Reexposed</em>'.
     * @see ca.mcgill.sel.core.COREConfiguration#getReexposed()
     * @see #getCOREConfiguration()
     * @generated
     */
	EReference getCOREConfiguration_Reexposed();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREConfiguration#getExtendingConfigurations <em>Extending Configurations</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Extending Configurations</em>'.
     * @see ca.mcgill.sel.core.COREConfiguration#getExtendingConfigurations()
     * @see #getCOREConfiguration()
     * @generated
     */
	EReference getCOREConfiguration_ExtendingConfigurations();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREConfiguration#getExtendedReuse <em>Extended Reuse</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Extended Reuse</em>'.
     * @see ca.mcgill.sel.core.COREConfiguration#getExtendedReuse()
     * @see #getCOREConfiguration()
     * @generated
     */
	EReference getCOREConfiguration_ExtendedReuse();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREFeatureModel <em>CORE Feature Model</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Feature Model</em>'.
     * @see ca.mcgill.sel.core.COREFeatureModel
     * @generated
     */
	EClass getCOREFeatureModel();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREFeatureModel#getFeatures <em>Features</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Features</em>'.
     * @see ca.mcgill.sel.core.COREFeatureModel#getFeatures()
     * @see #getCOREFeatureModel()
     * @generated
     */
	EReference getCOREFeatureModel_Features();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREFeatureModel#getRoot <em>Root</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Root</em>'.
     * @see ca.mcgill.sel.core.COREFeatureModel#getRoot()
     * @see #getCOREFeatureModel()
     * @generated
     */
	EReference getCOREFeatureModel_Root();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREModelReuse <em>CORE Model Reuse</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Model Reuse</em>'.
     * @see ca.mcgill.sel.core.COREModelReuse
     * @generated
     */
	EClass getCOREModelReuse();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREModelReuse#getReuse <em>Reuse</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Reuse</em>'.
     * @see ca.mcgill.sel.core.COREModelReuse#getReuse()
     * @see #getCOREModelReuse()
     * @generated
     */
	EReference getCOREModelReuse_Reuse();

	/**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.core.COREModelReuse#getConfiguration <em>Configuration</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Configuration</em>'.
     * @see ca.mcgill.sel.core.COREModelReuse#getConfiguration()
     * @see #getCOREModelReuse()
     * @generated
     */
	EReference getCOREModelReuse_Configuration();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREContribution <em>CORE Contribution</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Contribution</em>'.
     * @see ca.mcgill.sel.core.COREContribution
     * @generated
     */
	EClass getCOREContribution();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREContribution#getRelativeWeight <em>Relative Weight</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Relative Weight</em>'.
     * @see ca.mcgill.sel.core.COREContribution#getRelativeWeight()
     * @see #getCOREContribution()
     * @generated
     */
	EAttribute getCOREContribution_RelativeWeight();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREContribution#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Source</em>'.
     * @see ca.mcgill.sel.core.COREContribution#getSource()
     * @see #getCOREContribution()
     * @generated
     */
	EReference getCOREContribution_Source();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREContribution#getImpacts <em>Impacts</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Impacts</em>'.
     * @see ca.mcgill.sel.core.COREContribution#getImpacts()
     * @see #getCOREContribution()
     * @generated
     */
	EReference getCOREContribution_Impacts();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Layout Map</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueType="ca.mcgill.sel.core.LayoutElement" valueContainment="true" valueRequired="true"
     * @generated
     */
	EClass getLayoutMap();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getLayoutMap()
     * @generated
     */
	EReference getLayoutMap_Key();

	/**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getLayoutMap()
     * @generated
     */
	EReference getLayoutMap_Value();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.LayoutElement <em>Layout Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout Element</em>'.
     * @see ca.mcgill.sel.core.LayoutElement
     * @generated
     */
	EClass getLayoutElement();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.LayoutElement#getX <em>X</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>X</em>'.
     * @see ca.mcgill.sel.core.LayoutElement#getX()
     * @see #getLayoutElement()
     * @generated
     */
	EAttribute getLayoutElement_X();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.LayoutElement#getY <em>Y</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Y</em>'.
     * @see ca.mcgill.sel.core.LayoutElement#getY()
     * @see #getLayoutElement()
     * @generated
     */
	EAttribute getLayoutElement_Y();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Layout Container Map</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout Container Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueMapType="ca.mcgill.sel.core.LayoutMap&lt;org.eclipse.emf.ecore.EObject, ca.mcgill.sel.core.LayoutElement&gt;"
     * @generated
     */
	EClass getLayoutContainerMap();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getLayoutContainerMap()
     * @generated
     */
	EReference getLayoutContainerMap_Key();

	/**
     * Returns the meta object for the map '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getLayoutContainerMap()
     * @generated
     */
	EReference getLayoutContainerMap_Value();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREFeatureImpactNode <em>CORE Feature Impact Node</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Feature Impact Node</em>'.
     * @see ca.mcgill.sel.core.COREFeatureImpactNode
     * @generated
     */
	EClass getCOREFeatureImpactNode();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREFeatureImpactNode#getRelativeFeatureWeight <em>Relative Feature Weight</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Relative Feature Weight</em>'.
     * @see ca.mcgill.sel.core.COREFeatureImpactNode#getRelativeFeatureWeight()
     * @see #getCOREFeatureImpactNode()
     * @generated
     */
	EAttribute getCOREFeatureImpactNode_RelativeFeatureWeight();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREFeatureImpactNode#getRepresents <em>Represents</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Represents</em>'.
     * @see ca.mcgill.sel.core.COREFeatureImpactNode#getRepresents()
     * @see #getCOREFeatureImpactNode()
     * @generated
     */
	EReference getCOREFeatureImpactNode_Represents();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREFeatureImpactNode#getWeightedLinks <em>Weighted Links</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Weighted Links</em>'.
     * @see ca.mcgill.sel.core.COREFeatureImpactNode#getWeightedLinks()
     * @see #getCOREFeatureImpactNode()
     * @generated
     */
	EReference getCOREFeatureImpactNode_WeightedLinks();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREModelElementComposition <em>CORE Model Element Composition</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Model Element Composition</em>'.
     * @see ca.mcgill.sel.core.COREModelElementComposition
     * @generated
     */
	EClass getCOREModelElementComposition();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREWeightedLink <em>CORE Weighted Link</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Weighted Link</em>'.
     * @see ca.mcgill.sel.core.COREWeightedLink
     * @generated
     */
	EClass getCOREWeightedLink();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREWeightedLink#getWeight <em>Weight</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Weight</em>'.
     * @see ca.mcgill.sel.core.COREWeightedLink#getWeight()
     * @see #getCOREWeightedLink()
     * @generated
     */
	EAttribute getCOREWeightedLink_Weight();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREModelExtension <em>CORE Model Extension</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Model Extension</em>'.
     * @see ca.mcgill.sel.core.COREModelExtension
     * @generated
     */
	EClass getCOREModelExtension();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREMapping <em>CORE Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Mapping</em>'.
     * @see ca.mcgill.sel.core.COREMapping
     * @generated
     */
	EClass getCOREMapping();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREMapping#getMappings <em>Mappings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Mappings</em>'.
     * @see ca.mcgill.sel.core.COREMapping#getMappings()
     * @see #getCOREMapping()
     * @generated
     */
	EReference getCOREMapping_Mappings();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREMapping#getReferencedMappings <em>Referenced Mappings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Referenced Mappings</em>'.
     * @see ca.mcgill.sel.core.COREMapping#getReferencedMappings()
     * @see #getCOREMapping()
     * @generated
     */
	EReference getCOREMapping_ReferencedMappings();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREScene <em>CORE Scene</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Scene</em>'.
     * @see ca.mcgill.sel.core.COREScene
     * @generated
     */
	EClass getCOREScene();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREScene#getRealizes <em>Realizes</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Realizes</em>'.
     * @see ca.mcgill.sel.core.COREScene#getRealizes()
     * @see #getCOREScene()
     * @generated
     */
	EReference getCOREScene_Realizes();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREScene#getPerspectiveName <em>Perspective Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Perspective Name</em>'.
     * @see ca.mcgill.sel.core.COREScene#getPerspectiveName()
     * @see #getCOREScene()
     * @generated
     */
	EAttribute getCOREScene_PerspectiveName();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREScene#getElementMappings <em>Element Mappings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Element Mappings</em>'.
     * @see ca.mcgill.sel.core.COREScene#getElementMappings()
     * @see #getCOREScene()
     * @generated
     */
	EReference getCOREScene_ElementMappings();

	/**
     * Returns the meta object for the map '{@link ca.mcgill.sel.core.COREScene#getArtefacts <em>Artefacts</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Artefacts</em>'.
     * @see ca.mcgill.sel.core.COREScene#getArtefacts()
     * @see #getCOREScene()
     * @generated
     */
	EReference getCOREScene_Artefacts();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREPerspective <em>CORE Perspective</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Perspective</em>'.
     * @see ca.mcgill.sel.core.COREPerspective
     * @generated
     */
	EClass getCOREPerspective();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREPerspective#getMappings <em>Mappings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Mappings</em>'.
     * @see ca.mcgill.sel.core.COREPerspective#getMappings()
     * @see #getCOREPerspective()
     * @generated
     */
	EReference getCOREPerspective_Mappings();

	/**
     * Returns the meta object for the map '{@link ca.mcgill.sel.core.COREPerspective#getLanguages <em>Languages</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Languages</em>'.
     * @see ca.mcgill.sel.core.COREPerspective#getLanguages()
     * @see #getCOREPerspective()
     * @generated
     */
	EReference getCOREPerspective_Languages();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREPerspective#getDefault <em>Default</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Default</em>'.
     * @see ca.mcgill.sel.core.COREPerspective#getDefault()
     * @see #getCOREPerspective()
     * @generated
     */
	EAttribute getCOREPerspective_Default();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREPerspective#getNavigationMappings <em>Navigation Mappings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Navigation Mappings</em>'.
     * @see ca.mcgill.sel.core.COREPerspective#getNavigationMappings()
     * @see #getCOREPerspective()
     * @generated
     */
	EReference getCOREPerspective_NavigationMappings();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREExternalLanguage <em>CORE External Language</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE External Language</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage
     * @generated
     */
	EClass getCOREExternalLanguage();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREExternalLanguage#getNsURI <em>Ns URI</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Ns URI</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage#getNsURI()
     * @see #getCOREExternalLanguage()
     * @generated
     */
	EAttribute getCOREExternalLanguage_NsURI();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREExternalLanguage#getResourceFactory <em>Resource Factory</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Resource Factory</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage#getResourceFactory()
     * @see #getCOREExternalLanguage()
     * @generated
     */
	EAttribute getCOREExternalLanguage_ResourceFactory();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREExternalLanguage#getAdapterFactory <em>Adapter Factory</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Adapter Factory</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage#getAdapterFactory()
     * @see #getCOREExternalLanguage()
     * @generated
     */
	EAttribute getCOREExternalLanguage_AdapterFactory();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREExternalLanguage#getWeaverClassName <em>Weaver Class Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Weaver Class Name</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage#getWeaverClassName()
     * @see #getCOREExternalLanguage()
     * @generated
     */
	EAttribute getCOREExternalLanguage_WeaverClassName();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREExternalLanguage#getFileExtension <em>File Extension</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>File Extension</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage#getFileExtension()
     * @see #getCOREExternalLanguage()
     * @generated
     */
	EAttribute getCOREExternalLanguage_FileExtension();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.COREExternalLanguage#getLanguageElements <em>Language Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Language Elements</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage#getLanguageElements()
     * @see #getCOREExternalLanguage()
     * @generated
     */
	EReference getCOREExternalLanguage_LanguageElements();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREExternalLanguage#getModelUtilClassName <em>Model Util Class Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Model Util Class Name</em>'.
     * @see ca.mcgill.sel.core.COREExternalLanguage#getModelUtilClassName()
     * @see #getCOREExternalLanguage()
     * @generated
     */
	EAttribute getCOREExternalLanguage_ModelUtilClassName();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREPerspectiveAction <em>CORE Perspective Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Perspective Action</em>'.
     * @see ca.mcgill.sel.core.COREPerspectiveAction
     * @generated
     */
	EClass getCOREPerspectiveAction();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREPerspectiveAction#getForRole <em>For Role</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>For Role</em>'.
     * @see ca.mcgill.sel.core.COREPerspectiveAction#getForRole()
     * @see #getCOREPerspectiveAction()
     * @generated
     */
	EAttribute getCOREPerspectiveAction_ForRole();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREPerspectiveAction#getActionIdentifier <em>Action Identifier</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Action Identifier</em>'.
     * @see ca.mcgill.sel.core.COREPerspectiveAction#getActionIdentifier()
     * @see #getCOREPerspectiveAction()
     * @generated
     */
	EAttribute getCOREPerspectiveAction_ActionIdentifier();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREExternalArtefact <em>CORE External Artefact</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE External Artefact</em>'.
     * @see ca.mcgill.sel.core.COREExternalArtefact
     * @generated
     */
	EClass getCOREExternalArtefact();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREExternalArtefact#getRootModelElement <em>Root Model Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Root Model Element</em>'.
     * @see ca.mcgill.sel.core.COREExternalArtefact#getRootModelElement()
     * @see #getCOREExternalArtefact()
     * @generated
     */
	EReference getCOREExternalArtefact_RootModelElement();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREExternalArtefact#getLanguageName <em>Language Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Language Name</em>'.
     * @see ca.mcgill.sel.core.COREExternalArtefact#getLanguageName()
     * @see #getCOREExternalArtefact()
     * @generated
     */
	EAttribute getCOREExternalArtefact_LanguageName();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREUIElement <em>COREUI Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>COREUI Element</em>'.
     * @see ca.mcgill.sel.core.COREUIElement
     * @generated
     */
	EClass getCOREUIElement();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREUIElement#getModelElement <em>Model Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Model Element</em>'.
     * @see ca.mcgill.sel.core.COREUIElement#getModelElement()
     * @see #getCOREUIElement()
     * @generated
     */
	EReference getCOREUIElement_ModelElement();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORECIElement <em>CORECI Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORECI Element</em>'.
     * @see ca.mcgill.sel.core.CORECIElement
     * @generated
     */
	EClass getCORECIElement();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORECIElement#getPartiality <em>Partiality</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Partiality</em>'.
     * @see ca.mcgill.sel.core.CORECIElement#getPartiality()
     * @see #getCORECIElement()
     * @generated
     */
	EAttribute getCORECIElement_Partiality();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CORECIElement#getModelElement <em>Model Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Model Element</em>'.
     * @see ca.mcgill.sel.core.CORECIElement#getModelElement()
     * @see #getCORECIElement()
     * @generated
     */
	EReference getCORECIElement_ModelElement();

	/**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.core.CORECIElement#getMappingCardinality <em>Mapping Cardinality</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Mapping Cardinality</em>'.
     * @see ca.mcgill.sel.core.CORECIElement#getMappingCardinality()
     * @see #getCORECIElement()
     * @generated
     */
	EReference getCORECIElement_MappingCardinality();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.CORECIElement#getReferenceCardinality <em>Reference Cardinality</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Reference Cardinality</em>'.
     * @see ca.mcgill.sel.core.CORECIElement#getReferenceCardinality()
     * @see #getCORECIElement()
     * @generated
     */
	EReference getCORECIElement_ReferenceCardinality();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREMappingCardinality <em>CORE Mapping Cardinality</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Mapping Cardinality</em>'.
     * @see ca.mcgill.sel.core.COREMappingCardinality
     * @generated
     */
	EClass getCOREMappingCardinality();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREMappingCardinality#getLowerBound <em>Lower Bound</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Lower Bound</em>'.
     * @see ca.mcgill.sel.core.COREMappingCardinality#getLowerBound()
     * @see #getCOREMappingCardinality()
     * @generated
     */
	EAttribute getCOREMappingCardinality_LowerBound();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREMappingCardinality#getUpperBound <em>Upper Bound</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Upper Bound</em>'.
     * @see ca.mcgill.sel.core.COREMappingCardinality#getUpperBound()
     * @see #getCOREMappingCardinality()
     * @generated
     */
	EAttribute getCOREMappingCardinality_UpperBound();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREMappingCardinality#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.core.COREMappingCardinality#getName()
     * @see #getCOREMappingCardinality()
     * @generated
     */
	EAttribute getCOREMappingCardinality_Name();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORELanguageElementMapping <em>CORE Language Element Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Language Element Mapping</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElementMapping
     * @generated
     */
	EClass getCORELanguageElementMapping();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getActions <em>Actions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Actions</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#getActions()
     * @see #getCORELanguageElementMapping()
     * @generated
     */
	EReference getCORELanguageElementMapping_Actions();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getMappingEnds <em>Mapping Ends</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Mapping Ends</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#getMappingEnds()
     * @see #getCORELanguageElementMapping()
     * @generated
     */
	EReference getCORELanguageElementMapping_MappingEnds();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getIdentifier <em>Identifier</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Identifier</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#getIdentifier()
     * @see #getCORELanguageElementMapping()
     * @generated
     */
	EAttribute getCORELanguageElementMapping_Identifier();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getNestedMappings <em>Nested Mappings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Nested Mappings</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#getNestedMappings()
     * @see #getCORELanguageElementMapping()
     * @generated
     */
	EReference getCORELanguageElementMapping_NestedMappings();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORELanguageElementMapping#isMatchMaker <em>Match Maker</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Match Maker</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#isMatchMaker()
     * @see #getCORELanguageElementMapping()
     * @generated
     */
	EAttribute getCORELanguageElementMapping_MatchMaker();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREReexposeAction <em>CORE Reexpose Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Reexpose Action</em>'.
     * @see ca.mcgill.sel.core.COREReexposeAction
     * @generated
     */
	EClass getCOREReexposeAction();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.COREReexposeAction#getReexposedAction <em>Reexposed Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Reexposed Action</em>'.
     * @see ca.mcgill.sel.core.COREReexposeAction#getReexposedAction()
     * @see #getCOREReexposeAction()
     * @generated
     */
	EReference getCOREReexposeAction_ReexposedAction();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORERedefineAction <em>CORE Redefine Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Redefine Action</em>'.
     * @see ca.mcgill.sel.core.CORERedefineAction
     * @generated
     */
	EClass getCORERedefineAction();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CORERedefineAction#getRedefinedAction <em>Redefined Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Redefined Action</em>'.
     * @see ca.mcgill.sel.core.CORERedefineAction#getRedefinedAction()
     * @see #getCORERedefineAction()
     * @generated
     */
	EReference getCORERedefineAction_RedefinedAction();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.CORERedefineAction#getReusedActions <em>Reused Actions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Reused Actions</em>'.
     * @see ca.mcgill.sel.core.CORERedefineAction#getReusedActions()
     * @see #getCORERedefineAction()
     * @generated
     */
	EReference getCORERedefineAction_ReusedActions();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORELanguageAction <em>CORE Language Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Language Action</em>'.
     * @see ca.mcgill.sel.core.CORELanguageAction
     * @generated
     */
	EClass getCORELanguageAction();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORELanguageAction#getClassQualifiedName <em>Class Qualified Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Class Qualified Name</em>'.
     * @see ca.mcgill.sel.core.CORELanguageAction#getClassQualifiedName()
     * @see #getCORELanguageAction()
     * @generated
     */
	EAttribute getCORELanguageAction_ClassQualifiedName();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORELanguageAction#getMethodName <em>Method Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Method Name</em>'.
     * @see ca.mcgill.sel.core.CORELanguageAction#getMethodName()
     * @see #getCORELanguageAction()
     * @generated
     */
	EAttribute getCORELanguageAction_MethodName();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.CORELanguageAction#getParameters <em>Parameters</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Parameters</em>'.
     * @see ca.mcgill.sel.core.CORELanguageAction#getParameters()
     * @see #getCORELanguageAction()
     * @generated
     */
	EReference getCORELanguageAction_Parameters();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.CORELanguageAction#getSecondaryEffects <em>Secondary Effects</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Secondary Effects</em>'.
     * @see ca.mcgill.sel.core.CORELanguageAction#getSecondaryEffects()
     * @see #getCORELanguageAction()
     * @generated
     */
	EReference getCORELanguageAction_SecondaryEffects();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORELanguageAction#getActionType <em>Action Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Action Type</em>'.
     * @see ca.mcgill.sel.core.CORELanguageAction#getActionType()
     * @see #getCORELanguageAction()
     * @generated
     */
	EAttribute getCORELanguageAction_ActionType();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORECreateMapping <em>CORE Create Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Create Mapping</em>'.
     * @see ca.mcgill.sel.core.CORECreateMapping
     * @generated
     */
	EClass getCORECreateMapping();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CORECreateMapping#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Type</em>'.
     * @see ca.mcgill.sel.core.CORECreateMapping#getType()
     * @see #getCORECreateMapping()
     * @generated
     */
	EReference getCORECreateMapping_Type();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CORECreateMapping#getExtendedAction <em>Extended Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Extended Action</em>'.
     * @see ca.mcgill.sel.core.CORECreateMapping#getExtendedAction()
     * @see #getCORECreateMapping()
     * @generated
     */
	EReference getCORECreateMapping_ExtendedAction();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.CORECreateMapping#getModelElements <em>Model Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Model Elements</em>'.
     * @see ca.mcgill.sel.core.CORECreateMapping#getModelElements()
     * @see #getCORECreateMapping()
     * @generated
     */
	EReference getCORECreateMapping_ModelElements();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORELanguage <em>CORE Language</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Language</em>'.
     * @see ca.mcgill.sel.core.CORELanguage
     * @generated
     */
	EClass getCORELanguage();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.CORELanguage#getActions <em>Actions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Actions</em>'.
     * @see ca.mcgill.sel.core.CORELanguage#getActions()
     * @see #getCORELanguage()
     * @generated
     */
	EReference getCORELanguage_Actions();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREAction <em>CORE Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Action</em>'.
     * @see ca.mcgill.sel.core.COREAction
     * @generated
     */
	EClass getCOREAction();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREAction#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.core.COREAction#getName()
     * @see #getCOREAction()
     * @generated
     */
	EAttribute getCOREAction_Name();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CreateModelElementMapping <em>Create Model Element Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Create Model Element Mapping</em>'.
     * @see ca.mcgill.sel.core.CreateModelElementMapping
     * @generated
     */
	EClass getCreateModelElementMapping();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.CreateModelElementMapping#getReusedActions <em>Reused Actions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Reused Actions</em>'.
     * @see ca.mcgill.sel.core.CreateModelElementMapping#getReusedActions()
     * @see #getCreateModelElementMapping()
     * @generated
     */
	EReference getCreateModelElementMapping_ReusedActions();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREModelElementMapping <em>CORE Model Element Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Model Element Mapping</em>'.
     * @see ca.mcgill.sel.core.COREModelElementMapping
     * @generated
     */
	EClass getCOREModelElementMapping();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.COREModelElementMapping#getModelElements <em>Model Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Model Elements</em>'.
     * @see ca.mcgill.sel.core.COREModelElementMapping#getModelElements()
     * @see #getCOREModelElementMapping()
     * @generated
     */
	EReference getCOREModelElementMapping_ModelElements();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.COREModelElementMapping#getLEMid <em>LE Mid</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>LE Mid</em>'.
     * @see ca.mcgill.sel.core.COREModelElementMapping#getLEMid()
     * @see #getCOREModelElementMapping()
     * @generated
     */
	EAttribute getCOREModelElementMapping_LEMid();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.MappingEnd <em>Mapping End</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Mapping End</em>'.
     * @see ca.mcgill.sel.core.MappingEnd
     * @generated
     */
	EClass getMappingEnd();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.MappingEnd#getCardinality <em>Cardinality</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Cardinality</em>'.
     * @see ca.mcgill.sel.core.MappingEnd#getCardinality()
     * @see #getMappingEnd()
     * @generated
     */
	EAttribute getMappingEnd_Cardinality();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.MappingEnd#getRoleName <em>Role Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Role Name</em>'.
     * @see ca.mcgill.sel.core.MappingEnd#getRoleName()
     * @see #getMappingEnd()
     * @generated
     */
	EAttribute getMappingEnd_RoleName();

	/**
     * Returns the meta object for the container reference '{@link ca.mcgill.sel.core.MappingEnd#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Type</em>'.
     * @see ca.mcgill.sel.core.MappingEnd#getType()
     * @see #getMappingEnd()
     * @generated
     */
	EReference getMappingEnd_Type();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.MappingEnd#getLanguageElement <em>Language Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Language Element</em>'.
     * @see ca.mcgill.sel.core.MappingEnd#getLanguageElement()
     * @see #getMappingEnd()
     * @generated
     */
	EReference getMappingEnd_LanguageElement();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Language Map</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Language Map</em>'.
     * @see java.util.Map.Entry
     * @model keyDataType="org.eclipse.emf.ecore.EString"
     *        valueType="ca.mcgill.sel.core.CORELanguage"
     * @generated
     */
	EClass getLanguageMap();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getLanguageMap()
     * @generated
     */
	EAttribute getLanguageMap_Key();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getLanguageMap()
     * @generated
     */
	EReference getLanguageMap_Value();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Artefact Map</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Artefact Map</em>'.
     * @see java.util.Map.Entry
     * @model features="value key" 
     *        valueType="ca.mcgill.sel.core.COREArtefact" valueMany="true"
     *        keyDataType="org.eclipse.emf.ecore.EString"
     * @generated
     */
	EClass getArtefactMap();

	/**
     * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getArtefactMap()
     * @generated
     */
	EReference getArtefactMap_Value();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getArtefactMap()
     * @generated
     */
	EAttribute getArtefactMap_Key();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.Parameter <em>Parameter</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Parameter</em>'.
     * @see ca.mcgill.sel.core.Parameter
     * @generated
     */
	EClass getParameter();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.Parameter#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.core.Parameter#getName()
     * @see #getParameter()
     * @generated
     */
	EAttribute getParameter_Name();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.Parameter#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see ca.mcgill.sel.core.Parameter#getType()
     * @see #getParameter()
     * @generated
     */
	EAttribute getParameter_Type();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CORELanguageElement <em>CORE Language Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Language Element</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElement
     * @generated
     */
	EClass getCORELanguageElement();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguageActions <em>Language Actions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Language Actions</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElement#getLanguageActions()
     * @see #getCORELanguageElement()
     * @generated
     */
	EReference getCORELanguageElement_LanguageActions();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguageElement <em>Language Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Language Element</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElement#getLanguageElement()
     * @see #getCORELanguageElement()
     * @generated
     */
	EReference getCORELanguageElement_LanguageElement();

	/**
     * Returns the meta object for the container reference '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguage <em>Language</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Language</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElement#getLanguage()
     * @see #getCORELanguageElement()
     * @generated
     */
	EReference getCORELanguageElement_Language();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.CORELanguageElement#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElement#getName()
     * @see #getCORELanguageElement()
     * @generated
     */
	EAttribute getCORELanguageElement_Name();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.core.CORELanguageElement#getNestedElements <em>Nested Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Nested Elements</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElement#getNestedElements()
     * @see #getCORELanguageElement()
     * @generated
     */
	EReference getCORELanguageElement_NestedElements();

	/**
     * Returns the meta object for the container reference '{@link ca.mcgill.sel.core.CORELanguageElement#getOwner <em>Owner</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Owner</em>'.
     * @see ca.mcgill.sel.core.CORELanguageElement#getOwner()
     * @see #getCORELanguageElement()
     * @generated
     */
	EReference getCORELanguageElement_Owner();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.ExistingElementEffect <em>Existing Element Effect</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Existing Element Effect</em>'.
     * @see ca.mcgill.sel.core.ExistingElementEffect
     * @generated
     */
	EClass getExistingElementEffect();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.ExistingElementEffect#getParameter <em>Parameter</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Parameter</em>'.
     * @see ca.mcgill.sel.core.ExistingElementEffect#getParameter()
     * @see #getExistingElementEffect()
     * @generated
     */
	EReference getExistingElementEffect_Parameter();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.ExistingElementEffect#getParameterEffect <em>Parameter Effect</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Parameter Effect</em>'.
     * @see ca.mcgill.sel.core.ExistingElementEffect#getParameterEffect()
     * @see #getExistingElementEffect()
     * @generated
     */
	EAttribute getExistingElementEffect_ParameterEffect();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.DeleteEffect <em>Delete Effect</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Delete Effect</em>'.
     * @see ca.mcgill.sel.core.DeleteEffect
     * @generated
     */
	EClass getDeleteEffect();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.UpdateEffect <em>Update Effect</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Update Effect</em>'.
     * @see ca.mcgill.sel.core.UpdateEffect
     * @generated
     */
	EClass getUpdateEffect();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.UpdateEffect#getAffectedFeature <em>Affected Feature</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Affected Feature</em>'.
     * @see ca.mcgill.sel.core.UpdateEffect#getAffectedFeature()
     * @see #getUpdateEffect()
     * @generated
     */
	EReference getUpdateEffect_AffectedFeature();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.NavigationMapping <em>Navigation Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Navigation Mapping</em>'.
     * @see ca.mcgill.sel.core.NavigationMapping
     * @generated
     */
	EClass getNavigationMapping();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.NavigationMapping#isActive <em>Active</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Active</em>'.
     * @see ca.mcgill.sel.core.NavigationMapping#isActive()
     * @see #getNavigationMapping()
     * @generated
     */
	EAttribute getNavigationMapping_Active();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.InterLanguageMapping <em>Inter Language Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Inter Language Mapping</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMapping
     * @generated
     */
	EClass getInterLanguageMapping();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.InterLanguageMapping#isDefault <em>Default</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Default</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMapping#isDefault()
     * @see #getInterLanguageMapping()
     * @generated
     */
	EAttribute getInterLanguageMapping_Default();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.InterLanguageMapping#getCoreLanguageElementMapping <em>Core Language Element Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Core Language Element Mapping</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMapping#getCoreLanguageElementMapping()
     * @see #getInterLanguageMapping()
     * @generated
     */
	EReference getInterLanguageMapping_CoreLanguageElementMapping();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.InterLanguageMapping#getInterLanguageMappingEnds <em>Inter Language Mapping Ends</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Inter Language Mapping Ends</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMapping#getInterLanguageMappingEnds()
     * @see #getInterLanguageMapping()
     * @generated
     */
	EReference getInterLanguageMapping_InterLanguageMappingEnds();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.IntraLanguageMapping <em>Intra Language Mapping</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Intra Language Mapping</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping
     * @generated
     */
	EClass getIntraLanguageMapping();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.IntraLanguageMapping#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping#getName()
     * @see #getIntraLanguageMapping()
     * @generated
     */
	EAttribute getIntraLanguageMapping_Name();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.IntraLanguageMapping#isClosure <em>Closure</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Closure</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping#isClosure()
     * @see #getIntraLanguageMapping()
     * @generated
     */
	EAttribute getIntraLanguageMapping_Closure();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.IntraLanguageMapping#isReuse <em>Reuse</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Reuse</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping#isReuse()
     * @see #getIntraLanguageMapping()
     * @generated
     */
	EAttribute getIntraLanguageMapping_Reuse();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.IntraLanguageMapping#isIncreaseDepth <em>Increase Depth</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Increase Depth</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping#isIncreaseDepth()
     * @see #getIntraLanguageMapping()
     * @generated
     */
	EAttribute getIntraLanguageMapping_IncreaseDepth();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.IntraLanguageMapping#isChangeModel <em>Change Model</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Change Model</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping#isChangeModel()
     * @see #getIntraLanguageMapping()
     * @generated
     */
	EAttribute getIntraLanguageMapping_ChangeModel();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.IntraLanguageMapping#getFrom <em>From</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>From</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping#getFrom()
     * @see #getIntraLanguageMapping()
     * @generated
     */
	EReference getIntraLanguageMapping_From();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.core.IntraLanguageMapping#getHops <em>Hops</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Hops</em>'.
     * @see ca.mcgill.sel.core.IntraLanguageMapping#getHops()
     * @see #getIntraLanguageMapping()
     * @generated
     */
	EReference getIntraLanguageMapping_Hops();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.InterLanguageMappingEnd <em>Inter Language Mapping End</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Inter Language Mapping End</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMappingEnd
     * @generated
     */
	EClass getInterLanguageMappingEnd();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMappingEnd#getName()
     * @see #getInterLanguageMappingEnd()
     * @generated
     */
	EAttribute getInterLanguageMappingEnd_Name();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#isOrigin <em>Origin</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Origin</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMappingEnd#isOrigin()
     * @see #getInterLanguageMappingEnd()
     * @generated
     */
	EAttribute getInterLanguageMappingEnd_Origin();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#isDestination <em>Destination</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Destination</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMappingEnd#isDestination()
     * @see #getInterLanguageMappingEnd()
     * @generated
     */
	EAttribute getInterLanguageMappingEnd_Destination();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getMappingEnd <em>Mapping End</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Mapping End</em>'.
     * @see ca.mcgill.sel.core.InterLanguageMappingEnd#getMappingEnd()
     * @see #getInterLanguageMappingEnd()
     * @generated
     */
	EReference getInterLanguageMappingEnd_MappingEnd();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.CreateEffect <em>Create Effect</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Create Effect</em>'.
     * @see ca.mcgill.sel.core.CreateEffect
     * @generated
     */
	EClass getCreateEffect();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.core.CreateEffect#getCorelanguageElement <em>Corelanguage Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Corelanguage Element</em>'.
     * @see ca.mcgill.sel.core.CreateEffect#getCorelanguageElement()
     * @see #getCreateEffect()
     * @generated
     */
	EReference getCreateEffect_CorelanguageElement();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.ActionEffect <em>Action Effect</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Action Effect</em>'.
     * @see ca.mcgill.sel.core.ActionEffect
     * @generated
     */
	EClass getActionEffect();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.core.COREReuseArtefact <em>CORE Reuse Artefact</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>CORE Reuse Artefact</em>'.
     * @see ca.mcgill.sel.core.COREReuseArtefact
     * @generated
     */
    EClass getCOREReuseArtefact();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.core.COREFeatureRelationshipType <em>CORE Feature Relationship Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>CORE Feature Relationship Type</em>'.
     * @see ca.mcgill.sel.core.COREFeatureRelationshipType
     * @generated
     */
	EEnum getCOREFeatureRelationshipType();

	/**
     * Returns the meta object for enum '{@link ca.mcgill.sel.core.COREVisibilityType <em>CORE Visibility Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>CORE Visibility Type</em>'.
     * @see ca.mcgill.sel.core.COREVisibilityType
     * @generated
     */
	EEnum getCOREVisibilityType();

	/**
     * Returns the meta object for enum '{@link ca.mcgill.sel.core.COREPartialityType <em>CORE Partiality Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>CORE Partiality Type</em>'.
     * @see ca.mcgill.sel.core.COREPartialityType
     * @generated
     */
	EEnum getCOREPartialityType();

	/**
     * Returns the meta object for enum '{@link ca.mcgill.sel.core.CORERelationship <em>CORE Relationship</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>CORE Relationship</em>'.
     * @see ca.mcgill.sel.core.CORERelationship
     * @generated
     */
	EEnum getCORERelationship();

	/**
     * Returns the meta object for enum '{@link ca.mcgill.sel.core.Cardinality <em>Cardinality</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Cardinality</em>'.
     * @see ca.mcgill.sel.core.Cardinality
     * @generated
     */
	EEnum getCardinality();

	/**
     * Returns the meta object for enum '{@link ca.mcgill.sel.core.LanguageActionType <em>Language Action Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Language Action Type</em>'.
     * @see ca.mcgill.sel.core.LanguageActionType
     * @generated
     */
	EEnum getLanguageActionType();

	/**
     * Returns the meta object for enum '{@link ca.mcgill.sel.core.ParameterEffect <em>Parameter Effect</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Parameter Effect</em>'.
     * @see ca.mcgill.sel.core.ParameterEffect
     * @generated
     */
	EEnum getParameterEffect();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	CoreFactory getCoreFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREArtefactImpl <em>CORE Artefact</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREArtefactImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREArtefact()
         * @generated
         */
		EClass CORE_ARTEFACT = eINSTANCE.getCOREArtefact();

		/**
         * The meta object literal for the '<em><b>Model Reuses</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_ARTEFACT__MODEL_REUSES = eINSTANCE.getCOREArtefact_ModelReuses();

		/**
         * The meta object literal for the '<em><b>Core Concern</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_ARTEFACT__CORE_CONCERN = eINSTANCE.getCOREArtefact_CoreConcern();

		/**
         * The meta object literal for the '<em><b>Model Extensions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_ARTEFACT__MODEL_EXTENSIONS = eINSTANCE.getCOREArtefact_ModelExtensions();

		/**
         * The meta object literal for the '<em><b>Ui Elements</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_ARTEFACT__UI_ELEMENTS = eINSTANCE.getCOREArtefact_UiElements();

		/**
         * The meta object literal for the '<em><b>Ci Elements</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_ARTEFACT__CI_ELEMENTS = eINSTANCE.getCOREArtefact_CiElements();

		/**
         * The meta object literal for the '<em><b>Temporary Concern</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_ARTEFACT__TEMPORARY_CONCERN = eINSTANCE.getCOREArtefact_TemporaryConcern();

		/**
         * The meta object literal for the '<em><b>Scene</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_ARTEFACT__SCENE = eINSTANCE.getCOREArtefact_Scene();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREImpactModelImpl <em>CORE Impact Model</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREImpactModelImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREImpactModel()
         * @generated
         */
		EClass CORE_IMPACT_MODEL = eINSTANCE.getCOREImpactModel();

		/**
         * The meta object literal for the '<em><b>Impact Model Elements</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_IMPACT_MODEL__IMPACT_MODEL_ELEMENTS = eINSTANCE.getCOREImpactModel_ImpactModelElements();

		/**
         * The meta object literal for the '<em><b>Layouts</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_IMPACT_MODEL__LAYOUTS = eINSTANCE.getCOREImpactModel_Layouts();

		/**
         * The meta object literal for the '<em><b>Contributions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_IMPACT_MODEL__CONTRIBUTIONS = eINSTANCE.getCOREImpactModel_Contributions();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREConcernImpl <em>CORE Concern</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREConcernImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREConcern()
         * @generated
         */
		EClass CORE_CONCERN = eINSTANCE.getCOREConcern();

		/**
         * The meta object literal for the '<em><b>Artefacts</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONCERN__ARTEFACTS = eINSTANCE.getCOREConcern_Artefacts();

		/**
         * The meta object literal for the '<em><b>Feature Model</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONCERN__FEATURE_MODEL = eINSTANCE.getCOREConcern_FeatureModel();

		/**
         * The meta object literal for the '<em><b>Impact Model</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONCERN__IMPACT_MODEL = eINSTANCE.getCOREConcern_ImpactModel();

		/**
         * The meta object literal for the '<em><b>Scenes</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONCERN__SCENES = eINSTANCE.getCOREConcern_Scenes();

		/**
         * The meta object literal for the '<em><b>Reuses</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONCERN__REUSES = eINSTANCE.getCOREConcern_Reuses();

		/**
         * The meta object literal for the '<em><b>Temporary Artefacts</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONCERN__TEMPORARY_ARTEFACTS = eINSTANCE.getCOREConcern_TemporaryArtefacts();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREFeatureImpl <em>CORE Feature</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREFeatureImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeature()
         * @generated
         */
		EClass CORE_FEATURE = eINSTANCE.getCOREFeature();

		/**
         * The meta object literal for the '<em><b>Realized By</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE__REALIZED_BY = eINSTANCE.getCOREFeature_RealizedBy();

		/**
         * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE__CHILDREN = eINSTANCE.getCOREFeature_Children();

		/**
         * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE__PARENT = eINSTANCE.getCOREFeature_Parent();

		/**
         * The meta object literal for the '<em><b>Parent Relationship</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_FEATURE__PARENT_RELATIONSHIP = eINSTANCE.getCOREFeature_ParentRelationship();

		/**
         * The meta object literal for the '<em><b>Requires</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE__REQUIRES = eINSTANCE.getCOREFeature_Requires();

		/**
         * The meta object literal for the '<em><b>Excludes</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE__EXCLUDES = eINSTANCE.getCOREFeature_Excludes();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREModelCompositionImpl <em>CORE Model Composition</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREModelCompositionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelComposition()
         * @generated
         */
		EClass CORE_MODEL_COMPOSITION = eINSTANCE.getCOREModelComposition();

		/**
         * The meta object literal for the '<em><b>Source</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_MODEL_COMPOSITION__SOURCE = eINSTANCE.getCOREModelComposition_Source();

		/**
         * The meta object literal for the '<em><b>Compositions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_MODEL_COMPOSITION__COMPOSITIONS = eINSTANCE.getCOREModelComposition_Compositions();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORELinkImpl <em>CORE Link</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORELinkImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELink()
         * @generated
         */
		EClass CORE_LINK = eINSTANCE.getCORELink();

		/**
         * The meta object literal for the '<em><b>To</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LINK__TO = eINSTANCE.getCORELink_To();

		/**
         * The meta object literal for the '<em><b>From</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LINK__FROM = eINSTANCE.getCORELink_From();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORENamedElementImpl <em>CORE Named Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORENamedElementImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORENamedElement()
         * @generated
         */
		EClass CORE_NAMED_ELEMENT = eINSTANCE.getCORENamedElement();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_NAMED_ELEMENT__NAME = eINSTANCE.getCORENamedElement_Name();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREReuseImpl <em>CORE Reuse</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREReuseImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREReuse()
         * @generated
         */
		EClass CORE_REUSE = eINSTANCE.getCOREReuse();

		/**
         * The meta object literal for the '<em><b>Reused Concern</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_REUSE__REUSED_CONCERN = eINSTANCE.getCOREReuse_ReusedConcern();

		/**
         * The meta object literal for the '<em><b>Extends</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_REUSE__EXTENDS = eINSTANCE.getCOREReuse_Extends();

		/**
         * The meta object literal for the '<em><b>Model Reuses</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_REUSE__MODEL_REUSES = eINSTANCE.getCOREReuse_ModelReuses();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREImpactNodeImpl <em>CORE Impact Node</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREImpactNodeImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREImpactNode()
         * @generated
         */
		EClass CORE_IMPACT_NODE = eINSTANCE.getCOREImpactNode();

		/**
         * The meta object literal for the '<em><b>Scaling Factor</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_IMPACT_NODE__SCALING_FACTOR = eINSTANCE.getCOREImpactNode_ScalingFactor();

		/**
         * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_IMPACT_NODE__OFFSET = eINSTANCE.getCOREImpactNode_Offset();

		/**
         * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_IMPACT_NODE__OUTGOING = eINSTANCE.getCOREImpactNode_Outgoing();

		/**
         * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_IMPACT_NODE__INCOMING = eINSTANCE.getCOREImpactNode_Incoming();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREConfigurationImpl <em>CORE Configuration</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREConfigurationImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREConfiguration()
         * @generated
         */
		EClass CORE_CONFIGURATION = eINSTANCE.getCOREConfiguration();

		/**
         * The meta object literal for the '<em><b>Selected</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONFIGURATION__SELECTED = eINSTANCE.getCOREConfiguration_Selected();

		/**
         * The meta object literal for the '<em><b>Reexposed</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONFIGURATION__REEXPOSED = eINSTANCE.getCOREConfiguration_Reexposed();

		/**
         * The meta object literal for the '<em><b>Extending Configurations</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS = eINSTANCE.getCOREConfiguration_ExtendingConfigurations();

		/**
         * The meta object literal for the '<em><b>Extended Reuse</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONFIGURATION__EXTENDED_REUSE = eINSTANCE.getCOREConfiguration_ExtendedReuse();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREFeatureModelImpl <em>CORE Feature Model</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREFeatureModelImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeatureModel()
         * @generated
         */
		EClass CORE_FEATURE_MODEL = eINSTANCE.getCOREFeatureModel();

		/**
         * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE_MODEL__FEATURES = eINSTANCE.getCOREFeatureModel_Features();

		/**
         * The meta object literal for the '<em><b>Root</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE_MODEL__ROOT = eINSTANCE.getCOREFeatureModel_Root();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREModelReuseImpl <em>CORE Model Reuse</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREModelReuseImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelReuse()
         * @generated
         */
		EClass CORE_MODEL_REUSE = eINSTANCE.getCOREModelReuse();

		/**
         * The meta object literal for the '<em><b>Reuse</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_MODEL_REUSE__REUSE = eINSTANCE.getCOREModelReuse_Reuse();

		/**
         * The meta object literal for the '<em><b>Configuration</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_MODEL_REUSE__CONFIGURATION = eINSTANCE.getCOREModelReuse_Configuration();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREContributionImpl <em>CORE Contribution</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREContributionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREContribution()
         * @generated
         */
		EClass CORE_CONTRIBUTION = eINSTANCE.getCOREContribution();

		/**
         * The meta object literal for the '<em><b>Relative Weight</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_CONTRIBUTION__RELATIVE_WEIGHT = eINSTANCE.getCOREContribution_RelativeWeight();

		/**
         * The meta object literal for the '<em><b>Source</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONTRIBUTION__SOURCE = eINSTANCE.getCOREContribution_Source();

		/**
         * The meta object literal for the '<em><b>Impacts</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CONTRIBUTION__IMPACTS = eINSTANCE.getCOREContribution_Impacts();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.LayoutMapImpl <em>Layout Map</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.LayoutMapImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLayoutMap()
         * @generated
         */
		EClass LAYOUT_MAP = eINSTANCE.getLayoutMap();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LAYOUT_MAP__KEY = eINSTANCE.getLayoutMap_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LAYOUT_MAP__VALUE = eINSTANCE.getLayoutMap_Value();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.LayoutElementImpl <em>Layout Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.LayoutElementImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLayoutElement()
         * @generated
         */
		EClass LAYOUT_ELEMENT = eINSTANCE.getLayoutElement();

		/**
         * The meta object literal for the '<em><b>X</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute LAYOUT_ELEMENT__X = eINSTANCE.getLayoutElement_X();

		/**
         * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute LAYOUT_ELEMENT__Y = eINSTANCE.getLayoutElement_Y();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.LayoutContainerMapImpl <em>Layout Container Map</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.LayoutContainerMapImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLayoutContainerMap()
         * @generated
         */
		EClass LAYOUT_CONTAINER_MAP = eINSTANCE.getLayoutContainerMap();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LAYOUT_CONTAINER_MAP__KEY = eINSTANCE.getLayoutContainerMap_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LAYOUT_CONTAINER_MAP__VALUE = eINSTANCE.getLayoutContainerMap_Value();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREFeatureImpactNodeImpl <em>CORE Feature Impact Node</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREFeatureImpactNodeImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeatureImpactNode()
         * @generated
         */
		EClass CORE_FEATURE_IMPACT_NODE = eINSTANCE.getCOREFeatureImpactNode();

		/**
         * The meta object literal for the '<em><b>Relative Feature Weight</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_FEATURE_IMPACT_NODE__RELATIVE_FEATURE_WEIGHT = eINSTANCE.getCOREFeatureImpactNode_RelativeFeatureWeight();

		/**
         * The meta object literal for the '<em><b>Represents</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE_IMPACT_NODE__REPRESENTS = eINSTANCE.getCOREFeatureImpactNode_Represents();

		/**
         * The meta object literal for the '<em><b>Weighted Links</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_FEATURE_IMPACT_NODE__WEIGHTED_LINKS = eINSTANCE.getCOREFeatureImpactNode_WeightedLinks();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREModelElementCompositionImpl <em>CORE Model Element Composition</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREModelElementCompositionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelElementComposition()
         * @generated
         */
		EClass CORE_MODEL_ELEMENT_COMPOSITION = eINSTANCE.getCOREModelElementComposition();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREWeightedLinkImpl <em>CORE Weighted Link</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREWeightedLinkImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREWeightedLink()
         * @generated
         */
		EClass CORE_WEIGHTED_LINK = eINSTANCE.getCOREWeightedLink();

		/**
         * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_WEIGHTED_LINK__WEIGHT = eINSTANCE.getCOREWeightedLink_Weight();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREModelExtensionImpl <em>CORE Model Extension</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREModelExtensionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelExtension()
         * @generated
         */
		EClass CORE_MODEL_EXTENSION = eINSTANCE.getCOREModelExtension();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREMappingImpl <em>CORE Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREMapping()
         * @generated
         */
		EClass CORE_MAPPING = eINSTANCE.getCOREMapping();

		/**
         * The meta object literal for the '<em><b>Mappings</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_MAPPING__MAPPINGS = eINSTANCE.getCOREMapping_Mappings();

		/**
         * The meta object literal for the '<em><b>Referenced Mappings</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_MAPPING__REFERENCED_MAPPINGS = eINSTANCE.getCOREMapping_ReferencedMappings();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORESceneImpl <em>CORE Scene</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORESceneImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREScene()
         * @generated
         */
		EClass CORE_SCENE = eINSTANCE.getCOREScene();

		/**
         * The meta object literal for the '<em><b>Realizes</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_SCENE__REALIZES = eINSTANCE.getCOREScene_Realizes();

		/**
         * The meta object literal for the '<em><b>Perspective Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_SCENE__PERSPECTIVE_NAME = eINSTANCE.getCOREScene_PerspectiveName();

		/**
         * The meta object literal for the '<em><b>Element Mappings</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_SCENE__ELEMENT_MAPPINGS = eINSTANCE.getCOREScene_ElementMappings();

		/**
         * The meta object literal for the '<em><b>Artefacts</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_SCENE__ARTEFACTS = eINSTANCE.getCOREScene_Artefacts();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREPerspectiveImpl <em>CORE Perspective</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREPerspectiveImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREPerspective()
         * @generated
         */
		EClass CORE_PERSPECTIVE = eINSTANCE.getCOREPerspective();

		/**
         * The meta object literal for the '<em><b>Mappings</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_PERSPECTIVE__MAPPINGS = eINSTANCE.getCOREPerspective_Mappings();

		/**
         * The meta object literal for the '<em><b>Languages</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_PERSPECTIVE__LANGUAGES = eINSTANCE.getCOREPerspective_Languages();

		/**
         * The meta object literal for the '<em><b>Default</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_PERSPECTIVE__DEFAULT = eINSTANCE.getCOREPerspective_Default();

		/**
         * The meta object literal for the '<em><b>Navigation Mappings</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_PERSPECTIVE__NAVIGATION_MAPPINGS = eINSTANCE.getCOREPerspective_NavigationMappings();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl <em>CORE External Language</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREExternalLanguageImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREExternalLanguage()
         * @generated
         */
		EClass CORE_EXTERNAL_LANGUAGE = eINSTANCE.getCOREExternalLanguage();

		/**
         * The meta object literal for the '<em><b>Ns URI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_EXTERNAL_LANGUAGE__NS_URI = eINSTANCE.getCOREExternalLanguage_NsURI();

		/**
         * The meta object literal for the '<em><b>Resource Factory</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY = eINSTANCE.getCOREExternalLanguage_ResourceFactory();

		/**
         * The meta object literal for the '<em><b>Adapter Factory</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY = eINSTANCE.getCOREExternalLanguage_AdapterFactory();

		/**
         * The meta object literal for the '<em><b>Weaver Class Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME = eINSTANCE.getCOREExternalLanguage_WeaverClassName();

		/**
         * The meta object literal for the '<em><b>File Extension</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION = eINSTANCE.getCOREExternalLanguage_FileExtension();

		/**
         * The meta object literal for the '<em><b>Language Elements</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS = eINSTANCE.getCOREExternalLanguage_LanguageElements();

		/**
         * The meta object literal for the '<em><b>Model Util Class Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME = eINSTANCE.getCOREExternalLanguage_ModelUtilClassName();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREPerspectiveActionImpl <em>CORE Perspective Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREPerspectiveActionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREPerspectiveAction()
         * @generated
         */
		EClass CORE_PERSPECTIVE_ACTION = eINSTANCE.getCOREPerspectiveAction();

		/**
         * The meta object literal for the '<em><b>For Role</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_PERSPECTIVE_ACTION__FOR_ROLE = eINSTANCE.getCOREPerspectiveAction_ForRole();

		/**
         * The meta object literal for the '<em><b>Action Identifier</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER = eINSTANCE.getCOREPerspectiveAction_ActionIdentifier();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREExternalArtefactImpl <em>CORE External Artefact</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREExternalArtefactImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREExternalArtefact()
         * @generated
         */
		EClass CORE_EXTERNAL_ARTEFACT = eINSTANCE.getCOREExternalArtefact();

		/**
         * The meta object literal for the '<em><b>Root Model Element</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT = eINSTANCE.getCOREExternalArtefact_RootModelElement();

		/**
         * The meta object literal for the '<em><b>Language Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME = eINSTANCE.getCOREExternalArtefact_LanguageName();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREUIElementImpl <em>COREUI Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREUIElementImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREUIElement()
         * @generated
         */
		EClass COREUI_ELEMENT = eINSTANCE.getCOREUIElement();

		/**
         * The meta object literal for the '<em><b>Model Element</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference COREUI_ELEMENT__MODEL_ELEMENT = eINSTANCE.getCOREUIElement_ModelElement();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORECIElementImpl <em>CORECI Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORECIElementImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORECIElement()
         * @generated
         */
		EClass CORECI_ELEMENT = eINSTANCE.getCORECIElement();

		/**
         * The meta object literal for the '<em><b>Partiality</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORECI_ELEMENT__PARTIALITY = eINSTANCE.getCORECIElement_Partiality();

		/**
         * The meta object literal for the '<em><b>Model Element</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORECI_ELEMENT__MODEL_ELEMENT = eINSTANCE.getCORECIElement_ModelElement();

		/**
         * The meta object literal for the '<em><b>Mapping Cardinality</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORECI_ELEMENT__MAPPING_CARDINALITY = eINSTANCE.getCORECIElement_MappingCardinality();

		/**
         * The meta object literal for the '<em><b>Reference Cardinality</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORECI_ELEMENT__REFERENCE_CARDINALITY = eINSTANCE.getCORECIElement_ReferenceCardinality();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREMappingCardinalityImpl <em>CORE Mapping Cardinality</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREMappingCardinalityImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREMappingCardinality()
         * @generated
         */
		EClass CORE_MAPPING_CARDINALITY = eINSTANCE.getCOREMappingCardinality();

		/**
         * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_MAPPING_CARDINALITY__LOWER_BOUND = eINSTANCE.getCOREMappingCardinality_LowerBound();

		/**
         * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_MAPPING_CARDINALITY__UPPER_BOUND = eINSTANCE.getCOREMappingCardinality_UpperBound();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_MAPPING_CARDINALITY__NAME = eINSTANCE.getCOREMappingCardinality_Name();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl <em>CORE Language Element Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguageElementMapping()
         * @generated
         */
		EClass CORE_LANGUAGE_ELEMENT_MAPPING = eINSTANCE.getCORELanguageElementMapping();

		/**
         * The meta object literal for the '<em><b>Actions</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS = eINSTANCE.getCORELanguageElementMapping_Actions();

		/**
         * The meta object literal for the '<em><b>Mapping Ends</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS = eINSTANCE.getCORELanguageElementMapping_MappingEnds();

		/**
         * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER = eINSTANCE.getCORELanguageElementMapping_Identifier();

		/**
         * The meta object literal for the '<em><b>Nested Mappings</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS = eINSTANCE.getCORELanguageElementMapping_NestedMappings();

		/**
         * The meta object literal for the '<em><b>Match Maker</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER = eINSTANCE.getCORELanguageElementMapping_MatchMaker();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREReexposeActionImpl <em>CORE Reexpose Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREReexposeActionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREReexposeAction()
         * @generated
         */
		EClass CORE_REEXPOSE_ACTION = eINSTANCE.getCOREReexposeAction();

		/**
         * The meta object literal for the '<em><b>Reexposed Action</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_REEXPOSE_ACTION__REEXPOSED_ACTION = eINSTANCE.getCOREReexposeAction_ReexposedAction();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORERedefineActionImpl <em>CORE Redefine Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORERedefineActionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORERedefineAction()
         * @generated
         */
		EClass CORE_REDEFINE_ACTION = eINSTANCE.getCORERedefineAction();

		/**
         * The meta object literal for the '<em><b>Redefined Action</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_REDEFINE_ACTION__REDEFINED_ACTION = eINSTANCE.getCORERedefineAction_RedefinedAction();

		/**
         * The meta object literal for the '<em><b>Reused Actions</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_REDEFINE_ACTION__REUSED_ACTIONS = eINSTANCE.getCORERedefineAction_ReusedActions();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORELanguageActionImpl <em>CORE Language Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORELanguageActionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguageAction()
         * @generated
         */
		EClass CORE_LANGUAGE_ACTION = eINSTANCE.getCORELanguageAction();

		/**
         * The meta object literal for the '<em><b>Class Qualified Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME = eINSTANCE.getCORELanguageAction_ClassQualifiedName();

		/**
         * The meta object literal for the '<em><b>Method Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_LANGUAGE_ACTION__METHOD_NAME = eINSTANCE.getCORELanguageAction_MethodName();

		/**
         * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ACTION__PARAMETERS = eINSTANCE.getCORELanguageAction_Parameters();

		/**
         * The meta object literal for the '<em><b>Secondary Effects</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS = eINSTANCE.getCORELanguageAction_SecondaryEffects();

		/**
         * The meta object literal for the '<em><b>Action Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_LANGUAGE_ACTION__ACTION_TYPE = eINSTANCE.getCORELanguageAction_ActionType();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORECreateMappingImpl <em>CORE Create Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORECreateMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORECreateMapping()
         * @generated
         */
		EClass CORE_CREATE_MAPPING = eINSTANCE.getCORECreateMapping();

		/**
         * The meta object literal for the '<em><b>Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CREATE_MAPPING__TYPE = eINSTANCE.getCORECreateMapping_Type();

		/**
         * The meta object literal for the '<em><b>Extended Action</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CREATE_MAPPING__EXTENDED_ACTION = eINSTANCE.getCORECreateMapping_ExtendedAction();

		/**
         * The meta object literal for the '<em><b>Model Elements</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_CREATE_MAPPING__MODEL_ELEMENTS = eINSTANCE.getCORECreateMapping_ModelElements();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORELanguageImpl <em>CORE Language</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORELanguageImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguage()
         * @generated
         */
		EClass CORE_LANGUAGE = eINSTANCE.getCORELanguage();

		/**
         * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE__ACTIONS = eINSTANCE.getCORELanguage_Actions();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREActionImpl <em>CORE Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREActionImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREAction()
         * @generated
         */
		EClass CORE_ACTION = eINSTANCE.getCOREAction();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_ACTION__NAME = eINSTANCE.getCOREAction_Name();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CreateModelElementMappingImpl <em>Create Model Element Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CreateModelElementMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCreateModelElementMapping()
         * @generated
         */
		EClass CREATE_MODEL_ELEMENT_MAPPING = eINSTANCE.getCreateModelElementMapping();

		/**
         * The meta object literal for the '<em><b>Reused Actions</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS = eINSTANCE.getCreateModelElementMapping_ReusedActions();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREModelElementMappingImpl <em>CORE Model Element Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREModelElementMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREModelElementMapping()
         * @generated
         */
		EClass CORE_MODEL_ELEMENT_MAPPING = eINSTANCE.getCOREModelElementMapping();

		/**
         * The meta object literal for the '<em><b>Model Elements</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS = eINSTANCE.getCOREModelElementMapping_ModelElements();

		/**
         * The meta object literal for the '<em><b>LE Mid</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_MODEL_ELEMENT_MAPPING__LE_MID = eINSTANCE.getCOREModelElementMapping_LEMid();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.MappingEndImpl <em>Mapping End</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.MappingEndImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getMappingEnd()
         * @generated
         */
		EClass MAPPING_END = eINSTANCE.getMappingEnd();

		/**
         * The meta object literal for the '<em><b>Cardinality</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MAPPING_END__CARDINALITY = eINSTANCE.getMappingEnd_Cardinality();

		/**
         * The meta object literal for the '<em><b>Role Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MAPPING_END__ROLE_NAME = eINSTANCE.getMappingEnd_RoleName();

		/**
         * The meta object literal for the '<em><b>Type</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MAPPING_END__TYPE = eINSTANCE.getMappingEnd_Type();

		/**
         * The meta object literal for the '<em><b>Language Element</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MAPPING_END__LANGUAGE_ELEMENT = eINSTANCE.getMappingEnd_LanguageElement();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.LanguageMapImpl <em>Language Map</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.LanguageMapImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLanguageMap()
         * @generated
         */
		EClass LANGUAGE_MAP = eINSTANCE.getLanguageMap();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute LANGUAGE_MAP__KEY = eINSTANCE.getLanguageMap_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LANGUAGE_MAP__VALUE = eINSTANCE.getLanguageMap_Value();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.ArtefactMapImpl <em>Artefact Map</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.ArtefactMapImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getArtefactMap()
         * @generated
         */
		EClass ARTEFACT_MAP = eINSTANCE.getArtefactMap();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ARTEFACT_MAP__VALUE = eINSTANCE.getArtefactMap_Value();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ARTEFACT_MAP__KEY = eINSTANCE.getArtefactMap_Key();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.ParameterImpl <em>Parameter</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.ParameterImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getParameter()
         * @generated
         */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PARAMETER__NAME = eINSTANCE.getParameter_Name();

		/**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PARAMETER__TYPE = eINSTANCE.getParameter_Type();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl <em>CORE Language Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CORELanguageElementImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORELanguageElement()
         * @generated
         */
		EClass CORE_LANGUAGE_ELEMENT = eINSTANCE.getCORELanguageElement();

		/**
         * The meta object literal for the '<em><b>Language Actions</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS = eINSTANCE.getCORELanguageElement_LanguageActions();

		/**
         * The meta object literal for the '<em><b>Language Element</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT = eINSTANCE.getCORELanguageElement_LanguageElement();

		/**
         * The meta object literal for the '<em><b>Language</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT__LANGUAGE = eINSTANCE.getCORELanguageElement_Language();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute CORE_LANGUAGE_ELEMENT__NAME = eINSTANCE.getCORELanguageElement_Name();

		/**
         * The meta object literal for the '<em><b>Nested Elements</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS = eINSTANCE.getCORELanguageElement_NestedElements();

		/**
         * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CORE_LANGUAGE_ELEMENT__OWNER = eINSTANCE.getCORELanguageElement_Owner();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.ExistingElementEffectImpl <em>Existing Element Effect</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.ExistingElementEffectImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getExistingElementEffect()
         * @generated
         */
		EClass EXISTING_ELEMENT_EFFECT = eINSTANCE.getExistingElementEffect();

		/**
         * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference EXISTING_ELEMENT_EFFECT__PARAMETER = eINSTANCE.getExistingElementEffect_Parameter();

		/**
         * The meta object literal for the '<em><b>Parameter Effect</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT = eINSTANCE.getExistingElementEffect_ParameterEffect();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.DeleteEffectImpl <em>Delete Effect</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.DeleteEffectImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getDeleteEffect()
         * @generated
         */
		EClass DELETE_EFFECT = eINSTANCE.getDeleteEffect();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.UpdateEffectImpl <em>Update Effect</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.UpdateEffectImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getUpdateEffect()
         * @generated
         */
		EClass UPDATE_EFFECT = eINSTANCE.getUpdateEffect();

		/**
         * The meta object literal for the '<em><b>Affected Feature</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference UPDATE_EFFECT__AFFECTED_FEATURE = eINSTANCE.getUpdateEffect_AffectedFeature();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.NavigationMappingImpl <em>Navigation Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.NavigationMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getNavigationMapping()
         * @generated
         */
		EClass NAVIGATION_MAPPING = eINSTANCE.getNavigationMapping();

		/**
         * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute NAVIGATION_MAPPING__ACTIVE = eINSTANCE.getNavigationMapping_Active();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.InterLanguageMappingImpl <em>Inter Language Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.InterLanguageMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getInterLanguageMapping()
         * @generated
         */
		EClass INTER_LANGUAGE_MAPPING = eINSTANCE.getInterLanguageMapping();

		/**
         * The meta object literal for the '<em><b>Default</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTER_LANGUAGE_MAPPING__DEFAULT = eINSTANCE.getInterLanguageMapping_Default();

		/**
         * The meta object literal for the '<em><b>Core Language Element Mapping</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING = eINSTANCE.getInterLanguageMapping_CoreLanguageElementMapping();

		/**
         * The meta object literal for the '<em><b>Inter Language Mapping Ends</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS = eINSTANCE.getInterLanguageMapping_InterLanguageMappingEnds();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl <em>Intra Language Mapping</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.IntraLanguageMappingImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getIntraLanguageMapping()
         * @generated
         */
		EClass INTRA_LANGUAGE_MAPPING = eINSTANCE.getIntraLanguageMapping();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTRA_LANGUAGE_MAPPING__NAME = eINSTANCE.getIntraLanguageMapping_Name();

		/**
         * The meta object literal for the '<em><b>Closure</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTRA_LANGUAGE_MAPPING__CLOSURE = eINSTANCE.getIntraLanguageMapping_Closure();

		/**
         * The meta object literal for the '<em><b>Reuse</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTRA_LANGUAGE_MAPPING__REUSE = eINSTANCE.getIntraLanguageMapping_Reuse();

		/**
         * The meta object literal for the '<em><b>Increase Depth</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH = eINSTANCE.getIntraLanguageMapping_IncreaseDepth();

		/**
         * The meta object literal for the '<em><b>Change Model</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTRA_LANGUAGE_MAPPING__CHANGE_MODEL = eINSTANCE.getIntraLanguageMapping_ChangeModel();

		/**
         * The meta object literal for the '<em><b>From</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference INTRA_LANGUAGE_MAPPING__FROM = eINSTANCE.getIntraLanguageMapping_From();

		/**
         * The meta object literal for the '<em><b>Hops</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference INTRA_LANGUAGE_MAPPING__HOPS = eINSTANCE.getIntraLanguageMapping_Hops();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl <em>Inter Language Mapping End</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getInterLanguageMappingEnd()
         * @generated
         */
		EClass INTER_LANGUAGE_MAPPING_END = eINSTANCE.getInterLanguageMappingEnd();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTER_LANGUAGE_MAPPING_END__NAME = eINSTANCE.getInterLanguageMappingEnd_Name();

		/**
         * The meta object literal for the '<em><b>Origin</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTER_LANGUAGE_MAPPING_END__ORIGIN = eINSTANCE.getInterLanguageMappingEnd_Origin();

		/**
         * The meta object literal for the '<em><b>Destination</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute INTER_LANGUAGE_MAPPING_END__DESTINATION = eINSTANCE.getInterLanguageMappingEnd_Destination();

		/**
         * The meta object literal for the '<em><b>Mapping End</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference INTER_LANGUAGE_MAPPING_END__MAPPING_END = eINSTANCE.getInterLanguageMappingEnd_MappingEnd();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.CreateEffectImpl <em>Create Effect</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.CreateEffectImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCreateEffect()
         * @generated
         */
		EClass CREATE_EFFECT = eINSTANCE.getCreateEffect();

		/**
         * The meta object literal for the '<em><b>Corelanguage Element</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CREATE_EFFECT__CORELANGUAGE_ELEMENT = eINSTANCE.getCreateEffect_CorelanguageElement();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.ActionEffectImpl <em>Action Effect</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.ActionEffectImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getActionEffect()
         * @generated
         */
		EClass ACTION_EFFECT = eINSTANCE.getActionEffect();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.impl.COREReuseArtefactImpl <em>CORE Reuse Artefact</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.impl.COREReuseArtefactImpl
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREReuseArtefact()
         * @generated
         */
        EClass CORE_REUSE_ARTEFACT = eINSTANCE.getCOREReuseArtefact();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.core.COREFeatureRelationshipType <em>CORE Feature Relationship Type</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.COREFeatureRelationshipType
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREFeatureRelationshipType()
         * @generated
         */
		EEnum CORE_FEATURE_RELATIONSHIP_TYPE = eINSTANCE.getCOREFeatureRelationshipType();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.COREVisibilityType <em>CORE Visibility Type</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.COREVisibilityType
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREVisibilityType()
         * @generated
         */
		EEnum CORE_VISIBILITY_TYPE = eINSTANCE.getCOREVisibilityType();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.COREPartialityType <em>CORE Partiality Type</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.COREPartialityType
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCOREPartialityType()
         * @generated
         */
		EEnum CORE_PARTIALITY_TYPE = eINSTANCE.getCOREPartialityType();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.CORERelationship <em>CORE Relationship</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.CORERelationship
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCORERelationship()
         * @generated
         */
		EEnum CORE_RELATIONSHIP = eINSTANCE.getCORERelationship();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.Cardinality <em>Cardinality</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.Cardinality
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getCardinality()
         * @generated
         */
		EEnum CARDINALITY = eINSTANCE.getCardinality();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.LanguageActionType <em>Language Action Type</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.LanguageActionType
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getLanguageActionType()
         * @generated
         */
		EEnum LANGUAGE_ACTION_TYPE = eINSTANCE.getLanguageActionType();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.core.ParameterEffect <em>Parameter Effect</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.core.ParameterEffect
         * @see ca.mcgill.sel.core.impl.CorePackageImpl#getParameterEffect()
         * @generated
         */
		EEnum PARAMETER_EFFECT = eINSTANCE.getParameterEffect();

	}

} //CorePackage
