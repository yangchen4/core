/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Reuse</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREReuse#getReusedConcern <em>Reused Concern</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREReuse#getExtends <em>Extends</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREReuse#getModelReuses <em>Model Reuses</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREReuse()
 * @model
 * @generated
 */
public interface COREReuse extends CORENamedElement {
	/**
     * Returns the value of the '<em><b>Reused Concern</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reused Concern</em>' reference.
     * @see #setReusedConcern(COREConcern)
     * @see ca.mcgill.sel.core.CorePackage#getCOREReuse_ReusedConcern()
     * @model required="true"
     * @generated
     */
	COREConcern getReusedConcern();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREReuse#getReusedConcern <em>Reused Concern</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Reused Concern</em>' reference.
     * @see #getReusedConcern()
     * @generated
     */
	void setReusedConcern(COREConcern value);

	/**
     * Returns the value of the '<em><b>Extends</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Extends</em>' reference.
     * @see #setExtends(COREReuse)
     * @see ca.mcgill.sel.core.CorePackage#getCOREReuse_Extends()
     * @model
     * @generated
     */
	COREReuse getExtends();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREReuse#getExtends <em>Extends</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Extends</em>' reference.
     * @see #getExtends()
     * @generated
     */
	void setExtends(COREReuse value);

	/**
     * Returns the value of the '<em><b>Model Reuses</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREModelReuse}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREModelReuse#getReuse <em>Reuse</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Reuses</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREReuse_ModelReuses()
     * @see ca.mcgill.sel.core.COREModelReuse#getReuse
     * @model opposite="reuse"
     * @generated
     */
	EList<COREModelReuse> getModelReuses();

} // COREReuse
