/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Feature Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREFeatureModel#getFeatures <em>Features</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREFeatureModel#getRoot <em>Root</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREFeatureModel()
 * @model
 * @generated
 */
public interface COREFeatureModel extends COREArtefact {
	/**
     * Returns the value of the '<em><b>Features</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREFeature}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Features</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREFeatureModel_Features()
     * @model containment="true"
     * @generated
     */
	EList<COREFeature> getFeatures();

	/**
     * Returns the value of the '<em><b>Root</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Root</em>' reference.
     * @see #setRoot(COREFeature)
     * @see ca.mcgill.sel.core.CorePackage#getCOREFeatureModel_Root()
     * @model required="true"
     * @generated
     */
	COREFeature getRoot();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREFeatureModel#getRoot <em>Root</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Root</em>' reference.
     * @see #getRoot()
     * @generated
     */
	void setRoot(COREFeature value);

} // COREFeatureModel
