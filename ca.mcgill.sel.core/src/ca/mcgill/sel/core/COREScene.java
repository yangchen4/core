/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Scene</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREScene#getRealizes <em>Realizes</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREScene#getPerspectiveName <em>Perspective Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREScene#getElementMappings <em>Element Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREScene#getArtefacts <em>Artefacts</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREScene()
 * @model
 * @generated
 */
public interface COREScene extends CORENamedElement {
	/**
     * Returns the value of the '<em><b>Realizes</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREFeature}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREFeature#getRealizedBy <em>Realized By</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Realizes</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREScene_Realizes()
     * @see ca.mcgill.sel.core.COREFeature#getRealizedBy
     * @model opposite="realizedBy"
     * @generated
     */
	EList<COREFeature> getRealizes();

	/**
     * Returns the value of the '<em><b>Perspective Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Perspective Name</em>' attribute.
     * @see #setPerspectiveName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREScene_PerspectiveName()
     * @model
     * @generated
     */
	String getPerspectiveName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREScene#getPerspectiveName <em>Perspective Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Perspective Name</em>' attribute.
     * @see #getPerspectiveName()
     * @generated
     */
	void setPerspectiveName(String value);

	/**
     * Returns the value of the '<em><b>Element Mappings</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREModelElementMapping}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Element Mappings</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREScene_ElementMappings()
     * @model containment="true"
     * @generated
     */
	EList<COREModelElementMapping> getElementMappings();

	/**
     * Returns the value of the '<em><b>Artefacts</b></em>' map.
     * The key is of type {@link java.lang.String},
     * and the value is of type list of {@link ca.mcgill.sel.core.COREArtefact},
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Artefacts</em>' map.
     * @see ca.mcgill.sel.core.CorePackage#getCOREScene_Artefacts()
     * @model mapType="ca.mcgill.sel.core.ArtefactMap&lt;org.eclipse.emf.ecore.EString, ca.mcgill.sel.core.COREArtefact&gt;"
     * @generated
     */
	EMap<String, EList<COREArtefact>> getArtefacts();

} // COREScene
