/**
 */
package ca.mcgill.sel.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Cardinality</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.core.CorePackage#getCardinality()
 * @model
 * @generated
 */
public enum Cardinality implements Enumerator {
	/**
     * The '<em><b>Compulsory</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #COMPULSORY_VALUE
     * @generated
     * @ordered
     */
	COMPULSORY(0, "compulsory", "compulsory"),

	/**
     * The '<em><b>Optional</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #OPTIONAL_VALUE
     * @generated
     * @ordered
     */
	OPTIONAL(1, "optional", "optional"),

	/**
     * The '<em><b>Compulsory Multiple</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #COMPULSORY_MULTIPLE_VALUE
     * @generated
     * @ordered
     */
	COMPULSORY_MULTIPLE(2, "compulsoryMultiple", "compulsoryMultiple"),

	/**
     * The '<em><b>Optional Multiple</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #OPTIONAL_MULTIPLE_VALUE
     * @generated
     * @ordered
     */
	OPTIONAL_MULTIPLE(3, "optionalMultiple", "optionalMultiple");

	/**
     * The '<em><b>Compulsory</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #COMPULSORY
     * @model name="compulsory"
     * @generated
     * @ordered
     */
	public static final int COMPULSORY_VALUE = 0;

	/**
     * The '<em><b>Optional</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #OPTIONAL
     * @model name="optional"
     * @generated
     * @ordered
     */
	public static final int OPTIONAL_VALUE = 1;

	/**
     * The '<em><b>Compulsory Multiple</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #COMPULSORY_MULTIPLE
     * @model name="compulsoryMultiple"
     * @generated
     * @ordered
     */
	public static final int COMPULSORY_MULTIPLE_VALUE = 2;

	/**
     * The '<em><b>Optional Multiple</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #OPTIONAL_MULTIPLE
     * @model name="optionalMultiple"
     * @generated
     * @ordered
     */
	public static final int OPTIONAL_MULTIPLE_VALUE = 3;

	/**
     * An array of all the '<em><b>Cardinality</b></em>' enumerators.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static final Cardinality[] VALUES_ARRAY =
		new Cardinality[] {
            COMPULSORY,
            OPTIONAL,
            COMPULSORY_MULTIPLE,
            OPTIONAL_MULTIPLE,
        };

	/**
     * A public read-only list of all the '<em><b>Cardinality</b></em>' enumerators.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static final List<Cardinality> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
     * Returns the '<em><b>Cardinality</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static Cardinality get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            Cardinality result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

	/**
     * Returns the '<em><b>Cardinality</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static Cardinality getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            Cardinality result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

	/**
     * Returns the '<em><b>Cardinality</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static Cardinality get(int value) {
        switch (value) {
            case COMPULSORY_VALUE: return COMPULSORY;
            case OPTIONAL_VALUE: return OPTIONAL;
            case COMPULSORY_MULTIPLE_VALUE: return COMPULSORY_MULTIPLE;
            case OPTIONAL_MULTIPLE_VALUE: return OPTIONAL_MULTIPLE;
        }
        return null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final int value;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final String name;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final String literal;

	/**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private Cardinality(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getValue() {
      return value;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
      return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getLiteral() {
      return literal;
    }

	/**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        return literal;
    }
	
} //Cardinality
