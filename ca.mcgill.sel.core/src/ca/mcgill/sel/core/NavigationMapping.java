/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Navigation Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.NavigationMapping#isActive <em>Active</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getNavigationMapping()
 * @model
 * @generated
 */
public interface NavigationMapping extends EObject {
	/**
     * Returns the value of the '<em><b>Active</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Active</em>' attribute.
     * @see #setActive(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getNavigationMapping_Active()
     * @model
     * @generated
     */
	boolean isActive();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.NavigationMapping#isActive <em>Active</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Active</em>' attribute.
     * @see #isActive()
     * @generated
     */
	void setActive(boolean value);

} // NavigationMapping
