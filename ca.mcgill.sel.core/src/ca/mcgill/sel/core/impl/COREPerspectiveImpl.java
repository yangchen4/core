/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.NavigationMapping;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Perspective</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREPerspectiveImpl#getMappings <em>Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREPerspectiveImpl#getLanguages <em>Languages</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREPerspectiveImpl#getDefault <em>Default</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREPerspectiveImpl#getNavigationMappings <em>Navigation Mappings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREPerspectiveImpl extends CORELanguageImpl implements COREPerspective {
	/**
     * The cached value of the '{@link #getMappings() <em>Mappings</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMappings()
     * @generated
     * @ordered
     */
	protected EList<CORELanguageElementMapping> mappings;

	/**
     * The cached value of the '{@link #getLanguages() <em>Languages</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLanguages()
     * @generated
     * @ordered
     */
	protected EMap<String, CORELanguage> languages;

	/**
     * The default value of the '{@link #getDefault() <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDefault()
     * @generated
     * @ordered
     */
	protected static final String DEFAULT_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getDefault() <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDefault()
     * @generated
     * @ordered
     */
	protected String default_ = DEFAULT_EDEFAULT;

	/**
     * The cached value of the '{@link #getNavigationMappings() <em>Navigation Mappings</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getNavigationMappings()
     * @generated
     * @ordered
     */
	protected EList<NavigationMapping> navigationMappings;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREPerspectiveImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_PERSPECTIVE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<CORELanguageElementMapping> getMappings() {
        if (mappings == null) {
            mappings = new EObjectContainmentEList<CORELanguageElementMapping>(CORELanguageElementMapping.class, this, CorePackage.CORE_PERSPECTIVE__MAPPINGS);
        }
        return mappings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<String, CORELanguage> getLanguages() {
        if (languages == null) {
            languages = new EcoreEMap<String,CORELanguage>(CorePackage.Literals.LANGUAGE_MAP, LanguageMapImpl.class, this, CorePackage.CORE_PERSPECTIVE__LANGUAGES);
        }
        return languages;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getDefault() {
        return default_;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDefault(String newDefault) {
        String oldDefault = default_;
        default_ = newDefault;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_PERSPECTIVE__DEFAULT, oldDefault, default_));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<NavigationMapping> getNavigationMappings() {
        if (navigationMappings == null) {
            navigationMappings = new EObjectContainmentEList<NavigationMapping>(NavigationMapping.class, this, CorePackage.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS);
        }
        return navigationMappings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE__MAPPINGS:
                return ((InternalEList<?>)getMappings()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_PERSPECTIVE__LANGUAGES:
                return ((InternalEList<?>)getLanguages()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS:
                return ((InternalEList<?>)getNavigationMappings()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE__MAPPINGS:
                return getMappings();
            case CorePackage.CORE_PERSPECTIVE__LANGUAGES:
                if (coreType) return getLanguages();
                else return getLanguages().map();
            case CorePackage.CORE_PERSPECTIVE__DEFAULT:
                return getDefault();
            case CorePackage.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS:
                return getNavigationMappings();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE__MAPPINGS:
                getMappings().clear();
                getMappings().addAll((Collection<? extends CORELanguageElementMapping>)newValue);
                return;
            case CorePackage.CORE_PERSPECTIVE__LANGUAGES:
                ((EStructuralFeature.Setting)getLanguages()).set(newValue);
                return;
            case CorePackage.CORE_PERSPECTIVE__DEFAULT:
                setDefault((String)newValue);
                return;
            case CorePackage.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS:
                getNavigationMappings().clear();
                getNavigationMappings().addAll((Collection<? extends NavigationMapping>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE__MAPPINGS:
                getMappings().clear();
                return;
            case CorePackage.CORE_PERSPECTIVE__LANGUAGES:
                getLanguages().clear();
                return;
            case CorePackage.CORE_PERSPECTIVE__DEFAULT:
                setDefault(DEFAULT_EDEFAULT);
                return;
            case CorePackage.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS:
                getNavigationMappings().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE__MAPPINGS:
                return mappings != null && !mappings.isEmpty();
            case CorePackage.CORE_PERSPECTIVE__LANGUAGES:
                return languages != null && !languages.isEmpty();
            case CorePackage.CORE_PERSPECTIVE__DEFAULT:
                return DEFAULT_EDEFAULT == null ? default_ != null : !DEFAULT_EDEFAULT.equals(default_);
            case CorePackage.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS:
                return navigationMappings != null && !navigationMappings.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (default: ");
        result.append(default_);
        result.append(')');
        return result.toString();
    }

} //COREPerspectiveImpl
