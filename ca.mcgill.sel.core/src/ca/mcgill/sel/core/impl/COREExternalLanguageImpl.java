/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE External Language</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl#getNsURI <em>Ns URI</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl#getResourceFactory <em>Resource Factory</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl#getAdapterFactory <em>Adapter Factory</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl#getWeaverClassName <em>Weaver Class Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl#getFileExtension <em>File Extension</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl#getLanguageElements <em>Language Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalLanguageImpl#getModelUtilClassName <em>Model Util Class Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREExternalLanguageImpl extends CORELanguageImpl implements COREExternalLanguage {
	/**
     * The default value of the '{@link #getNsURI() <em>Ns URI</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getNsURI()
     * @generated
     * @ordered
     */
	protected static final String NS_URI_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getNsURI() <em>Ns URI</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getNsURI()
     * @generated
     * @ordered
     */
	protected String nsURI = NS_URI_EDEFAULT;

	/**
     * The default value of the '{@link #getResourceFactory() <em>Resource Factory</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getResourceFactory()
     * @generated
     * @ordered
     */
	protected static final String RESOURCE_FACTORY_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getResourceFactory() <em>Resource Factory</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getResourceFactory()
     * @generated
     * @ordered
     */
	protected String resourceFactory = RESOURCE_FACTORY_EDEFAULT;

	/**
     * The default value of the '{@link #getAdapterFactory() <em>Adapter Factory</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAdapterFactory()
     * @generated
     * @ordered
     */
	protected static final String ADAPTER_FACTORY_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getAdapterFactory() <em>Adapter Factory</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAdapterFactory()
     * @generated
     * @ordered
     */
	protected String adapterFactory = ADAPTER_FACTORY_EDEFAULT;

	/**
     * The default value of the '{@link #getWeaverClassName() <em>Weaver Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getWeaverClassName()
     * @generated
     * @ordered
     */
	protected static final String WEAVER_CLASS_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getWeaverClassName() <em>Weaver Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getWeaverClassName()
     * @generated
     * @ordered
     */
	protected String weaverClassName = WEAVER_CLASS_NAME_EDEFAULT;

	/**
     * The default value of the '{@link #getFileExtension() <em>File Extension</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getFileExtension()
     * @generated
     * @ordered
     */
	protected static final String FILE_EXTENSION_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getFileExtension() <em>File Extension</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getFileExtension()
     * @generated
     * @ordered
     */
	protected String fileExtension = FILE_EXTENSION_EDEFAULT;

	/**
     * The cached value of the '{@link #getLanguageElements() <em>Language Elements</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLanguageElements()
     * @generated
     * @ordered
     */
	protected EList<CORELanguageElement> languageElements;

	/**
     * The default value of the '{@link #getModelUtilClassName() <em>Model Util Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelUtilClassName()
     * @generated
     * @ordered
     */
	protected static final String MODEL_UTIL_CLASS_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getModelUtilClassName() <em>Model Util Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelUtilClassName()
     * @generated
     * @ordered
     */
	protected String modelUtilClassName = MODEL_UTIL_CLASS_NAME_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREExternalLanguageImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_EXTERNAL_LANGUAGE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getNsURI() {
        return nsURI;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setNsURI(String newNsURI) {
        String oldNsURI = nsURI;
        nsURI = newNsURI;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_LANGUAGE__NS_URI, oldNsURI, nsURI));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getResourceFactory() {
        return resourceFactory;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setResourceFactory(String newResourceFactory) {
        String oldResourceFactory = resourceFactory;
        resourceFactory = newResourceFactory;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY, oldResourceFactory, resourceFactory));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getAdapterFactory() {
        return adapterFactory;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setAdapterFactory(String newAdapterFactory) {
        String oldAdapterFactory = adapterFactory;
        adapterFactory = newAdapterFactory;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY, oldAdapterFactory, adapterFactory));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getWeaverClassName() {
        return weaverClassName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setWeaverClassName(String newWeaverClassName) {
        String oldWeaverClassName = weaverClassName;
        weaverClassName = newWeaverClassName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME, oldWeaverClassName, weaverClassName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getFileExtension() {
        return fileExtension;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setFileExtension(String newFileExtension) {
        String oldFileExtension = fileExtension;
        fileExtension = newFileExtension;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION, oldFileExtension, fileExtension));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<CORELanguageElement> getLanguageElements() {
        if (languageElements == null) {
            languageElements = new EObjectContainmentWithInverseEList<CORELanguageElement>(CORELanguageElement.class, this, CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS, CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE);
        }
        return languageElements;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getModelUtilClassName() {
        return modelUtilClassName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setModelUtilClassName(String newModelUtilClassName) {
        String oldModelUtilClassName = modelUtilClassName;
        modelUtilClassName = newModelUtilClassName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME, oldModelUtilClassName, modelUtilClassName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getLanguageElements()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS:
                return ((InternalEList<?>)getLanguageElements()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_LANGUAGE__NS_URI:
                return getNsURI();
            case CorePackage.CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY:
                return getResourceFactory();
            case CorePackage.CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY:
                return getAdapterFactory();
            case CorePackage.CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME:
                return getWeaverClassName();
            case CorePackage.CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION:
                return getFileExtension();
            case CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS:
                return getLanguageElements();
            case CorePackage.CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME:
                return getModelUtilClassName();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_LANGUAGE__NS_URI:
                setNsURI((String)newValue);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY:
                setResourceFactory((String)newValue);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY:
                setAdapterFactory((String)newValue);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME:
                setWeaverClassName((String)newValue);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION:
                setFileExtension((String)newValue);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS:
                getLanguageElements().clear();
                getLanguageElements().addAll((Collection<? extends CORELanguageElement>)newValue);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME:
                setModelUtilClassName((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_LANGUAGE__NS_URI:
                setNsURI(NS_URI_EDEFAULT);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY:
                setResourceFactory(RESOURCE_FACTORY_EDEFAULT);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY:
                setAdapterFactory(ADAPTER_FACTORY_EDEFAULT);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME:
                setWeaverClassName(WEAVER_CLASS_NAME_EDEFAULT);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION:
                setFileExtension(FILE_EXTENSION_EDEFAULT);
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS:
                getLanguageElements().clear();
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME:
                setModelUtilClassName(MODEL_UTIL_CLASS_NAME_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_LANGUAGE__NS_URI:
                return NS_URI_EDEFAULT == null ? nsURI != null : !NS_URI_EDEFAULT.equals(nsURI);
            case CorePackage.CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY:
                return RESOURCE_FACTORY_EDEFAULT == null ? resourceFactory != null : !RESOURCE_FACTORY_EDEFAULT.equals(resourceFactory);
            case CorePackage.CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY:
                return ADAPTER_FACTORY_EDEFAULT == null ? adapterFactory != null : !ADAPTER_FACTORY_EDEFAULT.equals(adapterFactory);
            case CorePackage.CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME:
                return WEAVER_CLASS_NAME_EDEFAULT == null ? weaverClassName != null : !WEAVER_CLASS_NAME_EDEFAULT.equals(weaverClassName);
            case CorePackage.CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION:
                return FILE_EXTENSION_EDEFAULT == null ? fileExtension != null : !FILE_EXTENSION_EDEFAULT.equals(fileExtension);
            case CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS:
                return languageElements != null && !languageElements.isEmpty();
            case CorePackage.CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME:
                return MODEL_UTIL_CLASS_NAME_EDEFAULT == null ? modelUtilClassName != null : !MODEL_UTIL_CLASS_NAME_EDEFAULT.equals(modelUtilClassName);
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (nsURI: ");
        result.append(nsURI);
        result.append(", resourceFactory: ");
        result.append(resourceFactory);
        result.append(", adapterFactory: ");
        result.append(adapterFactory);
        result.append(", weaverClassName: ");
        result.append(weaverClassName);
        result.append(", fileExtension: ");
        result.append(fileExtension);
        result.append(", modelUtilClassName: ");
        result.append(modelUtilClassName);
        result.append(')');
        return result.toString();
    }

} //COREExternalLanguageImpl
