/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORECI Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORECIElementImpl#getPartiality <em>Partiality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORECIElementImpl#getModelElement <em>Model Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORECIElementImpl#getMappingCardinality <em>Mapping Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORECIElementImpl#getReferenceCardinality <em>Reference Cardinality</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORECIElementImpl extends EObjectImpl implements CORECIElement {
	/**
     * The default value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPartiality()
     * @generated
     * @ordered
     */
	protected static final COREPartialityType PARTIALITY_EDEFAULT = COREPartialityType.NONE;

	/**
     * The cached value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPartiality()
     * @generated
     * @ordered
     */
	protected COREPartialityType partiality = PARTIALITY_EDEFAULT;

	/**
     * The cached value of the '{@link #getModelElement() <em>Model Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelElement()
     * @generated
     * @ordered
     */
	protected EObject modelElement;

	/**
     * The cached value of the '{@link #getMappingCardinality() <em>Mapping Cardinality</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMappingCardinality()
     * @generated
     * @ordered
     */
	protected COREMappingCardinality mappingCardinality;

	/**
     * The cached value of the '{@link #getReferenceCardinality() <em>Reference Cardinality</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReferenceCardinality()
     * @generated
     * @ordered
     */
	protected EList<COREMappingCardinality> referenceCardinality;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CORECIElementImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORECI_ELEMENT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREPartialityType getPartiality() {
        return partiality;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPartiality(COREPartialityType newPartiality) {
        COREPartialityType oldPartiality = partiality;
        partiality = newPartiality == null ? PARTIALITY_EDEFAULT : newPartiality;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORECI_ELEMENT__PARTIALITY, oldPartiality, partiality));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject getModelElement() {
        if (modelElement != null && modelElement.eIsProxy()) {
            InternalEObject oldModelElement = (InternalEObject)modelElement;
            modelElement = eResolveProxy(oldModelElement);
            if (modelElement != oldModelElement) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORECI_ELEMENT__MODEL_ELEMENT, oldModelElement, modelElement));
            }
        }
        return modelElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EObject basicGetModelElement() {
        return modelElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setModelElement(EObject newModelElement) {
        EObject oldModelElement = modelElement;
        modelElement = newModelElement;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORECI_ELEMENT__MODEL_ELEMENT, oldModelElement, modelElement));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREMappingCardinality getMappingCardinality() {
        return mappingCardinality;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetMappingCardinality(COREMappingCardinality newMappingCardinality, NotificationChain msgs) {
        COREMappingCardinality oldMappingCardinality = mappingCardinality;
        mappingCardinality = newMappingCardinality;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY, oldMappingCardinality, newMappingCardinality);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMappingCardinality(COREMappingCardinality newMappingCardinality) {
        if (newMappingCardinality != mappingCardinality) {
            NotificationChain msgs = null;
            if (mappingCardinality != null)
                msgs = ((InternalEObject)mappingCardinality).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY, null, msgs);
            if (newMappingCardinality != null)
                msgs = ((InternalEObject)newMappingCardinality).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY, null, msgs);
            msgs = basicSetMappingCardinality(newMappingCardinality, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY, newMappingCardinality, newMappingCardinality));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREMappingCardinality> getReferenceCardinality() {
        if (referenceCardinality == null) {
            referenceCardinality = new EObjectResolvingEList<COREMappingCardinality>(COREMappingCardinality.class, this, CorePackage.CORECI_ELEMENT__REFERENCE_CARDINALITY);
        }
        return referenceCardinality;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY:
                return basicSetMappingCardinality(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORECI_ELEMENT__PARTIALITY:
                return getPartiality();
            case CorePackage.CORECI_ELEMENT__MODEL_ELEMENT:
                if (resolve) return getModelElement();
                return basicGetModelElement();
            case CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY:
                return getMappingCardinality();
            case CorePackage.CORECI_ELEMENT__REFERENCE_CARDINALITY:
                return getReferenceCardinality();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORECI_ELEMENT__PARTIALITY:
                setPartiality((COREPartialityType)newValue);
                return;
            case CorePackage.CORECI_ELEMENT__MODEL_ELEMENT:
                setModelElement((EObject)newValue);
                return;
            case CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY:
                setMappingCardinality((COREMappingCardinality)newValue);
                return;
            case CorePackage.CORECI_ELEMENT__REFERENCE_CARDINALITY:
                getReferenceCardinality().clear();
                getReferenceCardinality().addAll((Collection<? extends COREMappingCardinality>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORECI_ELEMENT__PARTIALITY:
                setPartiality(PARTIALITY_EDEFAULT);
                return;
            case CorePackage.CORECI_ELEMENT__MODEL_ELEMENT:
                setModelElement((EObject)null);
                return;
            case CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY:
                setMappingCardinality((COREMappingCardinality)null);
                return;
            case CorePackage.CORECI_ELEMENT__REFERENCE_CARDINALITY:
                getReferenceCardinality().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORECI_ELEMENT__PARTIALITY:
                return partiality != PARTIALITY_EDEFAULT;
            case CorePackage.CORECI_ELEMENT__MODEL_ELEMENT:
                return modelElement != null;
            case CorePackage.CORECI_ELEMENT__MAPPING_CARDINALITY:
                return mappingCardinality != null;
            case CorePackage.CORECI_ELEMENT__REFERENCE_CARDINALITY:
                return referenceCardinality != null && !referenceCardinality.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (partiality: ");
        result.append(partiality);
        result.append(')');
        return result.toString();
    }

} //CORECIElementImpl
