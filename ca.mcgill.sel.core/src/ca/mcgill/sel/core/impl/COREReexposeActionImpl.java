/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Reexpose Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREReexposeActionImpl#getReexposedAction <em>Reexposed Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREReexposeActionImpl extends COREPerspectiveActionImpl implements COREReexposeAction {
	/**
     * The cached value of the '{@link #getReexposedAction() <em>Reexposed Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReexposedAction()
     * @generated
     * @ordered
     */
	protected COREAction reexposedAction;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREReexposeActionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_REEXPOSE_ACTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREAction getReexposedAction() {
        if (reexposedAction != null && reexposedAction.eIsProxy()) {
            InternalEObject oldReexposedAction = (InternalEObject)reexposedAction;
            reexposedAction = (COREAction)eResolveProxy(oldReexposedAction);
            if (reexposedAction != oldReexposedAction) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_REEXPOSE_ACTION__REEXPOSED_ACTION, oldReexposedAction, reexposedAction));
            }
        }
        return reexposedAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREAction basicGetReexposedAction() {
        return reexposedAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setReexposedAction(COREAction newReexposedAction) {
        COREAction oldReexposedAction = reexposedAction;
        reexposedAction = newReexposedAction;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_REEXPOSE_ACTION__REEXPOSED_ACTION, oldReexposedAction, reexposedAction));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_REEXPOSE_ACTION__REEXPOSED_ACTION:
                if (resolve) return getReexposedAction();
                return basicGetReexposedAction();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_REEXPOSE_ACTION__REEXPOSED_ACTION:
                setReexposedAction((COREAction)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_REEXPOSE_ACTION__REEXPOSED_ACTION:
                setReexposedAction((COREAction)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_REEXPOSE_ACTION__REEXPOSED_ACTION:
                return reexposedAction != null;
        }
        return super.eIsSet(featureID);
    }

} //COREReexposeActionImpl
