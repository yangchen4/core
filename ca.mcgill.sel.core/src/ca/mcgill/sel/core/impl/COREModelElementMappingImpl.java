/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Model Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelElementMappingImpl#getModelElements <em>Model Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelElementMappingImpl#getLEMid <em>LE Mid</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREModelElementMappingImpl extends EObjectImpl implements COREModelElementMapping {
	/**
     * The cached value of the '{@link #getModelElements() <em>Model Elements</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelElements()
     * @generated
     * @ordered
     */
	protected EList<EObject> modelElements;

	/**
     * The default value of the '{@link #getLEMid() <em>LE Mid</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLEMid()
     * @generated
     * @ordered
     */
	protected static final int LE_MID_EDEFAULT = 0;

	/**
     * The cached value of the '{@link #getLEMid() <em>LE Mid</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLEMid()
     * @generated
     * @ordered
     */
	protected int leMid = LE_MID_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREModelElementMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MODEL_ELEMENT_MAPPING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<EObject> getModelElements() {
        if (modelElements == null) {
            modelElements = new EObjectResolvingEList<EObject>(EObject.class, this, CorePackage.CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS);
        }
        return modelElements;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getLEMid() {
        return leMid;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setLEMid(int newLEMid) {
        int oldLEMid = leMid;
        leMid = newLEMid;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_ELEMENT_MAPPING__LE_MID, oldLEMid, leMid));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS:
                return getModelElements();
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__LE_MID:
                return getLEMid();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS:
                getModelElements().clear();
                getModelElements().addAll((Collection<? extends EObject>)newValue);
                return;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__LE_MID:
                setLEMid((Integer)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS:
                getModelElements().clear();
                return;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__LE_MID:
                setLEMid(LE_MID_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS:
                return modelElements != null && !modelElements.isEmpty();
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__LE_MID:
                return leMid != LE_MID_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (LEMid: ");
        result.append(leMid);
        result.append(')');
        return result.toString();
    }

} //COREModelElementMappingImpl
