/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.COREUIElement;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Artefact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREArtefactImpl#getModelReuses <em>Model Reuses</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREArtefactImpl#getCoreConcern <em>Core Concern</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREArtefactImpl#getModelExtensions <em>Model Extensions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREArtefactImpl#getUiElements <em>Ui Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREArtefactImpl#getCiElements <em>Ci Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREArtefactImpl#getTemporaryConcern <em>Temporary Concern</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREArtefactImpl#getScene <em>Scene</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class COREArtefactImpl extends CORENamedElementImpl implements COREArtefact {
	/**
     * The cached value of the '{@link #getModelReuses() <em>Model Reuses</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelReuses()
     * @generated
     * @ordered
     */
	protected EList<COREModelReuse> modelReuses;

	/**
     * The cached value of the '{@link #getModelExtensions() <em>Model Extensions</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelExtensions()
     * @generated
     * @ordered
     */
	protected EList<COREModelExtension> modelExtensions;

	/**
     * The cached value of the '{@link #getUiElements() <em>Ui Elements</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getUiElements()
     * @generated
     * @ordered
     */
	protected EList<COREUIElement> uiElements;

	/**
     * The cached value of the '{@link #getCiElements() <em>Ci Elements</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCiElements()
     * @generated
     * @ordered
     */
	protected EList<CORECIElement> ciElements;

	/**
     * The cached value of the '{@link #getTemporaryConcern() <em>Temporary Concern</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTemporaryConcern()
     * @generated
     * @ordered
     */
	protected COREConcern temporaryConcern;

	/**
     * The cached value of the '{@link #getScene() <em>Scene</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getScene()
     * @generated
     * @ordered
     */
	protected COREScene scene;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREArtefactImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_ARTEFACT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREModelReuse> getModelReuses() {
        if (modelReuses == null) {
            modelReuses = new EObjectContainmentEList<COREModelReuse>(COREModelReuse.class, this, CorePackage.CORE_ARTEFACT__MODEL_REUSES);
        }
        return modelReuses;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREConcern getCoreConcern() {
        if (eContainerFeatureID() != CorePackage.CORE_ARTEFACT__CORE_CONCERN) return null;
        return (COREConcern)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetCoreConcern(COREConcern newCoreConcern, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newCoreConcern, CorePackage.CORE_ARTEFACT__CORE_CONCERN, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setCoreConcern(COREConcern newCoreConcern) {
        if (newCoreConcern != eInternalContainer() || (eContainerFeatureID() != CorePackage.CORE_ARTEFACT__CORE_CONCERN && newCoreConcern != null)) {
            if (EcoreUtil.isAncestor(this, newCoreConcern))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newCoreConcern != null)
                msgs = ((InternalEObject)newCoreConcern).eInverseAdd(this, CorePackage.CORE_CONCERN__ARTEFACTS, COREConcern.class, msgs);
            msgs = basicSetCoreConcern(newCoreConcern, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_ARTEFACT__CORE_CONCERN, newCoreConcern, newCoreConcern));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREModelExtension> getModelExtensions() {
        if (modelExtensions == null) {
            modelExtensions = new EObjectContainmentEList<COREModelExtension>(COREModelExtension.class, this, CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS);
        }
        return modelExtensions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREUIElement> getUiElements() {
        if (uiElements == null) {
            uiElements = new EObjectContainmentEList<COREUIElement>(COREUIElement.class, this, CorePackage.CORE_ARTEFACT__UI_ELEMENTS);
        }
        return uiElements;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<CORECIElement> getCiElements() {
        if (ciElements == null) {
            ciElements = new EObjectContainmentEList<CORECIElement>(CORECIElement.class, this, CorePackage.CORE_ARTEFACT__CI_ELEMENTS);
        }
        return ciElements;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREConcern getTemporaryConcern() {
        if (temporaryConcern != null && temporaryConcern.eIsProxy()) {
            InternalEObject oldTemporaryConcern = (InternalEObject)temporaryConcern;
            temporaryConcern = (COREConcern)eResolveProxy(oldTemporaryConcern);
            if (temporaryConcern != oldTemporaryConcern) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN, oldTemporaryConcern, temporaryConcern));
            }
        }
        return temporaryConcern;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREConcern basicGetTemporaryConcern() {
        return temporaryConcern;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetTemporaryConcern(COREConcern newTemporaryConcern, NotificationChain msgs) {
        COREConcern oldTemporaryConcern = temporaryConcern;
        temporaryConcern = newTemporaryConcern;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN, oldTemporaryConcern, newTemporaryConcern);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setTemporaryConcern(COREConcern newTemporaryConcern) {
        if (newTemporaryConcern != temporaryConcern) {
            NotificationChain msgs = null;
            if (temporaryConcern != null)
                msgs = ((InternalEObject)temporaryConcern).eInverseRemove(this, CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS, COREConcern.class, msgs);
            if (newTemporaryConcern != null)
                msgs = ((InternalEObject)newTemporaryConcern).eInverseAdd(this, CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS, COREConcern.class, msgs);
            msgs = basicSetTemporaryConcern(newTemporaryConcern, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN, newTemporaryConcern, newTemporaryConcern));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREScene getScene() {
        if (scene != null && scene.eIsProxy()) {
            InternalEObject oldScene = (InternalEObject)scene;
            scene = (COREScene)eResolveProxy(oldScene);
            if (scene != oldScene) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_ARTEFACT__SCENE, oldScene, scene));
            }
        }
        return scene;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREScene basicGetScene() {
        return scene;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setScene(COREScene newScene) {
        COREScene oldScene = scene;
        scene = newScene;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_ARTEFACT__SCENE, oldScene, scene));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_ARTEFACT__CORE_CONCERN:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetCoreConcern((COREConcern)otherEnd, msgs);
            case CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN:
                if (temporaryConcern != null)
                    msgs = ((InternalEObject)temporaryConcern).eInverseRemove(this, CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS, COREConcern.class, msgs);
                return basicSetTemporaryConcern((COREConcern)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_ARTEFACT__MODEL_REUSES:
                return ((InternalEList<?>)getModelReuses()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_ARTEFACT__CORE_CONCERN:
                return basicSetCoreConcern(null, msgs);
            case CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS:
                return ((InternalEList<?>)getModelExtensions()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_ARTEFACT__UI_ELEMENTS:
                return ((InternalEList<?>)getUiElements()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_ARTEFACT__CI_ELEMENTS:
                return ((InternalEList<?>)getCiElements()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN:
                return basicSetTemporaryConcern(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID()) {
            case CorePackage.CORE_ARTEFACT__CORE_CONCERN:
                return eInternalContainer().eInverseRemove(this, CorePackage.CORE_CONCERN__ARTEFACTS, COREConcern.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_ARTEFACT__MODEL_REUSES:
                return getModelReuses();
            case CorePackage.CORE_ARTEFACT__CORE_CONCERN:
                return getCoreConcern();
            case CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS:
                return getModelExtensions();
            case CorePackage.CORE_ARTEFACT__UI_ELEMENTS:
                return getUiElements();
            case CorePackage.CORE_ARTEFACT__CI_ELEMENTS:
                return getCiElements();
            case CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN:
                if (resolve) return getTemporaryConcern();
                return basicGetTemporaryConcern();
            case CorePackage.CORE_ARTEFACT__SCENE:
                if (resolve) return getScene();
                return basicGetScene();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_ARTEFACT__MODEL_REUSES:
                getModelReuses().clear();
                getModelReuses().addAll((Collection<? extends COREModelReuse>)newValue);
                return;
            case CorePackage.CORE_ARTEFACT__CORE_CONCERN:
                setCoreConcern((COREConcern)newValue);
                return;
            case CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS:
                getModelExtensions().clear();
                getModelExtensions().addAll((Collection<? extends COREModelExtension>)newValue);
                return;
            case CorePackage.CORE_ARTEFACT__UI_ELEMENTS:
                getUiElements().clear();
                getUiElements().addAll((Collection<? extends COREUIElement>)newValue);
                return;
            case CorePackage.CORE_ARTEFACT__CI_ELEMENTS:
                getCiElements().clear();
                getCiElements().addAll((Collection<? extends CORECIElement>)newValue);
                return;
            case CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN:
                setTemporaryConcern((COREConcern)newValue);
                return;
            case CorePackage.CORE_ARTEFACT__SCENE:
                setScene((COREScene)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_ARTEFACT__MODEL_REUSES:
                getModelReuses().clear();
                return;
            case CorePackage.CORE_ARTEFACT__CORE_CONCERN:
                setCoreConcern((COREConcern)null);
                return;
            case CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS:
                getModelExtensions().clear();
                return;
            case CorePackage.CORE_ARTEFACT__UI_ELEMENTS:
                getUiElements().clear();
                return;
            case CorePackage.CORE_ARTEFACT__CI_ELEMENTS:
                getCiElements().clear();
                return;
            case CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN:
                setTemporaryConcern((COREConcern)null);
                return;
            case CorePackage.CORE_ARTEFACT__SCENE:
                setScene((COREScene)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_ARTEFACT__MODEL_REUSES:
                return modelReuses != null && !modelReuses.isEmpty();
            case CorePackage.CORE_ARTEFACT__CORE_CONCERN:
                return getCoreConcern() != null;
            case CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS:
                return modelExtensions != null && !modelExtensions.isEmpty();
            case CorePackage.CORE_ARTEFACT__UI_ELEMENTS:
                return uiElements != null && !uiElements.isEmpty();
            case CorePackage.CORE_ARTEFACT__CI_ELEMENTS:
                return ciElements != null && !ciElements.isEmpty();
            case CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN:
                return temporaryConcern != null;
            case CorePackage.CORE_ARTEFACT__SCENE:
                return scene != null;
        }
        return super.eIsSet(featureID);
    }

} //COREArtefactImpl
