/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORECreateMapping;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Create Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORECreateMappingImpl#getType <em>Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORECreateMappingImpl#getExtendedAction <em>Extended Action</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORECreateMappingImpl#getModelElements <em>Model Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORECreateMappingImpl extends COREPerspectiveActionImpl implements CORECreateMapping {
	/**
     * The cached value of the '{@link #getType() <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
	protected CORELanguageElementMapping type;

	/**
     * The cached value of the '{@link #getExtendedAction() <em>Extended Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getExtendedAction()
     * @generated
     * @ordered
     */
	protected CORECreateMapping extendedAction;

	/**
     * The cached value of the '{@link #getModelElements() <em>Model Elements</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelElements()
     * @generated
     * @ordered
     */
	protected EList<EObject> modelElements;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CORECreateMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_CREATE_MAPPING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElementMapping getType() {
        if (type != null && type.eIsProxy()) {
            InternalEObject oldType = (InternalEObject)type;
            type = (CORELanguageElementMapping)eResolveProxy(oldType);
            if (type != oldType) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_CREATE_MAPPING__TYPE, oldType, type));
            }
        }
        return type;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CORELanguageElementMapping basicGetType() {
        return type;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetType(CORELanguageElementMapping newType, NotificationChain msgs) {
        CORELanguageElementMapping oldType = type;
        type = newType;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.CORE_CREATE_MAPPING__TYPE, oldType, newType);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setType(CORELanguageElementMapping newType) {
        if (newType != type) {
            NotificationChain msgs = null;
            if (type != null)
                msgs = ((InternalEObject)type).eInverseRemove(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS, CORELanguageElementMapping.class, msgs);
            if (newType != null)
                msgs = ((InternalEObject)newType).eInverseAdd(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS, CORELanguageElementMapping.class, msgs);
            msgs = basicSetType(newType, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_CREATE_MAPPING__TYPE, newType, newType));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORECreateMapping getExtendedAction() {
        if (extendedAction != null && extendedAction.eIsProxy()) {
            InternalEObject oldExtendedAction = (InternalEObject)extendedAction;
            extendedAction = (CORECreateMapping)eResolveProxy(oldExtendedAction);
            if (extendedAction != oldExtendedAction) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_CREATE_MAPPING__EXTENDED_ACTION, oldExtendedAction, extendedAction));
            }
        }
        return extendedAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CORECreateMapping basicGetExtendedAction() {
        return extendedAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setExtendedAction(CORECreateMapping newExtendedAction) {
        CORECreateMapping oldExtendedAction = extendedAction;
        extendedAction = newExtendedAction;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_CREATE_MAPPING__EXTENDED_ACTION, oldExtendedAction, extendedAction));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<EObject> getModelElements() {
        if (modelElements == null) {
            modelElements = new EObjectResolvingEList<EObject>(EObject.class, this, CorePackage.CORE_CREATE_MAPPING__MODEL_ELEMENTS);
        }
        return modelElements;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_CREATE_MAPPING__TYPE:
                if (type != null)
                    msgs = ((InternalEObject)type).eInverseRemove(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS, CORELanguageElementMapping.class, msgs);
                return basicSetType((CORELanguageElementMapping)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_CREATE_MAPPING__TYPE:
                return basicSetType(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_CREATE_MAPPING__TYPE:
                if (resolve) return getType();
                return basicGetType();
            case CorePackage.CORE_CREATE_MAPPING__EXTENDED_ACTION:
                if (resolve) return getExtendedAction();
                return basicGetExtendedAction();
            case CorePackage.CORE_CREATE_MAPPING__MODEL_ELEMENTS:
                return getModelElements();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_CREATE_MAPPING__TYPE:
                setType((CORELanguageElementMapping)newValue);
                return;
            case CorePackage.CORE_CREATE_MAPPING__EXTENDED_ACTION:
                setExtendedAction((CORECreateMapping)newValue);
                return;
            case CorePackage.CORE_CREATE_MAPPING__MODEL_ELEMENTS:
                getModelElements().clear();
                getModelElements().addAll((Collection<? extends EObject>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_CREATE_MAPPING__TYPE:
                setType((CORELanguageElementMapping)null);
                return;
            case CorePackage.CORE_CREATE_MAPPING__EXTENDED_ACTION:
                setExtendedAction((CORECreateMapping)null);
                return;
            case CorePackage.CORE_CREATE_MAPPING__MODEL_ELEMENTS:
                getModelElements().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_CREATE_MAPPING__TYPE:
                return type != null;
            case CorePackage.CORE_CREATE_MAPPING__EXTENDED_ACTION:
                return extendedAction != null;
            case CorePackage.CORE_CREATE_MAPPING__MODEL_ELEMENTS:
                return modelElements != null && !modelElements.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //CORECreateMappingImpl
