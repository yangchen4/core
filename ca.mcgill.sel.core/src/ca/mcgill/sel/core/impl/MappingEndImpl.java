/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.MappingEnd;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mapping End</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.MappingEndImpl#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.MappingEndImpl#getRoleName <em>Role Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.MappingEndImpl#getType <em>Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.MappingEndImpl#getLanguageElement <em>Language Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MappingEndImpl extends EObjectImpl implements MappingEnd {
	/**
     * The default value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCardinality()
     * @generated
     * @ordered
     */
	protected static final Cardinality CARDINALITY_EDEFAULT = Cardinality.COMPULSORY;

	/**
     * The cached value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCardinality()
     * @generated
     * @ordered
     */
	protected Cardinality cardinality = CARDINALITY_EDEFAULT;

	/**
     * The default value of the '{@link #getRoleName() <em>Role Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getRoleName()
     * @generated
     * @ordered
     */
	protected static final String ROLE_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getRoleName() <em>Role Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getRoleName()
     * @generated
     * @ordered
     */
	protected String roleName = ROLE_NAME_EDEFAULT;

	/**
     * The cached value of the '{@link #getLanguageElement() <em>Language Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLanguageElement()
     * @generated
     * @ordered
     */
	protected CORELanguageElement languageElement;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MappingEndImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.MAPPING_END;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Cardinality getCardinality() {
        return cardinality;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setCardinality(Cardinality newCardinality) {
        Cardinality oldCardinality = cardinality;
        cardinality = newCardinality == null ? CARDINALITY_EDEFAULT : newCardinality;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MAPPING_END__CARDINALITY, oldCardinality, cardinality));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getRoleName() {
        return roleName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setRoleName(String newRoleName) {
        String oldRoleName = roleName;
        roleName = newRoleName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MAPPING_END__ROLE_NAME, oldRoleName, roleName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElementMapping getType() {
        if (eContainerFeatureID() != CorePackage.MAPPING_END__TYPE) return null;
        return (CORELanguageElementMapping)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetType(CORELanguageElementMapping newType, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newType, CorePackage.MAPPING_END__TYPE, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setType(CORELanguageElementMapping newType) {
        if (newType != eInternalContainer() || (eContainerFeatureID() != CorePackage.MAPPING_END__TYPE && newType != null)) {
            if (EcoreUtil.isAncestor(this, newType))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newType != null)
                msgs = ((InternalEObject)newType).eInverseAdd(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS, CORELanguageElementMapping.class, msgs);
            msgs = basicSetType(newType, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MAPPING_END__TYPE, newType, newType));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElement getLanguageElement() {
        if (languageElement != null && languageElement.eIsProxy()) {
            InternalEObject oldLanguageElement = (InternalEObject)languageElement;
            languageElement = (CORELanguageElement)eResolveProxy(oldLanguageElement);
            if (languageElement != oldLanguageElement) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.MAPPING_END__LANGUAGE_ELEMENT, oldLanguageElement, languageElement));
            }
        }
        return languageElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CORELanguageElement basicGetLanguageElement() {
        return languageElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setLanguageElement(CORELanguageElement newLanguageElement) {
        CORELanguageElement oldLanguageElement = languageElement;
        languageElement = newLanguageElement;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MAPPING_END__LANGUAGE_ELEMENT, oldLanguageElement, languageElement));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.MAPPING_END__TYPE:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetType((CORELanguageElementMapping)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.MAPPING_END__TYPE:
                return basicSetType(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID()) {
            case CorePackage.MAPPING_END__TYPE:
                return eInternalContainer().eInverseRemove(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS, CORELanguageElementMapping.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.MAPPING_END__CARDINALITY:
                return getCardinality();
            case CorePackage.MAPPING_END__ROLE_NAME:
                return getRoleName();
            case CorePackage.MAPPING_END__TYPE:
                return getType();
            case CorePackage.MAPPING_END__LANGUAGE_ELEMENT:
                if (resolve) return getLanguageElement();
                return basicGetLanguageElement();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.MAPPING_END__CARDINALITY:
                setCardinality((Cardinality)newValue);
                return;
            case CorePackage.MAPPING_END__ROLE_NAME:
                setRoleName((String)newValue);
                return;
            case CorePackage.MAPPING_END__TYPE:
                setType((CORELanguageElementMapping)newValue);
                return;
            case CorePackage.MAPPING_END__LANGUAGE_ELEMENT:
                setLanguageElement((CORELanguageElement)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.MAPPING_END__CARDINALITY:
                setCardinality(CARDINALITY_EDEFAULT);
                return;
            case CorePackage.MAPPING_END__ROLE_NAME:
                setRoleName(ROLE_NAME_EDEFAULT);
                return;
            case CorePackage.MAPPING_END__TYPE:
                setType((CORELanguageElementMapping)null);
                return;
            case CorePackage.MAPPING_END__LANGUAGE_ELEMENT:
                setLanguageElement((CORELanguageElement)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.MAPPING_END__CARDINALITY:
                return cardinality != CARDINALITY_EDEFAULT;
            case CorePackage.MAPPING_END__ROLE_NAME:
                return ROLE_NAME_EDEFAULT == null ? roleName != null : !ROLE_NAME_EDEFAULT.equals(roleName);
            case CorePackage.MAPPING_END__TYPE:
                return getType() != null;
            case CorePackage.MAPPING_END__LANGUAGE_ELEMENT:
                return languageElement != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (cardinality: ");
        result.append(cardinality);
        result.append(", roleName: ");
        result.append(roleName);
        result.append(')');
        return result.toString();
    }

} //MappingEndImpl
