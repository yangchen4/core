/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Reuse</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREReuseImpl#getReusedConcern <em>Reused Concern</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREReuseImpl#getExtends <em>Extends</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREReuseImpl#getModelReuses <em>Model Reuses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREReuseImpl extends CORENamedElementImpl implements COREReuse {
	/**
     * The cached value of the '{@link #getReusedConcern() <em>Reused Concern</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReusedConcern()
     * @generated
     * @ordered
     */
	protected COREConcern reusedConcern;

	/**
     * The cached value of the '{@link #getExtends() <em>Extends</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getExtends()
     * @generated
     * @ordered
     */
	protected COREReuse extends_;

	/**
     * The cached value of the '{@link #getModelReuses() <em>Model Reuses</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModelReuses()
     * @generated
     * @ordered
     */
	protected EList<COREModelReuse> modelReuses;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREReuseImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_REUSE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREConcern getReusedConcern() {
        if (reusedConcern != null && reusedConcern.eIsProxy()) {
            InternalEObject oldReusedConcern = (InternalEObject)reusedConcern;
            reusedConcern = (COREConcern)eResolveProxy(oldReusedConcern);
            if (reusedConcern != oldReusedConcern) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_REUSE__REUSED_CONCERN, oldReusedConcern, reusedConcern));
            }
        }
        return reusedConcern;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREConcern basicGetReusedConcern() {
        return reusedConcern;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setReusedConcern(COREConcern newReusedConcern) {
        COREConcern oldReusedConcern = reusedConcern;
        reusedConcern = newReusedConcern;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_REUSE__REUSED_CONCERN, oldReusedConcern, reusedConcern));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREReuse getExtends() {
        if (extends_ != null && extends_.eIsProxy()) {
            InternalEObject oldExtends = (InternalEObject)extends_;
            extends_ = (COREReuse)eResolveProxy(oldExtends);
            if (extends_ != oldExtends) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_REUSE__EXTENDS, oldExtends, extends_));
            }
        }
        return extends_;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREReuse basicGetExtends() {
        return extends_;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setExtends(COREReuse newExtends) {
        COREReuse oldExtends = extends_;
        extends_ = newExtends;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_REUSE__EXTENDS, oldExtends, extends_));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREModelReuse> getModelReuses() {
        if (modelReuses == null) {
            modelReuses = new EObjectWithInverseResolvingEList<COREModelReuse>(COREModelReuse.class, this, CorePackage.CORE_REUSE__MODEL_REUSES, CorePackage.CORE_MODEL_REUSE__REUSE);
        }
        return modelReuses;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_REUSE__MODEL_REUSES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getModelReuses()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_REUSE__MODEL_REUSES:
                return ((InternalEList<?>)getModelReuses()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_REUSE__REUSED_CONCERN:
                if (resolve) return getReusedConcern();
                return basicGetReusedConcern();
            case CorePackage.CORE_REUSE__EXTENDS:
                if (resolve) return getExtends();
                return basicGetExtends();
            case CorePackage.CORE_REUSE__MODEL_REUSES:
                return getModelReuses();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_REUSE__REUSED_CONCERN:
                setReusedConcern((COREConcern)newValue);
                return;
            case CorePackage.CORE_REUSE__EXTENDS:
                setExtends((COREReuse)newValue);
                return;
            case CorePackage.CORE_REUSE__MODEL_REUSES:
                getModelReuses().clear();
                getModelReuses().addAll((Collection<? extends COREModelReuse>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_REUSE__REUSED_CONCERN:
                setReusedConcern((COREConcern)null);
                return;
            case CorePackage.CORE_REUSE__EXTENDS:
                setExtends((COREReuse)null);
                return;
            case CorePackage.CORE_REUSE__MODEL_REUSES:
                getModelReuses().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_REUSE__REUSED_CONCERN:
                return reusedConcern != null;
            case CorePackage.CORE_REUSE__EXTENDS:
                return extends_ != null;
            case CorePackage.CORE_REUSE__MODEL_REUSES:
                return modelReuses != null && !modelReuses.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //COREReuseImpl
