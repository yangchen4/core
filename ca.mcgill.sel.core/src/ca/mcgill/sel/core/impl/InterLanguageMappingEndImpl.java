/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.InterLanguageMappingEnd;
import ca.mcgill.sel.core.MappingEnd;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inter Language Mapping End</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl#isOrigin <em>Origin</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl#isDestination <em>Destination</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.InterLanguageMappingEndImpl#getMappingEnd <em>Mapping End</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterLanguageMappingEndImpl extends EObjectImpl implements InterLanguageMappingEnd {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;



    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected String name = NAME_EDEFAULT;
				/**
     * The default value of the '{@link #isOrigin() <em>Origin</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isOrigin()
     * @generated
     * @ordered
     */
	protected static final boolean ORIGIN_EDEFAULT = false;
				/**
     * The cached value of the '{@link #isOrigin() <em>Origin</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isOrigin()
     * @generated
     * @ordered
     */
	protected boolean origin = ORIGIN_EDEFAULT;
				/**
     * The default value of the '{@link #isDestination() <em>Destination</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isDestination()
     * @generated
     * @ordered
     */
	protected static final boolean DESTINATION_EDEFAULT = false;
				/**
     * The cached value of the '{@link #isDestination() <em>Destination</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isDestination()
     * @generated
     * @ordered
     */
	protected boolean destination = DESTINATION_EDEFAULT;
				/**
     * The cached value of the '{@link #getMappingEnd() <em>Mapping End</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMappingEnd()
     * @generated
     * @ordered
     */
	protected MappingEnd mappingEnd;



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected InterLanguageMappingEndImpl() {
        super();
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.INTER_LANGUAGE_MAPPING_END;
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return name;
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTER_LANGUAGE_MAPPING_END__NAME, oldName, name));
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isOrigin() {
        return origin;
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setOrigin(boolean newOrigin) {
        boolean oldOrigin = origin;
        origin = newOrigin;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTER_LANGUAGE_MAPPING_END__ORIGIN, oldOrigin, origin));
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isDestination() {
        return destination;
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDestination(boolean newDestination) {
        boolean oldDestination = destination;
        destination = newDestination;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTER_LANGUAGE_MAPPING_END__DESTINATION, oldDestination, destination));
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MappingEnd getMappingEnd() {
        if (mappingEnd != null && mappingEnd.eIsProxy()) {
            InternalEObject oldMappingEnd = (InternalEObject)mappingEnd;
            mappingEnd = (MappingEnd)eResolveProxy(oldMappingEnd);
            if (mappingEnd != oldMappingEnd) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.INTER_LANGUAGE_MAPPING_END__MAPPING_END, oldMappingEnd, mappingEnd));
            }
        }
        return mappingEnd;
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MappingEnd basicGetMappingEnd() {
        return mappingEnd;
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMappingEnd(MappingEnd newMappingEnd) {
        MappingEnd oldMappingEnd = mappingEnd;
        mappingEnd = newMappingEnd;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTER_LANGUAGE_MAPPING_END__MAPPING_END, oldMappingEnd, mappingEnd));
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING_END__NAME:
                return getName();
            case CorePackage.INTER_LANGUAGE_MAPPING_END__ORIGIN:
                return isOrigin();
            case CorePackage.INTER_LANGUAGE_MAPPING_END__DESTINATION:
                return isDestination();
            case CorePackage.INTER_LANGUAGE_MAPPING_END__MAPPING_END:
                if (resolve) return getMappingEnd();
                return basicGetMappingEnd();
        }
        return super.eGet(featureID, resolve, coreType);
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING_END__NAME:
                setName((String)newValue);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__ORIGIN:
                setOrigin((Boolean)newValue);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__DESTINATION:
                setDestination((Boolean)newValue);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__MAPPING_END:
                setMappingEnd((MappingEnd)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING_END__NAME:
                setName(NAME_EDEFAULT);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__ORIGIN:
                setOrigin(ORIGIN_EDEFAULT);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__DESTINATION:
                setDestination(DESTINATION_EDEFAULT);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__MAPPING_END:
                setMappingEnd((MappingEnd)null);
                return;
        }
        super.eUnset(featureID);
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING_END__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case CorePackage.INTER_LANGUAGE_MAPPING_END__ORIGIN:
                return origin != ORIGIN_EDEFAULT;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__DESTINATION:
                return destination != DESTINATION_EDEFAULT;
            case CorePackage.INTER_LANGUAGE_MAPPING_END__MAPPING_END:
                return mappingEnd != null;
        }
        return super.eIsSet(featureID);
    }



				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", origin: ");
        result.append(origin);
        result.append(", destination: ");
        result.append(destination);
        result.append(')');
        return result.toString();
    }



				/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public String getName(EClass connectionClass) {
        if (this.mappingEnd != null) {
            String name;
            EObject languageElement = mappingEnd.getLanguageElement().getLanguageElement();
            if (mappingEnd.getLanguageElement().getName() != null) {
                name = mappingEnd.getRoleName() + " " + mappingEnd.getLanguageElement().getName();  
            } else if(languageElement instanceof EClass && languageElement!= connectionClass){
                name = ((ENamedElement) mappingEnd.getLanguageElement().getLanguageElement()).getName();  
            }else {
                name = mappingEnd.getRoleName();
            }
            
            return name;
        }
        throw new UnsupportedOperationException();
    }


} //InterLanguageMappingEndImpl
