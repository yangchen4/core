/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.CreateModelElementMapping;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Create Model Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CreateModelElementMappingImpl#getReusedActions <em>Reused Actions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CreateModelElementMappingImpl extends COREPerspectiveActionImpl implements CreateModelElementMapping {
	/**
     * The cached value of the '{@link #getReusedActions() <em>Reused Actions</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReusedActions()
     * @generated
     * @ordered
     */
	protected EList<COREAction> reusedActions;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CreateModelElementMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CREATE_MODEL_ELEMENT_MAPPING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREAction> getReusedActions() {
        if (reusedActions == null) {
            reusedActions = new EObjectResolvingEList<COREAction>(COREAction.class, this, CorePackage.CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS);
        }
        return reusedActions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS:
                return getReusedActions();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS:
                getReusedActions().clear();
                getReusedActions().addAll((Collection<? extends COREAction>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS:
                getReusedActions().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS:
                return reusedActions != null && !reusedActions.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //CreateModelElementMappingImpl
