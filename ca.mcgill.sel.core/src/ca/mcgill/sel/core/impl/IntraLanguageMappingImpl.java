/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.IntraLanguageMapping;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Intra Language Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl#isClosure <em>Closure</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl#isReuse <em>Reuse</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl#isIncreaseDepth <em>Increase Depth</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl#isChangeModel <em>Change Model</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl#getFrom <em>From</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.IntraLanguageMappingImpl#getHops <em>Hops</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntraLanguageMappingImpl extends NavigationMappingImpl implements IntraLanguageMapping {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected String name = NAME_EDEFAULT;

	/**
     * The default value of the '{@link #isClosure() <em>Closure</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isClosure()
     * @generated
     * @ordered
     */
	protected static final boolean CLOSURE_EDEFAULT = false;

	/**
     * The cached value of the '{@link #isClosure() <em>Closure</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isClosure()
     * @generated
     * @ordered
     */
	protected boolean closure = CLOSURE_EDEFAULT;

	/**
     * The default value of the '{@link #isReuse() <em>Reuse</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isReuse()
     * @generated
     * @ordered
     */
	protected static final boolean REUSE_EDEFAULT = false;

	/**
     * The cached value of the '{@link #isReuse() <em>Reuse</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isReuse()
     * @generated
     * @ordered
     */
	protected boolean reuse = REUSE_EDEFAULT;

	/**
     * The default value of the '{@link #isIncreaseDepth() <em>Increase Depth</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isIncreaseDepth()
     * @generated
     * @ordered
     */
	protected static final boolean INCREASE_DEPTH_EDEFAULT = false;

	/**
     * The cached value of the '{@link #isIncreaseDepth() <em>Increase Depth</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isIncreaseDepth()
     * @generated
     * @ordered
     */
	protected boolean increaseDepth = INCREASE_DEPTH_EDEFAULT;

	/**
     * The default value of the '{@link #isChangeModel() <em>Change Model</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isChangeModel()
     * @generated
     * @ordered
     */
	protected static final boolean CHANGE_MODEL_EDEFAULT = false;

	/**
     * The cached value of the '{@link #isChangeModel() <em>Change Model</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isChangeModel()
     * @generated
     * @ordered
     */
	protected boolean changeModel = CHANGE_MODEL_EDEFAULT;

	/**
     * The cached value of the '{@link #getFrom() <em>From</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getFrom()
     * @generated
     * @ordered
     */
	protected EClass from;

	/**
     * The cached value of the '{@link #getHops() <em>Hops</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getHops()
     * @generated
     * @ordered
     */
	protected EList<EReference> hops;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected IntraLanguageMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.INTRA_LANGUAGE_MAPPING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTRA_LANGUAGE_MAPPING__NAME, oldName, name));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isClosure() {
        return closure;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setClosure(boolean newClosure) {
        boolean oldClosure = closure;
        closure = newClosure;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTRA_LANGUAGE_MAPPING__CLOSURE, oldClosure, closure));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isReuse() {
        return reuse;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setReuse(boolean newReuse) {
        boolean oldReuse = reuse;
        reuse = newReuse;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTRA_LANGUAGE_MAPPING__REUSE, oldReuse, reuse));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isIncreaseDepth() {
        return increaseDepth;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setIncreaseDepth(boolean newIncreaseDepth) {
        boolean oldIncreaseDepth = increaseDepth;
        increaseDepth = newIncreaseDepth;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH, oldIncreaseDepth, increaseDepth));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isChangeModel() {
        return changeModel;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setChangeModel(boolean newChangeModel) {
        boolean oldChangeModel = changeModel;
        changeModel = newChangeModel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTRA_LANGUAGE_MAPPING__CHANGE_MODEL, oldChangeModel, changeModel));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getFrom() {
        if (from != null && from.eIsProxy()) {
            InternalEObject oldFrom = (InternalEObject)from;
            from = (EClass)eResolveProxy(oldFrom);
            if (from != oldFrom) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.INTRA_LANGUAGE_MAPPING__FROM, oldFrom, from));
            }
        }
        return from;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EClass basicGetFrom() {
        return from;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setFrom(EClass newFrom) {
        EClass oldFrom = from;
        from = newFrom;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTRA_LANGUAGE_MAPPING__FROM, oldFrom, from));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<EReference> getHops() {
        if (hops == null) {
            hops = new EObjectResolvingEList<EReference>(EReference.class, this, CorePackage.INTRA_LANGUAGE_MAPPING__HOPS);
        }
        return hops;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.INTRA_LANGUAGE_MAPPING__NAME:
                return getName();
            case CorePackage.INTRA_LANGUAGE_MAPPING__CLOSURE:
                return isClosure();
            case CorePackage.INTRA_LANGUAGE_MAPPING__REUSE:
                return isReuse();
            case CorePackage.INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH:
                return isIncreaseDepth();
            case CorePackage.INTRA_LANGUAGE_MAPPING__CHANGE_MODEL:
                return isChangeModel();
            case CorePackage.INTRA_LANGUAGE_MAPPING__FROM:
                if (resolve) return getFrom();
                return basicGetFrom();
            case CorePackage.INTRA_LANGUAGE_MAPPING__HOPS:
                return getHops();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.INTRA_LANGUAGE_MAPPING__NAME:
                setName((String)newValue);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__CLOSURE:
                setClosure((Boolean)newValue);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__REUSE:
                setReuse((Boolean)newValue);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH:
                setIncreaseDepth((Boolean)newValue);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__CHANGE_MODEL:
                setChangeModel((Boolean)newValue);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__FROM:
                setFrom((EClass)newValue);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__HOPS:
                getHops().clear();
                getHops().addAll((Collection<? extends EReference>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.INTRA_LANGUAGE_MAPPING__NAME:
                setName(NAME_EDEFAULT);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__CLOSURE:
                setClosure(CLOSURE_EDEFAULT);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__REUSE:
                setReuse(REUSE_EDEFAULT);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH:
                setIncreaseDepth(INCREASE_DEPTH_EDEFAULT);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__CHANGE_MODEL:
                setChangeModel(CHANGE_MODEL_EDEFAULT);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__FROM:
                setFrom((EClass)null);
                return;
            case CorePackage.INTRA_LANGUAGE_MAPPING__HOPS:
                getHops().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.INTRA_LANGUAGE_MAPPING__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case CorePackage.INTRA_LANGUAGE_MAPPING__CLOSURE:
                return closure != CLOSURE_EDEFAULT;
            case CorePackage.INTRA_LANGUAGE_MAPPING__REUSE:
                return reuse != REUSE_EDEFAULT;
            case CorePackage.INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH:
                return increaseDepth != INCREASE_DEPTH_EDEFAULT;
            case CorePackage.INTRA_LANGUAGE_MAPPING__CHANGE_MODEL:
                return changeModel != CHANGE_MODEL_EDEFAULT;
            case CorePackage.INTRA_LANGUAGE_MAPPING__FROM:
                return from != null;
            case CorePackage.INTRA_LANGUAGE_MAPPING__HOPS:
                return hops != null && !hops.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", closure: ");
        result.append(closure);
        result.append(", reuse: ");
        result.append(reuse);
        result.append(", increaseDepth: ");
        result.append(increaseDepth);
        result.append(", changeModel: ");
        result.append(changeModel);
        result.append(')');
        return result.toString();
    }

} //IntraLanguageMappingImpl
