/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Model Extension</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class COREModelExtensionImpl extends COREModelCompositionImpl implements COREModelExtension {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREModelExtensionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MODEL_EXTENSION;
    }

} //COREModelExtensionImpl
