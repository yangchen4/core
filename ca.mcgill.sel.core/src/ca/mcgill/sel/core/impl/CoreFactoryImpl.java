/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.*;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoreFactoryImpl extends EFactoryImpl implements CoreFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static CoreFactory init() {
        try {
            CoreFactory theCoreFactory = (CoreFactory)EPackage.Registry.INSTANCE.getEFactory(CorePackage.eNS_URI);
            if (theCoreFactory != null) {
                return theCoreFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new CoreFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CoreFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case CorePackage.CORE_IMPACT_MODEL: return createCOREImpactModel();
            case CorePackage.CORE_CONCERN: return createCOREConcern();
            case CorePackage.CORE_FEATURE: return createCOREFeature();
            case CorePackage.CORE_REUSE: return createCOREReuse();
            case CorePackage.CORE_IMPACT_NODE: return createCOREImpactNode();
            case CorePackage.CORE_CONFIGURATION: return createCOREConfiguration();
            case CorePackage.CORE_FEATURE_MODEL: return createCOREFeatureModel();
            case CorePackage.CORE_MODEL_REUSE: return createCOREModelReuse();
            case CorePackage.CORE_CONTRIBUTION: return createCOREContribution();
            case CorePackage.LAYOUT_MAP: return (EObject)createLayoutMap();
            case CorePackage.LAYOUT_ELEMENT: return createLayoutElement();
            case CorePackage.LAYOUT_CONTAINER_MAP: return (EObject)createLayoutContainerMap();
            case CorePackage.CORE_FEATURE_IMPACT_NODE: return createCOREFeatureImpactNode();
            case CorePackage.CORE_WEIGHTED_LINK: return createCOREWeightedLink();
            case CorePackage.CORE_MODEL_EXTENSION: return createCOREModelExtension();
            case CorePackage.CORE_MAPPING: return createCOREMapping();
            case CorePackage.CORE_SCENE: return createCOREScene();
            case CorePackage.CORE_PERSPECTIVE: return createCOREPerspective();
            case CorePackage.CORE_EXTERNAL_LANGUAGE: return createCOREExternalLanguage();
            case CorePackage.CORE_EXTERNAL_ARTEFACT: return createCOREExternalArtefact();
            case CorePackage.COREUI_ELEMENT: return createCOREUIElement();
            case CorePackage.CORECI_ELEMENT: return createCORECIElement();
            case CorePackage.CORE_MAPPING_CARDINALITY: return createCOREMappingCardinality();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING: return createCORELanguageElementMapping();
            case CorePackage.CORE_REEXPOSE_ACTION: return createCOREReexposeAction();
            case CorePackage.CORE_REDEFINE_ACTION: return createCORERedefineAction();
            case CorePackage.CORE_LANGUAGE_ACTION: return createCORELanguageAction();
            case CorePackage.CORE_CREATE_MAPPING: return createCORECreateMapping();
            case CorePackage.CREATE_MODEL_ELEMENT_MAPPING: return createCreateModelElementMapping();
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING: return createCOREModelElementMapping();
            case CorePackage.MAPPING_END: return createMappingEnd();
            case CorePackage.LANGUAGE_MAP: return (EObject)createLanguageMap();
            case CorePackage.ARTEFACT_MAP: return (EObject)createArtefactMap();
            case CorePackage.PARAMETER: return createParameter();
            case CorePackage.CORE_LANGUAGE_ELEMENT: return createCORELanguageElement();
            case CorePackage.DELETE_EFFECT: return createDeleteEffect();
            case CorePackage.UPDATE_EFFECT: return createUpdateEffect();
            case CorePackage.NAVIGATION_MAPPING: return createNavigationMapping();
            case CorePackage.INTER_LANGUAGE_MAPPING: return createInterLanguageMapping();
            case CorePackage.INTRA_LANGUAGE_MAPPING: return createIntraLanguageMapping();
            case CorePackage.INTER_LANGUAGE_MAPPING_END: return createInterLanguageMappingEnd();
            case CorePackage.CREATE_EFFECT: return createCreateEffect();
            case CorePackage.CORE_REUSE_ARTEFACT: return createCOREReuseArtefact();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case CorePackage.CORE_FEATURE_RELATIONSHIP_TYPE:
                return createCOREFeatureRelationshipTypeFromString(eDataType, initialValue);
            case CorePackage.CORE_VISIBILITY_TYPE:
                return createCOREVisibilityTypeFromString(eDataType, initialValue);
            case CorePackage.CORE_PARTIALITY_TYPE:
                return createCOREPartialityTypeFromString(eDataType, initialValue);
            case CorePackage.CORE_RELATIONSHIP:
                return createCORERelationshipFromString(eDataType, initialValue);
            case CorePackage.CARDINALITY:
                return createCardinalityFromString(eDataType, initialValue);
            case CorePackage.LANGUAGE_ACTION_TYPE:
                return createLanguageActionTypeFromString(eDataType, initialValue);
            case CorePackage.PARAMETER_EFFECT:
                return createParameterEffectFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case CorePackage.CORE_FEATURE_RELATIONSHIP_TYPE:
                return convertCOREFeatureRelationshipTypeToString(eDataType, instanceValue);
            case CorePackage.CORE_VISIBILITY_TYPE:
                return convertCOREVisibilityTypeToString(eDataType, instanceValue);
            case CorePackage.CORE_PARTIALITY_TYPE:
                return convertCOREPartialityTypeToString(eDataType, instanceValue);
            case CorePackage.CORE_RELATIONSHIP:
                return convertCORERelationshipToString(eDataType, instanceValue);
            case CorePackage.CARDINALITY:
                return convertCardinalityToString(eDataType, instanceValue);
            case CorePackage.LANGUAGE_ACTION_TYPE:
                return convertLanguageActionTypeToString(eDataType, instanceValue);
            case CorePackage.PARAMETER_EFFECT:
                return convertParameterEffectToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREImpactModel createCOREImpactModel() {
        COREImpactModelImpl coreImpactModel = new COREImpactModelImpl();
        return coreImpactModel;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREConcern createCOREConcern() {
        COREConcernImpl coreConcern = new COREConcernImpl();
        return coreConcern;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREFeature createCOREFeature() {
        COREFeatureImpl coreFeature = new COREFeatureImpl();
        return coreFeature;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREReuse createCOREReuse() {
        COREReuseImpl coreReuse = new COREReuseImpl();
        return coreReuse;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREImpactNode createCOREImpactNode() {
        COREImpactNodeImpl coreImpactNode = new COREImpactNodeImpl();
        return coreImpactNode;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREConfiguration createCOREConfiguration() {
        COREConfigurationImpl coreConfiguration = new COREConfigurationImpl();
        return coreConfiguration;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREFeatureModel createCOREFeatureModel() {
        COREFeatureModelImpl coreFeatureModel = new COREFeatureModelImpl();
        return coreFeatureModel;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREModelReuse createCOREModelReuse() {
        COREModelReuseImpl coreModelReuse = new COREModelReuseImpl();
        return coreModelReuse;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREContribution createCOREContribution() {
        COREContributionImpl coreContribution = new COREContributionImpl();
        return coreContribution;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<EObject, LayoutElement> createLayoutMap() {
        LayoutMapImpl layoutMap = new LayoutMapImpl();
        return layoutMap;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public LayoutElement createLayoutElement() {
        LayoutElementImpl layoutElement = new LayoutElementImpl();
        return layoutElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<EObject, EMap<EObject, LayoutElement>> createLayoutContainerMap() {
        LayoutContainerMapImpl layoutContainerMap = new LayoutContainerMapImpl();
        return layoutContainerMap;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREFeatureImpactNode createCOREFeatureImpactNode() {
        COREFeatureImpactNodeImpl coreFeatureImpactNode = new COREFeatureImpactNodeImpl();
        return coreFeatureImpactNode;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREWeightedLink createCOREWeightedLink() {
        COREWeightedLinkImpl coreWeightedLink = new COREWeightedLinkImpl();
        return coreWeightedLink;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREModelExtension createCOREModelExtension() {
        COREModelExtensionImpl coreModelExtension = new COREModelExtensionImpl();
        return coreModelExtension;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public <T> COREMapping<T> createCOREMapping() {
        COREMappingImpl<T> coreMapping = new COREMappingImpl<T>();
        return coreMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREScene createCOREScene() {
        CORESceneImpl coreScene = new CORESceneImpl();
        return coreScene;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREPerspective createCOREPerspective() {
        COREPerspectiveImpl corePerspective = new COREPerspectiveImpl();
        return corePerspective;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREExternalLanguage createCOREExternalLanguage() {
        COREExternalLanguageImpl coreExternalLanguage = new COREExternalLanguageImpl();
        return coreExternalLanguage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREExternalArtefact createCOREExternalArtefact() {
        COREExternalArtefactImpl coreExternalArtefact = new COREExternalArtefactImpl();
        return coreExternalArtefact;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREUIElement createCOREUIElement() {
        COREUIElementImpl coreuiElement = new COREUIElementImpl();
        return coreuiElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORECIElement createCORECIElement() {
        CORECIElementImpl coreciElement = new CORECIElementImpl();
        return coreciElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREMappingCardinality createCOREMappingCardinality() {
        COREMappingCardinalityImpl coreMappingCardinality = new COREMappingCardinalityImpl();
        return coreMappingCardinality;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElementMapping createCORELanguageElementMapping() {
        CORELanguageElementMappingImpl coreLanguageElementMapping = new CORELanguageElementMappingImpl();
        return coreLanguageElementMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREReexposeAction createCOREReexposeAction() {
        COREReexposeActionImpl coreReexposeAction = new COREReexposeActionImpl();
        return coreReexposeAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORERedefineAction createCORERedefineAction() {
        CORERedefineActionImpl coreRedefineAction = new CORERedefineActionImpl();
        return coreRedefineAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageAction createCORELanguageAction() {
        CORELanguageActionImpl coreLanguageAction = new CORELanguageActionImpl();
        return coreLanguageAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORECreateMapping createCORECreateMapping() {
        CORECreateMappingImpl coreCreateMapping = new CORECreateMappingImpl();
        return coreCreateMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CreateModelElementMapping createCreateModelElementMapping() {
        CreateModelElementMappingImpl createModelElementMapping = new CreateModelElementMappingImpl();
        return createModelElementMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREModelElementMapping createCOREModelElementMapping() {
        COREModelElementMappingImpl coreModelElementMapping = new COREModelElementMappingImpl();
        return coreModelElementMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MappingEnd createMappingEnd() {
        MappingEndImpl mappingEnd = new MappingEndImpl();
        return mappingEnd;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<String, CORELanguage> createLanguageMap() {
        LanguageMapImpl languageMap = new LanguageMapImpl();
        return languageMap;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<String, EList<COREArtefact>> createArtefactMap() {
        ArtefactMapImpl artefactMap = new ArtefactMapImpl();
        return artefactMap;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Parameter createParameter() {
        ParameterImpl parameter = new ParameterImpl();
        return parameter;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElement createCORELanguageElement() {
        CORELanguageElementImpl coreLanguageElement = new CORELanguageElementImpl();
        return coreLanguageElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public DeleteEffect createDeleteEffect() {
        DeleteEffectImpl deleteEffect = new DeleteEffectImpl();
        return deleteEffect;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public UpdateEffect createUpdateEffect() {
        UpdateEffectImpl updateEffect = new UpdateEffectImpl();
        return updateEffect;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NavigationMapping createNavigationMapping() {
        NavigationMappingImpl navigationMapping = new NavigationMappingImpl();
        return navigationMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public InterLanguageMapping createInterLanguageMapping() {
        InterLanguageMappingImpl interLanguageMapping = new InterLanguageMappingImpl();
        return interLanguageMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public IntraLanguageMapping createIntraLanguageMapping() {
        IntraLanguageMappingImpl intraLanguageMapping = new IntraLanguageMappingImpl();
        return intraLanguageMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public InterLanguageMappingEnd createInterLanguageMappingEnd() {
        InterLanguageMappingEndImpl interLanguageMappingEnd = new InterLanguageMappingEndImpl();
        return interLanguageMappingEnd;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CreateEffect createCreateEffect() {
        CreateEffectImpl createEffect = new CreateEffectImpl();
        return createEffect;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public COREReuseArtefact createCOREReuseArtefact() {
        COREReuseArtefactImpl coreReuseArtefact = new COREReuseArtefactImpl();
        return coreReuseArtefact;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREFeatureRelationshipType createCOREFeatureRelationshipTypeFromString(EDataType eDataType, String initialValue) {
        COREFeatureRelationshipType result = COREFeatureRelationshipType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertCOREFeatureRelationshipTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREVisibilityType createCOREVisibilityTypeFromString(EDataType eDataType, String initialValue) {
        COREVisibilityType result = COREVisibilityType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertCOREVisibilityTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREPartialityType createCOREPartialityTypeFromString(EDataType eDataType, String initialValue) {
        COREPartialityType result = COREPartialityType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertCOREPartialityTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CORERelationship createCORERelationshipFromString(EDataType eDataType, String initialValue) {
        CORERelationship result = CORERelationship.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertCORERelationshipToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Cardinality createCardinalityFromString(EDataType eDataType, String initialValue) {
        Cardinality result = Cardinality.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertCardinalityToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public LanguageActionType createLanguageActionTypeFromString(EDataType eDataType, String initialValue) {
        LanguageActionType result = LanguageActionType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertLanguageActionTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ParameterEffect createParameterEffectFromString(EDataType eDataType, String initialValue) {
        ParameterEffect result = ParameterEffect.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertParameterEffectToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CorePackage getCorePackage() {
        return (CorePackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static CorePackage getPackage() {
        return CorePackage.eINSTANCE;
    }

} //CoreFactoryImpl
