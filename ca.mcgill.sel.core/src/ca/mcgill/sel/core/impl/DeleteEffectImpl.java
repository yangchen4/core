/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.DeleteEffect;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Delete Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DeleteEffectImpl extends ExistingElementEffectImpl implements DeleteEffect {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DeleteEffectImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.DELETE_EFFECT;
    }

} //DeleteEffectImpl
