/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Model Composition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelCompositionImpl#getSource <em>Source</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelCompositionImpl#getCompositions <em>Compositions</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class COREModelCompositionImpl extends EObjectImpl implements COREModelComposition {
	/**
     * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSource()
     * @generated
     * @ordered
     */
	protected COREArtefact source;

	/**
     * The cached value of the '{@link #getCompositions() <em>Compositions</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCompositions()
     * @generated
     * @ordered
     */
	protected EList<COREModelElementComposition<?>> compositions;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREModelCompositionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MODEL_COMPOSITION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREArtefact getSource() {
        if (source != null && source.eIsProxy()) {
            InternalEObject oldSource = (InternalEObject)source;
            source = (COREArtefact)eResolveProxy(oldSource);
            if (source != oldSource) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_MODEL_COMPOSITION__SOURCE, oldSource, source));
            }
        }
        return source;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREArtefact basicGetSource() {
        return source;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setSource(COREArtefact newSource) {
        COREArtefact oldSource = source;
        source = newSource;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_COMPOSITION__SOURCE, oldSource, source));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREModelElementComposition<?>> getCompositions() {
        if (compositions == null) {
            compositions = new EObjectContainmentEList<COREModelElementComposition<?>>(COREModelElementComposition.class, this, CorePackage.CORE_MODEL_COMPOSITION__COMPOSITIONS);
        }
        return compositions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_COMPOSITION__COMPOSITIONS:
                return ((InternalEList<?>)getCompositions()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_COMPOSITION__SOURCE:
                if (resolve) return getSource();
                return basicGetSource();
            case CorePackage.CORE_MODEL_COMPOSITION__COMPOSITIONS:
                return getCompositions();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_COMPOSITION__SOURCE:
                setSource((COREArtefact)newValue);
                return;
            case CorePackage.CORE_MODEL_COMPOSITION__COMPOSITIONS:
                getCompositions().clear();
                getCompositions().addAll((Collection<? extends COREModelElementComposition<?>>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_COMPOSITION__SOURCE:
                setSource((COREArtefact)null);
                return;
            case CorePackage.CORE_MODEL_COMPOSITION__COMPOSITIONS:
                getCompositions().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_COMPOSITION__SOURCE:
                return source != null;
            case CorePackage.CORE_MODEL_COMPOSITION__COMPOSITIONS:
                return compositions != null && !compositions.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //COREModelCompositionImpl
