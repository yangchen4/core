/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.InterLanguageMapping;
import ca.mcgill.sel.core.InterLanguageMappingEnd;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inter Language Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.InterLanguageMappingImpl#isDefault <em>Default</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.InterLanguageMappingImpl#getCoreLanguageElementMapping <em>Core Language Element Mapping</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.InterLanguageMappingImpl#getInterLanguageMappingEnds <em>Inter Language Mapping Ends</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterLanguageMappingImpl extends NavigationMappingImpl implements InterLanguageMapping {
	/**
     * The default value of the '{@link #isDefault() <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isDefault()
     * @generated
     * @ordered
     */
	protected static final boolean DEFAULT_EDEFAULT = false;

	/**
     * The cached value of the '{@link #isDefault() <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isDefault()
     * @generated
     * @ordered
     */
	protected boolean default_ = DEFAULT_EDEFAULT;

	/**
     * The cached value of the '{@link #getCoreLanguageElementMapping() <em>Core Language Element Mapping</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCoreLanguageElementMapping()
     * @generated
     * @ordered
     */
	protected CORELanguageElementMapping coreLanguageElementMapping;

	/**
     * The cached value of the '{@link #getInterLanguageMappingEnds() <em>Inter Language Mapping Ends</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getInterLanguageMappingEnds()
     * @generated
     * @ordered
     */
	protected EList<InterLanguageMappingEnd> interLanguageMappingEnds;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected InterLanguageMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.INTER_LANGUAGE_MAPPING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isDefault() {
        return default_;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDefault(boolean newDefault) {
        boolean oldDefault = default_;
        default_ = newDefault;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTER_LANGUAGE_MAPPING__DEFAULT, oldDefault, default_));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElementMapping getCoreLanguageElementMapping() {
        if (coreLanguageElementMapping != null && coreLanguageElementMapping.eIsProxy()) {
            InternalEObject oldCoreLanguageElementMapping = (InternalEObject)coreLanguageElementMapping;
            coreLanguageElementMapping = (CORELanguageElementMapping)eResolveProxy(oldCoreLanguageElementMapping);
            if (coreLanguageElementMapping != oldCoreLanguageElementMapping) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING, oldCoreLanguageElementMapping, coreLanguageElementMapping));
            }
        }
        return coreLanguageElementMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CORELanguageElementMapping basicGetCoreLanguageElementMapping() {
        return coreLanguageElementMapping;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setCoreLanguageElementMapping(CORELanguageElementMapping newCoreLanguageElementMapping) {
        CORELanguageElementMapping oldCoreLanguageElementMapping = coreLanguageElementMapping;
        coreLanguageElementMapping = newCoreLanguageElementMapping;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING, oldCoreLanguageElementMapping, coreLanguageElementMapping));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<InterLanguageMappingEnd> getInterLanguageMappingEnds() {
        if (interLanguageMappingEnds == null) {
            interLanguageMappingEnds = new EObjectResolvingEList<InterLanguageMappingEnd>(InterLanguageMappingEnd.class, this, CorePackage.INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS);
        }
        return interLanguageMappingEnds;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING__DEFAULT:
                return isDefault();
            case CorePackage.INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING:
                if (resolve) return getCoreLanguageElementMapping();
                return basicGetCoreLanguageElementMapping();
            case CorePackage.INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS:
                return getInterLanguageMappingEnds();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING__DEFAULT:
                setDefault((Boolean)newValue);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING:
                setCoreLanguageElementMapping((CORELanguageElementMapping)newValue);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS:
                getInterLanguageMappingEnds().clear();
                getInterLanguageMappingEnds().addAll((Collection<? extends InterLanguageMappingEnd>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING__DEFAULT:
                setDefault(DEFAULT_EDEFAULT);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING:
                setCoreLanguageElementMapping((CORELanguageElementMapping)null);
                return;
            case CorePackage.INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS:
                getInterLanguageMappingEnds().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.INTER_LANGUAGE_MAPPING__DEFAULT:
                return default_ != DEFAULT_EDEFAULT;
            case CorePackage.INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING:
                return coreLanguageElementMapping != null;
            case CorePackage.INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS:
                return interLanguageMappingEnds != null && !interLanguageMappingEnds.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (default: ");
        result.append(default_);
        result.append(')');
        return result.toString();
    }

} //InterLanguageMappingImpl
