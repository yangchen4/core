/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.ActionEffect;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ActionEffectImpl extends EObjectImpl implements ActionEffect {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActionEffectImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.ACTION_EFFECT;
    }

} //ActionEffectImpl
