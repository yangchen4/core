/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.ActionEffect;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.CORECreateMapping;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREPerspectiveAction;
import ca.mcgill.sel.core.CORERedefineAction;
import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.CORERelationship;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseArtefact;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.COREUIElement;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.CreateEffect;
import ca.mcgill.sel.core.CreateModelElementMapping;
import ca.mcgill.sel.core.DeleteEffect;
import ca.mcgill.sel.core.ExistingElementEffect;
import ca.mcgill.sel.core.InterLanguageMapping;
import ca.mcgill.sel.core.InterLanguageMappingEnd;
import ca.mcgill.sel.core.IntraLanguageMapping;
import ca.mcgill.sel.core.LanguageActionType;
import ca.mcgill.sel.core.LayoutElement;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.NavigationMapping;
import ca.mcgill.sel.core.Parameter;
import ca.mcgill.sel.core.ParameterEffect;
import ca.mcgill.sel.core.UpdateEffect;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CorePackageImpl extends EPackageImpl implements CorePackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreArtefactEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreImpactModelEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreConcernEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreFeatureEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreModelCompositionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreLinkEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreNamedElementEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreReuseEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreImpactNodeEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreConfigurationEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreFeatureModelEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreModelReuseEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreContributionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass layoutMapEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass layoutElementEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass layoutContainerMapEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreFeatureImpactNodeEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreModelElementCompositionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreWeightedLinkEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreModelExtensionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreSceneEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass corePerspectiveEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreExternalLanguageEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass corePerspectiveActionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreExternalArtefactEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreuiElementEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreciElementEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreMappingCardinalityEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreLanguageElementMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreReexposeActionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreRedefineActionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreLanguageActionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreCreateMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreLanguageEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreActionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass createModelElementMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreModelElementMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass mappingEndEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass languageMapEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass artefactMapEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass parameterEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass coreLanguageElementEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass existingElementEffectEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass deleteEffectEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass updateEffectEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass navigationMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass interLanguageMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass intraLanguageMappingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass interLanguageMappingEndEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass createEffectEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass actionEffectEClass = null;

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreReuseArtefactEClass = null;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum coreFeatureRelationshipTypeEEnum = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum coreVisibilityTypeEEnum = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum corePartialityTypeEEnum = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum coreRelationshipEEnum = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum cardinalityEEnum = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum languageActionTypeEEnum = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum parameterEffectEEnum = null;

	/**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see ca.mcgill.sel.core.CorePackage#eNS_URI
     * @see #init()
     * @generated
     */
	private CorePackageImpl() {
        super(eNS_URI, CoreFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link CorePackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static CorePackage init() {
        if (isInited) return (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

        // Obtain or create and register package
        Object registeredCorePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        CorePackageImpl theCorePackage = registeredCorePackage instanceof CorePackageImpl ? (CorePackageImpl)registeredCorePackage : new CorePackageImpl();

        isInited = true;

        // Initialize simple dependencies
        XMLTypePackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theCorePackage.createPackageContents();

        // Initialize created meta-data
        theCorePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theCorePackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(CorePackage.eNS_URI, theCorePackage);
        return theCorePackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREArtefact() {
        return coreArtefactEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREArtefact_ModelReuses() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREArtefact_CoreConcern() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREArtefact_ModelExtensions() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREArtefact_UiElements() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREArtefact_CiElements() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREArtefact_TemporaryConcern() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREArtefact_Scene() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(6);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREImpactModel() {
        return coreImpactModelEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREImpactModel_ImpactModelElements() {
        return (EReference)coreImpactModelEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREImpactModel_Layouts() {
        return (EReference)coreImpactModelEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREImpactModel_Contributions() {
        return (EReference)coreImpactModelEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREConcern() {
        return coreConcernEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConcern_Artefacts() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConcern_FeatureModel() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConcern_ImpactModel() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConcern_Scenes() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConcern_Reuses() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConcern_TemporaryArtefacts() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREFeature() {
        return coreFeatureEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeature_RealizedBy() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeature_Children() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeature_Parent() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREFeature_ParentRelationship() {
        return (EAttribute)coreFeatureEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeature_Requires() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeature_Excludes() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREModelComposition() {
        return coreModelCompositionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREModelComposition_Source() {
        return (EReference)coreModelCompositionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREModelComposition_Compositions() {
        return (EReference)coreModelCompositionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORELink() {
        return coreLinkEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELink_To() {
        return (EReference)coreLinkEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELink_From() {
        return (EReference)coreLinkEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORENamedElement() {
        return coreNamedElementEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORENamedElement_Name() {
        return (EAttribute)coreNamedElementEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREReuse() {
        return coreReuseEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREReuse_ReusedConcern() {
        return (EReference)coreReuseEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREReuse_Extends() {
        return (EReference)coreReuseEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREReuse_ModelReuses() {
        return (EReference)coreReuseEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREImpactNode() {
        return coreImpactNodeEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREImpactNode_ScalingFactor() {
        return (EAttribute)coreImpactNodeEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREImpactNode_Offset() {
        return (EAttribute)coreImpactNodeEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREImpactNode_Outgoing() {
        return (EReference)coreImpactNodeEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREImpactNode_Incoming() {
        return (EReference)coreImpactNodeEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREConfiguration() {
        return coreConfigurationEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConfiguration_Selected() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConfiguration_Reexposed() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConfiguration_ExtendingConfigurations() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREConfiguration_ExtendedReuse() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREFeatureModel() {
        return coreFeatureModelEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeatureModel_Features() {
        return (EReference)coreFeatureModelEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeatureModel_Root() {
        return (EReference)coreFeatureModelEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREModelReuse() {
        return coreModelReuseEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREModelReuse_Reuse() {
        return (EReference)coreModelReuseEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREModelReuse_Configuration() {
        return (EReference)coreModelReuseEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREContribution() {
        return coreContributionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREContribution_RelativeWeight() {
        return (EAttribute)coreContributionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREContribution_Source() {
        return (EReference)coreContributionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREContribution_Impacts() {
        return (EReference)coreContributionEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getLayoutMap() {
        return layoutMapEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLayoutMap_Key() {
        return (EReference)layoutMapEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLayoutMap_Value() {
        return (EReference)layoutMapEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getLayoutElement() {
        return layoutElementEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getLayoutElement_X() {
        return (EAttribute)layoutElementEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getLayoutElement_Y() {
        return (EAttribute)layoutElementEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getLayoutContainerMap() {
        return layoutContainerMapEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLayoutContainerMap_Key() {
        return (EReference)layoutContainerMapEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLayoutContainerMap_Value() {
        return (EReference)layoutContainerMapEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREFeatureImpactNode() {
        return coreFeatureImpactNodeEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREFeatureImpactNode_RelativeFeatureWeight() {
        return (EAttribute)coreFeatureImpactNodeEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeatureImpactNode_Represents() {
        return (EReference)coreFeatureImpactNodeEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREFeatureImpactNode_WeightedLinks() {
        return (EReference)coreFeatureImpactNodeEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREModelElementComposition() {
        return coreModelElementCompositionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREWeightedLink() {
        return coreWeightedLinkEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREWeightedLink_Weight() {
        return (EAttribute)coreWeightedLinkEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREModelExtension() {
        return coreModelExtensionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREMapping() {
        return coreMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREMapping_Mappings() {
        return (EReference)coreMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREMapping_ReferencedMappings() {
        return (EReference)coreMappingEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREScene() {
        return coreSceneEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREScene_Realizes() {
        return (EReference)coreSceneEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREScene_PerspectiveName() {
        return (EAttribute)coreSceneEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREScene_ElementMappings() {
        return (EReference)coreSceneEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREScene_Artefacts() {
        return (EReference)coreSceneEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREPerspective() {
        return corePerspectiveEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREPerspective_Mappings() {
        return (EReference)corePerspectiveEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREPerspective_Languages() {
        return (EReference)corePerspectiveEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREPerspective_Default() {
        return (EAttribute)corePerspectiveEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREPerspective_NavigationMappings() {
        return (EReference)corePerspectiveEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREExternalLanguage() {
        return coreExternalLanguageEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREExternalLanguage_NsURI() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREExternalLanguage_ResourceFactory() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREExternalLanguage_AdapterFactory() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREExternalLanguage_WeaverClassName() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREExternalLanguage_FileExtension() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREExternalLanguage_LanguageElements() {
        return (EReference)coreExternalLanguageEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREExternalLanguage_ModelUtilClassName() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(6);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREPerspectiveAction() {
        return corePerspectiveActionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREPerspectiveAction_ForRole() {
        return (EAttribute)corePerspectiveActionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREPerspectiveAction_ActionIdentifier() {
        return (EAttribute)corePerspectiveActionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREExternalArtefact() {
        return coreExternalArtefactEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREExternalArtefact_RootModelElement() {
        return (EReference)coreExternalArtefactEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREExternalArtefact_LanguageName() {
        return (EAttribute)coreExternalArtefactEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREUIElement() {
        return coreuiElementEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREUIElement_ModelElement() {
        return (EReference)coreuiElementEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORECIElement() {
        return coreciElementEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORECIElement_Partiality() {
        return (EAttribute)coreciElementEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORECIElement_ModelElement() {
        return (EReference)coreciElementEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORECIElement_MappingCardinality() {
        return (EReference)coreciElementEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORECIElement_ReferenceCardinality() {
        return (EReference)coreciElementEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREMappingCardinality() {
        return coreMappingCardinalityEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREMappingCardinality_LowerBound() {
        return (EAttribute)coreMappingCardinalityEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREMappingCardinality_UpperBound() {
        return (EAttribute)coreMappingCardinalityEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREMappingCardinality_Name() {
        return (EAttribute)coreMappingCardinalityEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORELanguageElementMapping() {
        return coreLanguageElementMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElementMapping_Actions() {
        return (EReference)coreLanguageElementMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElementMapping_MappingEnds() {
        return (EReference)coreLanguageElementMappingEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORELanguageElementMapping_Identifier() {
        return (EAttribute)coreLanguageElementMappingEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElementMapping_NestedMappings() {
        return (EReference)coreLanguageElementMappingEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORELanguageElementMapping_MatchMaker() {
        return (EAttribute)coreLanguageElementMappingEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREReexposeAction() {
        return coreReexposeActionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREReexposeAction_ReexposedAction() {
        return (EReference)coreReexposeActionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORERedefineAction() {
        return coreRedefineActionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORERedefineAction_RedefinedAction() {
        return (EReference)coreRedefineActionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORERedefineAction_ReusedActions() {
        return (EReference)coreRedefineActionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORELanguageAction() {
        return coreLanguageActionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORELanguageAction_ClassQualifiedName() {
        return (EAttribute)coreLanguageActionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORELanguageAction_MethodName() {
        return (EAttribute)coreLanguageActionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageAction_Parameters() {
        return (EReference)coreLanguageActionEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageAction_SecondaryEffects() {
        return (EReference)coreLanguageActionEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORELanguageAction_ActionType() {
        return (EAttribute)coreLanguageActionEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORECreateMapping() {
        return coreCreateMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORECreateMapping_Type() {
        return (EReference)coreCreateMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORECreateMapping_ExtendedAction() {
        return (EReference)coreCreateMappingEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORECreateMapping_ModelElements() {
        return (EReference)coreCreateMappingEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORELanguage() {
        return coreLanguageEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguage_Actions() {
        return (EReference)coreLanguageEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREAction() {
        return coreActionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREAction_Name() {
        return (EAttribute)coreActionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCreateModelElementMapping() {
        return createModelElementMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCreateModelElementMapping_ReusedActions() {
        return (EReference)createModelElementMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCOREModelElementMapping() {
        return coreModelElementMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCOREModelElementMapping_ModelElements() {
        return (EReference)coreModelElementMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCOREModelElementMapping_LEMid() {
        return (EAttribute)coreModelElementMappingEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMappingEnd() {
        return mappingEndEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMappingEnd_Cardinality() {
        return (EAttribute)mappingEndEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMappingEnd_RoleName() {
        return (EAttribute)mappingEndEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMappingEnd_Type() {
        return (EReference)mappingEndEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMappingEnd_LanguageElement() {
        return (EReference)mappingEndEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getLanguageMap() {
        return languageMapEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getLanguageMap_Key() {
        return (EAttribute)languageMapEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLanguageMap_Value() {
        return (EReference)languageMapEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getArtefactMap() {
        return artefactMapEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getArtefactMap_Value() {
        return (EReference)artefactMapEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getArtefactMap_Key() {
        return (EAttribute)artefactMapEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getParameter() {
        return parameterEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getParameter_Name() {
        return (EAttribute)parameterEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getParameter_Type() {
        return (EAttribute)parameterEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCORELanguageElement() {
        return coreLanguageElementEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElement_LanguageActions() {
        return (EReference)coreLanguageElementEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElement_LanguageElement() {
        return (EReference)coreLanguageElementEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElement_Language() {
        return (EReference)coreLanguageElementEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getCORELanguageElement_Name() {
        return (EAttribute)coreLanguageElementEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElement_NestedElements() {
        return (EReference)coreLanguageElementEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCORELanguageElement_Owner() {
        return (EReference)coreLanguageElementEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getExistingElementEffect() {
        return existingElementEffectEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getExistingElementEffect_Parameter() {
        return (EReference)existingElementEffectEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getExistingElementEffect_ParameterEffect() {
        return (EAttribute)existingElementEffectEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDeleteEffect() {
        return deleteEffectEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getUpdateEffect() {
        return updateEffectEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getUpdateEffect_AffectedFeature() {
        return (EReference)updateEffectEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getNavigationMapping() {
        return navigationMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getNavigationMapping_Active() {
        return (EAttribute)navigationMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getInterLanguageMapping() {
        return interLanguageMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getInterLanguageMapping_Default() {
        return (EAttribute)interLanguageMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getInterLanguageMapping_CoreLanguageElementMapping() {
        return (EReference)interLanguageMappingEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getInterLanguageMapping_InterLanguageMappingEnds() {
        return (EReference)interLanguageMappingEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getIntraLanguageMapping() {
        return intraLanguageMappingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getIntraLanguageMapping_Name() {
        return (EAttribute)intraLanguageMappingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getIntraLanguageMapping_Closure() {
        return (EAttribute)intraLanguageMappingEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getIntraLanguageMapping_Reuse() {
        return (EAttribute)intraLanguageMappingEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getIntraLanguageMapping_IncreaseDepth() {
        return (EAttribute)intraLanguageMappingEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getIntraLanguageMapping_ChangeModel() {
        return (EAttribute)intraLanguageMappingEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getIntraLanguageMapping_From() {
        return (EReference)intraLanguageMappingEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getIntraLanguageMapping_Hops() {
        return (EReference)intraLanguageMappingEClass.getEStructuralFeatures().get(6);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getInterLanguageMappingEnd() {
        return interLanguageMappingEndEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getInterLanguageMappingEnd_Name() {
        return (EAttribute)interLanguageMappingEndEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getInterLanguageMappingEnd_Origin() {
        return (EAttribute)interLanguageMappingEndEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getInterLanguageMappingEnd_Destination() {
        return (EAttribute)interLanguageMappingEndEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getInterLanguageMappingEnd_MappingEnd() {
        return (EReference)interLanguageMappingEndEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getCreateEffect() {
        return createEffectEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getCreateEffect_CorelanguageElement() {
        return (EReference)createEffectEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getActionEffect() {
        return actionEffectEClass;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREReuseArtefact() {
        return coreReuseArtefactEClass;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getCOREFeatureRelationshipType() {
        return coreFeatureRelationshipTypeEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getCOREVisibilityType() {
        return coreVisibilityTypeEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getCOREPartialityType() {
        return corePartialityTypeEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getCORERelationship() {
        return coreRelationshipEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getCardinality() {
        return cardinalityEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getLanguageActionType() {
        return languageActionTypeEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getParameterEffect() {
        return parameterEffectEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CoreFactory getCoreFactory() {
        return (CoreFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        coreArtefactEClass = createEClass(CORE_ARTEFACT);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__MODEL_REUSES);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__CORE_CONCERN);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__MODEL_EXTENSIONS);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__UI_ELEMENTS);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__CI_ELEMENTS);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__TEMPORARY_CONCERN);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__SCENE);

        coreImpactModelEClass = createEClass(CORE_IMPACT_MODEL);
        createEReference(coreImpactModelEClass, CORE_IMPACT_MODEL__IMPACT_MODEL_ELEMENTS);
        createEReference(coreImpactModelEClass, CORE_IMPACT_MODEL__LAYOUTS);
        createEReference(coreImpactModelEClass, CORE_IMPACT_MODEL__CONTRIBUTIONS);

        coreConcernEClass = createEClass(CORE_CONCERN);
        createEReference(coreConcernEClass, CORE_CONCERN__ARTEFACTS);
        createEReference(coreConcernEClass, CORE_CONCERN__FEATURE_MODEL);
        createEReference(coreConcernEClass, CORE_CONCERN__IMPACT_MODEL);
        createEReference(coreConcernEClass, CORE_CONCERN__SCENES);
        createEReference(coreConcernEClass, CORE_CONCERN__REUSES);
        createEReference(coreConcernEClass, CORE_CONCERN__TEMPORARY_ARTEFACTS);

        coreFeatureEClass = createEClass(CORE_FEATURE);
        createEReference(coreFeatureEClass, CORE_FEATURE__REALIZED_BY);
        createEReference(coreFeatureEClass, CORE_FEATURE__CHILDREN);
        createEReference(coreFeatureEClass, CORE_FEATURE__PARENT);
        createEAttribute(coreFeatureEClass, CORE_FEATURE__PARENT_RELATIONSHIP);
        createEReference(coreFeatureEClass, CORE_FEATURE__REQUIRES);
        createEReference(coreFeatureEClass, CORE_FEATURE__EXCLUDES);

        coreModelCompositionEClass = createEClass(CORE_MODEL_COMPOSITION);
        createEReference(coreModelCompositionEClass, CORE_MODEL_COMPOSITION__SOURCE);
        createEReference(coreModelCompositionEClass, CORE_MODEL_COMPOSITION__COMPOSITIONS);

        coreLinkEClass = createEClass(CORE_LINK);
        createEReference(coreLinkEClass, CORE_LINK__TO);
        createEReference(coreLinkEClass, CORE_LINK__FROM);

        coreNamedElementEClass = createEClass(CORE_NAMED_ELEMENT);
        createEAttribute(coreNamedElementEClass, CORE_NAMED_ELEMENT__NAME);

        coreReuseEClass = createEClass(CORE_REUSE);
        createEReference(coreReuseEClass, CORE_REUSE__REUSED_CONCERN);
        createEReference(coreReuseEClass, CORE_REUSE__EXTENDS);
        createEReference(coreReuseEClass, CORE_REUSE__MODEL_REUSES);

        coreImpactNodeEClass = createEClass(CORE_IMPACT_NODE);
        createEAttribute(coreImpactNodeEClass, CORE_IMPACT_NODE__SCALING_FACTOR);
        createEAttribute(coreImpactNodeEClass, CORE_IMPACT_NODE__OFFSET);
        createEReference(coreImpactNodeEClass, CORE_IMPACT_NODE__OUTGOING);
        createEReference(coreImpactNodeEClass, CORE_IMPACT_NODE__INCOMING);

        coreConfigurationEClass = createEClass(CORE_CONFIGURATION);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__SELECTED);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__REEXPOSED);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__EXTENDED_REUSE);

        coreFeatureModelEClass = createEClass(CORE_FEATURE_MODEL);
        createEReference(coreFeatureModelEClass, CORE_FEATURE_MODEL__FEATURES);
        createEReference(coreFeatureModelEClass, CORE_FEATURE_MODEL__ROOT);

        coreModelReuseEClass = createEClass(CORE_MODEL_REUSE);
        createEReference(coreModelReuseEClass, CORE_MODEL_REUSE__REUSE);
        createEReference(coreModelReuseEClass, CORE_MODEL_REUSE__CONFIGURATION);

        coreContributionEClass = createEClass(CORE_CONTRIBUTION);
        createEAttribute(coreContributionEClass, CORE_CONTRIBUTION__RELATIVE_WEIGHT);
        createEReference(coreContributionEClass, CORE_CONTRIBUTION__SOURCE);
        createEReference(coreContributionEClass, CORE_CONTRIBUTION__IMPACTS);

        layoutMapEClass = createEClass(LAYOUT_MAP);
        createEReference(layoutMapEClass, LAYOUT_MAP__KEY);
        createEReference(layoutMapEClass, LAYOUT_MAP__VALUE);

        layoutElementEClass = createEClass(LAYOUT_ELEMENT);
        createEAttribute(layoutElementEClass, LAYOUT_ELEMENT__X);
        createEAttribute(layoutElementEClass, LAYOUT_ELEMENT__Y);

        layoutContainerMapEClass = createEClass(LAYOUT_CONTAINER_MAP);
        createEReference(layoutContainerMapEClass, LAYOUT_CONTAINER_MAP__KEY);
        createEReference(layoutContainerMapEClass, LAYOUT_CONTAINER_MAP__VALUE);

        coreFeatureImpactNodeEClass = createEClass(CORE_FEATURE_IMPACT_NODE);
        createEAttribute(coreFeatureImpactNodeEClass, CORE_FEATURE_IMPACT_NODE__RELATIVE_FEATURE_WEIGHT);
        createEReference(coreFeatureImpactNodeEClass, CORE_FEATURE_IMPACT_NODE__REPRESENTS);
        createEReference(coreFeatureImpactNodeEClass, CORE_FEATURE_IMPACT_NODE__WEIGHTED_LINKS);

        coreModelElementCompositionEClass = createEClass(CORE_MODEL_ELEMENT_COMPOSITION);

        coreWeightedLinkEClass = createEClass(CORE_WEIGHTED_LINK);
        createEAttribute(coreWeightedLinkEClass, CORE_WEIGHTED_LINK__WEIGHT);

        coreModelExtensionEClass = createEClass(CORE_MODEL_EXTENSION);

        coreMappingEClass = createEClass(CORE_MAPPING);
        createEReference(coreMappingEClass, CORE_MAPPING__MAPPINGS);
        createEReference(coreMappingEClass, CORE_MAPPING__REFERENCED_MAPPINGS);

        coreSceneEClass = createEClass(CORE_SCENE);
        createEReference(coreSceneEClass, CORE_SCENE__REALIZES);
        createEAttribute(coreSceneEClass, CORE_SCENE__PERSPECTIVE_NAME);
        createEReference(coreSceneEClass, CORE_SCENE__ELEMENT_MAPPINGS);
        createEReference(coreSceneEClass, CORE_SCENE__ARTEFACTS);

        corePerspectiveEClass = createEClass(CORE_PERSPECTIVE);
        createEReference(corePerspectiveEClass, CORE_PERSPECTIVE__MAPPINGS);
        createEReference(corePerspectiveEClass, CORE_PERSPECTIVE__LANGUAGES);
        createEAttribute(corePerspectiveEClass, CORE_PERSPECTIVE__DEFAULT);
        createEReference(corePerspectiveEClass, CORE_PERSPECTIVE__NAVIGATION_MAPPINGS);

        coreExternalLanguageEClass = createEClass(CORE_EXTERNAL_LANGUAGE);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__NS_URI);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION);
        createEReference(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME);

        corePerspectiveActionEClass = createEClass(CORE_PERSPECTIVE_ACTION);
        createEAttribute(corePerspectiveActionEClass, CORE_PERSPECTIVE_ACTION__FOR_ROLE);
        createEAttribute(corePerspectiveActionEClass, CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER);

        coreExternalArtefactEClass = createEClass(CORE_EXTERNAL_ARTEFACT);
        createEReference(coreExternalArtefactEClass, CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT);
        createEAttribute(coreExternalArtefactEClass, CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME);

        coreuiElementEClass = createEClass(COREUI_ELEMENT);
        createEReference(coreuiElementEClass, COREUI_ELEMENT__MODEL_ELEMENT);

        coreciElementEClass = createEClass(CORECI_ELEMENT);
        createEAttribute(coreciElementEClass, CORECI_ELEMENT__PARTIALITY);
        createEReference(coreciElementEClass, CORECI_ELEMENT__MODEL_ELEMENT);
        createEReference(coreciElementEClass, CORECI_ELEMENT__MAPPING_CARDINALITY);
        createEReference(coreciElementEClass, CORECI_ELEMENT__REFERENCE_CARDINALITY);

        coreMappingCardinalityEClass = createEClass(CORE_MAPPING_CARDINALITY);
        createEAttribute(coreMappingCardinalityEClass, CORE_MAPPING_CARDINALITY__LOWER_BOUND);
        createEAttribute(coreMappingCardinalityEClass, CORE_MAPPING_CARDINALITY__UPPER_BOUND);
        createEAttribute(coreMappingCardinalityEClass, CORE_MAPPING_CARDINALITY__NAME);

        coreLanguageElementMappingEClass = createEClass(CORE_LANGUAGE_ELEMENT_MAPPING);
        createEReference(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS);
        createEReference(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS);
        createEAttribute(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER);
        createEReference(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS);
        createEAttribute(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER);

        coreReexposeActionEClass = createEClass(CORE_REEXPOSE_ACTION);
        createEReference(coreReexposeActionEClass, CORE_REEXPOSE_ACTION__REEXPOSED_ACTION);

        coreRedefineActionEClass = createEClass(CORE_REDEFINE_ACTION);
        createEReference(coreRedefineActionEClass, CORE_REDEFINE_ACTION__REDEFINED_ACTION);
        createEReference(coreRedefineActionEClass, CORE_REDEFINE_ACTION__REUSED_ACTIONS);

        coreLanguageActionEClass = createEClass(CORE_LANGUAGE_ACTION);
        createEAttribute(coreLanguageActionEClass, CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME);
        createEAttribute(coreLanguageActionEClass, CORE_LANGUAGE_ACTION__METHOD_NAME);
        createEReference(coreLanguageActionEClass, CORE_LANGUAGE_ACTION__PARAMETERS);
        createEReference(coreLanguageActionEClass, CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS);
        createEAttribute(coreLanguageActionEClass, CORE_LANGUAGE_ACTION__ACTION_TYPE);

        coreCreateMappingEClass = createEClass(CORE_CREATE_MAPPING);
        createEReference(coreCreateMappingEClass, CORE_CREATE_MAPPING__TYPE);
        createEReference(coreCreateMappingEClass, CORE_CREATE_MAPPING__EXTENDED_ACTION);
        createEReference(coreCreateMappingEClass, CORE_CREATE_MAPPING__MODEL_ELEMENTS);

        coreLanguageEClass = createEClass(CORE_LANGUAGE);
        createEReference(coreLanguageEClass, CORE_LANGUAGE__ACTIONS);

        coreActionEClass = createEClass(CORE_ACTION);
        createEAttribute(coreActionEClass, CORE_ACTION__NAME);

        createModelElementMappingEClass = createEClass(CREATE_MODEL_ELEMENT_MAPPING);
        createEReference(createModelElementMappingEClass, CREATE_MODEL_ELEMENT_MAPPING__REUSED_ACTIONS);

        coreModelElementMappingEClass = createEClass(CORE_MODEL_ELEMENT_MAPPING);
        createEReference(coreModelElementMappingEClass, CORE_MODEL_ELEMENT_MAPPING__MODEL_ELEMENTS);
        createEAttribute(coreModelElementMappingEClass, CORE_MODEL_ELEMENT_MAPPING__LE_MID);

        mappingEndEClass = createEClass(MAPPING_END);
        createEAttribute(mappingEndEClass, MAPPING_END__CARDINALITY);
        createEAttribute(mappingEndEClass, MAPPING_END__ROLE_NAME);
        createEReference(mappingEndEClass, MAPPING_END__TYPE);
        createEReference(mappingEndEClass, MAPPING_END__LANGUAGE_ELEMENT);

        languageMapEClass = createEClass(LANGUAGE_MAP);
        createEAttribute(languageMapEClass, LANGUAGE_MAP__KEY);
        createEReference(languageMapEClass, LANGUAGE_MAP__VALUE);

        artefactMapEClass = createEClass(ARTEFACT_MAP);
        createEReference(artefactMapEClass, ARTEFACT_MAP__VALUE);
        createEAttribute(artefactMapEClass, ARTEFACT_MAP__KEY);

        parameterEClass = createEClass(PARAMETER);
        createEAttribute(parameterEClass, PARAMETER__NAME);
        createEAttribute(parameterEClass, PARAMETER__TYPE);

        coreLanguageElementEClass = createEClass(CORE_LANGUAGE_ELEMENT);
        createEReference(coreLanguageElementEClass, CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS);
        createEReference(coreLanguageElementEClass, CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT);
        createEReference(coreLanguageElementEClass, CORE_LANGUAGE_ELEMENT__LANGUAGE);
        createEAttribute(coreLanguageElementEClass, CORE_LANGUAGE_ELEMENT__NAME);
        createEReference(coreLanguageElementEClass, CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS);
        createEReference(coreLanguageElementEClass, CORE_LANGUAGE_ELEMENT__OWNER);

        existingElementEffectEClass = createEClass(EXISTING_ELEMENT_EFFECT);
        createEReference(existingElementEffectEClass, EXISTING_ELEMENT_EFFECT__PARAMETER);
        createEAttribute(existingElementEffectEClass, EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT);

        deleteEffectEClass = createEClass(DELETE_EFFECT);

        updateEffectEClass = createEClass(UPDATE_EFFECT);
        createEReference(updateEffectEClass, UPDATE_EFFECT__AFFECTED_FEATURE);

        navigationMappingEClass = createEClass(NAVIGATION_MAPPING);
        createEAttribute(navigationMappingEClass, NAVIGATION_MAPPING__ACTIVE);

        interLanguageMappingEClass = createEClass(INTER_LANGUAGE_MAPPING);
        createEAttribute(interLanguageMappingEClass, INTER_LANGUAGE_MAPPING__DEFAULT);
        createEReference(interLanguageMappingEClass, INTER_LANGUAGE_MAPPING__CORE_LANGUAGE_ELEMENT_MAPPING);
        createEReference(interLanguageMappingEClass, INTER_LANGUAGE_MAPPING__INTER_LANGUAGE_MAPPING_ENDS);

        intraLanguageMappingEClass = createEClass(INTRA_LANGUAGE_MAPPING);
        createEAttribute(intraLanguageMappingEClass, INTRA_LANGUAGE_MAPPING__NAME);
        createEAttribute(intraLanguageMappingEClass, INTRA_LANGUAGE_MAPPING__CLOSURE);
        createEAttribute(intraLanguageMappingEClass, INTRA_LANGUAGE_MAPPING__REUSE);
        createEAttribute(intraLanguageMappingEClass, INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH);
        createEAttribute(intraLanguageMappingEClass, INTRA_LANGUAGE_MAPPING__CHANGE_MODEL);
        createEReference(intraLanguageMappingEClass, INTRA_LANGUAGE_MAPPING__FROM);
        createEReference(intraLanguageMappingEClass, INTRA_LANGUAGE_MAPPING__HOPS);

        interLanguageMappingEndEClass = createEClass(INTER_LANGUAGE_MAPPING_END);
        createEAttribute(interLanguageMappingEndEClass, INTER_LANGUAGE_MAPPING_END__NAME);
        createEAttribute(interLanguageMappingEndEClass, INTER_LANGUAGE_MAPPING_END__ORIGIN);
        createEAttribute(interLanguageMappingEndEClass, INTER_LANGUAGE_MAPPING_END__DESTINATION);
        createEReference(interLanguageMappingEndEClass, INTER_LANGUAGE_MAPPING_END__MAPPING_END);

        createEffectEClass = createEClass(CREATE_EFFECT);
        createEReference(createEffectEClass, CREATE_EFFECT__CORELANGUAGE_ELEMENT);

        actionEffectEClass = createEClass(ACTION_EFFECT);

        coreReuseArtefactEClass = createEClass(CORE_REUSE_ARTEFACT);

        // Create enums
        coreFeatureRelationshipTypeEEnum = createEEnum(CORE_FEATURE_RELATIONSHIP_TYPE);
        coreVisibilityTypeEEnum = createEEnum(CORE_VISIBILITY_TYPE);
        corePartialityTypeEEnum = createEEnum(CORE_PARTIALITY_TYPE);
        coreRelationshipEEnum = createEEnum(CORE_RELATIONSHIP);
        cardinalityEEnum = createEEnum(CARDINALITY);
        languageActionTypeEEnum = createEEnum(LANGUAGE_ACTION_TYPE);
        parameterEffectEEnum = createEEnum(PARAMETER_EFFECT);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

        // Create type parameters
        ETypeParameter coreLinkEClass_T = addETypeParameter(coreLinkEClass, "T");
        addETypeParameter(coreModelElementCompositionEClass, "T");
        ETypeParameter coreMappingEClass_T = addETypeParameter(coreMappingEClass, "T");

        // Set bounds for type parameters

        // Add supertypes to classes
        coreArtefactEClass.getESuperTypes().add(this.getCORENamedElement());
        coreImpactModelEClass.getESuperTypes().add(this.getCOREArtefact());
        coreConcernEClass.getESuperTypes().add(this.getCORENamedElement());
        coreFeatureEClass.getESuperTypes().add(this.getCORENamedElement());
        EGenericType g1 = createEGenericType(this.getCOREModelElementComposition());
        EGenericType g2 = createEGenericType(coreLinkEClass_T);
        g1.getETypeArguments().add(g2);
        coreLinkEClass.getEGenericSuperTypes().add(g1);
        coreReuseEClass.getESuperTypes().add(this.getCORENamedElement());
        coreImpactNodeEClass.getESuperTypes().add(this.getCORENamedElement());
        coreConfigurationEClass.getESuperTypes().add(this.getCOREModelComposition());
        coreConfigurationEClass.getESuperTypes().add(this.getCORENamedElement());
        coreFeatureModelEClass.getESuperTypes().add(this.getCOREArtefact());
        coreModelReuseEClass.getESuperTypes().add(this.getCOREModelComposition());
        coreFeatureImpactNodeEClass.getESuperTypes().add(this.getCOREImpactNode());
        g1 = createEGenericType(this.getCORELink());
        g2 = createEGenericType(this.getCOREImpactNode());
        g1.getETypeArguments().add(g2);
        coreWeightedLinkEClass.getEGenericSuperTypes().add(g1);
        coreModelExtensionEClass.getESuperTypes().add(this.getCOREModelComposition());
        g1 = createEGenericType(this.getCORELink());
        g2 = createEGenericType(coreMappingEClass_T);
        g1.getETypeArguments().add(g2);
        coreMappingEClass.getEGenericSuperTypes().add(g1);
        coreSceneEClass.getESuperTypes().add(this.getCORENamedElement());
        corePerspectiveEClass.getESuperTypes().add(this.getCORELanguage());
        coreExternalLanguageEClass.getESuperTypes().add(this.getCORELanguage());
        corePerspectiveActionEClass.getESuperTypes().add(this.getCOREAction());
        coreExternalArtefactEClass.getESuperTypes().add(this.getCOREArtefact());
        coreReexposeActionEClass.getESuperTypes().add(this.getCOREPerspectiveAction());
        coreRedefineActionEClass.getESuperTypes().add(this.getCOREPerspectiveAction());
        coreLanguageActionEClass.getESuperTypes().add(this.getCOREAction());
        coreCreateMappingEClass.getESuperTypes().add(this.getCOREPerspectiveAction());
        coreLanguageEClass.getESuperTypes().add(this.getCOREArtefact());
        createModelElementMappingEClass.getESuperTypes().add(this.getCOREPerspectiveAction());
        existingElementEffectEClass.getESuperTypes().add(this.getActionEffect());
        deleteEffectEClass.getESuperTypes().add(this.getExistingElementEffect());
        updateEffectEClass.getESuperTypes().add(this.getExistingElementEffect());
        interLanguageMappingEClass.getESuperTypes().add(this.getNavigationMapping());
        intraLanguageMappingEClass.getESuperTypes().add(this.getNavigationMapping());
        createEffectEClass.getESuperTypes().add(this.getActionEffect());
        coreReuseArtefactEClass.getESuperTypes().add(this.getCOREArtefact());

        // Initialize classes and features; add operations and parameters
        initEClass(coreArtefactEClass, COREArtefact.class, "COREArtefact", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREArtefact_ModelReuses(), this.getCOREModelReuse(), null, "modelReuses", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_CoreConcern(), this.getCOREConcern(), this.getCOREConcern_Artefacts(), "coreConcern", null, 1, 1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_ModelExtensions(), this.getCOREModelExtension(), null, "modelExtensions", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_UiElements(), this.getCOREUIElement(), null, "uiElements", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_CiElements(), this.getCORECIElement(), null, "ciElements", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_TemporaryConcern(), this.getCOREConcern(), this.getCOREConcern_TemporaryArtefacts(), "temporaryConcern", null, 0, 1, COREArtefact.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_Scene(), this.getCOREScene(), null, "scene", null, 0, 1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreImpactModelEClass, COREImpactModel.class, "COREImpactModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREImpactModel_ImpactModelElements(), this.getCOREImpactNode(), null, "impactModelElements", null, 0, -1, COREImpactModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactModel_Layouts(), this.getLayoutContainerMap(), null, "layouts", null, 0, -1, COREImpactModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactModel_Contributions(), this.getCOREContribution(), null, "contributions", null, 0, -1, COREImpactModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreConcernEClass, COREConcern.class, "COREConcern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREConcern_Artefacts(), this.getCOREArtefact(), this.getCOREArtefact_CoreConcern(), "artefacts", null, 1, -1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_FeatureModel(), this.getCOREFeatureModel(), null, "featureModel", null, 1, 1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_ImpactModel(), this.getCOREImpactModel(), null, "impactModel", null, 0, 1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_Scenes(), this.getCOREScene(), null, "scenes", null, 0, -1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_Reuses(), this.getCOREReuse(), null, "reuses", null, 0, -1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_TemporaryArtefacts(), this.getCOREArtefact(), this.getCOREArtefact_TemporaryConcern(), "temporaryArtefacts", null, 0, -1, COREConcern.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreFeatureEClass, COREFeature.class, "COREFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREFeature_RealizedBy(), this.getCOREScene(), this.getCOREScene_Realizes(), "realizedBy", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Children(), this.getCOREFeature(), this.getCOREFeature_Parent(), "children", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Parent(), this.getCOREFeature(), this.getCOREFeature_Children(), "parent", null, 0, 1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREFeature_ParentRelationship(), this.getCOREFeatureRelationshipType(), "parentRelationship", "None", 1, 1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Requires(), this.getCOREFeature(), null, "requires", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Excludes(), this.getCOREFeature(), null, "excludes", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelCompositionEClass, COREModelComposition.class, "COREModelComposition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREModelComposition_Source(), this.getCOREArtefact(), null, "source", null, 1, 1, COREModelComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        g1 = createEGenericType(this.getCOREModelElementComposition());
        g2 = createEGenericType();
        g1.getETypeArguments().add(g2);
        initEReference(getCOREModelComposition_Compositions(), g1, null, "compositions", null, 0, -1, COREModelComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLinkEClass, CORELink.class, "CORELink", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        g1 = createEGenericType(coreLinkEClass_T);
        initEReference(getCORELink_To(), g1, null, "to", null, 1, 1, CORELink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        g1 = createEGenericType(coreLinkEClass_T);
        initEReference(getCORELink_From(), g1, null, "from", null, 1, 1, CORELink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreNamedElementEClass, CORENamedElement.class, "CORENamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCORENamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, CORENamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreReuseEClass, COREReuse.class, "COREReuse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREReuse_ReusedConcern(), this.getCOREConcern(), null, "reusedConcern", null, 1, 1, COREReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREReuse_Extends(), this.getCOREReuse(), null, "extends", null, 0, 1, COREReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREReuse_ModelReuses(), this.getCOREModelReuse(), this.getCOREModelReuse_Reuse(), "modelReuses", null, 0, -1, COREReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreImpactNodeEClass, COREImpactNode.class, "COREImpactNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREImpactNode_ScalingFactor(), ecorePackage.getEFloat(), "scalingFactor", null, 1, 1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREImpactNode_Offset(), ecorePackage.getEFloat(), "offset", null, 1, 1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactNode_Outgoing(), this.getCOREContribution(), this.getCOREContribution_Source(), "outgoing", null, 0, -1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactNode_Incoming(), this.getCOREContribution(), this.getCOREContribution_Impacts(), "incoming", null, 0, -1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreConfigurationEClass, COREConfiguration.class, "COREConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREConfiguration_Selected(), this.getCOREFeature(), null, "selected", null, 0, -1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConfiguration_Reexposed(), this.getCOREFeature(), null, "reexposed", null, 0, -1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConfiguration_ExtendingConfigurations(), this.getCOREConfiguration(), null, "extendingConfigurations", null, 0, -1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConfiguration_ExtendedReuse(), this.getCOREReuse(), null, "extendedReuse", null, 0, 1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreFeatureModelEClass, COREFeatureModel.class, "COREFeatureModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREFeatureModel_Features(), this.getCOREFeature(), null, "features", null, 0, -1, COREFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeatureModel_Root(), this.getCOREFeature(), null, "root", null, 1, 1, COREFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelReuseEClass, COREModelReuse.class, "COREModelReuse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREModelReuse_Reuse(), this.getCOREReuse(), this.getCOREReuse_ModelReuses(), "reuse", null, 1, 1, COREModelReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREModelReuse_Configuration(), this.getCOREConfiguration(), null, "configuration", null, 1, 1, COREModelReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreContributionEClass, COREContribution.class, "COREContribution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREContribution_RelativeWeight(), ecorePackage.getEInt(), "relativeWeight", null, 1, 1, COREContribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREContribution_Source(), this.getCOREImpactNode(), this.getCOREImpactNode_Outgoing(), "source", null, 1, 1, COREContribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREContribution_Impacts(), this.getCOREImpactNode(), this.getCOREImpactNode_Incoming(), "impacts", null, 1, 1, COREContribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(layoutMapEClass, Map.Entry.class, "LayoutMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getLayoutMap_Key(), ecorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLayoutMap_Value(), this.getLayoutElement(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(layoutElementEClass, LayoutElement.class, "LayoutElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLayoutElement_X(), ecorePackage.getEFloat(), "x", "0.0", 1, 1, LayoutElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLayoutElement_Y(), ecorePackage.getEFloat(), "y", "0.0", 1, 1, LayoutElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(layoutContainerMapEClass, Map.Entry.class, "LayoutContainerMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getLayoutContainerMap_Key(), ecorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLayoutContainerMap_Value(), this.getLayoutMap(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreFeatureImpactNodeEClass, COREFeatureImpactNode.class, "COREFeatureImpactNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREFeatureImpactNode_RelativeFeatureWeight(), ecorePackage.getEInt(), "relativeFeatureWeight", null, 1, 1, COREFeatureImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeatureImpactNode_Represents(), this.getCOREFeature(), null, "represents", null, 1, 1, COREFeatureImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeatureImpactNode_WeightedLinks(), this.getCOREWeightedLink(), null, "weightedLinks", null, 0, -1, COREFeatureImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelElementCompositionEClass, COREModelElementComposition.class, "COREModelElementComposition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(coreWeightedLinkEClass, COREWeightedLink.class, "COREWeightedLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREWeightedLink_Weight(), ecorePackage.getEInt(), "weight", null, 1, 1, COREWeightedLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelExtensionEClass, COREModelExtension.class, "COREModelExtension", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(coreMappingEClass, COREMapping.class, "COREMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        g1 = createEGenericType(this.getCOREMapping());
        g2 = createEGenericType();
        g1.getETypeArguments().add(g2);
        initEReference(getCOREMapping_Mappings(), g1, null, "mappings", null, 0, -1, COREMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        g1 = createEGenericType(this.getCOREMapping());
        g2 = createEGenericType();
        g1.getETypeArguments().add(g2);
        initEReference(getCOREMapping_ReferencedMappings(), g1, null, "referencedMappings", null, 0, -1, COREMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreSceneEClass, COREScene.class, "COREScene", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREScene_Realizes(), this.getCOREFeature(), this.getCOREFeature_RealizedBy(), "realizes", null, 0, -1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREScene_PerspectiveName(), ecorePackage.getEString(), "perspectiveName", null, 0, 1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREScene_ElementMappings(), this.getCOREModelElementMapping(), null, "elementMappings", null, 0, -1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREScene_Artefacts(), this.getArtefactMap(), null, "artefacts", null, 0, -1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(corePerspectiveEClass, COREPerspective.class, "COREPerspective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREPerspective_Mappings(), this.getCORELanguageElementMapping(), null, "mappings", null, 0, -1, COREPerspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREPerspective_Languages(), this.getLanguageMap(), null, "languages", null, 0, -1, COREPerspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREPerspective_Default(), ecorePackage.getEString(), "default", null, 0, 1, COREPerspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREPerspective_NavigationMappings(), this.getNavigationMapping(), null, "navigationMappings", null, 0, -1, COREPerspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreExternalLanguageEClass, COREExternalLanguage.class, "COREExternalLanguage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREExternalLanguage_NsURI(), ecorePackage.getEString(), "nsURI", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_ResourceFactory(), ecorePackage.getEString(), "resourceFactory", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_AdapterFactory(), ecorePackage.getEString(), "adapterFactory", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_WeaverClassName(), ecorePackage.getEString(), "weaverClassName", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_FileExtension(), ecorePackage.getEString(), "fileExtension", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREExternalLanguage_LanguageElements(), this.getCORELanguageElement(), this.getCORELanguageElement_Language(), "languageElements", null, 0, -1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_ModelUtilClassName(), ecorePackage.getEString(), "modelUtilClassName", null, 0, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(corePerspectiveActionEClass, COREPerspectiveAction.class, "COREPerspectiveAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREPerspectiveAction_ForRole(), ecorePackage.getEString(), "forRole", null, 0, 1, COREPerspectiveAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREPerspectiveAction_ActionIdentifier(), ecorePackage.getEInt(), "actionIdentifier", null, 0, 1, COREPerspectiveAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreExternalArtefactEClass, COREExternalArtefact.class, "COREExternalArtefact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREExternalArtefact_RootModelElement(), ecorePackage.getEObject(), null, "rootModelElement", null, 1, 1, COREExternalArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalArtefact_LanguageName(), ecorePackage.getEString(), "languageName", null, 0, 1, COREExternalArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreuiElementEClass, COREUIElement.class, "COREUIElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREUIElement_ModelElement(), ecorePackage.getEObject(), null, "modelElement", null, 1, 1, COREUIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreciElementEClass, CORECIElement.class, "CORECIElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCORECIElement_Partiality(), this.getCOREPartialityType(), "partiality", null, 0, 1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECIElement_ModelElement(), ecorePackage.getEObject(), null, "modelElement", null, 1, 1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECIElement_MappingCardinality(), this.getCOREMappingCardinality(), null, "mappingCardinality", null, 0, 1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECIElement_ReferenceCardinality(), this.getCOREMappingCardinality(), null, "referenceCardinality", null, 0, -1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreMappingCardinalityEClass, COREMappingCardinality.class, "COREMappingCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREMappingCardinality_LowerBound(), ecorePackage.getEInt(), "lowerBound", "0", 1, 1, COREMappingCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREMappingCardinality_UpperBound(), ecorePackage.getEInt(), "upperBound", "1", 1, 1, COREMappingCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREMappingCardinality_Name(), ecorePackage.getEString(), "name", null, 0, 1, COREMappingCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLanguageElementMappingEClass, CORELanguageElementMapping.class, "CORELanguageElementMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORELanguageElementMapping_Actions(), this.getCORECreateMapping(), this.getCORECreateMapping_Type(), "actions", null, 0, -1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElementMapping_MappingEnds(), this.getMappingEnd(), this.getMappingEnd_Type(), "mappingEnds", null, 2, -1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCORELanguageElementMapping_Identifier(), ecorePackage.getEInt(), "identifier", null, 1, 1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElementMapping_NestedMappings(), this.getCORELanguageElementMapping(), null, "nestedMappings", null, 0, -1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCORELanguageElementMapping_MatchMaker(), ecorePackage.getEBoolean(), "matchMaker", null, 0, 1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreReexposeActionEClass, COREReexposeAction.class, "COREReexposeAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREReexposeAction_ReexposedAction(), this.getCOREAction(), null, "reexposedAction", null, 1, 1, COREReexposeAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreRedefineActionEClass, CORERedefineAction.class, "CORERedefineAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORERedefineAction_RedefinedAction(), this.getCOREAction(), null, "redefinedAction", null, 1, 1, CORERedefineAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORERedefineAction_ReusedActions(), this.getCOREAction(), null, "reusedActions", null, 1, -1, CORERedefineAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLanguageActionEClass, CORELanguageAction.class, "CORELanguageAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCORELanguageAction_ClassQualifiedName(), ecorePackage.getEString(), "classQualifiedName", null, 1, 1, CORELanguageAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCORELanguageAction_MethodName(), ecorePackage.getEString(), "methodName", null, 1, 1, CORELanguageAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageAction_Parameters(), this.getParameter(), null, "parameters", null, 0, -1, CORELanguageAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageAction_SecondaryEffects(), this.getActionEffect(), null, "secondaryEffects", null, 0, -1, CORELanguageAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCORELanguageAction_ActionType(), this.getLanguageActionType(), "actionType", null, 1, 1, CORELanguageAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreCreateMappingEClass, CORECreateMapping.class, "CORECreateMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORECreateMapping_Type(), this.getCORELanguageElementMapping(), this.getCORELanguageElementMapping_Actions(), "type", null, 1, 1, CORECreateMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECreateMapping_ExtendedAction(), this.getCORECreateMapping(), null, "extendedAction", null, 0, 1, CORECreateMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECreateMapping_ModelElements(), ecorePackage.getEObject(), null, "modelElements", null, 2, -1, CORECreateMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLanguageEClass, CORELanguage.class, "CORELanguage", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORELanguage_Actions(), this.getCOREAction(), null, "actions", null, 0, -1, CORELanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreActionEClass, COREAction.class, "COREAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREAction_Name(), ecorePackage.getEString(), "name", null, 0, 1, COREAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(createModelElementMappingEClass, CreateModelElementMapping.class, "CreateModelElementMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCreateModelElementMapping_ReusedActions(), this.getCOREAction(), null, "reusedActions", null, 0, -1, CreateModelElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelElementMappingEClass, COREModelElementMapping.class, "COREModelElementMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREModelElementMapping_ModelElements(), ecorePackage.getEObject(), null, "modelElements", null, 1, -1, COREModelElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREModelElementMapping_LEMid(), ecorePackage.getEInt(), "LEMid", null, 1, 1, COREModelElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(mappingEndEClass, MappingEnd.class, "MappingEnd", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getMappingEnd_Cardinality(), this.getCardinality(), "cardinality", null, 1, 1, MappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getMappingEnd_RoleName(), ecorePackage.getEString(), "roleName", null, 0, 1, MappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMappingEnd_Type(), this.getCORELanguageElementMapping(), this.getCORELanguageElementMapping_MappingEnds(), "type", null, 1, 1, MappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMappingEnd_LanguageElement(), this.getCORELanguageElement(), null, "languageElement", null, 1, 1, MappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(languageMapEClass, Map.Entry.class, "LanguageMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLanguageMap_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLanguageMap_Value(), this.getCORELanguage(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(artefactMapEClass, Map.Entry.class, "ArtefactMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getArtefactMap_Value(), this.getCOREArtefact(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getArtefactMap_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getParameter_Name(), ecorePackage.getEString(), "name", null, 0, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getParameter_Type(), ecorePackage.getEString(), "type", null, 0, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLanguageElementEClass, CORELanguageElement.class, "CORELanguageElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORELanguageElement_LanguageActions(), this.getCORELanguageAction(), null, "languageActions", null, 0, -1, CORELanguageElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElement_LanguageElement(), ecorePackage.getEObject(), null, "languageElement", null, 0, 1, CORELanguageElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElement_Language(), this.getCOREExternalLanguage(), this.getCOREExternalLanguage_LanguageElements(), "language", null, 0, 1, CORELanguageElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCORELanguageElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, CORELanguageElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElement_NestedElements(), this.getCORELanguageElement(), this.getCORELanguageElement_Owner(), "nestedElements", null, 0, -1, CORELanguageElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElement_Owner(), this.getCORELanguageElement(), this.getCORELanguageElement_NestedElements(), "owner", null, 0, 1, CORELanguageElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(existingElementEffectEClass, ExistingElementEffect.class, "ExistingElementEffect", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getExistingElementEffect_Parameter(), this.getParameter(), null, "parameter", null, 0, 1, ExistingElementEffect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getExistingElementEffect_ParameterEffect(), this.getParameterEffect(), "parameterEffect", null, 1, 1, ExistingElementEffect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(deleteEffectEClass, DeleteEffect.class, "DeleteEffect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(updateEffectEClass, UpdateEffect.class, "UpdateEffect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getUpdateEffect_AffectedFeature(), ecorePackage.getEObject(), null, "affectedFeature", null, 0, 1, UpdateEffect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(navigationMappingEClass, NavigationMapping.class, "NavigationMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNavigationMapping_Active(), ecorePackage.getEBoolean(), "active", null, 0, 1, NavigationMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(interLanguageMappingEClass, InterLanguageMapping.class, "InterLanguageMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getInterLanguageMapping_Default(), ecorePackage.getEBoolean(), "default", null, 0, 1, InterLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getInterLanguageMapping_CoreLanguageElementMapping(), this.getCORELanguageElementMapping(), null, "coreLanguageElementMapping", null, 1, 1, InterLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getInterLanguageMapping_InterLanguageMappingEnds(), this.getInterLanguageMappingEnd(), null, "interLanguageMappingEnds", null, 0, -1, InterLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(intraLanguageMappingEClass, IntraLanguageMapping.class, "IntraLanguageMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getIntraLanguageMapping_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, IntraLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIntraLanguageMapping_Closure(), ecorePackage.getEBoolean(), "closure", null, 0, 1, IntraLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIntraLanguageMapping_Reuse(), ecorePackage.getEBoolean(), "reuse", null, 0, 1, IntraLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIntraLanguageMapping_IncreaseDepth(), ecorePackage.getEBoolean(), "increaseDepth", null, 0, 1, IntraLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIntraLanguageMapping_ChangeModel(), ecorePackage.getEBoolean(), "changeModel", null, 0, 1, IntraLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getIntraLanguageMapping_From(), ecorePackage.getEClass(), null, "from", null, 1, 1, IntraLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getIntraLanguageMapping_Hops(), ecorePackage.getEReference(), null, "hops", null, 1, -1, IntraLanguageMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(interLanguageMappingEndEClass, InterLanguageMappingEnd.class, "InterLanguageMappingEnd", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getInterLanguageMappingEnd_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, InterLanguageMappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getInterLanguageMappingEnd_Origin(), ecorePackage.getEBoolean(), "origin", null, 0, 1, InterLanguageMappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getInterLanguageMappingEnd_Destination(), ecorePackage.getEBoolean(), "destination", null, 0, 1, InterLanguageMappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getInterLanguageMappingEnd_MappingEnd(), this.getMappingEnd(), null, "mappingEnd", null, 1, 1, InterLanguageMappingEnd.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(createEffectEClass, CreateEffect.class, "CreateEffect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCreateEffect_CorelanguageElement(), this.getCORELanguageElement(), null, "corelanguageElement", null, 1, 1, CreateEffect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(actionEffectEClass, ActionEffect.class, "ActionEffect", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(coreReuseArtefactEClass, COREReuseArtefact.class, "COREReuseArtefact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        // Initialize enums and add enum literals
        initEEnum(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.class, "COREFeatureRelationshipType");
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.NONE);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.OPTIONAL);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.MANDATORY);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.XOR);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.OR);

        initEEnum(coreVisibilityTypeEEnum, COREVisibilityType.class, "COREVisibilityType");
        addEEnumLiteral(coreVisibilityTypeEEnum, COREVisibilityType.PUBLIC);
        addEEnumLiteral(coreVisibilityTypeEEnum, COREVisibilityType.CONCERN);

        initEEnum(corePartialityTypeEEnum, COREPartialityType.class, "COREPartialityType");
        addEEnumLiteral(corePartialityTypeEEnum, COREPartialityType.NONE);
        addEEnumLiteral(corePartialityTypeEEnum, COREPartialityType.PUBLIC);
        addEEnumLiteral(corePartialityTypeEEnum, COREPartialityType.CONCERN);

        initEEnum(coreRelationshipEEnum, CORERelationship.class, "CORERelationship");
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.EQUALITY);
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.REFINES);
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.USES);
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.EXTENDS);

        initEEnum(cardinalityEEnum, Cardinality.class, "Cardinality");
        addEEnumLiteral(cardinalityEEnum, Cardinality.COMPULSORY);
        addEEnumLiteral(cardinalityEEnum, Cardinality.OPTIONAL);
        addEEnumLiteral(cardinalityEEnum, Cardinality.COMPULSORY_MULTIPLE);
        addEEnumLiteral(cardinalityEEnum, Cardinality.OPTIONAL_MULTIPLE);

        initEEnum(languageActionTypeEEnum, LanguageActionType.class, "LanguageActionType");
        addEEnumLiteral(languageActionTypeEEnum, LanguageActionType.CREATE);
        addEEnumLiteral(languageActionTypeEEnum, LanguageActionType.UPDATE);
        addEEnumLiteral(languageActionTypeEEnum, LanguageActionType.DELETE);

        initEEnum(parameterEffectEEnum, ParameterEffect.class, "ParameterEffect");
        addEEnumLiteral(parameterEffectEEnum, ParameterEffect.ELEMENT);
        addEEnumLiteral(parameterEffectEEnum, ParameterEffect.ID);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http://www.eclipse.org/OCL/Import
        createImportAnnotations();
        // http://www.eclipse.org/emf/2002/Ecore
        createEcoreAnnotations();
        // http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
        createPivotAnnotations();
    }

	/**
     * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void createImportAnnotations() {
        String source = "http://www.eclipse.org/OCL/Import";
        addAnnotation
          (this,
           source,
           new String[] {
               "ecore", "http://www.eclipse.org/emf/2002/Ecore"
           });
    }

	/**
     * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void createEcoreAnnotations() {
        String source = "http://www.eclipse.org/emf/2002/Ecore";
        addAnnotation
          (this,
           source,
           new String[] {
               "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
               "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
               "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
           });
    }

	/**
     * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void createPivotAnnotations() {
        String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";
        addAnnotation
          (getCOREConcern_FeatureModel(),
           source,
           new String[] {
               "derivation", "if self.artefacts->exists(a | a.oclIsTypeOf(COREFeatureModel)) then\n                self.artefacts->any(a | a.oclIsTypeOf(COREFeatureModel)).oclAsType(COREFeatureModel)\n            else\n                null\n            endif"
           });
        addAnnotation
          (getCOREConcern_ImpactModel(),
           source,
           new String[] {
               "derivation", "\n            if self.artefacts->exists(a | a.oclIsTypeOf(COREImpactModel)) then\n                self.artefacts->any(a | a.oclIsTypeOf(COREImpactModel)).oclAsType(COREImpactModel)\n            else\n                null\n            endif"
           });
    }

} //CorePackageImpl
