/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Language Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl#getLanguageActions <em>Language Actions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl#getLanguageElement <em>Language Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl#getNestedElements <em>Nested Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementImpl#getOwner <em>Owner</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORELanguageElementImpl extends EObjectImpl implements CORELanguageElement {
	/**
     * The cached value of the '{@link #getLanguageActions() <em>Language Actions</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLanguageActions()
     * @generated
     * @ordered
     */
	protected EList<CORELanguageAction> languageActions;

	/**
     * The cached value of the '{@link #getLanguageElement() <em>Language Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLanguageElement()
     * @generated
     * @ordered
     */
	protected EObject languageElement;

	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected String name = NAME_EDEFAULT;

	/**
     * The cached value of the '{@link #getNestedElements() <em>Nested Elements</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getNestedElements()
     * @generated
     * @ordered
     */
	protected EList<CORELanguageElement> nestedElements;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CORELanguageElementImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_LANGUAGE_ELEMENT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<CORELanguageAction> getLanguageActions() {
        if (languageActions == null) {
            languageActions = new EObjectResolvingEList<CORELanguageAction>(CORELanguageAction.class, this, CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS);
        }
        return languageActions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject getLanguageElement() {
        if (languageElement != null && languageElement.eIsProxy()) {
            InternalEObject oldLanguageElement = (InternalEObject)languageElement;
            languageElement = eResolveProxy(oldLanguageElement);
            if (languageElement != oldLanguageElement) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT, oldLanguageElement, languageElement));
            }
        }
        return languageElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EObject basicGetLanguageElement() {
        return languageElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setLanguageElement(EObject newLanguageElement) {
        EObject oldLanguageElement = languageElement;
        languageElement = newLanguageElement;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT, oldLanguageElement, languageElement));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREExternalLanguage getLanguage() {
        if (eContainerFeatureID() != CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE) return null;
        return (COREExternalLanguage)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetLanguage(COREExternalLanguage newLanguage, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newLanguage, CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setLanguage(COREExternalLanguage newLanguage) {
        if (newLanguage != eInternalContainer() || (eContainerFeatureID() != CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE && newLanguage != null)) {
            if (EcoreUtil.isAncestor(this, newLanguage))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newLanguage != null)
                msgs = ((InternalEObject)newLanguage).eInverseAdd(this, CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS, COREExternalLanguage.class, msgs);
            msgs = basicSetLanguage(newLanguage, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE, newLanguage, newLanguage));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT__NAME, oldName, name));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<CORELanguageElement> getNestedElements() {
        if (nestedElements == null) {
            nestedElements = new EObjectContainmentWithInverseEList<CORELanguageElement>(CORELanguageElement.class, this, CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS, CorePackage.CORE_LANGUAGE_ELEMENT__OWNER);
        }
        return nestedElements;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElement getOwner() {
        if (eContainerFeatureID() != CorePackage.CORE_LANGUAGE_ELEMENT__OWNER) return null;
        return (CORELanguageElement)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetOwner(CORELanguageElement newOwner, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newOwner, CorePackage.CORE_LANGUAGE_ELEMENT__OWNER, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setOwner(CORELanguageElement newOwner) {
        if (newOwner != eInternalContainer() || (eContainerFeatureID() != CorePackage.CORE_LANGUAGE_ELEMENT__OWNER && newOwner != null)) {
            if (EcoreUtil.isAncestor(this, newOwner))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newOwner != null)
                msgs = ((InternalEObject)newOwner).eInverseAdd(this, CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS, CORELanguageElement.class, msgs);
            msgs = basicSetOwner(newOwner, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT__OWNER, newOwner, newOwner));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetLanguage((COREExternalLanguage)otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getNestedElements()).basicAdd(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT__OWNER:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetOwner((CORELanguageElement)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE:
                return basicSetLanguage(null, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS:
                return ((InternalEList<?>)getNestedElements()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT__OWNER:
                return basicSetOwner(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID()) {
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE:
                return eInternalContainer().eInverseRemove(this, CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS, COREExternalLanguage.class, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT__OWNER:
                return eInternalContainer().eInverseRemove(this, CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS, CORELanguageElement.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS:
                return getLanguageActions();
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT:
                if (resolve) return getLanguageElement();
                return basicGetLanguageElement();
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE:
                return getLanguage();
            case CorePackage.CORE_LANGUAGE_ELEMENT__NAME:
                return getName();
            case CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS:
                return getNestedElements();
            case CorePackage.CORE_LANGUAGE_ELEMENT__OWNER:
                return getOwner();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS:
                getLanguageActions().clear();
                getLanguageActions().addAll((Collection<? extends CORELanguageAction>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT:
                setLanguageElement((EObject)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE:
                setLanguage((COREExternalLanguage)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__NAME:
                setName((String)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS:
                getNestedElements().clear();
                getNestedElements().addAll((Collection<? extends CORELanguageElement>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__OWNER:
                setOwner((CORELanguageElement)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS:
                getLanguageActions().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT:
                setLanguageElement((EObject)null);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE:
                setLanguage((COREExternalLanguage)null);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__NAME:
                setName(NAME_EDEFAULT);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS:
                getNestedElements().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT__OWNER:
                setOwner((CORELanguageElement)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ACTIONS:
                return languageActions != null && !languageActions.isEmpty();
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE_ELEMENT:
                return languageElement != null;
            case CorePackage.CORE_LANGUAGE_ELEMENT__LANGUAGE:
                return getLanguage() != null;
            case CorePackage.CORE_LANGUAGE_ELEMENT__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case CorePackage.CORE_LANGUAGE_ELEMENT__NESTED_ELEMENTS:
                return nestedElements != null && !nestedElements.isEmpty();
            case CorePackage.CORE_LANGUAGE_ELEMENT__OWNER:
                return getOwner() != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(')');
        return result.toString();
    }

} //CORELanguageElementImpl
