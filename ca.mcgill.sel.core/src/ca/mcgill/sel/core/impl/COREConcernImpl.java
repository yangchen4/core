/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Concern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConcernImpl#getArtefacts <em>Artefacts</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConcernImpl#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConcernImpl#getImpactModel <em>Impact Model</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConcernImpl#getScenes <em>Scenes</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConcernImpl#getReuses <em>Reuses</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConcernImpl#getTemporaryArtefacts <em>Temporary Artefacts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREConcernImpl extends CORENamedElementImpl implements COREConcern {
	/**
     * The cached value of the '{@link #getArtefacts() <em>Artefacts</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getArtefacts()
     * @generated
     * @ordered
     */
	protected EList<COREArtefact> artefacts;

	/**
     * The cached setting delegate for the '{@link #getFeatureModel() <em>Feature Model</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getFeatureModel()
     * @generated
     * @ordered
     */
	protected EStructuralFeature.Internal.SettingDelegate FEATURE_MODEL__ESETTING_DELEGATE = ((EStructuralFeature.Internal)CorePackage.Literals.CORE_CONCERN__FEATURE_MODEL).getSettingDelegate();

	/**
     * The cached setting delegate for the '{@link #getImpactModel() <em>Impact Model</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getImpactModel()
     * @generated
     * @ordered
     */
	protected EStructuralFeature.Internal.SettingDelegate IMPACT_MODEL__ESETTING_DELEGATE = ((EStructuralFeature.Internal)CorePackage.Literals.CORE_CONCERN__IMPACT_MODEL).getSettingDelegate();

	/**
     * The cached value of the '{@link #getScenes() <em>Scenes</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getScenes()
     * @generated
     * @ordered
     */
	protected EList<COREScene> scenes;

	/**
     * The cached value of the '{@link #getReuses() <em>Reuses</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReuses()
     * @generated
     * @ordered
     */
	protected EList<COREReuse> reuses;

	/**
     * The cached value of the '{@link #getTemporaryArtefacts() <em>Temporary Artefacts</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTemporaryArtefacts()
     * @generated
     * @ordered
     */
	protected EList<COREArtefact> temporaryArtefacts;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREConcernImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_CONCERN;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREArtefact> getArtefacts() {
        if (artefacts == null) {
            artefacts = new EObjectContainmentWithInverseEList<COREArtefact>(COREArtefact.class, this, CorePackage.CORE_CONCERN__ARTEFACTS, CorePackage.CORE_ARTEFACT__CORE_CONCERN);
        }
        return artefacts;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREFeatureModel getFeatureModel() {
        return (COREFeatureModel)FEATURE_MODEL__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREFeatureModel basicGetFeatureModel() {
        return (COREFeatureModel)FEATURE_MODEL__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setFeatureModel(COREFeatureModel newFeatureModel) {
        FEATURE_MODEL__ESETTING_DELEGATE.dynamicSet(this, null, 0, newFeatureModel);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREImpactModel getImpactModel() {
        return (COREImpactModel)IMPACT_MODEL__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREImpactModel basicGetImpactModel() {
        return (COREImpactModel)IMPACT_MODEL__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setImpactModel(COREImpactModel newImpactModel) {
        IMPACT_MODEL__ESETTING_DELEGATE.dynamicSet(this, null, 0, newImpactModel);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREScene> getScenes() {
        if (scenes == null) {
            scenes = new EObjectContainmentEList<COREScene>(COREScene.class, this, CorePackage.CORE_CONCERN__SCENES);
        }
        return scenes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREReuse> getReuses() {
        if (reuses == null) {
            reuses = new EObjectContainmentEList<COREReuse>(COREReuse.class, this, CorePackage.CORE_CONCERN__REUSES);
        }
        return reuses;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREArtefact> getTemporaryArtefacts() {
        if (temporaryArtefacts == null) {
            temporaryArtefacts = new EObjectWithInverseResolvingEList<COREArtefact>(COREArtefact.class, this, CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS, CorePackage.CORE_ARTEFACT__TEMPORARY_CONCERN);
        }
        return temporaryArtefacts;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_CONCERN__ARTEFACTS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getArtefacts()).basicAdd(otherEnd, msgs);
            case CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getTemporaryArtefacts()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_CONCERN__ARTEFACTS:
                return ((InternalEList<?>)getArtefacts()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_CONCERN__SCENES:
                return ((InternalEList<?>)getScenes()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_CONCERN__REUSES:
                return ((InternalEList<?>)getReuses()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS:
                return ((InternalEList<?>)getTemporaryArtefacts()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_CONCERN__ARTEFACTS:
                return getArtefacts();
            case CorePackage.CORE_CONCERN__FEATURE_MODEL:
                if (resolve) return getFeatureModel();
                return basicGetFeatureModel();
            case CorePackage.CORE_CONCERN__IMPACT_MODEL:
                if (resolve) return getImpactModel();
                return basicGetImpactModel();
            case CorePackage.CORE_CONCERN__SCENES:
                return getScenes();
            case CorePackage.CORE_CONCERN__REUSES:
                return getReuses();
            case CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS:
                return getTemporaryArtefacts();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_CONCERN__ARTEFACTS:
                getArtefacts().clear();
                getArtefacts().addAll((Collection<? extends COREArtefact>)newValue);
                return;
            case CorePackage.CORE_CONCERN__FEATURE_MODEL:
                setFeatureModel((COREFeatureModel)newValue);
                return;
            case CorePackage.CORE_CONCERN__IMPACT_MODEL:
                setImpactModel((COREImpactModel)newValue);
                return;
            case CorePackage.CORE_CONCERN__SCENES:
                getScenes().clear();
                getScenes().addAll((Collection<? extends COREScene>)newValue);
                return;
            case CorePackage.CORE_CONCERN__REUSES:
                getReuses().clear();
                getReuses().addAll((Collection<? extends COREReuse>)newValue);
                return;
            case CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS:
                getTemporaryArtefacts().clear();
                getTemporaryArtefacts().addAll((Collection<? extends COREArtefact>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_CONCERN__ARTEFACTS:
                getArtefacts().clear();
                return;
            case CorePackage.CORE_CONCERN__FEATURE_MODEL:
                setFeatureModel((COREFeatureModel)null);
                return;
            case CorePackage.CORE_CONCERN__IMPACT_MODEL:
                setImpactModel((COREImpactModel)null);
                return;
            case CorePackage.CORE_CONCERN__SCENES:
                getScenes().clear();
                return;
            case CorePackage.CORE_CONCERN__REUSES:
                getReuses().clear();
                return;
            case CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS:
                getTemporaryArtefacts().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_CONCERN__ARTEFACTS:
                return artefacts != null && !artefacts.isEmpty();
            case CorePackage.CORE_CONCERN__FEATURE_MODEL:
                return FEATURE_MODEL__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
            case CorePackage.CORE_CONCERN__IMPACT_MODEL:
                return IMPACT_MODEL__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
            case CorePackage.CORE_CONCERN__SCENES:
                return scenes != null && !scenes.isEmpty();
            case CorePackage.CORE_CONCERN__REUSES:
                return reuses != null && !reuses.isEmpty();
            case CorePackage.CORE_CONCERN__TEMPORARY_ARTEFACTS:
                return temporaryArtefacts != null && !temporaryArtefacts.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //COREConcernImpl
