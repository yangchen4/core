/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.ActionEffect;
import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.LanguageActionType;
import ca.mcgill.sel.core.Parameter;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Language Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageActionImpl#getClassQualifiedName <em>Class Qualified Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageActionImpl#getMethodName <em>Method Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageActionImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageActionImpl#getSecondaryEffects <em>Secondary Effects</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageActionImpl#getActionType <em>Action Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORELanguageActionImpl extends COREActionImpl implements CORELanguageAction {
	/**
     * The default value of the '{@link #getClassQualifiedName() <em>Class Qualified Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getClassQualifiedName()
     * @generated
     * @ordered
     */
	protected static final String CLASS_QUALIFIED_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getClassQualifiedName() <em>Class Qualified Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getClassQualifiedName()
     * @generated
     * @ordered
     */
	protected String classQualifiedName = CLASS_QUALIFIED_NAME_EDEFAULT;

	/**
     * The default value of the '{@link #getMethodName() <em>Method Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMethodName()
     * @generated
     * @ordered
     */
	protected static final String METHOD_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getMethodName() <em>Method Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMethodName()
     * @generated
     * @ordered
     */
	protected String methodName = METHOD_NAME_EDEFAULT;

	/**
     * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getParameters()
     * @generated
     * @ordered
     */
	protected EList<Parameter> parameters;

	/**
     * The cached value of the '{@link #getSecondaryEffects() <em>Secondary Effects</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSecondaryEffects()
     * @generated
     * @ordered
     */
	protected EList<ActionEffect> secondaryEffects;

	/**
     * The default value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActionType()
     * @generated
     * @ordered
     */
	protected static final LanguageActionType ACTION_TYPE_EDEFAULT = LanguageActionType.CREATE;

	/**
     * The cached value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActionType()
     * @generated
     * @ordered
     */
	protected LanguageActionType actionType = ACTION_TYPE_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CORELanguageActionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_LANGUAGE_ACTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getClassQualifiedName() {
        return classQualifiedName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setClassQualifiedName(String newClassQualifiedName) {
        String oldClassQualifiedName = classQualifiedName;
        classQualifiedName = newClassQualifiedName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME, oldClassQualifiedName, classQualifiedName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getMethodName() {
        return methodName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMethodName(String newMethodName) {
        String oldMethodName = methodName;
        methodName = newMethodName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ACTION__METHOD_NAME, oldMethodName, methodName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Parameter> getParameters() {
        if (parameters == null) {
            parameters = new EObjectContainmentEList<Parameter>(Parameter.class, this, CorePackage.CORE_LANGUAGE_ACTION__PARAMETERS);
        }
        return parameters;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<ActionEffect> getSecondaryEffects() {
        if (secondaryEffects == null) {
            secondaryEffects = new EObjectContainmentEList<ActionEffect>(ActionEffect.class, this, CorePackage.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS);
        }
        return secondaryEffects;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public LanguageActionType getActionType() {
        return actionType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setActionType(LanguageActionType newActionType) {
        LanguageActionType oldActionType = actionType;
        actionType = newActionType == null ? ACTION_TYPE_EDEFAULT : newActionType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ACTION__ACTION_TYPE, oldActionType, actionType));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ACTION__PARAMETERS:
                return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS:
                return ((InternalEList<?>)getSecondaryEffects()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME:
                return getClassQualifiedName();
            case CorePackage.CORE_LANGUAGE_ACTION__METHOD_NAME:
                return getMethodName();
            case CorePackage.CORE_LANGUAGE_ACTION__PARAMETERS:
                return getParameters();
            case CorePackage.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS:
                return getSecondaryEffects();
            case CorePackage.CORE_LANGUAGE_ACTION__ACTION_TYPE:
                return getActionType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME:
                setClassQualifiedName((String)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__METHOD_NAME:
                setMethodName((String)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__PARAMETERS:
                getParameters().clear();
                getParameters().addAll((Collection<? extends Parameter>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS:
                getSecondaryEffects().clear();
                getSecondaryEffects().addAll((Collection<? extends ActionEffect>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__ACTION_TYPE:
                setActionType((LanguageActionType)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME:
                setClassQualifiedName(CLASS_QUALIFIED_NAME_EDEFAULT);
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__METHOD_NAME:
                setMethodName(METHOD_NAME_EDEFAULT);
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__PARAMETERS:
                getParameters().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS:
                getSecondaryEffects().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__ACTION_TYPE:
                setActionType(ACTION_TYPE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME:
                return CLASS_QUALIFIED_NAME_EDEFAULT == null ? classQualifiedName != null : !CLASS_QUALIFIED_NAME_EDEFAULT.equals(classQualifiedName);
            case CorePackage.CORE_LANGUAGE_ACTION__METHOD_NAME:
                return METHOD_NAME_EDEFAULT == null ? methodName != null : !METHOD_NAME_EDEFAULT.equals(methodName);
            case CorePackage.CORE_LANGUAGE_ACTION__PARAMETERS:
                return parameters != null && !parameters.isEmpty();
            case CorePackage.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS:
                return secondaryEffects != null && !secondaryEffects.isEmpty();
            case CorePackage.CORE_LANGUAGE_ACTION__ACTION_TYPE:
                return actionType != ACTION_TYPE_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (classQualifiedName: ");
        result.append(classQualifiedName);
        result.append(", methodName: ");
        result.append(methodName);
        result.append(", actionType: ");
        result.append(actionType);
        result.append(')');
        return result.toString();
    }

} //CORELanguageActionImpl
