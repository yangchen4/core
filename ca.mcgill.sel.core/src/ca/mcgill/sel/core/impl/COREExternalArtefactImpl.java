/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE External Artefact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalArtefactImpl#getRootModelElement <em>Root Model Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREExternalArtefactImpl#getLanguageName <em>Language Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREExternalArtefactImpl extends COREArtefactImpl implements COREExternalArtefact {
	/**
     * The cached value of the '{@link #getRootModelElement() <em>Root Model Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getRootModelElement()
     * @generated
     * @ordered
     */
	protected EObject rootModelElement;

	/**
     * The default value of the '{@link #getLanguageName() <em>Language Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLanguageName()
     * @generated
     * @ordered
     */
	protected static final String LANGUAGE_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getLanguageName() <em>Language Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLanguageName()
     * @generated
     * @ordered
     */
	protected String languageName = LANGUAGE_NAME_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREExternalArtefactImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_EXTERNAL_ARTEFACT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject getRootModelElement() {
        if (rootModelElement != null && rootModelElement.eIsProxy()) {
            InternalEObject oldRootModelElement = (InternalEObject)rootModelElement;
            rootModelElement = eResolveProxy(oldRootModelElement);
            if (rootModelElement != oldRootModelElement) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT, oldRootModelElement, rootModelElement));
            }
        }
        return rootModelElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EObject basicGetRootModelElement() {
        return rootModelElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setRootModelElement(EObject newRootModelElement) {
        EObject oldRootModelElement = rootModelElement;
        rootModelElement = newRootModelElement;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT, oldRootModelElement, rootModelElement));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getLanguageName() {
        return languageName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setLanguageName(String newLanguageName) {
        String oldLanguageName = languageName;
        languageName = newLanguageName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME, oldLanguageName, languageName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT:
                if (resolve) return getRootModelElement();
                return basicGetRootModelElement();
            case CorePackage.CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME:
                return getLanguageName();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT:
                setRootModelElement((EObject)newValue);
                return;
            case CorePackage.CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME:
                setLanguageName((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT:
                setRootModelElement((EObject)null);
                return;
            case CorePackage.CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME:
                setLanguageName(LANGUAGE_NAME_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT:
                return rootModelElement != null;
            case CorePackage.CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME:
                return LANGUAGE_NAME_EDEFAULT == null ? languageName != null : !LANGUAGE_NAME_EDEFAULT.equals(languageName);
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (languageName: ");
        result.append(languageName);
        result.append(')');
        return result.toString();
    }

} //COREExternalArtefactImpl
