/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.CreateEffect;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Create Effect</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CreateEffectImpl#getCorelanguageElement <em>Corelanguage Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CreateEffectImpl extends ActionEffectImpl implements CreateEffect {
	/**
     * The cached value of the '{@link #getCorelanguageElement() <em>Corelanguage Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getCorelanguageElement()
     * @generated
     * @ordered
     */
	protected CORELanguageElement corelanguageElement;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CreateEffectImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CREATE_EFFECT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public CORELanguageElement getCorelanguageElement() {
        if (corelanguageElement != null && corelanguageElement.eIsProxy()) {
            InternalEObject oldCorelanguageElement = (InternalEObject)corelanguageElement;
            corelanguageElement = (CORELanguageElement)eResolveProxy(oldCorelanguageElement);
            if (corelanguageElement != oldCorelanguageElement) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CREATE_EFFECT__CORELANGUAGE_ELEMENT, oldCorelanguageElement, corelanguageElement));
            }
        }
        return corelanguageElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CORELanguageElement basicGetCorelanguageElement() {
        return corelanguageElement;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setCorelanguageElement(CORELanguageElement newCorelanguageElement) {
        CORELanguageElement oldCorelanguageElement = corelanguageElement;
        corelanguageElement = newCorelanguageElement;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CREATE_EFFECT__CORELANGUAGE_ELEMENT, oldCorelanguageElement, corelanguageElement));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CREATE_EFFECT__CORELANGUAGE_ELEMENT:
                if (resolve) return getCorelanguageElement();
                return basicGetCorelanguageElement();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CREATE_EFFECT__CORELANGUAGE_ELEMENT:
                setCorelanguageElement((CORELanguageElement)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CREATE_EFFECT__CORELANGUAGE_ELEMENT:
                setCorelanguageElement((CORELanguageElement)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CREATE_EFFECT__CORELANGUAGE_ELEMENT:
                return corelanguageElement != null;
        }
        return super.eIsSet(featureID);
    }

} //CreateEffectImpl
