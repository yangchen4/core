/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Model Reuse</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelReuseImpl#getReuse <em>Reuse</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelReuseImpl#getConfiguration <em>Configuration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREModelReuseImpl extends COREModelCompositionImpl implements COREModelReuse {
	/**
     * The cached value of the '{@link #getReuse() <em>Reuse</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReuse()
     * @generated
     * @ordered
     */
	protected COREReuse reuse;

	/**
     * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getConfiguration()
     * @generated
     * @ordered
     */
	protected COREConfiguration configuration;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREModelReuseImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MODEL_REUSE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREReuse getReuse() {
        if (reuse != null && reuse.eIsProxy()) {
            InternalEObject oldReuse = (InternalEObject)reuse;
            reuse = (COREReuse)eResolveProxy(oldReuse);
            if (reuse != oldReuse) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_MODEL_REUSE__REUSE, oldReuse, reuse));
            }
        }
        return reuse;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREReuse basicGetReuse() {
        return reuse;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetReuse(COREReuse newReuse, NotificationChain msgs) {
        COREReuse oldReuse = reuse;
        reuse = newReuse;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_REUSE__REUSE, oldReuse, newReuse);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setReuse(COREReuse newReuse) {
        if (newReuse != reuse) {
            NotificationChain msgs = null;
            if (reuse != null)
                msgs = ((InternalEObject)reuse).eInverseRemove(this, CorePackage.CORE_REUSE__MODEL_REUSES, COREReuse.class, msgs);
            if (newReuse != null)
                msgs = ((InternalEObject)newReuse).eInverseAdd(this, CorePackage.CORE_REUSE__MODEL_REUSES, COREReuse.class, msgs);
            msgs = basicSetReuse(newReuse, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_REUSE__REUSE, newReuse, newReuse));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREConfiguration getConfiguration() {
        return configuration;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetConfiguration(COREConfiguration newConfiguration, NotificationChain msgs) {
        COREConfiguration oldConfiguration = configuration;
        configuration = newConfiguration;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_REUSE__CONFIGURATION, oldConfiguration, newConfiguration);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setConfiguration(COREConfiguration newConfiguration) {
        if (newConfiguration != configuration) {
            NotificationChain msgs = null;
            if (configuration != null)
                msgs = ((InternalEObject)configuration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.CORE_MODEL_REUSE__CONFIGURATION, null, msgs);
            if (newConfiguration != null)
                msgs = ((InternalEObject)newConfiguration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.CORE_MODEL_REUSE__CONFIGURATION, null, msgs);
            msgs = basicSetConfiguration(newConfiguration, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_REUSE__CONFIGURATION, newConfiguration, newConfiguration));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_REUSE__REUSE:
                if (reuse != null)
                    msgs = ((InternalEObject)reuse).eInverseRemove(this, CorePackage.CORE_REUSE__MODEL_REUSES, COREReuse.class, msgs);
                return basicSetReuse((COREReuse)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_REUSE__REUSE:
                return basicSetReuse(null, msgs);
            case CorePackage.CORE_MODEL_REUSE__CONFIGURATION:
                return basicSetConfiguration(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_REUSE__REUSE:
                if (resolve) return getReuse();
                return basicGetReuse();
            case CorePackage.CORE_MODEL_REUSE__CONFIGURATION:
                return getConfiguration();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_REUSE__REUSE:
                setReuse((COREReuse)newValue);
                return;
            case CorePackage.CORE_MODEL_REUSE__CONFIGURATION:
                setConfiguration((COREConfiguration)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_REUSE__REUSE:
                setReuse((COREReuse)null);
                return;
            case CorePackage.CORE_MODEL_REUSE__CONFIGURATION:
                setConfiguration((COREConfiguration)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_REUSE__REUSE:
                return reuse != null;
            case CorePackage.CORE_MODEL_REUSE__CONFIGURATION:
                return configuration != null;
        }
        return super.eIsSet(featureID);
    }

} //COREModelReuseImpl
