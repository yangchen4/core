/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.CORERedefineAction;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Redefine Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORERedefineActionImpl#getRedefinedAction <em>Redefined Action</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORERedefineActionImpl#getReusedActions <em>Reused Actions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORERedefineActionImpl extends COREPerspectiveActionImpl implements CORERedefineAction {
	/**
     * The cached value of the '{@link #getRedefinedAction() <em>Redefined Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getRedefinedAction()
     * @generated
     * @ordered
     */
	protected COREAction redefinedAction;

	/**
     * The cached value of the '{@link #getReusedActions() <em>Reused Actions</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReusedActions()
     * @generated
     * @ordered
     */
	protected EList<COREAction> reusedActions;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CORERedefineActionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_REDEFINE_ACTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public COREAction getRedefinedAction() {
        if (redefinedAction != null && redefinedAction.eIsProxy()) {
            InternalEObject oldRedefinedAction = (InternalEObject)redefinedAction;
            redefinedAction = (COREAction)eResolveProxy(oldRedefinedAction);
            if (redefinedAction != oldRedefinedAction) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_REDEFINE_ACTION__REDEFINED_ACTION, oldRedefinedAction, redefinedAction));
            }
        }
        return redefinedAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREAction basicGetRedefinedAction() {
        return redefinedAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setRedefinedAction(COREAction newRedefinedAction) {
        COREAction oldRedefinedAction = redefinedAction;
        redefinedAction = newRedefinedAction;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_REDEFINE_ACTION__REDEFINED_ACTION, oldRedefinedAction, redefinedAction));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREAction> getReusedActions() {
        if (reusedActions == null) {
            reusedActions = new EObjectResolvingEList<COREAction>(COREAction.class, this, CorePackage.CORE_REDEFINE_ACTION__REUSED_ACTIONS);
        }
        return reusedActions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_REDEFINE_ACTION__REDEFINED_ACTION:
                if (resolve) return getRedefinedAction();
                return basicGetRedefinedAction();
            case CorePackage.CORE_REDEFINE_ACTION__REUSED_ACTIONS:
                return getReusedActions();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_REDEFINE_ACTION__REDEFINED_ACTION:
                setRedefinedAction((COREAction)newValue);
                return;
            case CorePackage.CORE_REDEFINE_ACTION__REUSED_ACTIONS:
                getReusedActions().clear();
                getReusedActions().addAll((Collection<? extends COREAction>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_REDEFINE_ACTION__REDEFINED_ACTION:
                setRedefinedAction((COREAction)null);
                return;
            case CorePackage.CORE_REDEFINE_ACTION__REUSED_ACTIONS:
                getReusedActions().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_REDEFINE_ACTION__REDEFINED_ACTION:
                return redefinedAction != null;
            case CorePackage.CORE_REDEFINE_ACTION__REUSED_ACTIONS:
                return reusedActions != null && !reusedActions.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //CORERedefineActionImpl
