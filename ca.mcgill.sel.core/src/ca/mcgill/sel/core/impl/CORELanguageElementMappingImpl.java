/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORECreateMapping;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CorePackage;

import ca.mcgill.sel.core.MappingEnd;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Language Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getMappingEnds <em>Mapping Ends</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getNestedMappings <em>Nested Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#isMatchMaker <em>Match Maker</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORELanguageElementMappingImpl extends EObjectImpl implements CORELanguageElementMapping {
    /**
     * The cached value of the '{@link #getActions() <em>Actions</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActions()
     * @generated
     * @ordered
     */
	protected EList<CORECreateMapping> actions;

    /**
     * The cached value of the '{@link #getMappingEnds() <em>Mapping Ends</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMappingEnds()
     * @generated
     * @ordered
     */
    protected EList<MappingEnd> mappingEnds;

    /**
     * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getIdentifier()
     * @generated
     * @ordered
     */
    protected static final int IDENTIFIER_EDEFAULT = 0;
    /**
     * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getIdentifier()
     * @generated
     * @ordered
     */
	protected int identifier = IDENTIFIER_EDEFAULT;

				/**
     * The cached value of the '{@link #getNestedMappings() <em>Nested Mappings</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getNestedMappings()
     * @generated
     * @ordered
     */
	protected EList<CORELanguageElementMapping> nestedMappings;

				/**
     * The default value of the '{@link #isMatchMaker() <em>Match Maker</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isMatchMaker()
     * @generated
     * @ordered
     */
	protected static final boolean MATCH_MAKER_EDEFAULT = false;

				/**
     * The cached value of the '{@link #isMatchMaker() <em>Match Maker</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isMatchMaker()
     * @generated
     * @ordered
     */
	protected boolean matchMaker = MATCH_MAKER_EDEFAULT;
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CORELanguageElementMappingImpl() {
        super();
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_LANGUAGE_ELEMENT_MAPPING;
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<CORECreateMapping> getActions() {
        if (actions == null) {
            actions = new EObjectWithInverseResolvingEList<CORECreateMapping>(CORECreateMapping.class, this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS, CorePackage.CORE_CREATE_MAPPING__TYPE);
        }
        return actions;
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<MappingEnd> getMappingEnds() {
        if (mappingEnds == null) {
            mappingEnds = new EObjectContainmentWithInverseEList<MappingEnd>(MappingEnd.class, this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS, CorePackage.MAPPING_END__TYPE);
        }
        return mappingEnds;
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getIdentifier() {
        return identifier;
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setIdentifier(int newIdentifier) {
        int oldIdentifier = identifier;
        identifier = newIdentifier;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER, oldIdentifier, identifier));
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<CORELanguageElementMapping> getNestedMappings() {
        if (nestedMappings == null) {
            nestedMappings = new EObjectContainmentEList<CORELanguageElementMapping>(CORELanguageElementMapping.class, this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS);
        }
        return nestedMappings;
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isMatchMaker() {
        return matchMaker;
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMatchMaker(boolean newMatchMaker) {
        boolean oldMatchMaker = matchMaker;
        matchMaker = newMatchMaker;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER, oldMatchMaker, matchMaker));
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getActions()).basicAdd(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getMappingEnds()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS:
                return ((InternalEList<?>)getMappingEnds()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS:
                return ((InternalEList<?>)getNestedMappings()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return getActions();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS:
                return getMappingEnds();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER:
                return getIdentifier();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS:
                return getNestedMappings();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER:
                return isMatchMaker();
        }
        return super.eGet(featureID, resolve, coreType);
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                getActions().clear();
                getActions().addAll((Collection<? extends CORECreateMapping>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS:
                getMappingEnds().clear();
                getMappingEnds().addAll((Collection<? extends MappingEnd>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER:
                setIdentifier((Integer)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS:
                getNestedMappings().clear();
                getNestedMappings().addAll((Collection<? extends CORELanguageElementMapping>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER:
                setMatchMaker((Boolean)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                getActions().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS:
                getMappingEnds().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER:
                setIdentifier(IDENTIFIER_EDEFAULT);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS:
                getNestedMappings().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER:
                setMatchMaker(MATCH_MAKER_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return actions != null && !actions.isEmpty();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MAPPING_ENDS:
                return mappingEnds != null && !mappingEnds.isEmpty();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__IDENTIFIER:
                return identifier != IDENTIFIER_EDEFAULT;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__NESTED_MAPPINGS:
                return nestedMappings != null && !nestedMappings.isEmpty();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__MATCH_MAKER:
                return matchMaker != MATCH_MAKER_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }
				/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (identifier: ");
        result.append(identifier);
        result.append(", matchMaker: ");
        result.append(matchMaker);
        result.append(')');
        return result.toString();
    }
				/**
	 * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EList<COREModelElementMapping> getModelElementMappings(COREScene scene) {
        BasicEList<COREModelElementMapping> mappings = new BasicEList<COREModelElementMapping>();
        for (COREModelElementMapping m : scene.getElementMappings()) {
            if (m.getLEMid() == this.identifier) {
                mappings.add(m);
            }
        }
        
        return mappings;
    }


} //CORELanguageElementMappingImpl
