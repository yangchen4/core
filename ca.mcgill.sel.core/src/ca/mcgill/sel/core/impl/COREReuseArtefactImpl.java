/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREReuseArtefact;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Reuse Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class COREReuseArtefactImpl extends COREArtefactImpl implements COREReuseArtefact {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected COREReuseArtefactImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_REUSE_ARTEFACT;
    }

} //COREReuseArtefactImpl
