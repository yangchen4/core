/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREMappingImpl#getMappings <em>Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREMappingImpl#getReferencedMappings <em>Referenced Mappings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREMappingImpl<T> extends CORELinkImpl<T> implements COREMapping<T> {
	/**
     * The cached value of the '{@link #getMappings() <em>Mappings</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMappings()
     * @generated
     * @ordered
     */
	protected EList<COREMapping<?>> mappings;

	/**
     * The cached value of the '{@link #getReferencedMappings() <em>Referenced Mappings</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getReferencedMappings()
     * @generated
     * @ordered
     */
	protected EList<COREMapping<?>> referencedMappings;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MAPPING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific type known in this context.
     * @generated
     */
	@Override
	public void setTo(T newTo) {
        super.setTo(newTo);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific type known in this context.
     * @generated
     */
	@Override
	public void setFrom(T newFrom) {
        super.setFrom(newFrom);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREMapping<?>> getMappings() {
        if (mappings == null) {
            mappings = new EObjectContainmentEList<COREMapping<?>>(COREMapping.class, this, CorePackage.CORE_MAPPING__MAPPINGS);
        }
        return mappings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREMapping<?>> getReferencedMappings() {
        if (referencedMappings == null) {
            referencedMappings = new EObjectResolvingEList<COREMapping<?>>(COREMapping.class, this, CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS);
        }
        return referencedMappings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING__MAPPINGS:
                return ((InternalEList<?>)getMappings()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING__MAPPINGS:
                return getMappings();
            case CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS:
                return getReferencedMappings();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING__MAPPINGS:
                getMappings().clear();
                getMappings().addAll((Collection<? extends COREMapping<?>>)newValue);
                return;
            case CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS:
                getReferencedMappings().clear();
                getReferencedMappings().addAll((Collection<? extends COREMapping<?>>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING__MAPPINGS:
                getMappings().clear();
                return;
            case CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS:
                getReferencedMappings().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING__MAPPINGS:
                return mappings != null && !mappings.isEmpty();
            case CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS:
                return referencedMappings != null && !referencedMappings.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //COREMappingImpl
