/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Scene</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORESceneImpl#getRealizes <em>Realizes</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORESceneImpl#getPerspectiveName <em>Perspective Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORESceneImpl#getElementMappings <em>Element Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORESceneImpl#getArtefacts <em>Artefacts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORESceneImpl extends CORENamedElementImpl implements COREScene {
	/**
     * The cached value of the '{@link #getRealizes() <em>Realizes</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getRealizes()
     * @generated
     * @ordered
     */
	protected EList<COREFeature> realizes;

	/**
     * The default value of the '{@link #getPerspectiveName() <em>Perspective Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPerspectiveName()
     * @generated
     * @ordered
     */
	protected static final String PERSPECTIVE_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getPerspectiveName() <em>Perspective Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPerspectiveName()
     * @generated
     * @ordered
     */
	protected String perspectiveName = PERSPECTIVE_NAME_EDEFAULT;

	/**
     * The cached value of the '{@link #getElementMappings() <em>Element Mappings</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getElementMappings()
     * @generated
     * @ordered
     */
	protected EList<COREModelElementMapping> elementMappings;

	/**
     * The cached value of the '{@link #getArtefacts() <em>Artefacts</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getArtefacts()
     * @generated
     * @ordered
     */
	protected EMap<String, EList<COREArtefact>> artefacts;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected CORESceneImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_SCENE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREFeature> getRealizes() {
        if (realizes == null) {
            realizes = new EObjectWithInverseResolvingEList.ManyInverse<COREFeature>(COREFeature.class, this, CorePackage.CORE_SCENE__REALIZES, CorePackage.CORE_FEATURE__REALIZED_BY);
        }
        return realizes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getPerspectiveName() {
        return perspectiveName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPerspectiveName(String newPerspectiveName) {
        String oldPerspectiveName = perspectiveName;
        perspectiveName = newPerspectiveName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_SCENE__PERSPECTIVE_NAME, oldPerspectiveName, perspectiveName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<COREModelElementMapping> getElementMappings() {
        if (elementMappings == null) {
            elementMappings = new EObjectContainmentEList<COREModelElementMapping>(COREModelElementMapping.class, this, CorePackage.CORE_SCENE__ELEMENT_MAPPINGS);
        }
        return elementMappings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<String, EList<COREArtefact>> getArtefacts() {
        if (artefacts == null) {
            artefacts = new EcoreEMap<String,EList<COREArtefact>>(CorePackage.Literals.ARTEFACT_MAP, ArtefactMapImpl.class, this, CorePackage.CORE_SCENE__ARTEFACTS);
        }
        return artefacts;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_SCENE__REALIZES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getRealizes()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_SCENE__REALIZES:
                return ((InternalEList<?>)getRealizes()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_SCENE__ELEMENT_MAPPINGS:
                return ((InternalEList<?>)getElementMappings()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_SCENE__ARTEFACTS:
                return ((InternalEList<?>)getArtefacts()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_SCENE__REALIZES:
                return getRealizes();
            case CorePackage.CORE_SCENE__PERSPECTIVE_NAME:
                return getPerspectiveName();
            case CorePackage.CORE_SCENE__ELEMENT_MAPPINGS:
                return getElementMappings();
            case CorePackage.CORE_SCENE__ARTEFACTS:
                if (coreType) return getArtefacts();
                else return getArtefacts().map();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_SCENE__REALIZES:
                getRealizes().clear();
                getRealizes().addAll((Collection<? extends COREFeature>)newValue);
                return;
            case CorePackage.CORE_SCENE__PERSPECTIVE_NAME:
                setPerspectiveName((String)newValue);
                return;
            case CorePackage.CORE_SCENE__ELEMENT_MAPPINGS:
                getElementMappings().clear();
                getElementMappings().addAll((Collection<? extends COREModelElementMapping>)newValue);
                return;
            case CorePackage.CORE_SCENE__ARTEFACTS:
                ((EStructuralFeature.Setting)getArtefacts()).set(newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_SCENE__REALIZES:
                getRealizes().clear();
                return;
            case CorePackage.CORE_SCENE__PERSPECTIVE_NAME:
                setPerspectiveName(PERSPECTIVE_NAME_EDEFAULT);
                return;
            case CorePackage.CORE_SCENE__ELEMENT_MAPPINGS:
                getElementMappings().clear();
                return;
            case CorePackage.CORE_SCENE__ARTEFACTS:
                getArtefacts().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_SCENE__REALIZES:
                return realizes != null && !realizes.isEmpty();
            case CorePackage.CORE_SCENE__PERSPECTIVE_NAME:
                return PERSPECTIVE_NAME_EDEFAULT == null ? perspectiveName != null : !PERSPECTIVE_NAME_EDEFAULT.equals(perspectiveName);
            case CorePackage.CORE_SCENE__ELEMENT_MAPPINGS:
                return elementMappings != null && !elementMappings.isEmpty();
            case CorePackage.CORE_SCENE__ARTEFACTS:
                return artefacts != null && !artefacts.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (perspectiveName: ");
        result.append(perspectiveName);
        result.append(')');
        return result.toString();
    }

} //CORESceneImpl
