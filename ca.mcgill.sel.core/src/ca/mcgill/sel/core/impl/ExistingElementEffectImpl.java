/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.ExistingElementEffect;
import ca.mcgill.sel.core.Parameter;
import ca.mcgill.sel.core.ParameterEffect;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Existing Element Effect</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.ExistingElementEffectImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.ExistingElementEffectImpl#getParameterEffect <em>Parameter Effect</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ExistingElementEffectImpl extends ActionEffectImpl implements ExistingElementEffect {
	/**
     * The cached value of the '{@link #getParameter() <em>Parameter</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getParameter()
     * @generated
     * @ordered
     */
	protected Parameter parameter;

	/**
     * The default value of the '{@link #getParameterEffect() <em>Parameter Effect</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getParameterEffect()
     * @generated
     * @ordered
     */
	protected static final ParameterEffect PARAMETER_EFFECT_EDEFAULT = ParameterEffect.ELEMENT;

	/**
     * The cached value of the '{@link #getParameterEffect() <em>Parameter Effect</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getParameterEffect()
     * @generated
     * @ordered
     */
	protected ParameterEffect parameterEffect = PARAMETER_EFFECT_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ExistingElementEffectImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.EXISTING_ELEMENT_EFFECT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Parameter getParameter() {
        if (parameter != null && parameter.eIsProxy()) {
            InternalEObject oldParameter = (InternalEObject)parameter;
            parameter = (Parameter)eResolveProxy(oldParameter);
            if (parameter != oldParameter) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER, oldParameter, parameter));
            }
        }
        return parameter;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Parameter basicGetParameter() {
        return parameter;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setParameter(Parameter newParameter) {
        Parameter oldParameter = parameter;
        parameter = newParameter;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER, oldParameter, parameter));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ParameterEffect getParameterEffect() {
        return parameterEffect;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setParameterEffect(ParameterEffect newParameterEffect) {
        ParameterEffect oldParameterEffect = parameterEffect;
        parameterEffect = newParameterEffect == null ? PARAMETER_EFFECT_EDEFAULT : newParameterEffect;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT, oldParameterEffect, parameterEffect));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER:
                if (resolve) return getParameter();
                return basicGetParameter();
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT:
                return getParameterEffect();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER:
                setParameter((Parameter)newValue);
                return;
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT:
                setParameterEffect((ParameterEffect)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER:
                setParameter((Parameter)null);
                return;
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT:
                setParameterEffect(PARAMETER_EFFECT_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER:
                return parameter != null;
            case CorePackage.EXISTING_ELEMENT_EFFECT__PARAMETER_EFFECT:
                return parameterEffect != PARAMETER_EFFECT_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (parameterEffect: ");
        result.append(parameterEffect);
        result.append(')');
        return result.toString();
    }

} //ExistingElementEffectImpl
