/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Mapping Cardinality</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREMappingCardinalityImpl#getLowerBound <em>Lower Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREMappingCardinalityImpl#getUpperBound <em>Upper Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREMappingCardinalityImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREMappingCardinalityImpl extends EObjectImpl implements COREMappingCardinality {
	/**
     * The default value of the '{@link #getLowerBound() <em>Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLowerBound()
     * @generated
     * @ordered
     */
	protected static final int LOWER_BOUND_EDEFAULT = 0;

	/**
     * The cached value of the '{@link #getLowerBound() <em>Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLowerBound()
     * @generated
     * @ordered
     */
	protected int lowerBound = LOWER_BOUND_EDEFAULT;

	/**
     * The default value of the '{@link #getUpperBound() <em>Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getUpperBound()
     * @generated
     * @ordered
     */
	protected static final int UPPER_BOUND_EDEFAULT = 1;

	/**
     * The cached value of the '{@link #getUpperBound() <em>Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getUpperBound()
     * @generated
     * @ordered
     */
	protected int upperBound = UPPER_BOUND_EDEFAULT;

	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected String name = NAME_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREMappingCardinalityImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MAPPING_CARDINALITY;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getLowerBound() {
        return lowerBound;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setLowerBound(int newLowerBound) {
        int oldLowerBound = lowerBound;
        lowerBound = newLowerBound;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MAPPING_CARDINALITY__LOWER_BOUND, oldLowerBound, lowerBound));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getUpperBound() {
        return upperBound;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setUpperBound(int newUpperBound) {
        int oldUpperBound = upperBound;
        upperBound = newUpperBound;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MAPPING_CARDINALITY__UPPER_BOUND, oldUpperBound, upperBound));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MAPPING_CARDINALITY__NAME, oldName, name));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING_CARDINALITY__LOWER_BOUND:
                return getLowerBound();
            case CorePackage.CORE_MAPPING_CARDINALITY__UPPER_BOUND:
                return getUpperBound();
            case CorePackage.CORE_MAPPING_CARDINALITY__NAME:
                return getName();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING_CARDINALITY__LOWER_BOUND:
                setLowerBound((Integer)newValue);
                return;
            case CorePackage.CORE_MAPPING_CARDINALITY__UPPER_BOUND:
                setUpperBound((Integer)newValue);
                return;
            case CorePackage.CORE_MAPPING_CARDINALITY__NAME:
                setName((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING_CARDINALITY__LOWER_BOUND:
                setLowerBound(LOWER_BOUND_EDEFAULT);
                return;
            case CorePackage.CORE_MAPPING_CARDINALITY__UPPER_BOUND:
                setUpperBound(UPPER_BOUND_EDEFAULT);
                return;
            case CorePackage.CORE_MAPPING_CARDINALITY__NAME:
                setName(NAME_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MAPPING_CARDINALITY__LOWER_BOUND:
                return lowerBound != LOWER_BOUND_EDEFAULT;
            case CorePackage.CORE_MAPPING_CARDINALITY__UPPER_BOUND:
                return upperBound != UPPER_BOUND_EDEFAULT;
            case CorePackage.CORE_MAPPING_CARDINALITY__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (lowerBound: ");
        result.append(lowerBound);
        result.append(", upperBound: ");
        result.append(upperBound);
        result.append(", name: ");
        result.append(name);
        result.append(')');
        return result.toString();
    }

} //COREMappingCardinalityImpl
