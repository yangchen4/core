/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREPerspectiveAction;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Perspective Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREPerspectiveActionImpl#getForRole <em>For Role</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREPerspectiveActionImpl#getActionIdentifier <em>Action Identifier</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class COREPerspectiveActionImpl extends COREActionImpl implements COREPerspectiveAction {
	/**
     * The default value of the '{@link #getForRole() <em>For Role</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getForRole()
     * @generated
     * @ordered
     */
	protected static final String FOR_ROLE_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getForRole() <em>For Role</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getForRole()
     * @generated
     * @ordered
     */
	protected String forRole = FOR_ROLE_EDEFAULT;

	/**
     * The default value of the '{@link #getActionIdentifier() <em>Action Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActionIdentifier()
     * @generated
     * @ordered
     */
	protected static final int ACTION_IDENTIFIER_EDEFAULT = 0;

	/**
     * The cached value of the '{@link #getActionIdentifier() <em>Action Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActionIdentifier()
     * @generated
     * @ordered
     */
	protected int actionIdentifier = ACTION_IDENTIFIER_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected COREPerspectiveActionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_PERSPECTIVE_ACTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getForRole() {
        return forRole;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setForRole(String newForRole) {
        String oldForRole = forRole;
        forRole = newForRole;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_PERSPECTIVE_ACTION__FOR_ROLE, oldForRole, forRole));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getActionIdentifier() {
        return actionIdentifier;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setActionIdentifier(int newActionIdentifier) {
        int oldActionIdentifier = actionIdentifier;
        actionIdentifier = newActionIdentifier;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER, oldActionIdentifier, actionIdentifier));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE_ACTION__FOR_ROLE:
                return getForRole();
            case CorePackage.CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER:
                return getActionIdentifier();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE_ACTION__FOR_ROLE:
                setForRole((String)newValue);
                return;
            case CorePackage.CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER:
                setActionIdentifier((Integer)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE_ACTION__FOR_ROLE:
                setForRole(FOR_ROLE_EDEFAULT);
                return;
            case CorePackage.CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER:
                setActionIdentifier(ACTION_IDENTIFIER_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_PERSPECTIVE_ACTION__FOR_ROLE:
                return FOR_ROLE_EDEFAULT == null ? forRole != null : !FOR_ROLE_EDEFAULT.equals(forRole);
            case CorePackage.CORE_PERSPECTIVE_ACTION__ACTION_IDENTIFIER:
                return actionIdentifier != ACTION_IDENTIFIER_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (forRole: ");
        result.append(forRole);
        result.append(", actionIdentifier: ");
        result.append(actionIdentifier);
        result.append(')');
        return result.toString();
    }

} //COREPerspectiveActionImpl
