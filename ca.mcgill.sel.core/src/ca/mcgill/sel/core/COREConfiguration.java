/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREConfiguration#getSelected <em>Selected</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConfiguration#getReexposed <em>Reexposed</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConfiguration#getExtendingConfigurations <em>Extending Configurations</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConfiguration#getExtendedReuse <em>Extended Reuse</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREConfiguration()
 * @model
 * @generated
 */
public interface COREConfiguration extends COREModelComposition, CORENamedElement {
	/**
     * Returns the value of the '<em><b>Selected</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREFeature}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Selected</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREConfiguration_Selected()
     * @model
     * @generated
     */
	EList<COREFeature> getSelected();

	/**
     * Returns the value of the '<em><b>Reexposed</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREFeature}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reexposed</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREConfiguration_Reexposed()
     * @model
     * @generated
     */
	EList<COREFeature> getReexposed();

	/**
     * Returns the value of the '<em><b>Extending Configurations</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREConfiguration}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Extending Configurations</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREConfiguration_ExtendingConfigurations()
     * @model containment="true"
     * @generated
     */
	EList<COREConfiguration> getExtendingConfigurations();

	/**
     * Returns the value of the '<em><b>Extended Reuse</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Extended Reuse</em>' reference.
     * @see #setExtendedReuse(COREReuse)
     * @see ca.mcgill.sel.core.CorePackage#getCOREConfiguration_ExtendedReuse()
     * @model
     * @generated
     */
	COREReuse getExtendedReuse();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREConfiguration#getExtendedReuse <em>Extended Reuse</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Extended Reuse</em>' reference.
     * @see #getExtendedReuse()
     * @generated
     */
	void setExtendedReuse(COREReuse value);

} // COREConfiguration
