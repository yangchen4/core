/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Existing Element Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.ExistingElementEffect#getParameter <em>Parameter</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.ExistingElementEffect#getParameterEffect <em>Parameter Effect</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getExistingElementEffect()
 * @model abstract="true"
 * @generated
 */
public interface ExistingElementEffect extends ActionEffect {
	/**
     * Returns the value of the '<em><b>Parameter</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Parameter</em>' reference.
     * @see #setParameter(Parameter)
     * @see ca.mcgill.sel.core.CorePackage#getExistingElementEffect_Parameter()
     * @model
     * @generated
     */
	Parameter getParameter();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.ExistingElementEffect#getParameter <em>Parameter</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Parameter</em>' reference.
     * @see #getParameter()
     * @generated
     */
	void setParameter(Parameter value);

	/**
     * Returns the value of the '<em><b>Parameter Effect</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.ParameterEffect}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Parameter Effect</em>' attribute.
     * @see ca.mcgill.sel.core.ParameterEffect
     * @see #setParameterEffect(ParameterEffect)
     * @see ca.mcgill.sel.core.CorePackage#getExistingElementEffect_ParameterEffect()
     * @model required="true"
     * @generated
     */
	ParameterEffect getParameterEffect();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.ExistingElementEffect#getParameterEffect <em>Parameter Effect</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Parameter Effect</em>' attribute.
     * @see ca.mcgill.sel.core.ParameterEffect
     * @see #getParameterEffect()
     * @generated
     */
	void setParameterEffect(ParameterEffect value);

} // ExistingElementEffect
