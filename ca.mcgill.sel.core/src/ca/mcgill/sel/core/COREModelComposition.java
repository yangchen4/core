/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Model Composition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREModelComposition#getSource <em>Source</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREModelComposition#getCompositions <em>Compositions</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREModelComposition()
 * @model abstract="true"
 * @generated
 */
public interface COREModelComposition extends EObject {
	/**
     * Returns the value of the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Source</em>' reference.
     * @see #setSource(COREArtefact)
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelComposition_Source()
     * @model required="true"
     * @generated
     */
	COREArtefact getSource();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREModelComposition#getSource <em>Source</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Source</em>' reference.
     * @see #getSource()
     * @generated
     */
	void setSource(COREArtefact value);

	/**
     * Returns the value of the '<em><b>Compositions</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREModelElementComposition}<code>&lt;?&gt;</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Compositions</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelComposition_Compositions()
     * @model containment="true"
     * @generated
     */
	EList<COREModelElementComposition<?>> getCompositions();

} // COREModelComposition
