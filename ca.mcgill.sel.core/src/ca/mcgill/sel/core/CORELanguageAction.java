/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Language Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageAction#getClassQualifiedName <em>Class Qualified Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageAction#getMethodName <em>Method Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageAction#getParameters <em>Parameters</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageAction#getSecondaryEffects <em>Secondary Effects</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageAction#getActionType <em>Action Type</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELanguageAction()
 * @model
 * @generated
 */
public interface CORELanguageAction extends COREAction {
	/**
     * Returns the value of the '<em><b>Class Qualified Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Class Qualified Name</em>' attribute.
     * @see #setClassQualifiedName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageAction_ClassQualifiedName()
     * @model required="true"
     * @generated
     */
	String getClassQualifiedName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageAction#getClassQualifiedName <em>Class Qualified Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Class Qualified Name</em>' attribute.
     * @see #getClassQualifiedName()
     * @generated
     */
	void setClassQualifiedName(String value);

	/**
     * Returns the value of the '<em><b>Method Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Method Name</em>' attribute.
     * @see #setMethodName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageAction_MethodName()
     * @model required="true"
     * @generated
     */
	String getMethodName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageAction#getMethodName <em>Method Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Method Name</em>' attribute.
     * @see #getMethodName()
     * @generated
     */
	void setMethodName(String value);

	/**
     * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.Parameter}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Parameters</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageAction_Parameters()
     * @model containment="true"
     * @generated
     */
	EList<Parameter> getParameters();

	/**
     * Returns the value of the '<em><b>Secondary Effects</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.ActionEffect}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Secondary Effects</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageAction_SecondaryEffects()
     * @model containment="true"
     * @generated
     */
	EList<ActionEffect> getSecondaryEffects();

	/**
     * Returns the value of the '<em><b>Action Type</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.LanguageActionType}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Action Type</em>' attribute.
     * @see ca.mcgill.sel.core.LanguageActionType
     * @see #setActionType(LanguageActionType)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageAction_ActionType()
     * @model required="true"
     * @generated
     */
	LanguageActionType getActionType();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageAction#getActionType <em>Action Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Action Type</em>' attribute.
     * @see ca.mcgill.sel.core.LanguageActionType
     * @see #getActionType()
     * @generated
     */
	void setActionType(LanguageActionType value);

} // CORELanguageAction
