/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Language Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getLanguageActions <em>Language Actions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getLanguageElement <em>Language Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getLanguage <em>Language</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getNestedElements <em>Nested Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getOwner <em>Owner</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement()
 * @model
 * @generated
 */
public interface CORELanguageElement extends EObject {
	/**
     * Returns the value of the '<em><b>Language Actions</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageAction}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Language Actions</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_LanguageActions()
     * @model
     * @generated
     */
	EList<CORELanguageAction> getLanguageActions();

	/**
     * Returns the value of the '<em><b>Language Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Language Element</em>' reference.
     * @see #setLanguageElement(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_LanguageElement()
     * @model
     * @generated
     */
	EObject getLanguageElement();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguageElement <em>Language Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language Element</em>' reference.
     * @see #getLanguageElement()
     * @generated
     */
	void setLanguageElement(EObject value);

	/**
     * Returns the value of the '<em><b>Language</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREExternalLanguage#getLanguageElements <em>Language Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Language</em>' container reference.
     * @see #setLanguage(COREExternalLanguage)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_Language()
     * @see ca.mcgill.sel.core.COREExternalLanguage#getLanguageElements
     * @model opposite="languageElements" transient="false"
     * @generated
     */
	COREExternalLanguage getLanguage();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguage <em>Language</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language</em>' container reference.
     * @see #getLanguage()
     * @generated
     */
	void setLanguage(COREExternalLanguage value);

	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_Name()
     * @model
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElement#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Nested Elements</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageElement}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORELanguageElement#getOwner <em>Owner</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Nested Elements</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_NestedElements()
     * @see ca.mcgill.sel.core.CORELanguageElement#getOwner
     * @model opposite="owner" containment="true"
     * @generated
     */
	EList<CORELanguageElement> getNestedElements();

	/**
     * Returns the value of the '<em><b>Owner</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORELanguageElement#getNestedElements <em>Nested Elements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Owner</em>' container reference.
     * @see #setOwner(CORELanguageElement)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_Owner()
     * @see ca.mcgill.sel.core.CORELanguageElement#getNestedElements
     * @model opposite="nestedElements" transient="false"
     * @generated
     */
	CORELanguageElement getOwner();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElement#getOwner <em>Owner</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Owner</em>' container reference.
     * @see #getOwner()
     * @generated
     */
	void setOwner(CORELanguageElement value);

} // CORELanguageElement
