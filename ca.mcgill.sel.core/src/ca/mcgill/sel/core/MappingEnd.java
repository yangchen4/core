/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping End</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.MappingEnd#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.MappingEnd#getRoleName <em>Role Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.MappingEnd#getType <em>Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.MappingEnd#getLanguageElement <em>Language Element</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getMappingEnd()
 * @model
 * @generated
 */
public interface MappingEnd extends EObject {
	/**
     * Returns the value of the '<em><b>Cardinality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.Cardinality}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #setCardinality(Cardinality)
     * @see ca.mcgill.sel.core.CorePackage#getMappingEnd_Cardinality()
     * @model required="true"
     * @generated
     */
	Cardinality getCardinality();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.MappingEnd#getCardinality <em>Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #getCardinality()
     * @generated
     */
	void setCardinality(Cardinality value);

	/**
     * Returns the value of the '<em><b>Role Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Role Name</em>' attribute.
     * @see #setRoleName(String)
     * @see ca.mcgill.sel.core.CorePackage#getMappingEnd_RoleName()
     * @model
     * @generated
     */
	String getRoleName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.MappingEnd#getRoleName <em>Role Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Role Name</em>' attribute.
     * @see #getRoleName()
     * @generated
     */
	void setRoleName(String value);

	/**
     * Returns the value of the '<em><b>Type</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getMappingEnds <em>Mapping Ends</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' container reference.
     * @see #setType(CORELanguageElementMapping)
     * @see ca.mcgill.sel.core.CorePackage#getMappingEnd_Type()
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#getMappingEnds
     * @model opposite="mappingEnds" required="true" transient="false"
     * @generated
     */
	CORELanguageElementMapping getType();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.MappingEnd#getType <em>Type</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' container reference.
     * @see #getType()
     * @generated
     */
	void setType(CORELanguageElementMapping value);

	/**
     * Returns the value of the '<em><b>Language Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Language Element</em>' reference.
     * @see #setLanguageElement(CORELanguageElement)
     * @see ca.mcgill.sel.core.CorePackage#getMappingEnd_LanguageElement()
     * @model required="true"
     * @generated
     */
	CORELanguageElement getLanguageElement();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.MappingEnd#getLanguageElement <em>Language Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language Element</em>' reference.
     * @see #getLanguageElement()
     * @generated
     */
	void setLanguageElement(CORELanguageElement value);

} // MappingEnd
