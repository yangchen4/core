/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE External Language</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguage#getNsURI <em>Ns URI</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguage#getResourceFactory <em>Resource Factory</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguage#getAdapterFactory <em>Adapter Factory</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguage#getWeaverClassName <em>Weaver Class Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguage#getFileExtension <em>File Extension</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguage#getLanguageElements <em>Language Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguage#getModelUtilClassName <em>Model Util Class Name</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage()
 * @model
 * @generated
 */
public interface COREExternalLanguage extends CORELanguage {
	/**
     * Returns the value of the '<em><b>Ns URI</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Ns URI</em>' attribute.
     * @see #setNsURI(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage_NsURI()
     * @model required="true"
     * @generated
     */
	String getNsURI();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguage#getNsURI <em>Ns URI</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Ns URI</em>' attribute.
     * @see #getNsURI()
     * @generated
     */
	void setNsURI(String value);

	/**
     * Returns the value of the '<em><b>Resource Factory</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resource Factory</em>' attribute.
     * @see #setResourceFactory(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage_ResourceFactory()
     * @model required="true"
     * @generated
     */
	String getResourceFactory();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguage#getResourceFactory <em>Resource Factory</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource Factory</em>' attribute.
     * @see #getResourceFactory()
     * @generated
     */
	void setResourceFactory(String value);

	/**
     * Returns the value of the '<em><b>Adapter Factory</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Adapter Factory</em>' attribute.
     * @see #setAdapterFactory(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage_AdapterFactory()
     * @model required="true"
     * @generated
     */
	String getAdapterFactory();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguage#getAdapterFactory <em>Adapter Factory</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Adapter Factory</em>' attribute.
     * @see #getAdapterFactory()
     * @generated
     */
	void setAdapterFactory(String value);

	/**
     * Returns the value of the '<em><b>Weaver Class Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Weaver Class Name</em>' attribute.
     * @see #setWeaverClassName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage_WeaverClassName()
     * @model required="true"
     * @generated
     */
	String getWeaverClassName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguage#getWeaverClassName <em>Weaver Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Weaver Class Name</em>' attribute.
     * @see #getWeaverClassName()
     * @generated
     */
	void setWeaverClassName(String value);

	/**
     * Returns the value of the '<em><b>File Extension</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>File Extension</em>' attribute.
     * @see #setFileExtension(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage_FileExtension()
     * @model required="true"
     * @generated
     */
	String getFileExtension();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguage#getFileExtension <em>File Extension</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>File Extension</em>' attribute.
     * @see #getFileExtension()
     * @generated
     */
	void setFileExtension(String value);

	/**
     * Returns the value of the '<em><b>Language Elements</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageElement}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguage <em>Language</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Language Elements</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage_LanguageElements()
     * @see ca.mcgill.sel.core.CORELanguageElement#getLanguage
     * @model opposite="language" containment="true"
     * @generated
     */
	EList<CORELanguageElement> getLanguageElements();

	/**
     * Returns the value of the '<em><b>Model Util Class Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Util Class Name</em>' attribute.
     * @see #setModelUtilClassName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguage_ModelUtilClassName()
     * @model
     * @generated
     */
	String getModelUtilClassName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguage#getModelUtilClassName <em>Model Util Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Model Util Class Name</em>' attribute.
     * @see #getModelUtilClassName()
     * @generated
     */
	void setModelUtilClassName(String value);

} // COREExternalLanguage
