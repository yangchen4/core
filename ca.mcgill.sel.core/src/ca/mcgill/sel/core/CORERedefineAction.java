/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Redefine Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORERedefineAction#getRedefinedAction <em>Redefined Action</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORERedefineAction#getReusedActions <em>Reused Actions</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORERedefineAction()
 * @model
 * @generated
 */
public interface CORERedefineAction extends COREPerspectiveAction {
	/**
     * Returns the value of the '<em><b>Redefined Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Redefined Action</em>' reference.
     * @see #setRedefinedAction(COREAction)
     * @see ca.mcgill.sel.core.CorePackage#getCORERedefineAction_RedefinedAction()
     * @model required="true"
     * @generated
     */
	COREAction getRedefinedAction();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORERedefineAction#getRedefinedAction <em>Redefined Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Redefined Action</em>' reference.
     * @see #getRedefinedAction()
     * @generated
     */
	void setRedefinedAction(COREAction value);

	/**
     * Returns the value of the '<em><b>Reused Actions</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREAction}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reused Actions</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORERedefineAction_ReusedActions()
     * @model required="true"
     * @generated
     */
	EList<COREAction> getReusedActions();

} // CORERedefineAction
