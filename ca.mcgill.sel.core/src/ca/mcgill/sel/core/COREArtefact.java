/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREArtefact#getModelReuses <em>Model Reuses</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREArtefact#getCoreConcern <em>Core Concern</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREArtefact#getModelExtensions <em>Model Extensions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREArtefact#getUiElements <em>Ui Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREArtefact#getCiElements <em>Ci Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREArtefact#getTemporaryConcern <em>Temporary Concern</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREArtefact#getScene <em>Scene</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact()
 * @model abstract="true"
 * @generated
 */
public interface COREArtefact extends CORENamedElement {
	/**
     * Returns the value of the '<em><b>Model Reuses</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREModelReuse}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Reuses</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact_ModelReuses()
     * @model containment="true"
     * @generated
     */
	EList<COREModelReuse> getModelReuses();

	/**
     * Returns the value of the '<em><b>Core Concern</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREConcern#getArtefacts <em>Artefacts</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Core Concern</em>' container reference.
     * @see #setCoreConcern(COREConcern)
     * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact_CoreConcern()
     * @see ca.mcgill.sel.core.COREConcern#getArtefacts
     * @model opposite="artefacts" required="true" transient="false"
     * @generated
     */
	COREConcern getCoreConcern();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREArtefact#getCoreConcern <em>Core Concern</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Core Concern</em>' container reference.
     * @see #getCoreConcern()
     * @generated
     */
	void setCoreConcern(COREConcern value);

	/**
     * Returns the value of the '<em><b>Model Extensions</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREModelExtension}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Extensions</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact_ModelExtensions()
     * @model containment="true"
     * @generated
     */
	EList<COREModelExtension> getModelExtensions();

	/**
     * Returns the value of the '<em><b>Ui Elements</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREUIElement}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Ui Elements</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact_UiElements()
     * @model containment="true"
     * @generated
     */
	EList<COREUIElement> getUiElements();

	/**
     * Returns the value of the '<em><b>Ci Elements</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORECIElement}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Ci Elements</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact_CiElements()
     * @model containment="true"
     * @generated
     */
	EList<CORECIElement> getCiElements();

	/**
     * Returns the value of the '<em><b>Temporary Concern</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREConcern#getTemporaryArtefacts <em>Temporary Artefacts</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Temporary Concern</em>' reference.
     * @see #setTemporaryConcern(COREConcern)
     * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact_TemporaryConcern()
     * @see ca.mcgill.sel.core.COREConcern#getTemporaryArtefacts
     * @model opposite="temporaryArtefacts" transient="true"
     * @generated
     */
	COREConcern getTemporaryConcern();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREArtefact#getTemporaryConcern <em>Temporary Concern</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Temporary Concern</em>' reference.
     * @see #getTemporaryConcern()
     * @generated
     */
	void setTemporaryConcern(COREConcern value);

	/**
     * Returns the value of the '<em><b>Scene</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Scene</em>' reference.
     * @see #setScene(COREScene)
     * @see ca.mcgill.sel.core.CorePackage#getCOREArtefact_Scene()
     * @model
     * @generated
     */
	COREScene getScene();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREArtefact#getScene <em>Scene</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Scene</em>' reference.
     * @see #getScene()
     * @generated
     */
	void setScene(COREScene value);

} // COREArtefact
