/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Reexpose Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREReexposeAction#getReexposedAction <em>Reexposed Action</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREReexposeAction()
 * @model
 * @generated
 */
public interface COREReexposeAction extends COREPerspectiveAction {
	/**
     * Returns the value of the '<em><b>Reexposed Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reexposed Action</em>' reference.
     * @see #setReexposedAction(COREAction)
     * @see ca.mcgill.sel.core.CorePackage#getCOREReexposeAction_ReexposedAction()
     * @model required="true"
     * @generated
     */
	COREAction getReexposedAction();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREReexposeAction#getReexposedAction <em>Reexposed Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Reexposed Action</em>' reference.
     * @see #getReexposedAction()
     * @generated
     */
	void setReexposedAction(COREAction value);

} // COREReexposeAction
