/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COREUI Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREUIElement#getModelElement <em>Model Element</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREUIElement()
 * @model
 * @generated
 */
public interface COREUIElement extends EObject {
	/**
     * Returns the value of the '<em><b>Model Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Element</em>' reference.
     * @see #setModelElement(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getCOREUIElement_ModelElement()
     * @model required="true"
     * @generated
     */
	EObject getModelElement();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREUIElement#getModelElement <em>Model Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Model Element</em>' reference.
     * @see #getModelElement()
     * @generated
     */
	void setModelElement(EObject value);

} // COREUIElement
