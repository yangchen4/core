/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Update Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.UpdateEffect#getAffectedFeature <em>Affected Feature</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getUpdateEffect()
 * @model
 * @generated
 */
public interface UpdateEffect extends ExistingElementEffect {
	/**
     * Returns the value of the '<em><b>Affected Feature</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Affected Feature</em>' reference.
     * @see #setAffectedFeature(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getUpdateEffect_AffectedFeature()
     * @model
     * @generated
     */
	EObject getAffectedFeature();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.UpdateEffect#getAffectedFeature <em>Affected Feature</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Affected Feature</em>' reference.
     * @see #getAffectedFeature()
     * @generated
     */
	void setAffectedFeature(EObject value);

} // UpdateEffect
