/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inter Language Mapping End</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMappingEnd#isOrigin <em>Origin</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMappingEnd#isDestination <em>Destination</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getMappingEnd <em>Mapping End</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMappingEnd()
 * @model
 * @generated
 */
public interface InterLanguageMappingEnd extends EObject {
//    /**
//     * Returns the value of the '<em><b>Name</b></em>' attribute.
//     * <!-- begin-user-doc -->
//     * <!-- end-user-doc -->
//     * @return the value of the '<em>Name</em>' attribute.
//     * @see #setName(String)
//     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMappingEnd_Name()
//     * @model dataType="org.eclipse.emf.ecore.xml.type.String" derived="true"
//     * @generated
//     */
//    String getName();

    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMappingEnd_Name()
     * @model dataType="org.eclipse.emf.ecore.xml.type.String" derived="true"
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Mapping End</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Mapping End</em>' reference.
     * @see #setMappingEnd(MappingEnd)
     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMappingEnd_MappingEnd()
     * @model required="true"
     * @generated
     */
    MappingEnd getMappingEnd();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getMappingEnd <em>Mapping End</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Mapping End</em>' reference.
     * @see #getMappingEnd()
     * @generated
     */
    void setMappingEnd(MappingEnd value);

    /**
     * Returns the value of the '<em><b>Origin</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Origin</em>' attribute.
     * @see #setOrigin(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMappingEnd_Origin()
     * @model
     * @generated
     */
    boolean isOrigin();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#isOrigin <em>Origin</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Origin</em>' attribute.
     * @see #isOrigin()
     * @generated
     */
    void setOrigin(boolean value);

    /**
     * Returns the value of the '<em><b>Destination</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Destination</em>' attribute.
     * @see #setDestination(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMappingEnd_Destination()
     * @model
     * @generated
     */
    boolean isDestination();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#isDestination <em>Destination</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Destination</em>' attribute.
     * @see #isDestination()
     * @generated
     */
    void setDestination(boolean value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    String getName(EClass connectionClass);

} // InterLanguageMappingEnd
