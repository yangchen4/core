/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Perspective Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREPerspectiveAction#getForRole <em>For Role</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREPerspectiveAction#getActionIdentifier <em>Action Identifier</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREPerspectiveAction()
 * @model abstract="true"
 * @generated
 */
public interface COREPerspectiveAction extends COREAction {
	/**
     * Returns the value of the '<em><b>For Role</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>For Role</em>' attribute.
     * @see #setForRole(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREPerspectiveAction_ForRole()
     * @model
     * @generated
     */
	String getForRole();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREPerspectiveAction#getForRole <em>For Role</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>For Role</em>' attribute.
     * @see #getForRole()
     * @generated
     */
	void setForRole(String value);

	/**
     * Returns the value of the '<em><b>Action Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Action Identifier</em>' attribute.
     * @see #setActionIdentifier(int)
     * @see ca.mcgill.sel.core.CorePackage#getCOREPerspectiveAction_ActionIdentifier()
     * @model
     * @generated
     */
	int getActionIdentifier();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREPerspectiveAction#getActionIdentifier <em>Action Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Action Identifier</em>' attribute.
     * @see #getActionIdentifier()
     * @generated
     */
	void setActionIdentifier(int value);

} // COREPerspectiveAction
