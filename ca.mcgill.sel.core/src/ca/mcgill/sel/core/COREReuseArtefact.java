/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Reuse Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREReuseArtefact()
 * @model
 * @generated
 */
public interface COREReuseArtefact extends COREArtefact {
} // COREReuseArtefact
