/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Model Reuse</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREModelReuse#getReuse <em>Reuse</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREModelReuse#getConfiguration <em>Configuration</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREModelReuse()
 * @model
 * @generated
 */
public interface COREModelReuse extends COREModelComposition {
	/**
     * Returns the value of the '<em><b>Reuse</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREReuse#getModelReuses <em>Model Reuses</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reuse</em>' reference.
     * @see #setReuse(COREReuse)
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelReuse_Reuse()
     * @see ca.mcgill.sel.core.COREReuse#getModelReuses
     * @model opposite="modelReuses" required="true"
     * @generated
     */
	COREReuse getReuse();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREModelReuse#getReuse <em>Reuse</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Reuse</em>' reference.
     * @see #getReuse()
     * @generated
     */
	void setReuse(COREReuse value);

	/**
     * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Configuration</em>' containment reference.
     * @see #setConfiguration(COREConfiguration)
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelReuse_Configuration()
     * @model containment="true" required="true"
     * @generated
     */
	COREConfiguration getConfiguration();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREModelReuse#getConfiguration <em>Configuration</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Configuration</em>' containment reference.
     * @see #getConfiguration()
     * @generated
     */
	void setConfiguration(COREConfiguration value);

} // COREModelReuse
