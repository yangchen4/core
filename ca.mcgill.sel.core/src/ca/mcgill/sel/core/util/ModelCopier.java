package ca.mcgill.sel.core.util;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

public class ModelCopier extends Copier {

    private static final long serialVersionUID = 1L;
    
    private Map<EObject, EObject> myMap;
    
    /**
     * ModelCopier constructor.
     *
     * @param mappings a map that will be filled with the mappings 
     */
    public ModelCopier(Map<EObject, EObject> mappings) {
        super();
        myMap = mappings;
    }
    
    @Override
    protected EObject createCopy(EObject eObject) {
        // TODO Auto-generated method stub
        EObject copy = super.createCopy(eObject);
        myMap.put(eObject, copy);
        return copy;
    }

    /**
     * This operation returns a deep copy of the model referred to by the 'model' parameter, and also returns a map that
     * links every original model element contained in 'model' to the corresponding model element copy in the copy.
     * 
     * @param model the root element of the model that is to be copied 
     * @param mappings a map that will upon return contain mappings for every copied model element
     * @return the copy of the model
     */
    public static EObject copyModelAndRetrieveMappings(EObject model, Map<EObject, EObject> mappings) {
        ModelCopier mc = new ModelCopier(mappings);
        EObject result = mc.copy(model);
        mc.copyReferences();
        return result;
    }
    
}
