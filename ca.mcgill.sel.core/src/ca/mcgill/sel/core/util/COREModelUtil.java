package ca.mcgill.sel.core.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.FileManagerUtil;
import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;

/**
 * Helper class with convenient static methods for working with CORE EMF model objects.
 *
 * @author mschoettle
 */
public final class COREModelUtil {

    /**
     * The default directory for all models.
     */
    public static final String LANGUAGE_MODELS = ResourceUtil.getResourcesDirectory(COREModelUtil.class) + "models"
            + File.pathSeparator + "reusable_concern_library" + File.pathSeparator + "languages";

    private static final String REUSED_PREFIX = "Reused_";

    /**
     * Creates a new instance of {@link COREModelUtil}.
     */
    private COREModelUtil() {
        // suppress default constructor
    }

    /**
     * Creates a new {@link COREConcern} with an empty {@link COREFeatureModel} (one root feature).
     *
     * @param name the name of the concern
     * @return the newly created {@link COREConcern}
     */
    public static COREConcern createConcern(String name) {
        COREConcern concern = CoreFactory.eINSTANCE.createCOREConcern();
        concern.setName(name);

        // Create a Feature Model with a root feature.
        COREFeatureModel featureModel = CoreFactory.eINSTANCE.createCOREFeatureModel();

        COREFeature rootFeature = CoreFactory.eINSTANCE.createCOREFeature();
        rootFeature.setName(name);
        rootFeature.setParentRelationship(COREFeatureRelationshipType.NONE);

        featureModel.getFeatures().add(rootFeature);
        featureModel.setRoot(rootFeature);

        concern.setFeatureModel(featureModel);
        concern.getArtefacts().add(featureModel);

        COREImpactModel impactModel = CoreFactory.eINSTANCE.createCOREImpactModel();
        impactModel.setName("Impact Model");
        
        concern.setImpactModel(impactModel);
        concern.getArtefacts().add(impactModel);

        return concern;
    }
    
    /**
     * Get the local concern.
     * If the concern was not already reused, it creates a copy of the folder locally.
     * It then loads the local concern and returns it.
     * <b>Note:</b> The URIs to be provided are expected to be file URIs.
     * I.e., {@link URI#createFileURI(String)} should be used instead of {@link URI#createURI(String)}.
     *
     * @param reusedConcernURI The file URI of the reused concern
     * @param reusingConcernURI The file URI of the concern reusing another concern
     *
     * @return The local concern
     * @throws IOException - Thrown if not able to copy the directory
     */
    public static COREConcern getLocalConcern(URI reusedConcernURI, URI reusingConcernURI) throws IOException {
        String sourceDirectory = reusedConcernURI.trimSegments(1).toFileString();
        String destinationDirectory = reusingConcernURI.trimSegments(1).toFileString();
        if (reusedConcernURI.path().equals(reusingConcernURI.path())) {
            // Concern cannot reuse itself
            return null;
        }
        
        // Make sure the concern name is decoded.
        String concernName = URI.decode(reusedConcernURI.trimSegments(1).lastSegment());
        String targetDirectory = destinationDirectory.concat(File.separator).concat(REUSED_PREFIX).concat(concernName);
    
        if (!(new File(targetDirectory).isDirectory())) {
            FileManagerUtil.copyDirectory(sourceDirectory, targetDirectory);
        }
        
        // Ensure the concern file name is decoded.
        String concernFileName = URI.decode(reusedConcernURI.lastSegment());
        String modelFileName = targetDirectory.concat(File.separator).concat(concernFileName);
        
        return (COREConcern) ResourceManager.loadModel(modelFileName);
    }

    /**
     * Creates a new {@link COREReuse}, reusing the given concern.
     *
     * @param reusingConcern the reusing concern
     * @param reusedConcern the referenced concern (external concern) of the {@link COREReuse}
     * 
     * @return reuse the COREReuse
     */
    public static COREReuse createReuse(COREConcern reusingConcern, COREConcern reusedConcern) {
        COREReuse reuse = CoreFactory.eINSTANCE.createCOREReuse();
        reuse.setReusedConcern(reusedConcern);
        
        String reuseName = COREModelUtil.createUniqueNameFromElements(reusedConcern.getName(),
                reusingConcern.getReuses());
        reuse.setName(reuseName);
        
        return reuse;
    }
    
    /**
     * Creates a new {@link COREModelReuse} with the given source and reuse.
     * 
     * @param source the external model that is reused
     * @param reuse the responsible {@link COREReuse} 
     * @return the {@link COREModelReuse}
     */
    public static COREModelReuse createModelReuse(COREArtefact source, COREReuse reuse) {
        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        modelReuse.setSource(source);
        modelReuse.setReuse(reuse);
        
        return modelReuse;
    }
    
    /**
     * Creates a new {@link COREExternalArtefact} with the given model root.
     * 
     * @param rootModelElement - The external model, e.g., a RAM Aspect
     * @param feature - The feature it depends on
     * @return the {@link COREExternalArtefact}
     */
    public static COREExternalArtefact createExternalArtefact(EObject rootModelElement, COREFeature feature) {
        COREExternalArtefact externalArtefact = CoreFactory.eINSTANCE.createCOREExternalArtefact();
        externalArtefact.setRootModelElement(rootModelElement);
        externalArtefact.setName(feature.getName());
        return externalArtefact;
    }
    
    /**
     * Creates a new {@link COREExternalArtefact} with the given model root.
     * 
     * @param rootModelElement - The external model, e.g., a RAM Aspect
     * @param name - The name to be used
     * @return the {@link COREExternalArtefact}
     */
    public static COREExternalArtefact createExternalArtefact(EObject rootModelElement, String name) {
        COREExternalArtefact externalArtefact = CoreFactory.eINSTANCE.createCOREExternalArtefact();
        externalArtefact.setRootModelElement(rootModelElement);
        externalArtefact.setName(name);
        return externalArtefact;
    }
    
    /**
     * Creates a new {@link COREScene}.
     * 
     * @return the {@link COREScene}
     */
    public static COREScene createScene() {
        COREScene scene = CoreFactory.eINSTANCE.createCOREScene();
        return scene;
    }
    
    /**
     * Look for the closest realization model realizing a parent feature.
     * Only return non conflict resolution models.
     *
     * @param feature - The feature we want the realizing model for.
     * @param perspective - The perspective we're interested in
     * @param roleName - The role the model is playing in the perspective
     * @return the closest realization model, or null if none exists or they all are conflict resolution models.
     */
    public static COREExternalArtefact getParentArtefact(COREFeature feature, COREPerspective perspective,
            String roleName) {
        if (feature == null || feature.getParent() == null) {
            return null;
        }
        COREFeature parent = feature.getParent();

        // Collect all realizing scenes of the parent feature
        Collection<COREScene> scenes = EMFModelUtil.collectElementsOfType(parent,
                CorePackage.Literals.CORE_FEATURE__REALIZED_BY, CorePackage.eINSTANCE.getCOREScene());
        
        for (COREScene s : scenes) {
            if (s.getPerspectiveName().equals(perspective.getName())) {
                // we found a scene that is of the same perspective
                COREExternalArtefact parentArtefact = (COREExternalArtefact) 
                        s.getArtefacts().get(roleName).get(0);
                if (parentArtefact != null) {
                    return parentArtefact;
                }
            }
        }
        // No realizing externalArtefact was found in the parent -> search in the parent of the parent
        return getParentArtefact(parent, perspective, roleName);
    }
    
    /**
     * Creates a new {@link COREModelExtension} with the given source.
     * 
     * @param source the external model that is extended
     * @return the {@link COREModelExtension}
     */
    public static COREModelExtension createModelExtension(COREArtefact source) {
        COREModelExtension modelExtension = CoreFactory.eINSTANCE.createCOREModelExtension();
        modelExtension.setSource(source);
        
        return modelExtension;
    }

    /**
     * Get all the model reuses for a given {@link COREReuse} in a concern.
     *
     * @param coreReuse - The {@link COREReuse} to delete.
     * @return the set of modelReuses of this reuse.
     */
    public static Set<COREModelReuse> getModelReuses(COREReuse coreReuse) {
        COREConcern reusingConcern = (COREConcern) coreReuse.eContainer().eContainer().eContainer();
        return getModelReuses(reusingConcern, coreReuse);
    }

    /**
     * Get all the model reuses for a given {@link COREReuse} in a concern.
     *
     * @param concern - The concern that made the {@link COREReuse}.
     * @param coreReuse - The {@link COREReuse} to delete.
     * @return the set of modelReuses of this reuse.
     */
    public static Set<COREModelReuse> getModelReuses(COREConcern concern, COREReuse coreReuse) {
        Set<COREModelReuse> modelReuses = new HashSet<COREModelReuse>();
        if (concern == null || coreReuse == null) {
            return modelReuses;
        }
        
        for (COREArtefact model : concern.getArtefacts()) {
            for (COREModelReuse modelReuse : model.getModelReuses()) {
                if (modelReuse.getReuse() == coreReuse) {
                    modelReuses.add(modelReuse);
                }
            }
        }
        
        return modelReuses;
    }
    
    /**
     * Get the {@link COREModelComposition} of the currentModel that contains mappings to a
     * specific (reused or extended) external {@link COREArtefact}.
     *
     * @param currentModel - The current {@link COREArtefact}
     * @param externalModel - The external {@link COREArtefact}
     * @return the {@link COREModelComposition} that refers to the external model, if any.
     */
    public static COREModelComposition getCOREModelComposition(COREArtefact currentModel, COREArtefact externalModel) {
        
        for (COREModelReuse cmr : currentModel.getModelReuses()) {
            if (cmr.getSource() == externalModel) {
                return cmr;
            }
        }
        for (COREModelExtension cme : currentModel.getModelExtensions()) {
            if (cme.getSource() == externalModel) {
                return cme;
            }           
        }
        return null;
    }
    
   /**
     * Determines whether modelElement is a reference or not by looking through all model extensions
     * and model reuses to find a COREMapping that is not a reference.
     *
     * @param modelElement the modelElement for which the check should be made
     * @return boolean whether or not the element is a reference
     */
    public static boolean isReference(EObject modelElement) {
        /**
         * Due to a bug in Pivot OCL, templated properties are not supported currently (Eclipse Oxygen).
         * However, running the OCL derivation query a second time works just fine.
         * See https://bugs.eclipse.org/bugs/show_bug.cgi?id=492801#c24.
         */
        // TODO: Remove fix once bug in Pivot OCL is fixed 
        // TODO: Replace all calls with direct call to modelElement.isReference() once fixed.
//        boolean result;
//        
//        try {
//            result = modelElement.isReference();
//            // CHECKSTYLE:IGNORE IllegalCatch: RuntimeException thrown by Pivot OCL due to bug.
//        } catch (Exception e) {
//            result = modelElement.isReference();            
//        }
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(modelElement);        
        return COREArtefactUtil.isElementMapped(modelElement, artefact);
    }
    
    /**
     * Collects a set of model extensions for models that are extended by the model containing the objectOfInterest.
     * Collects these recursively in case an extended model also extends another model.
     *
     * @param objectOfInterest - The objectOfInterest contained by the model
     * @return the set of model extensions for models that are extended this model
     */
    public static Set<COREModelExtension> collectModelExtensions(EObject objectOfInterest) {
        Set<COREModelExtension> modelExtensions = new HashSet<>();
        
        collectModelExtensions(objectOfInterest, modelExtensions);
        
        return modelExtensions;
    }
    
    /**
     * Collects a set of model extensions for models that are extended by the model containing the objectOfInterest.
     * Collects these recursively in case an extended model also extends another model.
     * Model extensions are added to the provided set.
     * This means that duplicates, should they occur, are ignored.
     *
     * @param objectOfInterest - The objectOfInterest contained by the model
     * @param extendedModels the current set of found model extensions
     */
    private static void collectModelExtensions(EObject objectOfInterest, Set<COREModelExtension> extendedModels) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(objectOfInterest);
        for (COREModelExtension modelExtension : artefact.getModelExtensions()) {
            // If it was already added, it is not necessary to search for extended models of that model again
            // this prevents infinite loops in case of cyclic dependencies between models.
            if (extendedModels.add(modelExtension)) {
                collectModelExtensions(modelExtension.getSource(), extendedModels);
            }
        }
    }
    
//    /**
//     * Finds the {@link COREModelComposition}, if any, that specifies how to compose the model that contains
//     * objectInFromModel with the model that contains objectInToModel. For model extensions, this operation looks
//     * through the entire extension hierarchy for objectInFromModel.
//     * 
//     * @param objectInFromModel some object that is contained in the "from" model
//     * @param objectInToModel some object that is contained in the "to" model
//     * @return the model composition that connects the "from" to the "to" model, if any, otherwise null
//     */
//    public static COREModelComposition getModelCompositionFor(EObject objectInFromModel, EObject objectInToModel) {
//        COREArtefact fromArtefact = COREArtefactUtil.getReferencingExternalArtefact(objectInFromModel);
//        COREArtefact toArtefact = COREArtefactUtil.getReferencingExternalArtefact(objectInToModel);
//        
//        for (COREModelReuse modelReuse : toArtefact.getModelReuses()) {
//            if (modelReuse.getSource() == fromArtefact) {
//                return modelReuse;
//            }
//        }
//        
//        for (COREModelExtension modelExtension : toArtefact.getModelExtensions()) {
//            if (modelExtension.getSource() == fromArtefact) {
//                return modelExtension;
//            } else {
//                /**
//                 * Check whether the model is within the extension hierarchy.
//                 */
//                Set<COREArtefact> extendedModels = COREModelUtil.collectExtendedModels(modelExtension.getSource());
//                
//                if (extendedModels.contains(fromArtefact)) {
//                    return modelExtension;
//                }
//            }
//        }
//        return null;
//    }
//    
    /**
    /**
     * Finds the {@link COREModelComposition}, if any, that specifies how to compose the model that contains
     * objectInFromModel with the model that contains objectInToModel.
     * 
     * @param objectInFromModel some object that is contained in the "from" model
     * @param objectInToModel some object that is contained in the "to" model
     * @return the model composition that connects the "from" to the "to" model, if any, otherwise null
     */
    public static COREModelComposition getModelCompositionFor(EObject objectInFromModel, EObject objectInToModel) {
        COREArtefact fromArtefact = COREArtefactUtil.getReferencingExternalArtefact(objectInFromModel);
        COREArtefact toArtefact = COREArtefactUtil.getReferencingExternalArtefact(objectInToModel);
        
        for (COREModelReuse modelReuse : toArtefact.getModelReuses()) {
            if (modelReuse.getSource() == fromArtefact) {
                return modelReuse;
            }
        }
        
        for (COREModelExtension modelExtension : toArtefact.getModelExtensions()) {
            if (modelExtension.getSource() == fromArtefact) {
                return modelExtension;
            }
        }
        return null;
    }
    
    /**
     * Collects a set of models that are extended by the model containing the objectOfInterest.
     * Collects these recursively in case an extended model also extends another model.
     * Only models of the same type as the model containing the objectOfInterest are returned.
     *
     * @param objectOfInterest - The objectOfInterest contained by the model
     * @param <T> the concrete type of the model (extending {@link COREArtefact}
     * @return the set of models that are extended by this model
     */
    public static <T extends COREArtefact> Set<T> collectExtendedModels(EObject objectOfInterest) {
        COREArtefact model = COREArtefactUtil.getReferencingExternalArtefact(objectOfInterest);
        Set<COREModelExtension> modelExtensions = COREModelUtil.collectModelExtensions(model);
        Set<T> extendedModels = new HashSet<>();

        for (COREModelExtension modelExtension : modelExtensions) {
            
            /**
             * Ensure that the given model and the extension's source are the same.
             * 
             */
            if (modelExtension.getSource().eClass().isInstance(model)) {
                // Casting won't raise an exception due to Java's type erasure.
                // Could be enforced using: model.getClass().cast(modelExtension.getSource())
                @SuppressWarnings("unchecked")
                T extendedModel = (T) modelExtension.getSource();
                extendedModels.add(extendedModel);
            }
        }

        return extendedModels;
    }
    
    /**
     * Returns the set of model reuses of the model containing the objectOfInterest.
     * and all its extended models in the hierarchy.
     *
     * @param objectOfInterest - The objectOfInterest contained by the model
     * @return the set of model reuses
     */
    public static Set<COREModelReuse> collectAllModelReuses(EObject objectOfInterest) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(objectOfInterest);
        Set<COREModelReuse> modelReuses = new HashSet<>();
        
        Set<COREArtefact> extendedModels = collectExtendedModels(artefact);
        extendedModels.add(artefact);
        
        for (COREArtefact extendedModel : extendedModels) {
            modelReuses.addAll(extendedModel.getModelReuses());
        }
        
        return modelReuses;
    }
    
    /**
     * Returns all compositions for the model containing the objectOfInterest.
     * This includes all extensions as well as all model reuses.
     * 
     * @param objectOfInterest - The objectOfInterest contained by the model
     * @return the set of all model compositions
     */
    public static Set<COREModelComposition> collectAllModelCompositions(EObject objectOfInterest) {
        Set<COREModelComposition> allCompositions = new HashSet<>();
        
        allCompositions.addAll(COREModelUtil.collectModelExtensions(objectOfInterest));
        allCompositions.addAll(COREModelUtil.collectAllModelReuses(objectOfInterest));
        
        return allCompositions;
    }
    
    /**
     * Collects all model elements that the given model element also is/represents.
     * A model element represents another element if it is mapped, therefore,
     * any property of the mapped from element is also accessible by the given element.
     * Note that the returned set includes the given element as well.
     * 
     * @param element the {@link COREModelElement} to collect all elements for
     * @return the set of {@link COREModelElement}s that the given element represents
     */
    public static Set<EObject> collectModelElementsFor(EObject element) {
        Set<EObject> result = new HashSet<>();
        result.add(element);
        
        COREArtefact model = COREArtefactUtil.getReferencingExternalArtefact(element);
        
        collectModelElementsRecursive(result, model, element);        
        
        return result;
    }
    
    /**
     * Recursively collects all model elements that the given model element is/represents.
     * Recursively traverses the extension and reuse hierarchy to find all elements.
     * 
     * @param elements the current set of {@link EObject}s
     * @param model the current {@link COREArtefact} to investigate
     * @param element the current {@link EObject} to find corresponding elements for
     */
    private static void collectModelElementsRecursive(Set<EObject> elements, COREArtefact model,
            EObject element) {
        elements.add(element);
        
        Set<COREModelComposition> modelCompositions = new HashSet<COREModelComposition>();
        modelCompositions.addAll(model.getModelExtensions());
        modelCompositions.addAll(model.getModelReuses());

        for (COREModelComposition modelComposition : modelCompositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                @SuppressWarnings("unchecked")
                COREMapping<EObject> mapping = (COREMapping<EObject>) composition;
                if (mapping.getTo() == element) {
                    collectModelElementsRecursive(elements, modelComposition.getSource(), mapping.getFrom());
                }
            }
        }
    }
    
    /**
     * Filters the choice of values for the given object.
     * Removes all values that are not part of the same model
     * or any other model that is extended by the model of the given object.
     * 
     * @see #collectExtendedModels(COREArtefact)
     * @param object the object that contains a property with the choices
     * @param choiceOfValues the choice of values to be filtered
     * @return the filtered choice of values
     */
    public static Collection<?> filterExtendedChoiceOfValues(EObject object, Collection<?> choiceOfValues) {
        EObject model = EcoreUtil.getRootContainer(object);
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
        
        // Create a set of artefacts where elements are allowed from.
        // Besides the current artefact, add extended artefacts.
        Set<COREExternalArtefact> artefacts = COREModelUtil.collectExtendedModels(artefact);
        artefacts.add(artefact);
        Set<EObject> models = new HashSet<EObject>();
        for (COREExternalArtefact a : artefacts) {
            models.add(a.getRootModelElement());
        }
        
        // Filter out all classes that are not contained in one of the possible models.
        // Also filter out all types that are not part of the current model.
        for (Iterator<?> iterator = choiceOfValues.iterator(); iterator.hasNext(); ) {
            EObject value = (EObject) iterator.next();

            // null is also contained in the list
            if (value != null) {
                EObject objectContainer = EcoreUtil.getRootContainer(value);

                if (!models.contains(objectContainer)) {
                    iterator.remove();
                } 
            }
        }

        return choiceOfValues;
    }
    
    /**
     * Filters out all elements from the result that are mapped to another element.
     *  
     * @param result the unfiltered result collection
     * @param compositions the set of {@link COREModelComposition}s to consider
     */
    public static void filterMappedElements(Collection<?> result, Set<? extends COREModelComposition> compositions) {
        for (COREModelComposition modelComposition : compositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                if (composition instanceof COREMapping) {
                    COREMapping<?> mapping = (COREMapping<?>) composition;
                    result.remove(mapping.getFrom());
                    
                    TreeIterator<EObject> iterator = mapping.eAllContents();
                    while (iterator.hasNext()) {
                        COREMapping<?> nestedMapping = (COREMapping<?>) iterator.next();
                        
                        result.remove(nestedMapping.getFrom());
                    }
                }
            }
        }
    }
    
    /**
     * Filters out all elements from the result that are mapped to another element.
     *  
     * @param result - The unfiltered result collection
     */
    public static void filterMappedElements(Collection<? extends EObject> result) {
        Set<COREModelComposition> compositions = new HashSet<>();
        for (EObject element : result) {
            compositions.addAll(collectAllModelCompositions(element));
        }
        for (COREModelComposition modelComposition : compositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                if (composition instanceof COREMapping) {
                    COREMapping<?> mapping = (COREMapping<?>) composition;
                    result.remove(mapping.getFrom());
                    
                    TreeIterator<EObject> iterator = mapping.eAllContents();
                    while (iterator.hasNext()) {
                        COREMapping<?> nestedMapping = (COREMapping<?>) iterator.next();
                        
                        result.remove(nestedMapping.getFrom());
                    }
                }
            }
        }
    }

    /**
     * Generate a unique name based on the given name and the name of already existing elements.
     *
     * @param baseName - The default name to give.
     * @param elements - The elements to compare the name to.
     * @return The newly created name. Of form baseName{int}
     *
     * @param <T> The type of the elements to compare the name to.
     */
    public static <T extends CORENamedElement> String createUniqueNameFromElements(String baseName,
            Collection<T> elements) {
        Set<String> names = new HashSet<String>();

        for (CORENamedElement namedElement : elements) {
            names.add(namedElement.getName());
        }

        return StringUtil.createUniqueName(baseName, names);
    }

//    /**
//     * Function called to collect all models realized by the selected Features in the diagram.
//     *
//     * @param selectedFeatures The set of selected features
//     * @param modelType The type of COREArtefact
//     * @param <T> The type of COREArtefact
//     * @return SetofModels
//     */
//    public static <T extends COREArtefact> Set<T> getRealizationModels(Set<COREFeature> selectedFeatures,
//            EClassifier modelType) {
//        // TODO: Check where this operation is still used now that we have getRealizationScenes
//        Set<T> result = new HashSet<T>();
//        for (COREFeature feature : selectedFeatures) {
//            Collection<T> realizingModels = EMFModelUtil.collectElementsOfType(feature,
//                    CorePackage.Literals.CORE_FEATURE__REALIZED_BY, modelType);
//            loop: for (T model : realizingModels) {
//                for (COREFeature realizedFeature : model.getRealizes()) {
//                    if (!selectedFeatures.contains(realizedFeature)) {
//                        continue loop;
//                    }
//                }
//                result.add(model);
//            }
//        }
//        return resolveConflicts(result);
//    }

    /**
     * Function that collects all COREScenes realized by the selected features, taking into account
     * conflict resolution scenes, if any.
     *
     * @param selectedFeatures - the set of selected features
     * @return Set of COREScenes
     */
    public static Set<COREScene> getRealizationScenes(Set<COREFeature> selectedFeatures) {
        // create an empty set
        Set<COREScene> result = new HashSet<COREScene>();
        for (COREFeature feature : selectedFeatures) {
            for (COREScene s : feature.getRealizedBy()) {
                // Add all scenes that are that are linked to the selected features and not to any unselected features
                if (selectedFeatures.containsAll(s.getRealizes())) {            
                    result.add(s);
                }
            }
        }
        // now take into account conflict resolution scenes
        return filterConflictResolutionScenes(result);
    }

    /**
     * Resolves conflicts when there are scenes that realize more than
     * one feature. The algorithm loops through the set of scenes and finds for
     * each scene if its set of realizing features includes the set of
     * realizing features for another model. If this is the case, it
     * removes the other model.
     *
     * @param inputScenes - the selected scenes of the concern.
     * @return set of scenes after resolving conflicts.
     */
    private static Set<COREScene> filterConflictResolutionScenes(Set<COREScene> inputScenes) {

        ArrayList<COREScene> resolvedScenes = new ArrayList<COREScene>();
        Set<COREScene> resultingScenes = new HashSet<COREScene>();
        Set<COREFeature> handledFeatures = new HashSet<COREFeature>();        
        resolvedScenes.addAll(inputScenes);
        
        // sort the realization scenes in decreasing order of number of features they realize
        Collections.sort(resolvedScenes, new Comparator<COREScene>() {
            public int compare(COREScene s1, COREScene s2) {
                int result = 0;
                if (s1.getRealizes().size() > s2.getRealizes().size()) {
                    result = -1;
                } else if (s1.getRealizes().size() < s2.getRealizes().size()) {
                    result = 1;
                }
                return result;
            }
            });
        // start with the highest realization numbers and add all scenes that realize that many features
        // continue with successively lower realization numbers until all features are covered
        
        int currentIndex = 0;
        while (currentIndex < resolvedScenes.size()) {
            if (!(handledFeatures.containsAll(resolvedScenes.get(currentIndex).getRealizes()))) {
                resultingScenes.add(resolvedScenes.get(currentIndex));
                handledFeatures.addAll(resolvedScenes.get(currentIndex).getRealizes());
                resolvedScenes.remove(currentIndex);
            } else {
                currentIndex++;
            }
        }
            
        return resultingScenes;
    }
    
    /**
     * Unloads all external resources that the given model references.
     *
     * @param model the model that has references to external resources
     */
    public static void unloadExternalResources(COREArtefact model) {
        Set<COREModelComposition> modelCompositions = new HashSet<COREModelComposition>();
        modelCompositions.addAll(collectAllModelReuses(model));
        modelCompositions.addAll(collectModelExtensions(model));

        for (COREModelComposition modelComposition : modelCompositions) {
            COREArtefact artefact = modelComposition.getSource();
            if (artefact instanceof COREExternalArtefact) {
                unloadEObject(((COREExternalArtefact) artefact).getRootModelElement());
            }
        }
    }

    /**
     * Dispose the adapter factory for this EObject and unload his resource.
     *
     * @param objectToUnload The EObject to unload.
     */
    public static void unloadEObject(EObject objectToUnload) {
        if (objectToUnload != null) {
            AdapterFactoryRegistry.INSTANCE.disposeAdapterFactoryFor(objectToUnload);
            ResourceManager.unloadResource(objectToUnload.eResource());
        }
    }
    
    /**
     * Recursively gather all COREExternalArtefacts that the given {@link COREExternalArtefact} extends and reused.
     * 
     * @param currentArtefact - The given artefact
     * @return a collection of all the {@link COREExternalArtefact} gathered
     */
    public static Collection<COREExternalArtefact> getExtendedAndReusedArtefacts(COREExternalArtefact currentArtefact) {
        Set<COREExternalArtefact> result = new HashSet<>();
        result.addAll(getExtendedArtefacts(currentArtefact));
        result.addAll(getReusedArtefacts(currentArtefact));
        return result;
    }

    /**
     * Recursively gather all COREExternalArtefacts that the given {@link COREExternalArtefact} extends.
     * 
     * @param currentArtefact - The given artefact
     * @return a collection of all the {@link COREExternalArtefact} gathered
     */
    public static Collection<COREExternalArtefact> getExtendedArtefacts(
            COREExternalArtefact currentArtefact) {
        Collection<COREExternalArtefact> extendedArtefacts = COREModelUtil.rGetExtendedArtefacts(currentArtefact);
        extendedArtefacts.remove(currentArtefact);
        return extendedArtefacts;
    }
    
    /**
     * Recursively gather all COREExternalArtefacts that the given {@link COREExternalArtefact} extends.
     * Return also the artefact given as parameter
     * 
     * @param currentArtefact - The given artefact
     * @return a collection of all the {@link COREExternalArtefact} gathered
     */
    private static Collection<COREExternalArtefact> rGetExtendedArtefacts(
            COREExternalArtefact currentArtefact) {

        Collection<COREExternalArtefact> externalArtefactsResult = new ArrayList<>();

        // Add the actual CEA
        externalArtefactsResult.add(currentArtefact);

        // Add all the CEA in the COREModelExtension associate with the CEA
        if (currentArtefact.getModelExtensions().size() > 0) {
            EList<COREModelExtension> modelExtensions = currentArtefact.getModelExtensions();
            for (COREModelExtension modelExtension : modelExtensions) {
                COREExternalArtefact artefact = (COREExternalArtefact) modelExtension.getSource();
                externalArtefactsResult.addAll(rGetExtendedArtefacts(artefact));
            }
        }

        return externalArtefactsResult;
    }

    /**
     * Recursively gather all COREExternalArtefacts that the given {@link COREExternalArtefact} reuses.
     * 
     * @param currentArtefact - The given artefact
     * @return a collection of all the {@link COREExternalArtefact} gathered
     */
    public static Collection<COREExternalArtefact> getReusedArtefacts(COREExternalArtefact currentArtefact) {
        Collection<COREExternalArtefact> reusedArtefacts = COREModelUtil.rGetReusedArtefacts(currentArtefact);
        reusedArtefacts.remove(currentArtefact);
        return reusedArtefacts;
    }
    
    /**
     * Recursively gather all COREExternalArtefacts that the given {@link COREExternalArtefact} reuses.
     * Return also the artefact given as parameter
     * 
     * @param currentArtefact - The given artefact
     * @return a collection of all the {@link COREExternalArtefact} gathered
     */
    private static Collection<COREExternalArtefact> rGetReusedArtefacts(COREExternalArtefact currentArtefact) {

        Collection<COREExternalArtefact> externalArtefactsResult = new ArrayList<>();

        // Add the current artefact
        externalArtefactsResult.add(currentArtefact);

        // Add all the artefacts associated in COREModelReuses 
        if (currentArtefact.getModelReuses().size() > 0) {
            EList<COREModelReuse> modelReuses = currentArtefact.getModelReuses();
            for (COREModelReuse modelReuse : modelReuses) {
                COREExternalArtefact artefact = (COREExternalArtefact) modelReuse.getSource();
                externalArtefactsResult.addAll(rGetReusedArtefacts(artefact));
            }
        }
        return externalArtefactsResult;
    }
    
    
}
