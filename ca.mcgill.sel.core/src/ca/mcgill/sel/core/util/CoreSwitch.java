/**
 */
package ca.mcgill.sel.core.util;

import ca.mcgill.sel.core.*;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.core.CorePackage
 * @generated
 */
public class CoreSwitch<T1> extends Switch<T1> {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static CorePackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CoreSwitch() {
        if (modelPackage == null) {
            modelPackage = CorePackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T1 doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case CorePackage.CORE_ARTEFACT: {
                COREArtefact coreArtefact = (COREArtefact)theEObject;
                T1 result = caseCOREArtefact(coreArtefact);
                if (result == null) result = caseCORENamedElement(coreArtefact);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_IMPACT_MODEL: {
                COREImpactModel coreImpactModel = (COREImpactModel)theEObject;
                T1 result = caseCOREImpactModel(coreImpactModel);
                if (result == null) result = caseCOREArtefact(coreImpactModel);
                if (result == null) result = caseCORENamedElement(coreImpactModel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_CONCERN: {
                COREConcern coreConcern = (COREConcern)theEObject;
                T1 result = caseCOREConcern(coreConcern);
                if (result == null) result = caseCORENamedElement(coreConcern);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_FEATURE: {
                COREFeature coreFeature = (COREFeature)theEObject;
                T1 result = caseCOREFeature(coreFeature);
                if (result == null) result = caseCORENamedElement(coreFeature);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_MODEL_COMPOSITION: {
                COREModelComposition coreModelComposition = (COREModelComposition)theEObject;
                T1 result = caseCOREModelComposition(coreModelComposition);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_LINK: {
                CORELink<?> coreLink = (CORELink<?>)theEObject;
                T1 result = caseCORELink(coreLink);
                if (result == null) result = caseCOREModelElementComposition(coreLink);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_NAMED_ELEMENT: {
                CORENamedElement coreNamedElement = (CORENamedElement)theEObject;
                T1 result = caseCORENamedElement(coreNamedElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_REUSE: {
                COREReuse coreReuse = (COREReuse)theEObject;
                T1 result = caseCOREReuse(coreReuse);
                if (result == null) result = caseCORENamedElement(coreReuse);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_IMPACT_NODE: {
                COREImpactNode coreImpactNode = (COREImpactNode)theEObject;
                T1 result = caseCOREImpactNode(coreImpactNode);
                if (result == null) result = caseCORENamedElement(coreImpactNode);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_CONFIGURATION: {
                COREConfiguration coreConfiguration = (COREConfiguration)theEObject;
                T1 result = caseCOREConfiguration(coreConfiguration);
                if (result == null) result = caseCOREModelComposition(coreConfiguration);
                if (result == null) result = caseCORENamedElement(coreConfiguration);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_FEATURE_MODEL: {
                COREFeatureModel coreFeatureModel = (COREFeatureModel)theEObject;
                T1 result = caseCOREFeatureModel(coreFeatureModel);
                if (result == null) result = caseCOREArtefact(coreFeatureModel);
                if (result == null) result = caseCORENamedElement(coreFeatureModel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_MODEL_REUSE: {
                COREModelReuse coreModelReuse = (COREModelReuse)theEObject;
                T1 result = caseCOREModelReuse(coreModelReuse);
                if (result == null) result = caseCOREModelComposition(coreModelReuse);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_CONTRIBUTION: {
                COREContribution coreContribution = (COREContribution)theEObject;
                T1 result = caseCOREContribution(coreContribution);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.LAYOUT_MAP: {
                @SuppressWarnings("unchecked") Map.Entry<EObject, LayoutElement> layoutMap = (Map.Entry<EObject, LayoutElement>)theEObject;
                T1 result = caseLayoutMap(layoutMap);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.LAYOUT_ELEMENT: {
                LayoutElement layoutElement = (LayoutElement)theEObject;
                T1 result = caseLayoutElement(layoutElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.LAYOUT_CONTAINER_MAP: {
                @SuppressWarnings("unchecked") Map.Entry<EObject, EMap<EObject, LayoutElement>> layoutContainerMap = (Map.Entry<EObject, EMap<EObject, LayoutElement>>)theEObject;
                T1 result = caseLayoutContainerMap(layoutContainerMap);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_FEATURE_IMPACT_NODE: {
                COREFeatureImpactNode coreFeatureImpactNode = (COREFeatureImpactNode)theEObject;
                T1 result = caseCOREFeatureImpactNode(coreFeatureImpactNode);
                if (result == null) result = caseCOREImpactNode(coreFeatureImpactNode);
                if (result == null) result = caseCORENamedElement(coreFeatureImpactNode);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_MODEL_ELEMENT_COMPOSITION: {
                COREModelElementComposition<?> coreModelElementComposition = (COREModelElementComposition<?>)theEObject;
                T1 result = caseCOREModelElementComposition(coreModelElementComposition);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_WEIGHTED_LINK: {
                COREWeightedLink coreWeightedLink = (COREWeightedLink)theEObject;
                T1 result = caseCOREWeightedLink(coreWeightedLink);
                if (result == null) result = caseCORELink(coreWeightedLink);
                if (result == null) result = caseCOREModelElementComposition(coreWeightedLink);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_MODEL_EXTENSION: {
                COREModelExtension coreModelExtension = (COREModelExtension)theEObject;
                T1 result = caseCOREModelExtension(coreModelExtension);
                if (result == null) result = caseCOREModelComposition(coreModelExtension);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_MAPPING: {
                COREMapping<?> coreMapping = (COREMapping<?>)theEObject;
                T1 result = caseCOREMapping(coreMapping);
                if (result == null) result = caseCORELink(coreMapping);
                if (result == null) result = caseCOREModelElementComposition(coreMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_SCENE: {
                COREScene coreScene = (COREScene)theEObject;
                T1 result = caseCOREScene(coreScene);
                if (result == null) result = caseCORENamedElement(coreScene);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_PERSPECTIVE: {
                COREPerspective corePerspective = (COREPerspective)theEObject;
                T1 result = caseCOREPerspective(corePerspective);
                if (result == null) result = caseCORELanguage(corePerspective);
                if (result == null) result = caseCOREArtefact(corePerspective);
                if (result == null) result = caseCORENamedElement(corePerspective);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_EXTERNAL_LANGUAGE: {
                COREExternalLanguage coreExternalLanguage = (COREExternalLanguage)theEObject;
                T1 result = caseCOREExternalLanguage(coreExternalLanguage);
                if (result == null) result = caseCORELanguage(coreExternalLanguage);
                if (result == null) result = caseCOREArtefact(coreExternalLanguage);
                if (result == null) result = caseCORENamedElement(coreExternalLanguage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_PERSPECTIVE_ACTION: {
                COREPerspectiveAction corePerspectiveAction = (COREPerspectiveAction)theEObject;
                T1 result = caseCOREPerspectiveAction(corePerspectiveAction);
                if (result == null) result = caseCOREAction(corePerspectiveAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_EXTERNAL_ARTEFACT: {
                COREExternalArtefact coreExternalArtefact = (COREExternalArtefact)theEObject;
                T1 result = caseCOREExternalArtefact(coreExternalArtefact);
                if (result == null) result = caseCOREArtefact(coreExternalArtefact);
                if (result == null) result = caseCORENamedElement(coreExternalArtefact);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.COREUI_ELEMENT: {
                COREUIElement coreuiElement = (COREUIElement)theEObject;
                T1 result = caseCOREUIElement(coreuiElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORECI_ELEMENT: {
                CORECIElement coreciElement = (CORECIElement)theEObject;
                T1 result = caseCORECIElement(coreciElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_MAPPING_CARDINALITY: {
                COREMappingCardinality coreMappingCardinality = (COREMappingCardinality)theEObject;
                T1 result = caseCOREMappingCardinality(coreMappingCardinality);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING: {
                CORELanguageElementMapping coreLanguageElementMapping = (CORELanguageElementMapping)theEObject;
                T1 result = caseCORELanguageElementMapping(coreLanguageElementMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_REEXPOSE_ACTION: {
                COREReexposeAction coreReexposeAction = (COREReexposeAction)theEObject;
                T1 result = caseCOREReexposeAction(coreReexposeAction);
                if (result == null) result = caseCOREPerspectiveAction(coreReexposeAction);
                if (result == null) result = caseCOREAction(coreReexposeAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_REDEFINE_ACTION: {
                CORERedefineAction coreRedefineAction = (CORERedefineAction)theEObject;
                T1 result = caseCORERedefineAction(coreRedefineAction);
                if (result == null) result = caseCOREPerspectiveAction(coreRedefineAction);
                if (result == null) result = caseCOREAction(coreRedefineAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_LANGUAGE_ACTION: {
                CORELanguageAction coreLanguageAction = (CORELanguageAction)theEObject;
                T1 result = caseCORELanguageAction(coreLanguageAction);
                if (result == null) result = caseCOREAction(coreLanguageAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_CREATE_MAPPING: {
                CORECreateMapping coreCreateMapping = (CORECreateMapping)theEObject;
                T1 result = caseCORECreateMapping(coreCreateMapping);
                if (result == null) result = caseCOREPerspectiveAction(coreCreateMapping);
                if (result == null) result = caseCOREAction(coreCreateMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_LANGUAGE: {
                CORELanguage coreLanguage = (CORELanguage)theEObject;
                T1 result = caseCORELanguage(coreLanguage);
                if (result == null) result = caseCOREArtefact(coreLanguage);
                if (result == null) result = caseCORENamedElement(coreLanguage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_ACTION: {
                COREAction coreAction = (COREAction)theEObject;
                T1 result = caseCOREAction(coreAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CREATE_MODEL_ELEMENT_MAPPING: {
                CreateModelElementMapping createModelElementMapping = (CreateModelElementMapping)theEObject;
                T1 result = caseCreateModelElementMapping(createModelElementMapping);
                if (result == null) result = caseCOREPerspectiveAction(createModelElementMapping);
                if (result == null) result = caseCOREAction(createModelElementMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING: {
                COREModelElementMapping coreModelElementMapping = (COREModelElementMapping)theEObject;
                T1 result = caseCOREModelElementMapping(coreModelElementMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.MAPPING_END: {
                MappingEnd mappingEnd = (MappingEnd)theEObject;
                T1 result = caseMappingEnd(mappingEnd);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.LANGUAGE_MAP: {
                @SuppressWarnings("unchecked") Map.Entry<String, CORELanguage> languageMap = (Map.Entry<String, CORELanguage>)theEObject;
                T1 result = caseLanguageMap(languageMap);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.ARTEFACT_MAP: {
                @SuppressWarnings("unchecked") Map.Entry<String, EList<COREArtefact>> artefactMap = (Map.Entry<String, EList<COREArtefact>>)theEObject;
                T1 result = caseArtefactMap(artefactMap);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.PARAMETER: {
                Parameter parameter = (Parameter)theEObject;
                T1 result = caseParameter(parameter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_LANGUAGE_ELEMENT: {
                CORELanguageElement coreLanguageElement = (CORELanguageElement)theEObject;
                T1 result = caseCORELanguageElement(coreLanguageElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.EXISTING_ELEMENT_EFFECT: {
                ExistingElementEffect existingElementEffect = (ExistingElementEffect)theEObject;
                T1 result = caseExistingElementEffect(existingElementEffect);
                if (result == null) result = caseActionEffect(existingElementEffect);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.DELETE_EFFECT: {
                DeleteEffect deleteEffect = (DeleteEffect)theEObject;
                T1 result = caseDeleteEffect(deleteEffect);
                if (result == null) result = caseExistingElementEffect(deleteEffect);
                if (result == null) result = caseActionEffect(deleteEffect);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.UPDATE_EFFECT: {
                UpdateEffect updateEffect = (UpdateEffect)theEObject;
                T1 result = caseUpdateEffect(updateEffect);
                if (result == null) result = caseExistingElementEffect(updateEffect);
                if (result == null) result = caseActionEffect(updateEffect);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.NAVIGATION_MAPPING: {
                NavigationMapping navigationMapping = (NavigationMapping)theEObject;
                T1 result = caseNavigationMapping(navigationMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.INTER_LANGUAGE_MAPPING: {
                InterLanguageMapping interLanguageMapping = (InterLanguageMapping)theEObject;
                T1 result = caseInterLanguageMapping(interLanguageMapping);
                if (result == null) result = caseNavigationMapping(interLanguageMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.INTRA_LANGUAGE_MAPPING: {
                IntraLanguageMapping intraLanguageMapping = (IntraLanguageMapping)theEObject;
                T1 result = caseIntraLanguageMapping(intraLanguageMapping);
                if (result == null) result = caseNavigationMapping(intraLanguageMapping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.INTER_LANGUAGE_MAPPING_END: {
                InterLanguageMappingEnd interLanguageMappingEnd = (InterLanguageMappingEnd)theEObject;
                T1 result = caseInterLanguageMappingEnd(interLanguageMappingEnd);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CREATE_EFFECT: {
                CreateEffect createEffect = (CreateEffect)theEObject;
                T1 result = caseCreateEffect(createEffect);
                if (result == null) result = caseActionEffect(createEffect);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.ACTION_EFFECT: {
                ActionEffect actionEffect = (ActionEffect)theEObject;
                T1 result = caseActionEffect(actionEffect);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case CorePackage.CORE_REUSE_ARTEFACT: {
                COREReuseArtefact coreReuseArtefact = (COREReuseArtefact)theEObject;
                T1 result = caseCOREReuseArtefact(coreReuseArtefact);
                if (result == null) result = caseCOREArtefact(coreReuseArtefact);
                if (result == null) result = caseCORENamedElement(coreReuseArtefact);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Artefact</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Artefact</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREArtefact(COREArtefact object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Impact Model</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Impact Model</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREImpactModel(COREImpactModel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Concern</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Concern</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREConcern(COREConcern object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Feature</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Feature</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREFeature(COREFeature object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Model Composition</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Model Composition</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREModelComposition(COREModelComposition object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Link</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Link</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T> T1 caseCORELink(CORELink<T> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T> T1 caseCOREMapping(COREMapping<T> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Scene</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Scene</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREScene(COREScene object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Perspective</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Perspective</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREPerspective(COREPerspective object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE External Language</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE External Language</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREExternalLanguage(COREExternalLanguage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREAction(COREAction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Create Model Element Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Create Model Element Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCreateModelElementMapping(CreateModelElementMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Model Element Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Model Element Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREModelElementMapping(COREModelElementMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Mapping End</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Mapping End</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseMappingEnd(MappingEnd object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Language Map</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Language Map</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseLanguageMap(Map.Entry<String, CORELanguage> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Artefact Map</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Artefact Map</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseArtefactMap(Map.Entry<String, EList<COREArtefact>> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseParameter(Parameter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Language Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Language Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORELanguageElement(CORELanguageElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Existing Element Effect</em>'.
     * <!-- begin-user-doc -->
=======
	 * Returns the result of interpreting the object as an instance of '<em>Existing Element Effect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Existing Element Effect</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseExistingElementEffect(ExistingElementEffect object) {
        return null;
    }

				/**
     * Returns the result of interpreting the object as an instance of '<em>Delete Effect</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Delete Effect</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseDeleteEffect(DeleteEffect object) {
        return null;
    }

				/**
     * Returns the result of interpreting the object as an instance of '<em>Action Effect</em>'.
     * <!-- begin-user-doc -->
>>>>>>> refs/heads/perspective-master
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Action Effect</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseActionEffect(ActionEffect object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Reuse Artefact</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Reuse Artefact</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREReuseArtefact(COREReuseArtefact object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Update Effect</em>'.
     * <!-- begin-user-doc -->
=======
	 * Returns the result of interpreting the object as an instance of '<em>Update Effect</em>'.
	 * <!-- begin-user-doc -->
>>>>>>> refs/heads/perspective-master
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Update Effect</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseUpdateEffect(UpdateEffect object) {
        return null;
    }

				/**
     * Returns the result of interpreting the object as an instance of '<em>Create Effect</em>'.
     * <!-- begin-user-doc -->
=======
	 * Returns the result of interpreting the object as an instance of '<em>Create Effect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Create Effect</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseCreateEffect(CreateEffect object) {
        return null;
    }

				/**
     * Returns the result of interpreting the object as an instance of '<em>Navigation Mapping</em>'.
     * <!-- begin-user-doc -->
>>>>>>> refs/heads/perspective-master
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Navigation Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseNavigationMapping(NavigationMapping object) {
        return null;
    }

                /**
     * Returns the result of interpreting the object as an instance of '<em>Inter Language Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Inter Language Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseInterLanguageMapping(InterLanguageMapping object) {
        return null;
    }

                /**
     * Returns the result of interpreting the object as an instance of '<em>Intra Language Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Intra Language Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseIntraLanguageMapping(IntraLanguageMapping object) {
        return null;
    }

                /**
     * Returns the result of interpreting the object as an instance of '<em>Inter Language Mapping End</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Inter Language Mapping End</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseInterLanguageMappingEnd(InterLanguageMappingEnd object) {
        return null;
    }

                /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Perspective Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Perspective Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREPerspectiveAction(COREPerspectiveAction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE External Artefact</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE External Artefact</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREExternalArtefact(COREExternalArtefact object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>COREUI Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>COREUI Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREUIElement(COREUIElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORECI Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORECI Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORECIElement(CORECIElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Mapping Cardinality</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Mapping Cardinality</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREMappingCardinality(COREMappingCardinality object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Language Element Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Language Element Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORELanguageElementMapping(CORELanguageElementMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Reexpose Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Reexpose Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREReexposeAction(COREReexposeAction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Redefine Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Redefine Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORERedefineAction(CORERedefineAction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Language Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Language Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORELanguageAction(CORELanguageAction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Create Mapping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Create Mapping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORECreateMapping(CORECreateMapping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Language</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Language</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORELanguage(CORELanguage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Named Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Named Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCORENamedElement(CORENamedElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Reuse</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Reuse</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREReuse(COREReuse object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Impact Node</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Impact Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREImpactNode(COREImpactNode object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Configuration</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Configuration</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREConfiguration(COREConfiguration object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Feature Model</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Feature Model</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREFeatureModel(COREFeatureModel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Model Reuse</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Model Reuse</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREModelReuse(COREModelReuse object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Contribution</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Contribution</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREContribution(COREContribution object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Layout Map</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Layout Map</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseLayoutMap(Map.Entry<EObject, LayoutElement> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Layout Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Layout Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseLayoutElement(LayoutElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Layout Container Map</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Layout Container Map</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseLayoutContainerMap(Map.Entry<EObject, EMap<EObject, LayoutElement>> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Feature Impact Node</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Feature Impact Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREFeatureImpactNode(COREFeatureImpactNode object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Model Element Composition</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Model Element Composition</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T> T1 caseCOREModelElementComposition(COREModelElementComposition<T> object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Weighted Link</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Weighted Link</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREWeightedLink(COREWeightedLink object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CORE Model Extension</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CORE Model Extension</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseCOREModelExtension(COREModelExtension object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T1 defaultCase(EObject object) {
        return null;
    }

} //CoreSwitch
