/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Intra Language Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.IntraLanguageMapping#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.IntraLanguageMapping#isClosure <em>Closure</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.IntraLanguageMapping#isReuse <em>Reuse</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.IntraLanguageMapping#isIncreaseDepth <em>Increase Depth</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.IntraLanguageMapping#isChangeModel <em>Change Model</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.IntraLanguageMapping#getFrom <em>From</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.IntraLanguageMapping#getHops <em>Hops</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping()
 * @model
 * @generated
 */
public interface IntraLanguageMapping extends NavigationMapping {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping_Name()
     * @model dataType="org.eclipse.emf.ecore.xml.type.String"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.IntraLanguageMapping#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Closure</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Closure</em>' attribute.
     * @see #setClosure(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping_Closure()
     * @model
     * @generated
     */
	boolean isClosure();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.IntraLanguageMapping#isClosure <em>Closure</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Closure</em>' attribute.
     * @see #isClosure()
     * @generated
     */
	void setClosure(boolean value);

	/**
     * Returns the value of the '<em><b>Reuse</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reuse</em>' attribute.
     * @see #setReuse(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping_Reuse()
     * @model
     * @generated
     */
	boolean isReuse();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.IntraLanguageMapping#isReuse <em>Reuse</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Reuse</em>' attribute.
     * @see #isReuse()
     * @generated
     */
	void setReuse(boolean value);

	/**
     * Returns the value of the '<em><b>Increase Depth</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Increase Depth</em>' attribute.
     * @see #setIncreaseDepth(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping_IncreaseDepth()
     * @model
     * @generated
     */
	boolean isIncreaseDepth();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.IntraLanguageMapping#isIncreaseDepth <em>Increase Depth</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Increase Depth</em>' attribute.
     * @see #isIncreaseDepth()
     * @generated
     */
	void setIncreaseDepth(boolean value);

	/**
     * Returns the value of the '<em><b>Change Model</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Change Model</em>' attribute.
     * @see #setChangeModel(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping_ChangeModel()
     * @model
     * @generated
     */
	boolean isChangeModel();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.IntraLanguageMapping#isChangeModel <em>Change Model</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Change Model</em>' attribute.
     * @see #isChangeModel()
     * @generated
     */
	void setChangeModel(boolean value);

	/**
     * Returns the value of the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>From</em>' reference.
     * @see #setFrom(EClass)
     * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping_From()
     * @model required="true"
     * @generated
     */
	EClass getFrom();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.IntraLanguageMapping#getFrom <em>From</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>From</em>' reference.
     * @see #getFrom()
     * @generated
     */
	void setFrom(EClass value);

	/**
     * Returns the value of the '<em><b>Hops</b></em>' reference list.
     * The list contents are of type {@link org.eclipse.emf.ecore.EReference}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Hops</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getIntraLanguageMapping_Hops()
     * @model required="true"
     * @generated
     */
	EList<EReference> getHops();

} // IntraLanguageMapping
