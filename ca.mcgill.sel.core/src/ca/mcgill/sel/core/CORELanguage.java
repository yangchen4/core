/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Language</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORELanguage#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELanguage()
 * @model abstract="true"
 * @generated
 */
public interface CORELanguage extends COREArtefact {
	/**
     * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREAction}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actions</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguage_Actions()
     * @model containment="true"
     * @generated
     */
	EList<COREAction> getActions();

} // CORELanguage
