/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORELink#getTo <em>To</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELink#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELink()
 * @model abstract="true"
 * @generated
 */
public interface CORELink<T> extends COREModelElementComposition<T> {
	/**
     * Returns the value of the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>To</em>' reference.
     * @see #setTo(Object)
     * @see ca.mcgill.sel.core.CorePackage#getCORELink_To()
     * @model kind="reference" required="true"
     * @generated
     */
	T getTo();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELink#getTo <em>To</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>To</em>' reference.
     * @see #getTo()
     * @generated
     */
	void setTo(T value);

	/**
     * Returns the value of the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>From</em>' reference.
     * @see #setFrom(Object)
     * @see ca.mcgill.sel.core.CorePackage#getCORELink_From()
     * @model kind="reference" required="true"
     * @generated
     */
	T getFrom();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELink#getFrom <em>From</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>From</em>' reference.
     * @see #getFrom()
     * @generated
     */
	void setFrom(T value);

} // CORELink
