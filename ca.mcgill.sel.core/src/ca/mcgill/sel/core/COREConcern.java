/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Concern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getArtefacts <em>Artefacts</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getImpactModel <em>Impact Model</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getScenes <em>Scenes</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getReuses <em>Reuses</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREConcern#getTemporaryArtefacts <em>Temporary Artefacts</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREConcern()
 * @model
 * @generated
 */
public interface COREConcern extends CORENamedElement {
	/**
     * Returns the value of the '<em><b>Artefacts</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREArtefact}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREArtefact#getCoreConcern <em>Core Concern</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Artefacts</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREConcern_Artefacts()
     * @see ca.mcgill.sel.core.COREArtefact#getCoreConcern
     * @model opposite="coreConcern" containment="true" required="true"
     * @generated
     */
	EList<COREArtefact> getArtefacts();

	/**
     * Returns the value of the '<em><b>Feature Model</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Feature Model</em>' reference.
     * @see #setFeatureModel(COREFeatureModel)
     * @see ca.mcgill.sel.core.CorePackage#getCOREConcern_FeatureModel()
     * @model required="true" derived="true"
     *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='if self.artefacts-&gt;exists(a | a.oclIsTypeOf(COREFeatureModel)) then\n                self.artefacts-&gt;any(a | a.oclIsTypeOf(COREFeatureModel)).oclAsType(COREFeatureModel)\n            else\n                null\n            endif'"
     * @generated
     */
	COREFeatureModel getFeatureModel();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREConcern#getFeatureModel <em>Feature Model</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Feature Model</em>' reference.
     * @see #getFeatureModel()
     * @generated
     */
	void setFeatureModel(COREFeatureModel value);

	/**
     * Returns the value of the '<em><b>Impact Model</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Impact Model</em>' reference.
     * @see #setImpactModel(COREImpactModel)
     * @see ca.mcgill.sel.core.CorePackage#getCOREConcern_ImpactModel()
     * @model derived="true"
     *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='\n            if self.artefacts-&gt;exists(a | a.oclIsTypeOf(COREImpactModel)) then\n                self.artefacts-&gt;any(a | a.oclIsTypeOf(COREImpactModel)).oclAsType(COREImpactModel)\n            else\n                null\n            endif'"
     * @generated
     */
	COREImpactModel getImpactModel();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREConcern#getImpactModel <em>Impact Model</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Impact Model</em>' reference.
     * @see #getImpactModel()
     * @generated
     */
	void setImpactModel(COREImpactModel value);

	/**
     * Returns the value of the '<em><b>Scenes</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREScene}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Scenes</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREConcern_Scenes()
     * @model containment="true"
     * @generated
     */
	EList<COREScene> getScenes();

	/**
     * Returns the value of the '<em><b>Reuses</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREReuse}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reuses</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREConcern_Reuses()
     * @model containment="true"
     * @generated
     */
	EList<COREReuse> getReuses();

	/**
     * Returns the value of the '<em><b>Temporary Artefacts</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREArtefact}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREArtefact#getTemporaryConcern <em>Temporary Concern</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Temporary Artefacts</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREConcern_TemporaryArtefacts()
     * @see ca.mcgill.sel.core.COREArtefact#getTemporaryConcern
     * @model opposite="temporaryConcern" transient="true"
     * @generated
     */
	EList<COREArtefact> getTemporaryArtefacts();

} // COREConcern
