/**
 */
package ca.mcgill.sel.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>CORE Relationship</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.core.CorePackage#getCORERelationship()
 * @model
 * @generated
 */
public enum CORERelationship implements Enumerator {
	/**
     * The '<em><b>Equality</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #EQUALITY_VALUE
     * @generated
     * @ordered
     */
	EQUALITY(0, "equality", "equality"),

	/**
     * The '<em><b>Refines</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #REFINES_VALUE
     * @generated
     * @ordered
     */
	REFINES(1, "refines", "refines"),

	/**
     * The '<em><b>Uses</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #USES_VALUE
     * @generated
     * @ordered
     */
	USES(2, "uses", "uses"),

	/**
     * The '<em><b>Extends</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #EXTENDS_VALUE
     * @generated
     * @ordered
     */
	EXTENDS(3, "extends", "extends");

	/**
     * The '<em><b>Equality</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #EQUALITY
     * @model name="equality"
     * @generated
     * @ordered
     */
	public static final int EQUALITY_VALUE = 0;

	/**
     * The '<em><b>Refines</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #REFINES
     * @model name="refines"
     * @generated
     * @ordered
     */
	public static final int REFINES_VALUE = 1;

	/**
     * The '<em><b>Uses</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #USES
     * @model name="uses"
     * @generated
     * @ordered
     */
	public static final int USES_VALUE = 2;

	/**
     * The '<em><b>Extends</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #EXTENDS
     * @model name="extends"
     * @generated
     * @ordered
     */
	public static final int EXTENDS_VALUE = 3;

	/**
     * An array of all the '<em><b>CORE Relationship</b></em>' enumerators.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static final CORERelationship[] VALUES_ARRAY =
		new CORERelationship[] {
            EQUALITY,
            REFINES,
            USES,
            EXTENDS,
        };

	/**
     * A public read-only list of all the '<em><b>CORE Relationship</b></em>' enumerators.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static final List<CORERelationship> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
     * Returns the '<em><b>CORE Relationship</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static CORERelationship get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            CORERelationship result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

	/**
     * Returns the '<em><b>CORE Relationship</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static CORERelationship getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            CORERelationship result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

	/**
     * Returns the '<em><b>CORE Relationship</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static CORERelationship get(int value) {
        switch (value) {
            case EQUALITY_VALUE: return EQUALITY;
            case REFINES_VALUE: return REFINES;
            case USES_VALUE: return USES;
            case EXTENDS_VALUE: return EXTENDS;
        }
        return null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final int value;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final String name;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final String literal;

	/**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private CORERelationship(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getValue() {
      return value;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
      return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getLiteral() {
      return literal;
    }

	/**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        return literal;
    }
	
} //CORERelationship
