/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CreateEffect#getCorelanguageElement <em>Corelanguage Element</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCreateEffect()
 * @model
 * @generated
 */
public interface CreateEffect extends ActionEffect {
	/**
     * Returns the value of the '<em><b>Corelanguage Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Corelanguage Element</em>' reference.
     * @see #setCorelanguageElement(CORELanguageElement)
     * @see ca.mcgill.sel.core.CorePackage#getCreateEffect_CorelanguageElement()
     * @model required="true"
     * @generated
     */
	CORELanguageElement getCorelanguageElement();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CreateEffect#getCorelanguageElement <em>Corelanguage Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Corelanguage Element</em>' reference.
     * @see #getCorelanguageElement()
     * @generated
     */
	void setCorelanguageElement(CORELanguageElement value);

} // CreateEffect
