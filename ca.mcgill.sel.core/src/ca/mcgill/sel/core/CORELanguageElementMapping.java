/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Language Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getActions <em>Actions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getMappingEnds <em>Mapping Ends</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getNestedMappings <em>Nested Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#isMatchMaker <em>Match Maker</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping()
 * @model
 * @generated
 */
public interface CORELanguageElementMapping extends EObject {
	/**
     * Returns the value of the '<em><b>Actions</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORECreateMapping}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORECreateMapping#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actions</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_Actions()
     * @see ca.mcgill.sel.core.CORECreateMapping#getType
     * @model opposite="type"
     * @generated
     */
	EList<CORECreateMapping> getActions();

    /**
     * Returns the value of the '<em><b>Mapping Ends</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.MappingEnd}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.MappingEnd#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Mapping Ends</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_MappingEnds()
     * @see ca.mcgill.sel.core.MappingEnd#getType
     * @model opposite="type" containment="true" lower="2"
     * @generated
     */
    EList<MappingEnd> getMappingEnds();

    /**
     * Returns the value of the '<em><b>Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Identifier</em>' attribute.
     * @see #setIdentifier(int)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_Identifier()
     * @model required="true"
     * @generated
     */
    int getIdentifier();
    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getIdentifier <em>Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Identifier</em>' attribute.
     * @see #getIdentifier()
     * @generated
     */
    void setIdentifier(int value);


    /**
     * Returns the value of the '<em><b>Nested Mappings</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageElementMapping}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Nested Mappings</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_NestedMappings()
     * @model containment="true"
     * @generated
     */
	EList<CORELanguageElementMapping> getNestedMappings();

				/**
     * Returns the value of the '<em><b>Match Maker</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Match Maker</em>' attribute.
     * @see #setMatchMaker(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_MatchMaker()
     * @model
     * @generated
     */
	boolean isMatchMaker();

				/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElementMapping#isMatchMaker <em>Match Maker</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Match Maker</em>' attribute.
     * @see #isMatchMaker()
     * @generated
     */
	void setMatchMaker(boolean value);

				/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
     EList<COREModelElementMapping> getModelElementMappings(COREScene scene);

} // CORELanguageElementMapping
