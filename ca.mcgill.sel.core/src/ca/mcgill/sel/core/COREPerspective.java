/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Perspective</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREPerspective#getMappings <em>Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREPerspective#getLanguages <em>Languages</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREPerspective#getDefault <em>Default</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREPerspective#getNavigationMappings <em>Navigation Mappings</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREPerspective()
 * @model
 * @generated
 */
public interface COREPerspective extends CORELanguage {
	/**
     * Returns the value of the '<em><b>Mappings</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageElementMapping}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mappings</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREPerspective_Mappings()
     * @model containment="true"
     * @generated
     */
	EList<CORELanguageElementMapping> getMappings();

	/**
     * Returns the value of the '<em><b>Languages</b></em>' map.
     * The key is of type {@link java.lang.String},
     * and the value is of type {@link ca.mcgill.sel.core.CORELanguage},
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Languages</em>' map.
     * @see ca.mcgill.sel.core.CorePackage#getCOREPerspective_Languages()
     * @model mapType="ca.mcgill.sel.core.LanguageMap&lt;org.eclipse.emf.ecore.EString, ca.mcgill.sel.core.CORELanguage&gt;"
     * @generated
     */
	EMap<String, CORELanguage> getLanguages();

	/**
     * Returns the value of the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Default</em>' attribute.
     * @see #setDefault(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREPerspective_Default()
     * @model
     * @generated
     */
	String getDefault();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREPerspective#getDefault <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Default</em>' attribute.
     * @see #getDefault()
     * @generated
     */
	void setDefault(String value);

	/**
     * Returns the value of the '<em><b>Navigation Mappings</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.NavigationMapping}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Navigation Mappings</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREPerspective_NavigationMappings()
     * @model containment="true"
     * @generated
     */
	EList<NavigationMapping> getNavigationMappings();

} // COREPerspective
