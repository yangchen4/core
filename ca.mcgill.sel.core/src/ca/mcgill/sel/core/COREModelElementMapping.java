/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Model Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREModelElementMapping#getModelElements <em>Model Elements</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREModelElementMapping#getLEMid <em>LE Mid</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementMapping()
 * @model
 * @generated
 */
public interface COREModelElementMapping extends EObject {
	/**
     * Returns the value of the '<em><b>Model Elements</b></em>' reference list.
     * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Elements</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementMapping_ModelElements()
     * @model required="true"
     * @generated
     */
	EList<EObject> getModelElements();

	/**
     * Returns the value of the '<em><b>LE Mid</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>LE Mid</em>' attribute.
     * @see #setLEMid(int)
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementMapping_LEMid()
     * @model required="true"
     * @generated
     */
	int getLEMid();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREModelElementMapping#getLEMid <em>LE Mid</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>LE Mid</em>' attribute.
     * @see #getLEMid()
     * @generated
     */
	void setLEMid(int value);

} // COREModelElementMapping
