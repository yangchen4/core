/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Model Element Composition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementComposition()
 * @model abstract="true"
 * @generated
 */
public interface COREModelElementComposition<T> extends EObject {
} // COREModelElementComposition
