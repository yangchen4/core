/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Mapping Cardinality</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREMappingCardinality#getLowerBound <em>Lower Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREMappingCardinality#getUpperBound <em>Upper Bound</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREMappingCardinality#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREMappingCardinality()
 * @model
 * @generated
 */
public interface COREMappingCardinality extends EObject {
	/**
     * Returns the value of the '<em><b>Lower Bound</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Lower Bound</em>' attribute.
     * @see #setLowerBound(int)
     * @see ca.mcgill.sel.core.CorePackage#getCOREMappingCardinality_LowerBound()
     * @model default="0" required="true"
     * @generated
     */
	int getLowerBound();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREMappingCardinality#getLowerBound <em>Lower Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Lower Bound</em>' attribute.
     * @see #getLowerBound()
     * @generated
     */
	void setLowerBound(int value);

	/**
     * Returns the value of the '<em><b>Upper Bound</b></em>' attribute.
     * The default value is <code>"1"</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Upper Bound</em>' attribute.
     * @see #setUpperBound(int)
     * @see ca.mcgill.sel.core.CorePackage#getCOREMappingCardinality_UpperBound()
     * @model default="1" required="true"
     * @generated
     */
	int getUpperBound();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREMappingCardinality#getUpperBound <em>Upper Bound</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Upper Bound</em>' attribute.
     * @see #getUpperBound()
     * @generated
     */
	void setUpperBound(int value);

	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREMappingCardinality_Name()
     * @model
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREMappingCardinality#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

} // COREMappingCardinality
