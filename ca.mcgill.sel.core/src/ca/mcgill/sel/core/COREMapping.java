/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREMapping#getMappings <em>Mappings</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREMapping#getReferencedMappings <em>Referenced Mappings</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREMapping()
 * @model
 * @generated
 */
public interface COREMapping<T> extends CORELink<T> {
	/**
     * Returns the value of the '<em><b>Mappings</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREMapping}<code>&lt;?&gt;</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mappings</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREMapping_Mappings()
     * @model containment="true"
     * @generated
     */
	EList<COREMapping<?>> getMappings();

	/**
     * Returns the value of the '<em><b>Referenced Mappings</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREMapping}<code>&lt;?&gt;</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Referenced Mappings</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREMapping_ReferencedMappings()
     * @model
     * @generated
     */
	EList<COREMapping<?>> getReferencedMappings();

} // COREMapping
