/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Create Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORECreateMapping#getType <em>Type</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORECreateMapping#getExtendedAction <em>Extended Action</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORECreateMapping#getModelElements <em>Model Elements</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORECreateMapping()
 * @model
 * @generated
 */
public interface CORECreateMapping extends COREPerspectiveAction {
	/**
     * Returns the value of the '<em><b>Type</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getActions <em>Actions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' reference.
     * @see #setType(CORELanguageElementMapping)
     * @see ca.mcgill.sel.core.CorePackage#getCORECreateMapping_Type()
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#getActions
     * @model opposite="actions" required="true"
     * @generated
     */
	CORELanguageElementMapping getType();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORECreateMapping#getType <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' reference.
     * @see #getType()
     * @generated
     */
	void setType(CORELanguageElementMapping value);

	/**
     * Returns the value of the '<em><b>Extended Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Extended Action</em>' reference.
     * @see #setExtendedAction(CORECreateMapping)
     * @see ca.mcgill.sel.core.CorePackage#getCORECreateMapping_ExtendedAction()
     * @model
     * @generated
     */
	CORECreateMapping getExtendedAction();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORECreateMapping#getExtendedAction <em>Extended Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Extended Action</em>' reference.
     * @see #getExtendedAction()
     * @generated
     */
	void setExtendedAction(CORECreateMapping value);

	/**
     * Returns the value of the '<em><b>Model Elements</b></em>' reference list.
     * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Elements</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORECreateMapping_ModelElements()
     * @model lower="2"
     * @generated
     */
	EList<EObject> getModelElements();

} // CORECreateMapping
