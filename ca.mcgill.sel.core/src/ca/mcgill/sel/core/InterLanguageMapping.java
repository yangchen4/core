/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inter Language Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMapping#isDefault <em>Default</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMapping#getCoreLanguageElementMapping <em>Core Language Element Mapping</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMapping#getInterLanguageMappingEnds <em>Inter Language Mapping Ends</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMapping()
 * @model
 * @generated
 */
public interface InterLanguageMapping extends NavigationMapping {
	/**
     * Returns the value of the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Default</em>' attribute.
     * @see #setDefault(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMapping_Default()
     * @model
     * @generated
     */
	boolean isDefault();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.InterLanguageMapping#isDefault <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Default</em>' attribute.
     * @see #isDefault()
     * @generated
     */
	void setDefault(boolean value);

	/**
     * Returns the value of the '<em><b>Core Language Element Mapping</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Core Language Element Mapping</em>' reference.
     * @see #setCoreLanguageElementMapping(CORELanguageElementMapping)
     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMapping_CoreLanguageElementMapping()
     * @model required="true"
     * @generated
     */
	CORELanguageElementMapping getCoreLanguageElementMapping();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.InterLanguageMapping#getCoreLanguageElementMapping <em>Core Language Element Mapping</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Core Language Element Mapping</em>' reference.
     * @see #getCoreLanguageElementMapping()
     * @generated
     */
	void setCoreLanguageElementMapping(CORELanguageElementMapping value);

	/**
     * Returns the value of the '<em><b>Inter Language Mapping Ends</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.InterLanguageMappingEnd}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Inter Language Mapping Ends</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getInterLanguageMapping_InterLanguageMappingEnds()
     * @model
     * @generated
     */
	EList<InterLanguageMappingEnd> getInterLanguageMappingEnds();

} // InterLanguageMapping
