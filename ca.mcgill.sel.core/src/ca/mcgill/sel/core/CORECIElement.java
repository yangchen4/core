/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORECI Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORECIElement#getPartiality <em>Partiality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORECIElement#getModelElement <em>Model Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORECIElement#getMappingCardinality <em>Mapping Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORECIElement#getReferenceCardinality <em>Reference Cardinality</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORECIElement()
 * @model
 * @generated
 */
public interface CORECIElement extends EObject {
	/**
     * Returns the value of the '<em><b>Partiality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.COREPartialityType}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Partiality</em>' attribute.
     * @see ca.mcgill.sel.core.COREPartialityType
     * @see #setPartiality(COREPartialityType)
     * @see ca.mcgill.sel.core.CorePackage#getCORECIElement_Partiality()
     * @model
     * @generated
     */
	COREPartialityType getPartiality();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORECIElement#getPartiality <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Partiality</em>' attribute.
     * @see ca.mcgill.sel.core.COREPartialityType
     * @see #getPartiality()
     * @generated
     */
	void setPartiality(COREPartialityType value);

	/**
     * Returns the value of the '<em><b>Model Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Model Element</em>' reference.
     * @see #setModelElement(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getCORECIElement_ModelElement()
     * @model required="true"
     * @generated
     */
	EObject getModelElement();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORECIElement#getModelElement <em>Model Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Model Element</em>' reference.
     * @see #getModelElement()
     * @generated
     */
	void setModelElement(EObject value);

	/**
     * Returns the value of the '<em><b>Mapping Cardinality</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mapping Cardinality</em>' containment reference.
     * @see #setMappingCardinality(COREMappingCardinality)
     * @see ca.mcgill.sel.core.CorePackage#getCORECIElement_MappingCardinality()
     * @model containment="true"
     * @generated
     */
	COREMappingCardinality getMappingCardinality();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORECIElement#getMappingCardinality <em>Mapping Cardinality</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Mapping Cardinality</em>' containment reference.
     * @see #getMappingCardinality()
     * @generated
     */
	void setMappingCardinality(COREMappingCardinality value);

	/**
     * Returns the value of the '<em><b>Reference Cardinality</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREMappingCardinality}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Reference Cardinality</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORECIElement_ReferenceCardinality()
     * @model
     * @generated
     */
	EList<COREMappingCardinality> getReferenceCardinality();

} // CORECIElement
