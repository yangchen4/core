/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE External Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREExternalArtefact#getRootModelElement <em>Root Model Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalArtefact#getLanguageName <em>Language Name</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREExternalArtefact()
 * @model
 * @generated
 */
public interface COREExternalArtefact extends COREArtefact {
	/**
     * Returns the value of the '<em><b>Root Model Element</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Root Model Element</em>' reference.
     * @see #setRootModelElement(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalArtefact_RootModelElement()
     * @model required="true"
     * @generated
     */
	EObject getRootModelElement();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalArtefact#getRootModelElement <em>Root Model Element</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Root Model Element</em>' reference.
     * @see #getRootModelElement()
     * @generated
     */
	void setRootModelElement(EObject value);

	/**
     * Returns the value of the '<em><b>Language Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Language Name</em>' attribute.
     * @see #setLanguageName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalArtefact_LanguageName()
     * @model
     * @generated
     */
	String getLanguageName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalArtefact#getLanguageName <em>Language Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language Name</em>' attribute.
     * @see #getLanguageName()
     * @generated
     */
	void setLanguageName(String value);

} // COREExternalArtefact
