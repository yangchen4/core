/**
 */
package ca.mcgill.sel.core.provider;


import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.core.COREMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class COREMappingItemProvider
    extends CORELinkItemProvider {
    
    /**
     * A (more or less) generic {@link ItemPropertyDescriptor} for from elements which can be used by sub-classes.
     * Overrides {@link ItemPropertyDescriptor#getChoiceOfValues(Object)} and filters out all objects that are not from the instantiated model
     * as well as elements not contained in the element that is mapped in the containing mapping (e.g., operations not belonging to the mapped class).
     * 
     * @author mschoettle
     * @generated NOT
     */
    protected class MappingFromItemPropertyDescriptor extends ItemPropertyDescriptor {
        
        public MappingFromItemPropertyDescriptor
        (AdapterFactory adapterFactory,
         ResourceLocator resourceLocator,
         String displayName,
         String description,
         EStructuralFeature feature, 
         boolean isSettable,
         boolean multiLine,
         boolean sortChoices,
         Object staticImage,
         String category,
         String [] filterFlags) {
            super(adapterFactory, 
                    resourceLocator, 
                    displayName, 
                    description, 
                    feature, 
                    isSettable, 
                    multiLine, 
                    sortChoices, 
                    staticImage, 
                    category, 
                    filterFlags);
        }
        
        @Override
        public Collection<?> getChoiceOfValues(Object object) {
            COREMapping<?> mapping = (COREMapping<?>) object;
            // The generic type of the Mapping this concrete Mapping is using...
            EGenericType genericType = getGenericType(mapping.eClass());
            COREModelComposition modelComposition = EMFModelUtil.getRootContainerOfType(mapping, CorePackage.Literals.CORE_MODEL_COMPOSITION);
            COREArtefact externalModel = modelComposition.getSource();
            
            // Get only all reachable objects of the correct type for this mapping...
            Collection<?> choiceOfValues = super.getReachableObjectsOfType(mapping, genericType);
            Collection<?> result = COREModelUtil.filterExtendedChoiceOfValues(mapping, choiceOfValues);
            
            // Filter out any already mapped element.
            Set<COREModelComposition> modelExtensions = new HashSet<COREModelComposition>(COREModelUtil.collectModelExtensions(externalModel));
            // Make sure any existing mapped element of this composition is filtered out as well.
            modelExtensions.add(modelComposition);
            COREModelUtil.filterMappedElements(result, modelExtensions);
            
            //TODO: Figure out what this does
//            Set<EObject> parentElements = null;
//            
//            if (mapping.eContainer() instanceof COREMapping<?>) {
//                COREMapping<?> parentMapping = (COREMapping<?>) mapping.eContainer();
//                parentElements = COREModelUtil.collectModelElementsFor(parentMapping.getFrom());            
//            }
//            
//            EObject currentContainer = EMFModelUtil.getRootContainerOfType(mapping, CorePackage.Literals.CORE_MODEL);
//            
//            // Filter out all objects not contained in the external model...
//            for (Iterator<?> iterator = result.iterator(); iterator.hasNext(); ) {
//                EObject value = (EObject) iterator.next();
//                
//                // Null is also contained in the list...
//                if (value != null) {
//                    EObject objectContainer = EMFModelUtil.getRootContainerOfType(value, CorePackage.Literals.CORE_MODEL);
//                    
//                    /**
//                     * Make sure that the containing feature is part of this class,
//                     * to avoid exceptions.
//                     * If there is no parent, don't filter out by default.
//                     */
//                    boolean parentMapped = true;
//                    
//                    if (parentElements != null) {
//                        /**
//                         * Make sure the value is contained or referenced by any possible parent element.
//                         */
//                        if (!parentElements.contains(value.eContainer())) {
//                            parentMapped = false;
//                        }
//                    }
//                    
//                    // If it is not from the external model it should be filtered out...
//                    if (currentContainer == objectContainer
//                            || !parentMapped
//                            || shouldFilterValue(externalModel, mapping, value)) {
//                        iterator.remove();
//                    }
//                }
//            }
            
            // add null since it is a reference feature and not many
            // (see ItemPropertyDescriptor line 807)
            result.add(null);

            return result;
        }

        /**
         * Returns whether the given object should be filtered out from the choice of values.
         * May be overridden by sub-classes to provide custom filtering.
         * 
         * @param externalModel the reused model
         * @param mapping the mapping
         * @param object the object
         * @return true, if the object should be filtered, false otherwise
         */
        protected boolean shouldFilterValue(COREArtefact externalModel, COREMapping<?> mapping, EObject object) {
            return false;
        }
        
        @Override
        public IItemLabelProvider getLabelProvider(Object object) {
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            return new IItemLabelProvider() {
                
                @Override
                public String getText(Object object) {
                    if (object != null) {

                        // The target is one of the mappings in the model.
                        // It is not necessarily the mapping we are currently looking at,
                        // but regardless it allows us to retrieve the current model.
                        COREMapping<?> target = (COREMapping<?>) getTarget();
                        COREModelComposition composition = EMFModelUtil.getRootContainerOfType(target, 
                                CorePackage.Literals.CORE_MODEL_COMPOSITION);
                        COREArtefact model = COREArtefactUtil.getReferencingExternalArtefact((EObject) object);
                        
                        /**
                         * It is possible that on removal of an element, 
                         * the target is not contained anymore and is null.
                         * 
                         * Only do this for elements not from the current model.
                         */
                        if (composition != null
                                && composition.getSource() != null
                                && model != composition.getSource()) {
//                            StringBuffer result = new StringBuffer();
                            
//                            result.append(EMFEditUtil.getTypeName(element));
//                            result.append(" ");
//                            result.append(EMFEditUtil.getTextFor(getAdapterFactory(), model));
//                            result.append(".");
//                            
//                            result.append(EMFEditUtil.getTextFor(getAdapterFactory(), element));
                            
//                            return result.toString();
                            return "Bonjour";
                        }
                    }
                    
                    return itemDelegator.getText(object);
                }
                
                @Override
                public Object getImage(Object object) {
                    return itemDelegator.getImage(object);
                }
            };
        }
        
    }
    
    /**
     * A (more or less) generic {@link ItemPropertyDescriptor} for to elements which can be used by sub-classes.
     * Overrides {@link ItemPropertyDescriptor#getChoiceOfValues(Object)} and filters out all objects that are not from the current aspect
     * as well as elements not contained in the element that is mapped in the containing mapping (e.g., operations not belonging to the mapped class).
     * 
     * @author mschoettle
     * @generated NOT
     */
    public class MappingToItemPropertyDescriptor extends ItemPropertyDescriptor {
        
        public MappingToItemPropertyDescriptor(
                AdapterFactory adapterFactory,
                ResourceLocator resourceLocator,
                String displayName,
                String description,
                EStructuralFeature feature, 
                boolean isSettable,
                boolean multiLine,
                boolean sortChoices,
                Object staticImage,
                String category,
                String [] filterFlags) {
                    super(adapterFactory, 
                            resourceLocator, 
                            displayName, 
                            description, 
                            feature, 
                            isSettable, 
                            multiLine, 
                            sortChoices, 
                            staticImage, 
                            category, 
                            filterFlags);
        }
        
        @Override
        public Collection<?> getChoiceOfValues(Object object) {
            COREMapping<?> currentMapping = (COREMapping<?>) object;
            
            COREModelComposition modelComposition = EMFModelUtil.getRootContainerOfType(currentMapping,
                    CorePackage.Literals.CORE_MODEL_COMPOSITION);
            COREArtefact model = (COREArtefact) modelComposition.eContainer();
            
            if (currentMapping.getFrom() == null) {
                return Collections.EMPTY_LIST;
            }
            
            // The generic type of the Mapping this concrete Mapping is using...
            EGenericType genericType = getGenericType(currentMapping.eClass());
            
            // Get only all reachable objects of the correct type for this mapping...
            Collection<?> choiceOfValues = super.getReachableObjectsOfType(currentMapping, genericType);
            Collection<?> result = COREModelUtil.filterExtendedChoiceOfValues(currentMapping, choiceOfValues);
            
            // Filter out any already mapped element.
            Set<COREModelExtension> modelExtensions = COREModelUtil.collectModelExtensions(model);
            COREModelUtil.filterMappedElements(result, modelExtensions);
            
            //TODO: Figure out what this does
//            COREModelElement parentToElement = null;
//            if (currentMapping.eContainer() instanceof COREMapping<?>) {                
//                COREMapping<?> parentMapping = (COREMapping<?>) currentMapping.eContainer();
//                parentToElement = parentMapping.getTo();
//            }
//            
//            for (Iterator<?> iterator = result.iterator(); iterator.hasNext();) {
//                EObject value = (EObject) iterator.next();
//                
//                // Null is also contained in the list...
//                if (value != null) {
//                    /**
//                     * Make sure that the containing feature is part of this the class,
//                     * to avoid exceptions.
//                     * If there is no parent, don't filter out by default.
//                     */
//                    boolean parentMapped = true;
//                    
//                    if (parentToElement != null) {
//                        /**
//                         * Make sure the value is contained or referenced by the parent element.
//                         */
//                        if (value.eContainer() != parentToElement) {
//                            parentMapped = false;
//                        }
//                    }
//                    
//                    // Filter out elements not of a parent (if it exists)..
//                    // Also, provide possibility for custom filter...
//                    if (!parentMapped 
//                            || shouldFilterValue(model, currentMapping, value)) {
//                        iterator.remove();
//                    }
//                }
//            }
            
            // add null since it is a reference feature and not many
            // (see ItemPropertyDescriptor line 807)
            result.add(null);
            
            return result;
        }
        
        /**
         * Returns whether the given object should be filtered out from the choice of values.
         * May be overridden by sub-classes to provide custom filtering.
         * 
         * @param model the model the mapping is part of
         * @param mapping the mapping
         * @param object the object
         * @return true, if the object should be filtered, false otherwise
         */
        protected boolean shouldFilterValue(COREArtefact model, COREMapping<?> mapping, EObject object) {
            return false;
        }
        
        @Override
        public IItemLabelProvider getLabelProvider(Object object) {
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            return new IItemLabelProvider() {
                
                @Override
                public String getText(Object object) {
                    if (object != null) {
                        // The target is one of the mappings in the model.
                        // It is not necessarily the mapping we are currently looking at,
                        // but regardless it allows us to retrieve the current model.
                        EObject target = (EObject) getTarget();
                        COREArtefact currentModel = EMFModelUtil.getRootContainerOfType(target, 
                                CorePackage.Literals.CORE_ARTEFACT);
                        COREArtefact model = COREArtefactUtil.getReferencingExternalArtefact((EObject) object);
                        
                        // Only do this for elements not from the current model.
                        if (model != currentModel
                                /**
                                 * It is possible that on removal of an element, 
                                 * the target is not contained anymore,
                                 * i.e., the currentModel (container) is null, 
                                 * which would lead to the prepending of the external model, 
                                 * even though this cannot be guaranteed. Hence, it needs to be prevented.
                                 */
                                && currentModel != null) {
                            
//                            StringBuffer result = new StringBuffer();
                            
//                            result.append(EMFEditUtil.getTypeName(element));
//                            result.append(" ");
//                            result.append(EMFEditUtil.getTextFor(getAdapterFactory(), model));
//                            result.append(".");
//                            
//                            result.append(EMFEditUtil.getTextFor(getAdapterFactory(), element));
//                            
//                            return result.toString();
                            return "Bonjour";
                        }
                    }
                    
                    return itemDelegator.getText(object);
                }
                
                @Override
                public Object getImage(Object object) {
                    return itemDelegator.getImage(object);
                }
            };
        }
        
    }    
    
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public COREMappingItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addReferencedMappingsPropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

    /**
     * This adds a property descriptor for the Referenced Mappings feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addReferencedMappingsPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREMapping_referencedMappings_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREMapping_referencedMappings_feature", "_UI_COREMapping_type"),
                 CorePackage.Literals.CORE_MAPPING__REFERENCED_MAPPINGS,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(CorePackage.Literals.CORE_MAPPING__MAPPINGS);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This returns COREMapping.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/COREMapping"));
    }

    /**
     * Returns the first generic type of the given class's supertype.
     * The assumption is that the given {@link EClass} is a {@link COREMapping},
     * which has exactly one generic type.
     * 
     * @param eClass the {@link EClass} to retrieve the first generic type for
     * @return the first generic type of the given class's super type
     */
    private EGenericType getGenericType(EClass eClass) {
        EGenericType genericType = eClass.getEGenericSuperTypes().get(0).getETypeArguments().get(0);
        
        return genericType;
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText(Object object) {
        return getString("_UI_COREMapping_type");
    }

    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(COREMapping.class)) {
            case CorePackage.CORE_MAPPING__MAPPINGS:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 CoreFactory.eINSTANCE.createCOREMapping()));
    }

}
