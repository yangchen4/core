/**
 */
package ca.mcgill.sel.core.provider;


import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.core.COREPerspective} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class COREPerspectiveItemProvider extends CORELanguageItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public COREPerspectiveItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addLanguagesPropertyDescriptor(object);
            addDefaultPropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

    /**
     * This adds a property descriptor for the Languages feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addLanguagesPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREPerspective_languages_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREPerspective_languages_feature", "_UI_COREPerspective_type"),
                 CorePackage.Literals.CORE_PERSPECTIVE__LANGUAGES,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Default feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addDefaultPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREPerspective_default_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREPerspective_default_feature", "_UI_COREPerspective_type"),
                 CorePackage.Literals.CORE_PERSPECTIVE__DEFAULT,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(CorePackage.Literals.CORE_PERSPECTIVE__MAPPINGS);
            childrenFeatures.add(CorePackage.Literals.CORE_PERSPECTIVE__LANGUAGES);
            childrenFeatures.add(CorePackage.Literals.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This returns COREPerspective.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/COREPerspective"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText(Object object) {
        String label = ((COREPerspective)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_COREPerspective_type") :
            getString("_UI_COREPerspective_type") + " " + label;
    }
    

    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(COREPerspective.class)) {
            case CorePackage.CORE_PERSPECTIVE__DEFAULT:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
                return;
            case CorePackage.CORE_PERSPECTIVE__MAPPINGS:
            case CorePackage.CORE_PERSPECTIVE__LANGUAGES:
            case CorePackage.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_PERSPECTIVE__MAPPINGS,
                 CoreFactory.eINSTANCE.createCORELanguageElementMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_PERSPECTIVE__LANGUAGES,
                 CoreFactory.eINSTANCE.create(CorePackage.Literals.LANGUAGE_MAP)));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS,
                 CoreFactory.eINSTANCE.createNavigationMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS,
                 CoreFactory.eINSTANCE.createInterLanguageMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_PERSPECTIVE__NAVIGATION_MAPPINGS,
                 CoreFactory.eINSTANCE.createIntraLanguageMapping()));
    }

}
