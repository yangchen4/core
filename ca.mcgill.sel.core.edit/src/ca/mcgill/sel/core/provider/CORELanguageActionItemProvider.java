/**
 */
package ca.mcgill.sel.core.provider;


import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.core.CORELanguageAction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CORELanguageActionItemProvider extends COREActionItemProvider {
	/**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CORELanguageActionItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

	/**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addClassQualifiedNamePropertyDescriptor(object);
            addMethodNamePropertyDescriptor(object);
            addParametersPropertyDescriptor(object);
            addSecondaryEffectsPropertyDescriptor(object);
            addActionTypePropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

    /**
     * This adds a property descriptor for the Class Qualified Name feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addClassQualifiedNamePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELanguageAction_classQualifiedName_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_CORELanguageAction_classQualifiedName_feature", "_UI_CORELanguageAction_type"),
                 CorePackage.Literals.CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Method Name feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addMethodNamePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELanguageAction_methodName_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_CORELanguageAction_methodName_feature", "_UI_CORELanguageAction_type"),
                 CorePackage.Literals.CORE_LANGUAGE_ACTION__METHOD_NAME,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Action Type feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addActionTypePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELanguageAction_actionType_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_CORELanguageAction_actionType_feature", "_UI_CORELanguageAction_type"),
                 CorePackage.Literals.CORE_LANGUAGE_ACTION__ACTION_TYPE,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(CorePackage.Literals.CORE_LANGUAGE_ACTION__PARAMETERS);
            childrenFeatures.add(CorePackage.Literals.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This adds a property descriptor for the Parameters feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addParametersPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELanguageAction_parameters_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_CORELanguageAction_parameters_feature", "_UI_CORELanguageAction_type"),
                 CorePackage.Literals.CORE_LANGUAGE_ACTION__PARAMETERS,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Secondary Effects feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addSecondaryEffectsPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELanguageAction_secondaryEffects_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_CORELanguageAction_secondaryEffects_feature", "_UI_CORELanguageAction_type"),
                 CorePackage.Literals.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This returns CORELanguageAction.gif.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/CORELanguageAction"));
    }

	/**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getText(Object object) {
        String label = ((CORELanguageAction)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_CORELanguageAction_type") :
            getString("_UI_CORELanguageAction_type") + " " + label;
    }


	/**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(CORELanguageAction.class)) {
            case CorePackage.CORE_LANGUAGE_ACTION__CLASS_QUALIFIED_NAME:
            case CorePackage.CORE_LANGUAGE_ACTION__METHOD_NAME:
            case CorePackage.CORE_LANGUAGE_ACTION__ACTION_TYPE:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
                return;
            case CorePackage.CORE_LANGUAGE_ACTION__PARAMETERS:
            case CorePackage.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

	/**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_LANGUAGE_ACTION__PARAMETERS,
                 CoreFactory.eINSTANCE.createParameter()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS,
                 CoreFactory.eINSTANCE.createDeleteEffect()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS,
                 CoreFactory.eINSTANCE.createUpdateEffect()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_LANGUAGE_ACTION__SECONDARY_EFFECTS,
                 CoreFactory.eINSTANCE.createCreateEffect()));
    }

}
