/**
 */
package ca.mcgill.sel.core.provider;


import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.IntraLanguageMapping;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.core.IntraLanguageMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class IntraLanguageMappingItemProvider extends NavigationMappingItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public IntraLanguageMappingItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addNamePropertyDescriptor(object);
            addClosurePropertyDescriptor(object);
            addReusePropertyDescriptor(object);
            addIncreaseDepthPropertyDescriptor(object);
            addChangeModelPropertyDescriptor(object);
            addFromPropertyDescriptor(object);
            addHopsPropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

    /**
     * This adds a property descriptor for the Name feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addNamePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_IntraLanguageMapping_name_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_IntraLanguageMapping_name_feature", "_UI_IntraLanguageMapping_type"),
                 CorePackage.Literals.INTRA_LANGUAGE_MAPPING__NAME,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Closure feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addClosurePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_IntraLanguageMapping_closure_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_IntraLanguageMapping_closure_feature", "_UI_IntraLanguageMapping_type"),
                 CorePackage.Literals.INTRA_LANGUAGE_MAPPING__CLOSURE,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Reuse feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addReusePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_IntraLanguageMapping_reuse_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_IntraLanguageMapping_reuse_feature", "_UI_IntraLanguageMapping_type"),
                 CorePackage.Literals.INTRA_LANGUAGE_MAPPING__REUSE,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Increase Depth feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addIncreaseDepthPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_IntraLanguageMapping_increaseDepth_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_IntraLanguageMapping_increaseDepth_feature", "_UI_IntraLanguageMapping_type"),
                 CorePackage.Literals.INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Change Model feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addChangeModelPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_IntraLanguageMapping_changeModel_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_IntraLanguageMapping_changeModel_feature", "_UI_IntraLanguageMapping_type"),
                 CorePackage.Literals.INTRA_LANGUAGE_MAPPING__CHANGE_MODEL,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the From feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addFromPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_IntraLanguageMapping_from_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_IntraLanguageMapping_from_feature", "_UI_IntraLanguageMapping_type"),
                 CorePackage.Literals.INTRA_LANGUAGE_MAPPING__FROM,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Hops feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addHopsPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_IntraLanguageMapping_hops_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_IntraLanguageMapping_hops_feature", "_UI_IntraLanguageMapping_type"),
                 CorePackage.Literals.INTRA_LANGUAGE_MAPPING__HOPS,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This returns IntraLanguageMapping.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/IntraLanguageMapping"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText(Object object) {
        String label = ((IntraLanguageMapping)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_IntraLanguageMapping_type") :
            getString("_UI_IntraLanguageMapping_type") + " " + label;
    }


    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(IntraLanguageMapping.class)) {
            case CorePackage.INTRA_LANGUAGE_MAPPING__NAME:
            case CorePackage.INTRA_LANGUAGE_MAPPING__CLOSURE:
            case CorePackage.INTRA_LANGUAGE_MAPPING__REUSE:
            case CorePackage.INTRA_LANGUAGE_MAPPING__INCREASE_DEPTH:
            case CorePackage.INTRA_LANGUAGE_MAPPING__CHANGE_MODEL:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
                return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);
    }

}
