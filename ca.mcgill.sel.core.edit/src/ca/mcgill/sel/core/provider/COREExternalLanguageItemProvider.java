/**
 */
package ca.mcgill.sel.core.provider;


import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.core.COREExternalLanguage} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class COREExternalLanguageItemProvider extends CORELanguageItemProvider {
	/**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public COREExternalLanguageItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

	/**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addNsURIPropertyDescriptor(object);
            addResourceFactoryPropertyDescriptor(object);
            addAdapterFactoryPropertyDescriptor(object);
            addWeaverClassNamePropertyDescriptor(object);
            addFileExtensionPropertyDescriptor(object);
            addLanguageElementsPropertyDescriptor(object);
            addModelUtilClassNamePropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

	/**
     * This adds a property descriptor for the Ns URI feature.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void addNsURIPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREExternalLanguage_nsURI_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREExternalLanguage_nsURI_feature", "_UI_COREExternalLanguage_type"),
                 CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__NS_URI,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

	/**
     * This adds a property descriptor for the Resource Factory feature.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void addResourceFactoryPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREExternalLanguage_resourceFactory_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREExternalLanguage_resourceFactory_feature", "_UI_COREExternalLanguage_type"),
                 CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

	/**
     * This adds a property descriptor for the Adapter Factory feature.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void addAdapterFactoryPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREExternalLanguage_adapterFactory_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREExternalLanguage_adapterFactory_feature", "_UI_COREExternalLanguage_type"),
                 CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

	/**
     * This adds a property descriptor for the Weaver Class Name feature.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void addWeaverClassNamePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREExternalLanguage_weaverClassName_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREExternalLanguage_weaverClassName_feature", "_UI_COREExternalLanguage_type"),
                 CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

	/**
     * This adds a property descriptor for the File Extension feature.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void addFileExtensionPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREExternalLanguage_fileExtension_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREExternalLanguage_fileExtension_feature", "_UI_COREExternalLanguage_type"),
                 CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Language Elements feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addLanguageElementsPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREExternalLanguage_languageElements_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREExternalLanguage_languageElements_feature", "_UI_COREExternalLanguage_type"),
                 CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Model Util Class Name feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addModelUtilClassNamePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_COREExternalLanguage_modelUtilClassName_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_COREExternalLanguage_modelUtilClassName_feature", "_UI_COREExternalLanguage_type"),
                 CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This returns COREExternalLanguage.gif.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/COREExternalLanguage"));
    }

	/**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getText(Object object) {
        String label = ((COREExternalLanguage)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_COREExternalLanguage_type") :
            getString("_UI_COREExternalLanguage_type") + " " + label;
    }


	/**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(COREExternalLanguage.class)) {
            case CorePackage.CORE_EXTERNAL_LANGUAGE__NS_URI:
            case CorePackage.CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY:
            case CorePackage.CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY:
            case CorePackage.CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME:
            case CorePackage.CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION:
            case CorePackage.CORE_EXTERNAL_LANGUAGE__MODEL_UTIL_CLASS_NAME:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
                return;
            case CorePackage.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

	/**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_EXTERNAL_LANGUAGE__LANGUAGE_ELEMENTS,
                 CoreFactory.eINSTANCE.createCORELanguageElement()));
    }

}
