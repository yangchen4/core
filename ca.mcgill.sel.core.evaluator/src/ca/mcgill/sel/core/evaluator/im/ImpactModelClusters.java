package ca.mcgill.sel.core.evaluator.im;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;

/**
 * This class encapsulates the impact model, clusters in it and the root goal.
 * @author berkduran
 */
public class ImpactModelClusters {

    private COREImpactModel impactModel;
    private Set<COREImpactNode> rootGoals = new HashSet<>();
    private Map<COREImpactNode, List<COREImpactNode>> clusters = new HashMap<>();
    private Map<COREImpactNode, Integer> evalCount = new HashMap<>();
    private Map<COREFeature, COREFeatureImpactNode> featureToImpactNodeMap = new HashMap<>();
    
    /**
     * Constructor takes the impact model and divides into clusters.
     * @param impactModel is the impact model.
     */
    public ImpactModelClusters(COREImpactModel impactModel) {
        this.impactModel = impactModel;
        divideImpactModelIntoClusters();
        prepareEvalCount();
        
        initializeFeatureToImpactNodeMap();
    }
    
    /**
     * Prepares the map for each feature and its featureimpactnode.
     */
    private void initializeFeatureToImpactNodeMap() {

        for (COREImpactNode anImpactNode : impactModel.getImpactModelElements()) {
            if (anImpactNode instanceof COREFeatureImpactNode) {
                COREFeatureImpactNode featureImpactNode = (COREFeatureImpactNode) anImpactNode;
                featureToImpactNodeMap.put(featureImpactNode.getRepresents(), featureImpactNode);
            }
        }
    }
    
    /**
     * Sets the root goal in the impact model.
     */
    private void divideImpactModelIntoClusters() {
        //Make sure there are no duplicates in the cluster.
        for (COREImpactNode anElement : impactModel.getImpactModelElements()) {
            if (anElement.getOutgoing().isEmpty()) {
                rootGoals.add(anElement);
            }
            if (!anElement.getIncoming().isEmpty()) {
                List<COREImpactNode> children = new ArrayList<>();
                for (COREContribution incomingContribution : anElement.getIncoming()) {
                    children.add(incomingContribution.getSource());
                }
                clusters.put(anElement, children);
            }
        }
    }

    /**
     * Given clusters, initializes the evalCount at the beginning for each cluster.
     * Evaluated value is the number of children that are not features.
     * Evaluation should start from the clusters with evalCount = 0 and every evaluated goal decrements its parent's
     * evaluation count.
     */
    private void prepareEvalCount() {
        for (COREImpactNode parent : clusters.keySet()) {
            int val = 0;
            for (COREImpactNode child : clusters.get(parent)) {
                //this can be done by counting the ones that are not yet evaluated.
                if (!(child instanceof COREFeatureImpactNode)) {
                    val++;
                }
            }
            evalCount.put(parent, val);
        }
    }

    /**
     * Getter for the root goal.
     * @return root goal in the impact model.
     */
    public Set<COREImpactNode> getRootGoals() {
        return rootGoals;
    }

    /**
     * Getter for clusters.
     * @return the map containing a goal and its contributors.
     */
    public Map<COREImpactNode, List<COREImpactNode>> getClusters() {
        return clusters;
    }

    /**
     * Getter for evalCount.
     * @return evalCount is the prepared evalCount map.
     */
    public Map<COREImpactNode, Integer> getEvalCount() {
        return evalCount;
    }

    /**
     * Getter for featureToImpactNodeMap.
     * @return featureToImpactNodeMap maps the features to set featureimpactnodes
     */
    public Map<COREFeature, COREFeatureImpactNode> getFeatureToImpactNodeMap() {
        return featureToImpactNodeMap;
    }

}
