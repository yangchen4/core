package ca.mcgill.sel.core.evaluator.im;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREReuse;

/**
 * Class that encapsulates the results of the propagation.
 *
 * @author berkduran
 *
 */
public class PropagationResult {

    private Map<COREImpactNode, Float> evaluations  = new HashMap<>();
    private Map<COREReuse, PropagationResult> reuseEvaluations = new HashMap<>();

    /**
     * Constructor.
     */
    public PropagationResult() {
    }

    /**
     * Getter for Current Level Evaluations.
     *
     * @return the evaluations
     */
    public Map<COREImpactNode, Float> getEvaluations() {
        return evaluations;
    }

    /**
     * Setter for evaluations.
     *
     * @param evaluations to set
     */
    public void setEvaluations(Map<COREImpactNode, Float> evaluations) {
        this.evaluations = evaluations;
    }

    /**
     * Getter for Reuse Evaluations.
     *
     * @return the reuseEvaluations
     */
    public Map<COREReuse, PropagationResult> getReuseEvaluations() {
        return reuseEvaluations;
    }

    /**
     * Setter for Reuse Evaluations.
     *
     * @param reuseEvaluations the reuseEvaluations to set
     */
    public void setReuseEvaluations(Map<COREReuse, PropagationResult> reuseEvaluations) {
        this.reuseEvaluations = reuseEvaluations;
    }

    /**
     * Function to retrieve the evaluation map (COREImpactNode, Float) for a specific reuse.
     *
     * @param reuse The specific reuse
     * @return the PropagationResult class one level down the hierarchy.
     */
    public PropagationResult getEvaluationsForReuse(COREReuse reuse) {
        return this.reuseEvaluations.get(reuse);
    }

    /**
     * Function to retrieve the reuses that are evaluated.
     *
     * @return the set of reuses.
     */
    public Set<COREReuse> getReusesThatAreEvaluated() {
        return this.reuseEvaluations.keySet();
    }

    /**
     * Getter for an evaluation for an impact node at the current level.
     *
     * @param impactNode is the node for which the evaluation result is desired.
     * @return the evaluation for impactNode
     */
    public Float getEvaluationForImpactNode(COREImpactNode impactNode) {
        return evaluations.get(impactNode);
    }

}
