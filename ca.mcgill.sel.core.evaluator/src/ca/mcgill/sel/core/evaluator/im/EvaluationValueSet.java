package ca.mcgill.sel.core.evaluator.im;

import java.util.HashSet;
import java.util.Set;

import ca.mcgill.sel.core.COREFeature;

/**
 * Table contains the index - corresponding configuration - corresponding value.
 * @author berkduran
 */
class EvaluationValueSet {
    private int[] index;
    private float value;
    private Set<COREFeature> configuration = new HashSet<>();
    private Set<COREFeature> unwanted = new HashSet<>();
    
    /**
     * Constructor creates the configuration list.
     */
    EvaluationValueSet() {
    }
    
    /**
     * Getter for index.
     * @return index value representing children branches.
     */
    public int[] getIndex() {
        return index;
    }
    
    /**
     * Setter for index.
     * @param index is the value to be set.
     */
    public void setIndex(int[] index) {
        this.index = index;
    }
    
    /**
     * Getter for configuration.
     * @return set of features in the configuration.
     */
    public Set<COREFeature> getConfiguration() {
        return configuration;
    }
    
    /**
     * Setter for configuration.
     * Removes the possible duplicates.
     * @param configuration is the configuration with set of features to be set.
     */
    void setConfiguration(Set<COREFeature> configuration) {
        this.configuration = configuration;
    }
    
    /**
     * This method is used to add a feature to the configuration.
     * It checks if the feature is already in the configuration.
     * @param feature is added to the configuration if it is not already in it.
     */
    void addToConfiguration(COREFeature feature) {
        this.configuration.add(feature);
    }
    
    /**
     * Getter for the summation value.
     * @return the sum for the configuration.
     */
    public float getValue() {
        return value;
    }
    
    /**
     * Setter for the value.
     * @param value is the value to be set.
     */
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * Getter for unwanted list.
     * @return set of features in the unwanted list.
     */
    public Set<COREFeature> getUnwanted() {
        return unwanted;
    }

    /**
     * Setter for unwanted list.
     * Removes the possible duplicates.
     * @param unwanted is the list with set of features to be set.
     */
    public void setUnwanted(Set<COREFeature> unwanted) {
        this.unwanted = unwanted;
    }
    
    /**
     * This method is used to add a feature to the unwanted list.
     * It checks if the feature is already in the list.
     * @param feature is added to the list if it is not already in it.
     */
    void addToUnwanted(COREFeature feature) {
        this.unwanted.add(feature);
    }
}
