package ca.mcgill.sel.core.controller.util;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;

public final class COREReferenceUtil {

    /**
     * Private constructor to prevent instantiation.
     */
    private COREReferenceUtil() {
        // Suppress and hide default constructor.
    }


    /**
     * Ensures that the given element is localized. I.e., if it is not already contained in the same model as the given
     * local model element, it is cloned and added to the model. In addition, a mapping reference is added to connect
     * the external and the local element. All changes are added as commands to the given command.
     * Supports any kind of model element. Localizes any containers as well, if necessary. 
     * Furthermore, all references of the element are localized as well.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to add commands to
     * @param localModelElement an element of the local model
     * @param element the element to localize
     * @param <T> the type of the element to be localized
     * @return the given element, if it is already local, 
     *          or a copy of the element, which will be added to the local model
     */
    public static <T extends EObject> T localizeElement(EditingDomain domain, CompoundCommand command, 
                                            EObject localModelElement, T element) {
        EObject localModel = EcoreUtil.getRootContainer(localModelElement);
        EObject externalModel = EcoreUtil.getRootContainer(element);
        T localElement = element;
        
        if (element != null && localModel != externalModel) {
            COREExternalArtefact localArtefact = COREArtefactUtil.getReferencingExternalArtefact(localModel);
            
            COREModelComposition modelComposition = COREModelUtil.getModelCompositionFor(externalModel, localModel);
            
            if (modelComposition == null) {
                // We're trying to localize an element that is inside a model that we're not extending / reusing
                // We therefore need to create a new model extension first!
                modelComposition = CoreFactory.eINSTANCE.createCOREModelExtension();
                modelComposition.setSource(COREArtefactUtil.getReferencingExternalArtefact(externalModel));
                command.append(AddCommand.create(domain, localArtefact,
                        CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS, modelComposition));
            }
            
            EObject externalContainer = element.eContainer();
            while (externalContainer != externalModel) {
                // here we need to localize the container first!
            }
            
//            if (!(element instanceof Classifier) && !(element instanceof REnum)) {
//                EObject externalContainer = element.eContainer();
//                
//                EObject localContainer = 
//                        getOrCreateLocalContainer(domain, command, currentArtefact, externalContainer);
//                
//                localElement = createLocalElement(domain, command, currentArtefact, localContainer, element);
//            } else {
            localElement = getOrCreateLocalElement(domain, command, localArtefact, element, modelComposition);
            
            // Update properties with external references of local element to local ones.
            // updateLocalElementProperties(domain, command, currentAspect, localElement);
        }
        
        return localElement;
    }
    
    /**
     * Returns the local element corresponding to the external element passed as a parameter, either by
     * retrieving the existing one or creating a copy of the given element.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to append commands to
     * @param currentArtefact the local artefact
     * @param externalElement the external element
     * @param comp the COREModelComposition in which to look for the mapping
     * @param <T> the type of the element to be localized
     * @return the local copy of the container element
     */
    private static <T extends EObject> T getOrCreateLocalElement(EditingDomain domain, CompoundCommand command,
            COREExternalArtefact currentArtefact, T externalElement, COREModelComposition comp) {
        T localElement;
        COREMapping<T> mapping = getMappingFromComposition(command, externalElement, comp);
        
        if (mapping == null) {
            localElement = createLocalElement(domain, command, currentArtefact.getRootModelElement(),
                externalElement, comp);
        } else {
            localElement = mapping.getTo();
        }
        
        return localElement;
    }

    /**
     * Creates a copy of the class and adds it to the aspect.
     * Adds commands to the given command to add the class, its layout and a mapping reference to the given aspect.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to append commands to
     * @param localElementContainer the container the localized element should be added to
     * @param element the element which is contained in another model
     * @param comp the COREModelComposition
     * @param <T> the type of the element to be localized
     * @return the element copy, which will be added to the local model
     */
    private static <T extends EObject> T createLocalElement(EditingDomain domain, CompoundCommand command,
            EObject localElementContainer, T element, COREModelComposition comp) {
        T localElement = cloneElement(element);
        
        command.append(AddCommand.create(domain, localElementContainer, element.eContainingFeature(), localElement));
//        
//        // Create mapping and add it to the correct container.
//        Aspect aspect = (Aspect) EcoreUtil.getRootContainer(localElementContainer);
//        Aspect externalAspect = (Aspect) EcoreUtil.getRootContainer(element);
//        
//        COREModelComposition modelComposition = COREModelUtil.getModelCompositionFor(externalAspect, aspect);
//        EStructuralFeature mappingContainingFeature = CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS;
//        EClass mappingClass = null;
//        
//        if (RamPackage.Literals.RENUM.isInstance(element)) {
//            mappingClass = RamPackage.Literals.ENUM_MAPPING;
//        } else if (RamPackage.Literals.CLASSIFIER.isInstance(element)) {
//            mappingClass = RamPackage.Literals.CLASSIFIER_MAPPING;
//        }
//        
        createAndAddMappingReference(domain, command, comp, element, localElement);
        
        return localElement;
    }

    /**
     * Returns the mapping for a given model element, where the model element is the "from", of
     * the composition comp.
     * Besides looking within the current model, also checks the result of the current command, in order to ensure
     * that a mapping that will be added is already considered.
     * 
     * @param <T> the type of object
     * @param command the current command being populated
     * @param from the {@link Classifier} the classifier mapping is searched for where it is the from element
     * @param comp the composition specification in which to look for the mapping
     * @return the {@link ClassifierMapping} for the given classifier, null if none found
     */
    private static <T extends EObject> COREMapping<T> getMappingFromComposition(Command command,
            T from, COREModelComposition comp) {
                
        
        for (COREModelElementComposition<?> m : comp.getCompositions()) {
            if (m instanceof COREMapping<?>) {
                @SuppressWarnings("unchecked")
                COREMapping<T> mapping = (COREMapping<T>) m;
                if (mapping.getFrom() == from) {
                    return mapping;
                }
            }
        }
        
        Collection<COREMapping<T>> mappings = EcoreUtil.getObjectsByType(command.getResult(), 
                    CorePackage.Literals.CORE_MAPPING);
            
        for (COREMapping<T> currentMapping : mappings) {
            if (currentMapping.getFrom() == from) {
                return currentMapping;
            }
        }
        
        return null;
    }

    /**
     * Clones the given element.
     * Creates a shallow copy.
     * 
     * @param original the element to be cloned
     * @param <T> the type of the element being cloned
     * @return a clone of the element
     */
    private static <T extends EObject> T cloneElement(T original) {
        // Create a shallow copy using EcoreUtil.Copier by overriding copyContainment
        Copier copier = new EcoreUtil.Copier() {

            private static final long serialVersionUID = 0L;

            @Override
            protected void copyContainment(EReference eReference, EObject eObject, EObject copyEObject) {
                // empty on purpose
            }
        };
        @SuppressWarnings("unchecked")
        T result = (T) copier.copy(original);
        return result;
    }

    
    /**
     * Creates a mapping from the "from" to the "to" element and adds it to the given owner.
     * The mapping will be a reference, which will be executed as a separate command 
     * to allow interested clients to receive the appropriate notification.
     * The specific mapping that needs to be created is determined by looking through the metamodel of the 
     * external language.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to append commands to
     * @param owner the {@link EObject} the mapping should be added to
     * @param from the element to map from, i.e., the external element
     * @param to the element to map to, i.e., the local element
     */
    private static void createAndAddMappingReference(EditingDomain domain, CompoundCommand command, EObject owner,
            EObject from, EObject to) {
        
        EClass metaclass = to.eClass();
        
        // look in the metamodel of the language for the right mapping, then create an instance of the mapping
        EClass mappingClass = findMappingMetaclass(metaclass);
        @SuppressWarnings("unchecked")
        COREMapping<EObject> mapping = (COREMapping<EObject>) EcoreUtil.create(mappingClass);

        // now find the EStructuralFeature that contains the list of mappings in the owner class
        int containingFeature = 0;
        // if the metaclass is at the top of the containment hierarchy, then we need to insert the mapping into the
        // model composition itself. "owner" should therefore be a model composition.
        if (metaclass.eContainer() == EcoreUtil.getRootContainer(metaclass)) {
            if (!(owner instanceof COREModelComposition)) {
                // This should never happen
                throw new RuntimeException();
            } else {
                containingFeature = CorePackage.CORE_MODEL_COMPOSITION__COMPOSITIONS;
            }            
        } else {
            // This happens when we're localizing an element that is contained in another element.
            // TODO: In this case we should find the mapping that maps the containing object, and then
            // add it to that mapping
            throw new RuntimeException();
        }
        
        // Set to property using command to force notification of derived property (reference) in model element.
        mapping.setFrom(from);
        // Also directly set it in order to be able to use it in getOrCreateLocalContainer.
        mapping.setTo(to);
        
        command.append(AddCommand.create(domain, owner, containingFeature, mapping));
        command.append(SetCommand.create(domain, mapping, CorePackage.Literals.CORE_LINK__TO, to));
    }
    
    private static EClass findMappingMetaclass(EClass metaclass) {
        EClass coreMappingEClass = CorePackage.eINSTANCE.getCOREMapping();
        List<EObject> referencingObject = EMFModelUtil.findObjectsThatCrossReference(metaclass);
        for (EObject candidate : referencingObject) {
            EObject can = candidate;
            while (!(can instanceof EClass)) {
                can = can.eContainer();
            }
            if (coreMappingEClass.isSuperTypeOf((EClass) can)) {
                return (EClass) can;
            }
        }
        for (EClass supertype : metaclass.getESuperTypes()) {
            EClass mapping = findMappingMetaclass(supertype);
            if (mapping != null) {
                return mapping;
            }
        }
        return null;
    }
}
