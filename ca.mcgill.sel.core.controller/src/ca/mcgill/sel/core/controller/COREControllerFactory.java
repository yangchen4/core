package ca.mcgill.sel.core.controller;

/**
 * A factory to obtain all controllers related to CORE.
 *
 * @author mschoettle
 */
public final class COREControllerFactory {

    /**
     * The singleton instance of this factory.
     */
    public static final COREControllerFactory INSTANCE = new COREControllerFactory();

    private FeatureController featureController;
    private ReuseController reuseController;
    private ImpactModelElementController impactModelElementController;
    private FeatureImpactController featureImpactController;
    private ContributionController contributionController;
    private ConcernController concernController;
    private MappingCardinalityController mappingCardinalityController;
    private MappingController mappingController;
    private PerspectiveController perspectiveController;
    
    /**
     * Creates a new instance of {@link COREControllerFactory}.
     */
    private COREControllerFactory() {

    }

    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREFeature}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREFeature}s
     */
    public FeatureController getFeatureController() {
        if (featureController == null) {
            featureController = new FeatureController();
        }
        return featureController;
    }

    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREReuse}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREReuse}s
     */
    public ReuseController getReuseController() {
        if (reuseController == null) {
            reuseController = new ReuseController();
        }
        return reuseController;
    }

    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREImpactNode}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREImpactNode}s
     */
    public ImpactModelElementController getImpactModelElementController() {
        if (impactModelElementController == null) {
            impactModelElementController = new ImpactModelElementController();
        }
        return impactModelElementController;
    }

    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREFeatureImpactNode}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREFeatureImpactNode}s
     */
    public FeatureImpactController getFeatureImpactController() {
        if (featureImpactController == null) {
            featureImpactController = new FeatureImpactController();
        }
        return featureImpactController;
    }

    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREContribution}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREContribution}s
     */
    public ContributionController getContributionController() {
        if (contributionController == null) {
            contributionController = new ContributionController();
        }
        return contributionController;
    }

    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREConcern}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREConcern}s
     */
    public ConcernController getConcernController() {
        if (concernController == null) {
            concernController = new ConcernController();
        }
        return concernController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREMappingCardinality}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREMappingCardinality}s
     */
    public MappingCardinalityController getMappingCardinalityController() {
        if (mappingCardinalityController == null) {
            mappingCardinalityController = new MappingCardinalityController();
        }
        return mappingCardinalityController;
    }
    
    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREMapping}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREMapping}s
     */
    public MappingController getMappingController() {
        if (mappingController == null) {
            mappingController = new MappingController();
        }
        return mappingController;
    }

    /**
     * Returns the controller for {@link ca.mcgill.sel.core.COREModelElementMapping}s.
     *
     * @return the controller for {@link ca.mcgill.sel.core.COREModelElementMapping}s
     */
    public PerspectiveController getPerspectiveController() {
        if (perspectiveController == null) {
            perspectiveController = new PerspectiveController();
        }
        return perspectiveController;
    }

}
