package ca.mcgill.sel.core.controller;

import org.eclipse.emf.ecore.EObject;

/**
 * When introducing a new language into CORE, this interface has to be implemented by the "top"
 * controller. It defines just one operation, namely createNewEmptyModel, that needs to be implemented.
 *  
 * @author gemini
 *
 */
public interface ModelUtil {

    /**
     * This operation must create a new empty model, and initialize everything so that the model can be
     * used.
     * 
     * @param name the name that is given to the model
     * @return the EObject referring to the created model 
     */
    EObject createNewEmptyModel(String name);
}
