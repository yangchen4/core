package ca.mcgill.sel.core.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseArtefact;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * The controller for {link @COREReuse and @COREModelReuse}.
 *
 * @author oalam
 */
public class ReuseController extends CoreBaseController {
    
    /**
     * Creates a new instance of {@link ReuseController}.
     */
    protected ReuseController() {
        // prevent anyone outside this package to instantiate
    }

    /**
     * Creates a new {@link COREReuse} and {@link COREModelReuse} for the given models.
     *
     * @param owner the model that is reusing
     * @param externalArtefact the referenced model (external woven model for models of selected features)
     * @param configuration the configuration for the reuse
     * @param reuse the reuse that is configured
     */
    public void createModelReuse(COREArtefact owner, COREArtefact externalArtefact, 
            COREConfiguration configuration, COREReuse reuse) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        
        CompoundCommand addModelReuseCommand = new CompoundCommand();
        
        // create the model reuse with: a reuse, a configuration and an external artefact
        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        modelReuse.setSource(externalArtefact);
        modelReuse.setConfiguration(configuration);
        
        Command setReuseCommand = SetCommand.create(editingDomain, modelReuse,
                CorePackage.Literals.CORE_MODEL_REUSE__REUSE, reuse);
        addModelReuseCommand.append(setReuseCommand);
        
        Command addModelCommand = AddCommand.create(editingDomain, owner,
                CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
        addModelReuseCommand.append(addModelCommand);
        
        doExecute(editingDomain, addModelReuseCommand);     
    }
    
    /**
     * Creates a new {@link COREReuse} and {@link COREModelReuse} for the given models.
     *
     * @param owner the model that is reusing
     * @param reusingConcern the concern that the model belong to
     * @param reusedConcern the reused concern
     * @param externalArtefact the referenced model (external woven model for models of selected features)
     * @param configuration the configuration for the reuse
     */
    public void createNewReuseAndModelReuse(COREArtefact owner, COREConcern reusingConcern, COREConcern reusedConcern,
            COREArtefact externalArtefact, COREConfiguration configuration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // create new reuse
        COREReuse reuse = COREModelUtil.createReuse(reusingConcern, reusedConcern);
               
        CompoundCommand compoundCommandConcern = new CompoundCommand();

        // Add the reuse to the concern
        Command addCOREReuseCommand = AddCommand.create(editingDomain, reusingConcern,
                CorePackage.Literals.CORE_CONCERN__REUSES, reuse);
        compoundCommandConcern.append(addCOREReuseCommand);

        // initialize the configuration's properties
        configuration.setSource(reusedConcern.getFeatureModel());
        //configuration.setExtendedReuse(reuse);
        
        // create a name for the configuration
        String configurationName = reusedConcern.getName() + "_";
        for (COREFeature feature : configuration.getSelected()) {
            configurationName += feature.getName();
        }
        configuration.setName(configurationName);

        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        modelReuse.setSource(externalArtefact);
        modelReuse.setConfiguration(configuration);
        modelReuse.setReuse(reuse);
                
        Command addCOREModelReuseCommand = AddCommand.create(editingDomain, owner,
                CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
        compoundCommandConcern.append(addCOREModelReuseCommand);
        
        doExecute(editingDomain, compoundCommandConcern);
    }
    
    /**
     * Creates a new {@link COREReuse}, and {@link COREModelReuse} for the given models.
     * If there exists a {@link COREReuseArtefact} in the owner concern, we add the model reuse to the existing reuse
     * artefact, otherwise, create a new dummy artefact and add it to the concern.
     * 
     * @author Bowen
     *
     * @param owner the concern that is reusing
     * @param reusedConcern the concern that is being reused
     * @param configuration the configuration for the reused concern
     */
    public void createNewReuseAndModelReuseInDummyReuseArtefact(COREConcern owner, COREConcern reusedConcern, 
            COREConfiguration configuration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        
        CompoundCommand compoundCommandConcern = new CompoundCommand();
        
        // create new reuse
        COREReuse reuse = COREModelUtil.createReuse(owner, reusedConcern);
        
        // create command to add the reuse to the concern
        Command addCOREReuseCommand = AddCommand.create(editingDomain, owner,
                CorePackage.Literals.CORE_CONCERN__REUSES, reuse);
        compoundCommandConcern.append(addCOREReuseCommand);

        // initialize the configuration's properties
        configuration.setSource(reusedConcern.getFeatureModel());
        
        // create a name for the configuration
        String configurationName = reusedConcern.getName() + "_";
        for (COREFeature feature : configuration.getSelected()) {
            configurationName += feature.getName();
        }
        configuration.setName(configurationName);

        // create a model reuse that contains the dummy core reuse artefact, configuration and reuse
        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        modelReuse.setConfiguration(configuration);
        modelReuse.setReuse(reuse);
        
        // find the reuse artefact of owner if it exists
        COREReuseArtefact reuseArtefactOfOwner = null;
        for (COREArtefact artefact : owner.getArtefacts()) {
            if (artefact instanceof COREReuseArtefact) {
                reuseArtefactOfOwner = (COREReuseArtefact) artefact;
            }
        }
        
        // if reuse artefact exists, add model reuse to that reuse artefact, otherwise, create dummy reuse artefact
        if (reuseArtefactOfOwner == null) {
            // create new dummy reuse artefact
            COREReuseArtefact dummyCoreReuseArtefact = CoreFactory.eINSTANCE.createCOREReuseArtefact();
            
            // create command to add the model reuse to the dummy core reuse artefact
            Command addCOREModelReuseCommand = AddCommand.create(editingDomain, dummyCoreReuseArtefact,
                    CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
            compoundCommandConcern.append(addCOREModelReuseCommand);
            
            // create command to add the dummy core reuse artefact to the owner core concern
            Command addArtefactCommand = AddCommand.create(editingDomain, owner, CorePackage.Literals.CORE_ARTEFACT, 
                    dummyCoreReuseArtefact);
            compoundCommandConcern.append(addArtefactCommand);
        } else {
            // add the model reuse to the reuse artefact of the owner
            Command addCOREModelReuseCommand = AddCommand.create(editingDomain, reuseArtefactOfOwner,
                    CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
            compoundCommandConcern.append(addCOREModelReuseCommand);
        }
        
        // execute all commands
        doExecute(editingDomain, compoundCommandConcern);
    }
    
    /**
     * Updates the {@link COREConfiguration} for the given {@link COREReuse}'s model reuses.
     * 
     * @param owner the concern the user is working on
     * @param reuse the reuse that we want to change the configuration of
     * @param configuration the newly updated configuration
     */
    public void updateCOREConfiguration(COREConcern owner, COREReuse reuse, COREConfiguration configuration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        
        CompoundCommand compoundCommand = new CompoundCommand(); 

        for (COREModelReuse modelReuse : reuse.getModelReuses()) {
            Command setConfigurationCommand = SetCommand.create(editingDomain, modelReuse, 
                    CorePackage.Literals.CORE_MODEL_REUSE__CONFIGURATION, configuration);
            compoundCommand.append(setConfigurationCommand);
        }
        
        // execute all commands
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Creates a new {@link COREModelExtension} for the given model.
     *
     * @param owner the model that is reusing
     * @param extendedModel the referenced model
     */
    public void createModelExtension(COREArtefact owner, COREArtefact extendedModel) {
        COREModelExtension extension = COREModelUtil.createModelExtension(extendedModel);
        doAdd(owner, CorePackage.Literals.CORE_ARTEFACT__MODEL_EXTENSIONS, extension);
    }
    
    /**
     * Removes the input {@COERReuse}, and all associated {@link COREModelReuse} from {@link COREReuseArtefact}.
     * If there are no more model reuses in the reuse artefact, remove the reuse artefact as well.
     * 
     * @author Bowen
     * 
     * @param owner the concern that is reusing
     * @param reuse the concern that is reused and to be removed
     */
    public void removeCOREReuseAndModelReuses(COREConcern owner, COREReuse reuse) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // remove reuse from owner concern
        Command removeReuseCommand = RemoveCommand.create(editingDomain, owner, CorePackage.Literals
                .CORE_CONCERN__REUSES, reuse);
        compoundCommand.append(removeReuseCommand);
        
        // find the reuse artefact if it exists
        COREReuseArtefact reuseArtefact = null;
        for (COREArtefact artefact : owner.getArtefacts()) {
            if (artefact instanceof COREReuseArtefact) {
                reuseArtefact = (COREReuseArtefact) artefact;
            }
        }
                
        // remove all model reuses from the reuse artefact if it exists
        if (reuseArtefact != null) {
            for (COREModelReuse modelReuse : reuse.getModelReuses()) {
                Command removeModelReuseCommand = RemoveCommand.create(editingDomain, reuseArtefact, CorePackage
                        .Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
                compoundCommand.append(removeModelReuseCommand);
            }
            
            // if there are no remaining model reuses in the reuse artefact, remove the reuse artefact as well
            if (reuse.getModelReuses().size() == reuseArtefact.getModelReuses().size()) {
                Command removeReuseArtefactCommand = RemoveCommand.create(editingDomain, owner, CorePackage.Literals
                        .CORE_ARTEFACT, reuseArtefact);
                compoundCommand.append(removeReuseArtefactCommand);
            }
        }
        
        // execute all commands
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes a model Reuse.
     *
     * @param modelReuse to be removed
     */
    public void removeModelReuse(COREModelReuse modelReuse) {
        
        COREReuse reuse = modelReuse.getReuse();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(reuse);
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        Command artefactCommand = RemoveCommand.create(editingDomain, modelReuse.eContainer(), 
                CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
        compoundCommand.append(artefactCommand);
        
        Command command = RemoveCommand
                .create(editingDomain, reuse, CorePackage.Literals.CORE_REUSE__MODEL_REUSES, modelReuse);
        compoundCommand.append(command);
        
        if (reuse.getModelReuses().size() == 1) {
            Command reuseCommand = RemoveCommand.create(editingDomain, reuse.eContainer(),
                    CorePackage.Literals.CORE_CONCERN__REUSES, reuse);
            compoundCommand.append(reuseCommand);
        }

        doExecute(editingDomain, compoundCommand);
    }
    
    
    /**
     * Removes a model Extension.
     *
     * @param modelExtension to be removed
     */
    public void removeModelExtension(COREModelExtension modelExtension) {
        doRemove(modelExtension);
    }

    /**
     * Delete the given COREReuse.
     *
     * @param reuse the COREReuse to be deleted
     */
    public void deleteCOREReuse(COREReuse reuse) {
        doRemove(reuse);
    }

}
