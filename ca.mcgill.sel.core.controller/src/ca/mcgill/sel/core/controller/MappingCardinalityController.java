package ca.mcgill.sel.core.controller;

import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;

/**
 * The controller for mapping cardinalities.
 * @author jmilleret
 *
 */
public class MappingCardinalityController extends CoreBaseController {
    
    /**
     * Create a new instance of MappingCardinalityController.
     */
    MappingCardinalityController() {
        // prevent anyone outside this package to instantiate
    }
    
    /**
     * Create a new / update an existing {@link COREMappingCardinality} link to a {@link CORECIElement}
     * with the mappingCardinality composition.
     * 
     * @param ciElement - The ciElement that will contains the new {@link COREMappingCardinality}
     * @param name 
     * @param lowerBound 
     * @param upperBound 
     */
    public void setMappingCardinality(CORECIElement ciElement, String name, int lowerBound, int upperBound) {
        COREMappingCardinality mappingCardinality = null;
        CompoundCommand compoundCommand = new CompoundCommand();
        
        if (ciElement.getMappingCardinality() != null) {
            // We update it instead of creating a new one
            mappingCardinality = ciElement.getMappingCardinality();
        } else {
            mappingCardinality = CoreFactory.eINSTANCE.createCOREMappingCardinality();
        }

        mappingCardinality.setName(name);
        mappingCardinality.setLowerBound(lowerBound);
        mappingCardinality.setUpperBound(upperBound);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ciElement.getModelElement());
        
        // If needed, we delete the old reference cardinalities
        if (ciElement.getReferenceCardinality().size() != 0) {
            Command removePreviousCardinalityCommand = RemoveCommand.create(editingDomain, ciElement,
                    CorePackage.Literals.CORECI_ELEMENT__REFERENCE_CARDINALITY, ciElement.getReferenceCardinality());
            compoundCommand.append(removePreviousCardinalityCommand);
        }
        
        // If it's only an update, we replace the old object with its updated version
        Command addMappingCardinalityCommand = SetCommand.create(editingDomain, ciElement,
                CorePackage.Literals.CORECI_ELEMENT__MAPPING_CARDINALITY, mappingCardinality);
        compoundCommand.append(addMappingCardinalityCommand);
        
        doExecute(editingDomain, compoundCommand);
        
    }

    /**
     * Add newCardinalities to the set of {@link COREMappingCardinality} for a {@link CORECIElement}, link with
     * the referenceCardinality reference.
     * Remove the oldCardinalities from that set.
     * If a set of {@link COREMappingCardinality} already exists, it will be replaced.
     * 
     * @param ciElement - The ciElement that will contains the new Set of {@link COREMappingCardinality}
     * @param newCardinalities - The reference cardinalities that we want to add
     * @param oldCardinalities - The reference cardinalities that we want to delete
     */
    public void setReferenceCardinalities(CORECIElement ciElement, Set<COREMappingCardinality> newCardinalities,
            Set<COREMappingCardinality> oldCardinalities) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ciElement.getModelElement());
        
        CompoundCommand compoundCommand = new CompoundCommand();
        // Remove previous reference cardinalities
        if (oldCardinalities.size() != 0) {
            Command removePreviousCardinalityCommand = RemoveCommand.create(editingDomain, ciElement,
                    CorePackage.Literals.CORECI_ELEMENT__REFERENCE_CARDINALITY, oldCardinalities);
            compoundCommand.append(removePreviousCardinalityCommand);
        }
        
        if (newCardinalities.size() != 0) {
            Command addReferenceCardinalityCommand = AddCommand.create(editingDomain, ciElement,
                    CorePackage.Literals.CORECI_ELEMENT__REFERENCE_CARDINALITY, newCardinalities);
            compoundCommand.append(addReferenceCardinalityCommand);
        }

        // Remove previous mapping cardinalities
        if (ciElement.getMappingCardinality() != null) {
            Command removeMappingCardinalityCommand = DeleteCommand.create(editingDomain, 
                    ciElement.getMappingCardinality());
            compoundCommand.append(removeMappingCardinalityCommand);
        }
        
        doExecute(editingDomain, compoundCommand);
        
        
    }
}
