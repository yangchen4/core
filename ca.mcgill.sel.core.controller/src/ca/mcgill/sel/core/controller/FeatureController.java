package ca.mcgill.sel.core.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * The controller for features.
 *
 * @author Nishanth
 *
 */
public class FeatureController extends CoreBaseController {

    /**
     * Creates a new instance of {@link FeatureController}.
     */
    FeatureController() {
        // prevent anyone outside this package to instantiate
    }

    /**
     * Controller to add new feature (FeatureName passed as string) to feature.
     *
     * @param concern - The concern which is the editing domain
     * @param position - The position at which the new feature is to be added.
     * @param parentFeature - The feature for which a new child is to be added.
     * @param newFeature - The name of the new child feature to be added.
     * @param association - The association between the new feature and the current feature.
     */
    public void addFeature(COREConcern concern, int position, COREFeature parentFeature, String newFeature,
            COREFeatureRelationshipType association) {

        // Create the new COREFeature
        COREFeature newCOREFeature = CoreFactory.eINSTANCE.createCOREFeature();
        newCOREFeature.setName(newFeature);
        newCOREFeature.setParentRelationship(association);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        CompoundCommand compoundCommand = new CompoundCommand();

        if (parentFeature.getChildren().size() > 0) {
            COREFeatureRelationshipType rel = parentFeature.getChildren().get(0).getParentRelationship();
            // If the relationship is XOR or OR and it was not the case for other children, update their relationship
            if ((association == COREFeatureRelationshipType.XOR || association == COREFeatureRelationshipType.OR)
                    && rel != association) {
                compoundCommand.append(getChangeLinkCommand(editingDomain, parentFeature, association));
                // If the relationship is MANDATORY and children are XOR or OR, update children relationship to OPTIONAL
            } else if ((association == COREFeatureRelationshipType.MANDATORY
                    || association == COREFeatureRelationshipType.OPTIONAL)
                    && (rel == COREFeatureRelationshipType.XOR || rel == COREFeatureRelationshipType.OR)) {
                compoundCommand.append(getChangeLinkCommand(editingDomain, parentFeature,
                        COREFeatureRelationshipType.OPTIONAL));
            }
        }

        Command addChildCommand = AddCommand.create(editingDomain, concern.getFeatureModel(),
                CorePackage.Literals.CORE_FEATURE_MODEL__FEATURES, newCOREFeature);

        Command addFeatureAsChildCommand = AddCommand.create(editingDomain, parentFeature,
                CorePackage.Literals.CORE_FEATURE__CHILDREN, newCOREFeature, position);

        compoundCommand.append(addFeatureAsChildCommand);
        compoundCommand.append(addChildCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Add an already existing a feature as a child of another one.
     *
     * @param concern - The concern which is the editing domain.
     * @param feature - The feature to add
     * @param parentFeature - The parent feature
     * @param association - The {@link COREFeatureRelationshipType} with the parent feature.
     */
    public void addExistingFeature(COREConcern concern, COREFeature feature, COREFeature parentFeature,
            COREFeatureRelationshipType association) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        // First set the association to the new parent.
        Command setCommand = SetCommand.create(editingDomain, feature,
                CorePackage.Literals.CORE_FEATURE__PARENT_RELATIONSHIP, association);

        // Second remove the feature from Existing parent
        Command removeChildCommand = RemoveCommand.create(editingDomain, feature.getParent(),
                CorePackage.Literals.CORE_FEATURE__CHILDREN, feature);

        // Add to the new Parent
        Command addChildCommand = AddCommand.create(editingDomain, parentFeature,
                CorePackage.Literals.CORE_FEATURE__CHILDREN, feature);

        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(setCommand);
        compoundCommand.append(removeChildCommand);
        compoundCommand.append(addChildCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Move a feature to a new position.
     *
     * @param concern - The concern containing the feature
     * @param parent - The parent of the feature
     * @param feature - The feature to move
     * @param newPosition - The new position of the feature
     */
    public void changePositionOfFeature(COREConcern concern, COREFeature parent, COREFeature feature, int newPosition) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        Command changePositionCommand =
                MoveCommand.create(editingDomain, parent, CorePackage.Literals.CORE_FEATURE__CHILDREN, feature,
                        newPosition);

        doExecute(editingDomain, changePositionCommand);
    }

    /**
     * Controller to delete a feature.
     *
     * @param concern - The concern which is the editing domain.
     * @param listOfFeatures - The list of features to be deleted.
     */
    public void deleteFeature(COREConcern concern, List<COREFeature> listOfFeatures) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Delete constraints from the feature
        for (COREFeature toDelete : listOfFeatures) {
            for (COREFeature feature : toDelete.getExcludes()) {
                Command removeConstraint = RemoveCommand.create(editingDomain, toDelete,
                        CorePackage.Literals.CORE_FEATURE__EXCLUDES, feature);
                compoundCommand.append(removeConstraint);
            }
            for (COREFeature feature : toDelete.getRequires()) {
                Command removeConstraint = RemoveCommand.create(editingDomain, toDelete,
                        CorePackage.Literals.CORE_FEATURE__REQUIRES, feature);
                compoundCommand.append(removeConstraint);
            }
        }

        // Delete constraints to the feature
        for (COREFeature feature : concern.getFeatureModel().getFeatures()) {
            for (COREFeature toDelete : listOfFeatures) {
                if (feature.getExcludes().contains(toDelete)) {
                    Command removeConstraint = RemoveCommand.create(editingDomain, feature,
                            CorePackage.Literals.CORE_FEATURE__EXCLUDES, toDelete);
                    compoundCommand.append(removeConstraint);
                }
                if (feature.getRequires().contains(toDelete)) {
                    Command removeConstraint = RemoveCommand.create(editingDomain, feature,
                            CorePackage.Literals.CORE_FEATURE__REQUIRES, toDelete);
                    compoundCommand.append(removeConstraint);
                }
            }
        }

        // Delete COREFeatureImpacts
        COREImpactModel impactModel = concern.getImpactModel();
        if (impactModel != null) {
            for (COREImpactNode impactModelElement : impactModel.getImpactModelElements()) {
                if (impactModelElement instanceof COREFeatureImpactNode) {
                    COREFeatureImpactNode featureImpact = (COREFeatureImpactNode) impactModelElement;
                    if (listOfFeatures.contains(featureImpact.getRepresents())) {
                        compoundCommand.append(COREControllerFactory.INSTANCE.getFeatureImpactController()
                                .createDeleteImpactModelElementCommand(editingDomain, impactModel, null,
                                        featureImpact, false, new HashSet<COREImpactNode>()));
                    }
                }
            }
        }

        // Delete reuses that are currently used exclusively by the features that are going to be
        // deleted
        for (COREFeature feature : concern.getFeatureModel().getFeatures()) {
            
            for (COREReuse r : concern.getReuses()) {
                boolean okToDelete = true;
                
                for (COREModelReuse mr : r.getModelReuses()) {
                    
                    List<COREFeature> rf = ((COREArtefact) mr.eContainer()).getScene().getRealizes();
                    if (!listOfFeatures.containsAll(rf)) {
                        okToDelete = false;
                        break;
                    }
                }
                if (okToDelete) {
                    compoundCommand.append(RemoveCommand.create(editingDomain, concern.getFeatureModel(),
                            CorePackage.Literals.CORE_FEATURE_MODEL__FEATURES, feature));                    
                }
            }                
        }
        
        for (COREFeature feature : concern.getFeatureModel().getFeatures()) {

            // Delete link to and from realization scenes, and delete scenes that are not used anymore
            for (COREScene scene : feature.getRealizedBy()) {
                compoundCommand.append(RemoveCommand.create(editingDomain, feature,
                        CorePackage.Literals.CORE_FEATURE__REALIZED_BY, scene));
                // Check whether scene is attached to only one feature. If yes, delete it from the concern
                if (scene.getRealizes().size() == 1) {
                    compoundCommand.append(RemoveCommand.create(editingDomain, concern,
                            CorePackage.Literals.CORE_CONCERN__SCENES, scene));
                    // delete also all the artefacts of the scene from the concern
                    ArrayList<COREArtefact> toDelete = new ArrayList<COREArtefact>();
                    for (EList<COREArtefact> l : scene.getArtefacts().values()) {
                        toDelete.addAll(l);
                    }
                    for (COREArtefact a : toDelete) {
                        compoundCommand.append(RemoveCommand.create(editingDomain, concern,
                                CorePackage.Literals.CORE_CONCERN__ARTEFACTS, a));
                    }
                }
            }
            
            Command removeCommand = RemoveCommand.create(editingDomain, feature.getParent(),
                            CorePackage.Literals.CORE_FEATURE__CHILDREN, feature);
            Command deleteCommand = DeleteCommand.create(editingDomain, feature);
            compoundCommand.append(deleteCommand);
            compoundCommand.append(removeCommand);
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Controller used to change the link of the feature between itself and its children / parent. If the relationship
     * is a mandatory / optional it is called on the child with respect to its parent. Else if the relationship is XOR /
     * OR it is called on the parent.
     *
     * @param concern - The concern which is the editing domain.
     * @param feature - The feature (child / parent) depending on the association.
     * @param relationship - The new relationship which should be on the feature.
     */
    public void changeFeatureLink(COREConcern concern, COREFeature feature, COREFeatureRelationshipType relationship) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        if (relationship == COREFeatureRelationshipType.OPTIONAL
                || relationship == COREFeatureRelationshipType.MANDATORY) {

            doSet(feature, CorePackage.Literals.CORE_FEATURE__PARENT_RELATIONSHIP, relationship);

        } else {

            CompoundCommand compoundCommand = getChangeLinkCommand(editingDomain, feature, relationship);

            doExecute(editingDomain, compoundCommand);
        }
    }

    /**
     * Create a coumpound command for changing {@link COREFeatureRelationshipType} of a feature's children to a new one.
     *
     * @param editingDomain - The editing domain.
     * @param feature - The parent feature to change the association.
     * @param newRelationship - The new relationship which should be on the feature.
     * @return CompoundCommand which changes the relationship of all children features.
     */
    private CompoundCommand getChangeLinkCommand(EditingDomain editingDomain, COREFeature feature,
            COREFeatureRelationshipType newRelationship) {
        CompoundCommand compoundCommand = new CompoundCommand();

        for (COREFeature child : feature.getChildren()) {
            Command setCommand =
                    SetCommand.create(editingDomain, child, CorePackage.Literals.CORE_FEATURE__PARENT_RELATIONSHIP,
                            newRelationship);
            compoundCommand.append(setCommand);
        }
        return compoundCommand;
    }

    /**
     * Add an artefact to a concern's artefacts.
     *
     * @param concern - The concern
     * @param artefact - The artefact to add
     */
    public void addArtefactToConcern(COREConcern concern, COREArtefact artefact) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(artefact);

        Command addArtefactCommand =
                AddCommand.create(editingDomain, concern, CorePackage.Literals.CORE_CONCERN__ARTEFACTS, artefact);

        doExecute(editingDomain, addArtefactCommand);
    }

//    /**
//     * Controller used to associate a model with a feature.
//     *
//     * @param concern - The concern which is the editing domain.
//     * @param feature - The feature with which a model is to be associated.
//     * @param model - The model which is realized by the feature.
//     */
//    public void associateModel(COREConcern concern, COREFeature feature, COREArtefact model) {
//        COREArtefact newModel = model;
//        // Resolve the proxy for the model if any
//        if (model.eIsProxy()) {
//            newModel = (COREArtefact) EcoreUtil.resolve(model, concern);
//        }
//
//        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);
//        CompoundCommand compoundCommand = new CompoundCommand();
//
//        // If the model is not part of the concern models, add it and clear its realizations
//        if (!concern.getModels().contains(newModel)) {
//            // Add the model to the concern models
//            compoundCommand.append(AddCommand.create(editingDomain, concern,
//                    CorePackage.Literals.CORE_CONCERN__ARTEFACTS, newModel));
//            // Clear the model realizations
//            for (COREFeature rf : newModel.getRealizes()) {
//                compoundCommand.append(RemoveCommand.create(editingDomain, newModel,
//                        CorePackage.Literals.CORE_MODEL__REALIZES, rf));
//            }
//        }
//
//        Command addModelCommand = AddCommand.create(editingDomain, feature,
//                CorePackage.Literals.CORE_FEATURE__REALIZED_BY, newModel);
//        compoundCommand.append(addModelCommand);
//
//        doExecute(editingDomain, compoundCommand);
//    }
//
    /**
     * Add a new model to the concern as a realization model of the given feature.
     * This will create a scene and an external artefact
     * TODO: at some point we have to figure out how to make this work with more complex perspectives :)
     *
     * @param concern the concern which is the editing domain
     * @param feature the feature realized by the new model
     * @param perspective the perspective that the scene should be typed with
     * @param roleName the name of the role the model plays in that perspective
     * @param model the model to add and associate
     * @param modelName the name to be used for the scene associated with the feature
     */
    public void addModelAndAssociate(COREConcern concern, COREFeature feature, COREPerspective perspective,
            String roleName, EObject model, String modelName) {
        
        // Do it in the editing domain of the model in order to mark it automatically as unsaved.
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        // Create a new COREExternalArtefact
        COREExternalArtefact newExternalArtefact = COREModelUtil.createExternalArtefact(model, feature);
        // Set the language of the artefact to the language found in the perspective under the given role name
        newExternalArtefact.setLanguageName(perspective.getLanguages().get(roleName).getName());
        
        // Create the new scene
        COREScene scene = COREModelUtil.createScene();
        EList<COREArtefact> artefacts = new BasicEList<COREArtefact>();
        artefacts.add(newExternalArtefact);
        scene.getArtefacts().put(roleName, artefacts);
        newExternalArtefact.setScene(scene);
        
        scene.setPerspectiveName(perspective.getName());
        scene.setName(modelName);
        
        CompoundCommand compoundCommand = new CompoundCommand();
        // Add the external artefact to the concern models
        compoundCommand.append(SetCommand.create(editingDomain, newExternalArtefact,
                CorePackage.Literals.CORE_ARTEFACT__CORE_CONCERN, concern));
        // Add the scene to the concern models
        compoundCommand.append(AddCommand.create(editingDomain, concern,
                CorePackage.Literals.CORE_CONCERN__SCENES, scene));
        
        // Associate the scene to the feature
        compoundCommand.append(AddCommand.create(editingDomain, scene,
                CorePackage.Literals.CORE_SCENE__REALIZES, feature));        
        
        // Extend model realizing parent feature if any.
        COREExternalArtefact parentArtefact = COREModelUtil.getParentArtefact(feature, perspective, roleName);
        if (parentArtefact != null) {
            COREModelExtension modelExtension = COREModelUtil.createModelExtension(parentArtefact);
            
            compoundCommand.append(AddCommand.create(editingDomain, newExternalArtefact,
                    CorePackage.Literals.CORE_ARTEFACT__MODEL_EXTENSIONS, modelExtension));
        }
        
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Adds a new model to the concern as a realization model of the given feature.
     * This action should be called internally, potentially from from perspective action.
     * This will create an external artefact
     * @param concern
     * @param scene
     * @param feature
     * @param perspective
     * @param roleName
     * @param model
     * @param modelName
     */
    public void addModelAndAssociateWithScene(COREConcern concern, COREScene scene, COREFeature feature, 
            COREPerspective perspective, String roleName, EObject model, String modelName) {
        
        // Do it in the editing domain of the model in order to mark it automatically as unsaved.
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        // Create a new COREExternalArtefact
        COREExternalArtefact newExternalArtefact = COREModelUtil.createExternalArtefact(model, feature);
        // Set the language of the artefact to the language found in the perspective under the given role name
        newExternalArtefact.setLanguageName(perspective.getLanguages().get(roleName).getName());
       
        EList<COREArtefact> artefacts = new BasicEList<COREArtefact>();
        artefacts.add(newExternalArtefact);
        scene.getArtefacts().put(roleName, artefacts);
        newExternalArtefact.setScene(scene);

        
        CompoundCommand compoundCommand = new CompoundCommand();
        // Add the external artefact to the concern models
        compoundCommand.append(SetCommand.create(editingDomain, newExternalArtefact,
                CorePackage.Literals.CORE_ARTEFACT__CORE_CONCERN, concern));
//        // Add the scene to the concern models
//        compoundCommand.append(AddCommand.create(editingDomain, concern,
//                CorePackage.Literals.CORE_CONCERN__SCENES, scene));
//        
//        // Associate the scene to the feature
//        compoundCommand.append(AddCommand.create(editingDomain, scene,
//                CorePackage.Literals.CORE_SCENE__REALIZES, feature));        
        
        // Extend model realizing parent feature if any.
        COREExternalArtefact parentArtefact = COREModelUtil.getParentArtefact(feature, perspective, roleName);
        if (parentArtefact != null) {
            COREModelExtension modelExtension = COREModelUtil.createModelExtension(parentArtefact);
            
            compoundCommand.append(AddCommand.create(editingDomain, newExternalArtefact,
                    CorePackage.Literals.CORE_ARTEFACT__MODEL_EXTENSIONS, modelExtension));
        }
        
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Add an existing scene of a concern to the list of realization scenes of a feature. 
     * If the scene or any of the artefacts that are part of the scene are not contained in a concern,
     * then they will be added to the concern of the feature.
     *
     * @param scene the scene that is to be added
     * @param feature the feature to which the scene should be added
     */
    public void associateSceneToFeature(COREScene scene, COREFeature feature) {
        
        // Fnd the enclosing concern
        COREConcern c = (COREConcern) EcoreUtil.getRootContainer(feature);
        
        // Do it in the editing domain of the concern in order to mark it automatically as unsaved.
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(c);

        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Add connection between scene and feature
        compoundCommand.append(AddCommand.create(editingDomain, scene,
                CorePackage.Literals.CORE_SCENE__REALIZES, feature));

        if (scene.eContainer() == null) {
            // Add the scene to the concern models
            compoundCommand.append(AddCommand.create(editingDomain, c,
                    CorePackage.Literals.CORE_CONCERN__SCENES, scene));            
        }
        
        for (List<COREArtefact> l : scene.getArtefacts().values()) {
            for (COREArtefact a : l) {
                if (a.eContainer() == null) {
                    // Add the artefact to the concern models
                    compoundCommand.append(SetCommand.create(editingDomain, a,
                            CorePackage.Literals.CORE_ARTEFACT__CORE_CONCERN, c));
                }
            }
        }
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Controller used to delete an association of the model from the feature.
     *
     * @param concern - The concern which is the editing domain.
     * @param feature - The feature whose realization is to be deleted.
     */
    public void removeModelAssociation(COREConcern concern, COREFeature feature) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        Command removeModelCommand =
                RemoveCommand.create(editingDomain, feature, CorePackage.Literals.CORE_FEATURE__REALIZED_BY,
                        feature.getRealizedBy());

        doExecute(editingDomain, removeModelCommand);
    }

    /**
     * Controller used to delete an association of the model from the feature.
     *
     * @param concern - The concern which is the editing domain.
     * @param feature - The feature whose realization is to be deleted.
     * @param model - The model which is realized by the feature.
     */
    public void removeModelAssociation(COREConcern concern, COREFeature feature, COREArtefact model) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        COREArtefact newModel = model;
        // Resolve the proxy for the model if any
        if (model.eIsProxy()) {
            newModel = (COREArtefact) EcoreUtil.resolve(model, concern);
        }

        Command removeModelCommand =
                RemoveCommand.create(editingDomain, feature, CorePackage.Literals.CORE_FEATURE__REALIZED_BY, newModel);

        doExecute(editingDomain, removeModelCommand);
    }

    /**
     * Create a 'A requires B' constraint between two features.
     *
     * @param concern - The concern which is the editing domain.
     * @param owner - The feature which owns the constraint
     * @param constraint - The feature which is the target of the constraint
     */
    public void addRequiresConstraint(COREConcern concern, COREFeature owner, COREFeature constraint) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        Command addConstraint =
                AddCommand.create(editingDomain, owner, CorePackage.Literals.CORE_FEATURE__REQUIRES, constraint);

        doExecute(editingDomain, addConstraint);
    }

    /**
     * Create an exclude constraint between two features.
     *
     * @param concern - The concern which is the editing domain.
     * @param owner - The feature which owns the constraint
     * @param constraint - The feature which is the target of the constraint
     */
    public void addExcludesConstraint(COREConcern concern, COREFeature owner, COREFeature constraint) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        Command addConstraint =
                AddCommand.create(editingDomain, owner, CorePackage.Literals.CORE_FEATURE__EXCLUDES, constraint);

        doExecute(editingDomain, addConstraint);
    }

    /**
     * Remove the require constraint between two features.
     *
     * @param concern - The concern which is the editing domain.
     * @param owner - The feature which owns the constraint
     * @param constraint - The feature which is the target of the constraint
     */
    public void removeRequiresConstraint(COREConcern concern, COREFeature owner, COREFeature constraint) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        Command removeConstraint =
                RemoveCommand.create(editingDomain, owner, CorePackage.Literals.CORE_FEATURE__REQUIRES, constraint);

        doExecute(editingDomain, removeConstraint);
    }

    /**
     * Remove the exclude constraint between two features.
     *
     * @param concern - The concern which is the editing domain.
     * @param owner - The feature which owns the constraint
     * @param constraint - The feature which is the target of the constraint
     */
    public void removeExcludesConstraint(COREConcern concern, COREFeature owner, COREFeature constraint) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

        Command removeConstraint =
                RemoveCommand.create(editingDomain, owner, CorePackage.Literals.CORE_FEATURE__EXCLUDES, constraint);

        doExecute(editingDomain, removeConstraint);
    }

    /**
     * Deletes a {@link COREModelReuse}.
     *
     * @param eachModel - The {@link COREArtefact} which is the editing domain.
     * @param modelReuse - The model reuse to delete
     */
    public void removeModelReuse(COREArtefact eachModel, COREModelReuse modelReuse) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(eachModel);

        Command deleteCommand = DeleteCommand.create(editingDomain, modelReuse);

        doExecute(editingDomain, deleteCommand);

    }

}
