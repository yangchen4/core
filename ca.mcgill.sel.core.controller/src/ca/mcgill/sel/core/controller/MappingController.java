package ca.mcgill.sel.core.controller;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;

/**
 * The controller for COREMappings.
 * 
 * @author Jehan
 *
 */
public class MappingController extends CoreBaseController {
    
    /**
     * Create the from relation in a {@link COREMapping}. 
     * Also handle the creation of referencedMappings
     * 
     * @param mapping the COREMapping
     * @param element the element the from relation will refer to
     */
    public void setFromMapping(COREMapping<? extends EObject> mapping, EObject element) {
        CompoundCommand compoundCommand = new CompoundCommand();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(element);
        
        // Add the to mapping
        Command addMappingCommand = SetCommand.create(editingDomain, mapping,
                CorePackage.Literals.CORE_LINK__FROM, element);
        compoundCommand.append(addMappingCommand);
        
        // Collect the references
        Collection<COREMapping<? extends EObject>> references = 
                COREArtefactUtil.getNextReferencedMappings(element);
        
        // Add the references
        if (references != null) {
            Command addReferencesCommand = AddCommand.create(editingDomain, mapping,
                    CorePackage.Literals.CORE_MAPPING__REFERENCED_MAPPINGS, references);
            compoundCommand.append(addReferencesCommand);
        }
        
        doExecute(editingDomain, compoundCommand);
    }
}
